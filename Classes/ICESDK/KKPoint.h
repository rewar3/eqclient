//
//  KKPoint.h
//  ICEConnectDemo
//
//  Created by wsw on 13-12-30.
//  Copyright (c) 2013年 wsw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KKPoint : NSObject



@property (nonatomic , assign) float x ;
@property (nonatomic , assign) float y;
@property (nonatomic , assign) float p ;//pressure 压感
@property (nonatomic , assign) float lineWidth ;//线的粗细

@end
