//
//  ICEHelper.m
//  NewWiFiPlus
//
//  Created by wsw on 17/12/12.
//  Copyright (c) 2012年 天脉聚源传媒科技有限公司. All rights reserved.
//

#import "ICEHelper.h"
#import "NSString+JSONSerialization.h"
//#import "CachedHelper.h"

static ICEHelper *ice = nil;

BOOL isCheckNull(id temp)
{
    if ([temp isKindOfClass:[NSNull class]])
    {
        if ((NSNull *)temp == [NSNull null])
        {
            return YES ;
        }
    }
    if ([temp isKindOfClass:[NSString class]])
    {
        if ([temp isEqualToString:@""])
        {
            return YES ;
        }
        else
        {
            return NO ;
        }
    }
    else
    {
        return NO ;
    }
}


@implementation ICEHelper
@synthesize iceManager = iceManager_ ;




+(id)shared
{
    @synchronized(self)
    {
        if (ice == nil) {
            ice = [[ICEHelper alloc] init];
            
        }
    }
    
    return ice ;
}

- (void)initManager
{
    iceManager_ = [[ICEManager alloc] init];
    iceManager_.delegate = self ;
}

+(id) allocWithZone:(NSZone *)zone{
    @synchronized(self)
    {
        if (ice == nil)
        {
            ice = [super allocWithZone:zone];
            return ice;
        }
    }
    return nil;
}


#pragma mark ICEManagerDelegate
- (void)searchingTimeout:(ICEManager *)manager
{

    [[NSNotificationCenter defaultCenter] postNotificationName:kICESearchTimeout object:nil];
}
- (void)searchingCompletedWithServiceList:(NSArray *)servicelistArr
{
    NSLog(@"center control's name is %@",[(ICELoginEntity *)[servicelistArr objectAtIndex:0] connectICEName]);

    loginEntity = (ICELoginEntity *)[servicelistArr objectAtIndex:0];
    searchResultICELoginEntityArray = servicelistArr ;
    [[NSNotificationCenter defaultCenter] postNotificationName:kICEFounded object:nil];
    
}
- (void)searchingUpdatedWithServiceList:(NSArray *)updateServicelistArr
{
    //当同一环境下有多个中控，新添加中控时回调
    DLog(@"updateServicelistArr%d",updateServicelistArr.count);
    
//    if(updateServicelistArr.count>1)
//    {
//        loginEntity = (ICELoginEntity *)[updateServicelistArr objectAtIndex:0];
//        searchResultICELoginEntityArray = updateServicelistArr ;
//        [[NSNotificationCenter defaultCenter] postNotificationName:kICEFounded object:nil];
//    }
}


//连接超时
- (void)connectionTimeout
{
    //    NSLog(@"connection time out");
    //
    //    [HUD hide:NO];
    //    [UIView animateWithDuration:0.3 animations:^{
    //        [maskView setAlpha:0];
    //    }];
    //
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"接入超时!" delegate:self cancelButtonTitle:@"再试一次" otherButtonTitles:nil, nil];
    //    [alert setTag:AlertTypeConnectionTimeOut];
    //    [alert show];
}

- (void)connectionFailedWithICEError:(ICEErrorEntity *)error
{
    
    NSInteger result = [error code];

    if (result == -9)//当code == -9  连接密码错误
    {
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Notice", nil) message:NSLocalizedString(@"Please reset the password in the 'Settings' and then login",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
//        [alert show];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kPasswordError object:nil];
//        [[NSNotificationCenter defaultCenter] postNotificationName:kICELoginError object:nil];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kICELoginError object:nil];
    }

    
}
- (void)connectionSucceedWithTargetServiceIP:(NSString *)ip userPower:(NSString *)power ;
{
    //连接成功获取，得到中控ip，以及获取登录后的权限。
    NSLog(@"the ICE ip is %@",ip);

    [[KKCache sharedCache] setHTTPIP:[NSString stringWithFormat:@"http://%@",ip]];

    if ([power isEqualToString:[GroupTypeEntity ANONYMOUS]])//绿色用户 权限 低
    {
        NSLog(@"绿色用户");
        [[NSNotificationCenter defaultCenter] postNotificationName:kICELoginError object:nil];
        
        return;
//        [[CachedUserInfo shared] setUserPower:[GroupTypeEntity ANONYMOUS]];
    }
    if ([power isEqualToString:[GroupTypeEntity PRIVILEGES]])//黄色用户 权限 中
    {
        NSLog(@"黄色用户");
        [[CachedUserInfo shared] setUserPower:[GroupTypeEntity PRIVILEGES]];
        [[NSNotificationCenter defaultCenter] postNotificationName:kICELoginFinished object:nil];
        
    }
    if([power isEqualToString:[GroupTypeEntity ADMINISTRATOR]])//红色用户 权限 高
    {
        NSLog(@"红色用户");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kICELoginError object:nil];
        
        return;
        
//        [[CachedUserInfo shared] setUserPower:[GroupTypeEntity ADMINISTRATOR]];
//        [[NSNotificationCenter defaultCenter] postNotificationName:kICELoginFinished object:nil];
        
    }
    
}


- (void)connectionRetryFailed
{
    NSLog(@"- (void)connectionRetryFailed");
}
- (void)connectionWillRetry
{
    NSLog(@"- (void)connectionWillRetry");
    //    [[RoamModalInfo shared] setReConnecting:YES];
    //    [[RoamModalInfo shared] setReConnectioned:NO];
    //
}
- (void)connectionRetrySuccess
{
    NSLog(@"- (void)connectionRetrySuccess");
    //    [[RoamModalInfo shared] setReConnecting:NO];
    //    [[RoamModalInfo shared] setReConnectioned:YES];
}



//连接中控后，即TCP连接，所有得到的消息，都在此回调，通过notification 广播出去。
- (void)receivedCMD:(CommandEntity *)commandEntity
{

    NSLog(@"___________________________________");
    NSLog(@"ClientVaildcode %@",[commandEntity ClientVaildcode]);
    NSLog(@"CmdTYPE %@",[commandEntity CmdTYPE]);
    NSLog(@"FROM %@",[commandEntity FROM]);
    NSLog(@"BODY %@",[commandEntity body]);
    NSLog(@"___________________________________");
    
    if ([commandEntity.CmdTYPE isEqualToString:[CommandTypeEntity FUNCTION1]])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dealStackData" object:commandEntity.body];
        
    }
    
    if ([commandEntity.CmdTYPE isEqualToString:[CommandTypeEntity FUNCTION2]])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveStreamImage object:commandEntity.body];
        
    }
    
    if([[commandEntity CmdTYPE] isEqualToString:[CommandTypeEntity CACHE]])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kReciveCacheNotificaiton object:nil userInfo:nil];
    }
    
    if ([commandEntity.CmdTYPE isEqualToString:[CommandTypeEntity FORCE]])
    {
        NSDictionary *dict = [commandEntity.body JSONValue];
        
//        if ([[dict objectForKey:ACTION] isEqualToString:FOLLOW] && [[dict objectForKey:POWER] isEqualToString:ON])//0关，1开
//        {
//            //post notification off
//            [[KKCache sharedCache] setIsForceFollowing:YES];
//            [[NSNotificationCenter defaultCenter] postNotificationName:kForceFollowPower object:nil userInfo:[NSDictionary dictionaryWithObject:ON forKey:POWER]];
//        }
//        if ([[dict objectForKey:ACTION] isEqualToString:FOLLOW] && [[dict objectForKey:POWER] isEqualToString:OFF])//0关，1开
//        {
//            [[KKCache sharedCache] setIsForceFollowing:NO];
//            [[NSNotificationCenter defaultCenter] postNotificationName:kForceFollowPower object:nil userInfo:[NSDictionary dictionaryWithObject:OFF forKey:POWER]];
//        }
        if ([[dict objectForKey:ACTION] isEqualToString:ANSWER] && [[dict objectForKey:POWER] isEqualToString:ON])//0关，1开
        {
            [[KKCache sharedCache] setIsForceAnswer:YES];
            [[KKCache sharedCache] setQuestionGuid:[dict objectForKey:GUID]];
            [[KKCache sharedCache] setQuestionType:[NSString stringWithFormat:@"%@",[dict objectForKey:@"type"]]];
            [[NSNotificationCenter defaultCenter] postNotificationName:kForceAnswer object:dict userInfo:[NSDictionary dictionaryWithObject:ON forKey:POWER]];
        }
        if ([[dict objectForKey:ACTION] isEqualToString:ANSWER] && [[dict objectForKey:POWER] isEqualToString:OFF])//0关，1开
        {
            [[KKCache sharedCache] setIsForceAnswer:NO];
            [[KKCache sharedCache] setQuestionGuid:[dict objectForKey:GUID]];
            [[KKCache sharedCache] setQuestionType:[NSString stringWithFormat:@"%@",[dict objectForKey:@"type"]]];
            [[NSNotificationCenter defaultCenter] postNotificationName:kForceAnswer object:dict userInfo:[NSDictionary dictionaryWithObject:OFF forKey:POWER]];
        }
        
    
    }
    if ([commandEntity.CmdTYPE isEqualToString:[CommandTypeEntity GO]])
    {
//        [[CachedHelper shared] setMainLineLastGuid:[[[commandEntity body]JSONValue] objectForKey:@"guid"]];
//        
//        if(![[[CachedHelper shared] mainLinePackage] isEqualToString:[[[commandEntity body] JSONValue] objectForKey:@"pkg"]])
//        {
//            if(![[[[commandEntity body] JSONValue] objectForKey:@"pkg"] isKindOfClass:[[NSNull null] class]])
//            {
//                [[CachedHelper shared] setMainLinePackage:[[[commandEntity body] JSONValue] objectForKey:@"pkg"]];
//                [[CachedHelper shared] mainLineDidChangedToPackage:[[[commandEntity body] JSONValue] objectForKey:@"pkg"]];
//            }
//        }
//        else
//        {
//            if(![[[[commandEntity body] JSONValue] objectForKey:@"pkg"] isKindOfClass:[[NSNull null] class]])
//            {
//                [[CachedHelper shared] setMainLinePackage:[[[commandEntity body] JSONValue] objectForKey:@"pkg"]];
//            }
//        }
        
        
        

        NSDictionary *tempDict = (NSDictionary *)[commandEntity.body JSONValue];
        if (isCheckNull([tempDict objectForKey:@"pkg"]))
        {
            //GoPush
            if (isCheckNull([tempDict objectForKey:GUID]))
            {
                NSLog(@"go guid 为空");
            }
            else
            {
                [[KKCache sharedCache] setLastPushGoDict:[NSDictionary dictionaryWithObjectsAndKeys:[tempDict objectForKey:GUID],GUID,nil]];
                [[NSNotificationCenter defaultCenter] postNotificationName:kPushGoCommand object:nil];
            }
        
        }
        else
        {
            //GoMainLine
//            [[KKCache sharedCache] setLastMainLineGoDict:[NSDictionary dictionaryWithObjectsAndKeys:[tempDict objectForKey:@"pkg"],PackageName,[tempDict objectForKey:GUID],GUID,nil]];
//            [[KKCache sharedCache] setLastGoIsPush:NO];
//            [[NSNotificationCenter defaultCenter] postNotificationName:kMainLineGoCommand object:nil];
            
            [[KKCache sharedCache] setLastPushGoDict:[NSDictionary dictionaryWithObjectsAndKeys:[tempDict objectForKey:GUID],GUID,nil]];
            [[NSNotificationCenter defaultCenter] postNotificationName:kPushGoCommand object:nil];
        }
    
    }

}
#pragma mark BLDelegate
-(void)bleWrongEquipment;
{
    //    //notification 通知给漫游controller
    //    [[NSNotificationCenter defaultCenter] postNotificationName:kBLUnSupport object:nil];
}
-(void)nearestBLE:(BLEPeripheralEntity*)onePerpheral;
{
    //    //notification 通知给漫游controller
    //    [[NSNotificationCenter defaultCenter] postNotificationName:kBLFounded object:onePerpheral];
}

- (void)doConnectICE
{
    if ([searchResultICELoginEntityArray count] != 0)
    {
        if([[loginEntity loginMethod] isEqualToString:[ICELoginMethodEntity DEFAULT]])
        {
            NSLog(@"Default");
            
            UserInfoEntity *userinfo = [[UserInfoEntity alloc] init:[UserTypeEntity USER]];
            [userinfo setId:[[CachedUserInfo shared] tvmid]];
            [userinfo setName:kAPPNAME];
            
            [[[ICEHelper shared] iceManager] startConnectionICELoginEntity:loginEntity withUserInfo:userinfo timeout:10];
        }
        if ([[loginEntity loginMethod] isEqualToString:[ICELoginMethodEntity PASSWORD]])
        {
            NSLog(@"Password%@",[[CachedUserInfo shared] tvmid]);
            
            UserInfoEntity *userinfo = [[UserInfoEntity alloc] init:[UserTypeEntity USER]];
            [userinfo setId:[[CachedUserInfo shared] tvmid]];
            [userinfo setName:kAPPNAME];
            
//            [loginEntity setPassword:[[NSUserDefaults standardUserDefaults] objectForKey:SettingPassword]];
            
            NSString *pwd = GetObjectUserDefault(kPassword);
            [loginEntity setPassword:pwd];
            
            [[[ICEHelper shared] iceManager] startConnectionICELoginEntity:loginEntity withUserInfo:userinfo timeout:10];
            
        }
        
        if ([[(ICELoginEntity *)[searchResultICELoginEntityArray objectAtIndex:0] loginMethod] isEqualToString:[ICELoginMethodEntity REGISTRY]])
        {
            UserInfoEntity *userinfo = [[UserInfoEntity alloc] init:[UserTypeEntity USER]];
            [userinfo setId:[[CachedUserInfo shared] tvmid]];
            [userinfo setName:kAPPNAME];
            
            [[[ICEHelper shared] iceManager] startConnectionICELoginEntity:loginEntity withUserInfo:userinfo timeout:10];
        }
    }
}



@end
