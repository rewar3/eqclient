//
//  KKStreamDraw.m
//  ICEConnectDemo
//
//  Created by wsw on 14-1-14.
//  Copyright (c) 2014年 wsw. All rights reserved.
//

#import "KKStreamDraw.h"

@implementation KKStreamDraw

float dertX(float containerWidth, float resourceWidth)
{
    return (containerWidth - resourceWidth)/2;
}


- (void)_saveCurrentFrameImage
{
    
    if (saveContext)
    {
        CGContextRelease(saveContext);
    }
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    saveContext = CGBitmapContextCreate(nil,firstManager.resourceWidth,firstManager.resourceHeight,8,0,colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CFRelease(colorSpace);
    
    CGContextSetBlendMode(saveContext, kCGBlendModeCopy);
    CGContextSetLineJoin(saveContext, kCGLineJoinRound);
    CGContextSetLineCap(saveContext, kCGLineCapRound);
    CGContextSetAllowsAntialiasing(saveContext, YES);
    if (self.bgImage)
    {
        
        //因为传过来的tranformY已经是进行矩阵变换后的数值，所以不需要再进行变换，但翻转后的Y轴位移是没有进行ctx scale转换的，所以要进行相应的转换，就得到 0-transformY-(ctmY-1.f)*firstManager.resourceHeight 这样的公式  至于为什么，我现在也不知道，但是结果是对的。
        CGContextDrawImage(saveContext, CGRectMake(0   , 0, firstManager.resourceWidth, firstManager.resourceHeight), self.bgImage.CGImage);

    }
    CGContextScaleCTM(saveContext, 1.0, -1.0);
    CGContextTranslateCTM(saveContext, 0, 0 - firstManager.resourceHeight);
    
    
    CGContextTranslateCTM(saveContext, 0, 0);
    CGContextScaleCTM(saveContext, 1, 1);
    
    
    for (int i = 0; i < [drawDataArray count]; i++)
    {
        KKPartLineManager *manager = [drawDataArray objectAtIndex:i];
        
        if ([manager.tid isEqualToString:@"2"])
        {
            
            CGContextSetStrokeColorWithColor(saveContext, [[self colorFromHexString:manager.partLine.color16] CGColor]);
            
            for (int i = 0; i < [manager.partLine.points count]; i++)
            {
                KKPoint *p = [manager.partLine.points objectAtIndex:i];
                KKPoint *pNext ;
                if (i+1 == [manager.partLine.points count])
                {
                    
                    pNext = p;
                }
                else
                {
                    pNext = [manager.partLine.points objectAtIndex:i+1];
                }
                
                if (i == 0)
                {
                    CGContextSetLineWidth(saveContext, p.lineWidth);
                    CGContextMoveToPoint(saveContext, p.x, p.y);
                }
                else
                {
                    CGContextAddLineToPoint(saveContext, pNext.x, pNext.y);
                    CGContextStrokePath(saveContext);
                    CGContextMoveToPoint(saveContext, pNext.x, pNext.y);
                    CGContextSetLineWidth(saveContext, p.lineWidth);
                }
                
            }
            
        }
        
    }
    
    
    CGImageRef image = CGBitmapContextCreateImage(saveContext);
    UIImage* image2 = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    CGContextRelease(saveContext);
    saveContext = nil ;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate onCurrentFrameImageSavedWithImage:image2];
    });
}

- (void)saveCurrentFrameImage
{
    @autoreleasepool
    {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(_saveCurrentFrameImage)
                                                                                 object:nil];
        [queue addOperation:operation];
        
    }
}

- (void)getExistHandWritingImageWithGuid:(NSString *)inkGuid
{
    [queue cancelAllOperations];
    
    existHandWritingArray = nil ;
    existHandWritingArray = [[NSMutableArray alloc] init];
    
    existHandWritingLastIndex = -1 ;
    
    @autoreleasepool
    {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(_getExistHandWritingImageWithGuid:)
                                                                                 object:inkGuid];
        [queue addOperation:operation];
        
    }
}

- (void)_getExistHandWritingImageWithGuid:(NSString *)inkGuid
{
    if ([inkGuid isEqualToString:@""] || inkGuid == nil)
    {
        return ;
    }
    else
    {
        
        
        NSString *urlString = [NSString stringWithFormat:@"%@/%@",[[KKCache sharedCache] HTTPIP],[ICEFileEntity URL_SANDBOX_SEARCH_FILE]];
        
        NSString *postString = [NSString stringWithFormat:@"%@=%@",GUID,inkGuid];
                
        NSData *postData    =  [postString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *postLength	= [NSString stringWithFormat:@"%ld", [postData length]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                           timeoutInterval:60.0];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:nil];
        
        
        
        
        if ([[dict objectForKey:@"code"] integerValue] == 0)
        {
            
            if ([[dict objectForKey:@"msg"] isKindOfClass:[NSArray class]] && [[dict objectForKey:@"msg"] count] != 0)
            {
                NSDictionary *resultDict = [[dict objectForKey:@"msg"] objectAtIndex:0];
                
                
                
                
                NSString *inkDataString = [[NSString stringWithFormat:@"%@/resource/%@/%@",[[KKCache sharedCache] HTTPIP],[resultDict objectForKey:PackageName],[resultDict objectForKey:FileNameWithExtend]] encodeURL];
                
                NSData *inkdata = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:inkDataString]];
//                
//                NSData *gzipdata = [[[NSData alloc] initWithBase64EncodedData:inkdata options:NSDataBase64DecodingIgnoreUnknownCharacters] gzipInflate];
                NSData *gzipdata = [[NSData dataWithBase64EncodedString:[[NSString alloc] initWithData:inkdata encoding:NSUTF8StringEncoding]] gzipInflate];
                
                NSLog(@"the final string is %@",[[NSString alloc] initWithData:gzipdata encoding:NSUTF8StringEncoding]);
                
                
                NSDictionary *inkDict = [NSJSONSerialization JSONObjectWithData:gzipdata options:NSJSONReadingMutableLeaves error:nil];
                
                existHandWritingManager = nil ;
                existHandWritingManager = [[KKPartLineManager alloc] init];
                
                existHandWritingManager.resourceHeight = [[[inkDict objectForKey:@"InkStartData"] objectForKey:ResourceHeight] floatValue];
                existHandWritingManager.resourceWidth  = [[[inkDict objectForKey:@"InkStartData"] objectForKey:ResourceWidth] floatValue];
                existHandWritingManager.conainerHeight = [[[inkDict objectForKey:@"InkStartData"] objectForKey:ConainerHeight] floatValue];
                existHandWritingManager.conainerWidth  = [[[inkDict objectForKey:@"InkStartData"] objectForKey:ConainerWidth] floatValue];
                
                if ([[[inkDict objectForKey:TID] stringValue] isEqualToString:@"2"])
                {
                    
                    if ([[[inkDict objectForKey:OP] stringValue] isEqualToString:@"0"])
                    {
                        if (![[inkDict objectForKey:DATA] isEqualToString:@""] && [inkDict objectForKey:DATA])
                        {
                            //解析data数据，可能包括多个data块数据
                            NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:[[inkDict objectForKey:DATA] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                            
                            if ([dataArray isKindOfClass:[NSArray class]])
                            {
                                // 处理 index 相同的适合，manager的partline唯一，只有一条。
                                
                                for (int i = 0; i < dataArray.count; i++)
                                {
                                    //取上一个partlinemanager.partline.points add points
                                    
                                    NSDictionary *inlinedict = [dataArray objectAtIndex:i];
                                    
                                    if (existHandWritingLastIndex == [[inlinedict objectForKey:Index] integerValue])
                                    {
                                        
                                        KKPartLineManager *partLineManager = (KKPartLineManager *)[managerArray lastObject];
                                        
                                        for (int j = 0; j < [[inlinedict objectForKey:Points] count]; j++)
                                        {
                                            KKPoint *p = [[KKPoint alloc] init];
                                            p.p = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:Pressure] floatValue];
                                            p.x = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PX] floatValue];//+dertX(firstManager.conainerWidth, firstManager.resourceWidth);//板书拖出来 不用考虑偏移量
                                            p.y = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PY] floatValue];
                                            p.lineWidth = p.p * partLineManager.partLine.thickness ;
                                            
                                            [partLineManager.partLine.points addObject:p];
                                        }
                                        
                                    }
                                    else
                                    {
                                        
                                        KKPartLine *partLine = [[KKPartLine alloc] init];
                                        partLine.color16 = [inlinedict objectForKey:Color];
                                        partLine.index = [[inlinedict objectForKey:Index] integerValue];
                                        partLine.thickness = [[inlinedict objectForKey:Thickness] floatValue];
                                        
                                        NSMutableArray *pointsArray = [[NSMutableArray alloc] init];
                                        
                                        for (int j = 0; j < [[inlinedict objectForKey:Points] count]; j++)
                                        {
                                            KKPoint *p = [[KKPoint alloc] init];
                                            p.p = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:Pressure] floatValue];
                                            p.x = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PX] floatValue];//+dertX(firstManager.conainerWidth, firstManager.resourceWidth);//板书拖出来 不用考虑偏移量
                                            p.y = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PY] floatValue];
                                            p.lineWidth = p.p * partLine.thickness ;
                                            
                                            [pointsArray addObject:p];
                                        }
                                        
                                        partLine.points = pointsArray ;
                                        
                                        existHandWritingLastIndex = [[inlinedict objectForKey:Index] integerValue];//放到里面来回头测试
                                        
                                        
                                        KKPartLineManager *manager = [[KKPartLineManager alloc] init];
                                        manager.op = [[inkDict objectForKey:OP] stringValue];
                                        manager.offset = [[inkDict objectForKey:Offset] stringValue];
                                        manager.tcount = [[inkDict objectForKey:TCount] stringValue];
                                        manager.tid = [[inkDict objectForKey:TID] stringValue];
                                        manager.uid = [[inkDict objectForKey:UID] stringValue];
                                        manager.conainerHeight = [[inkDict objectForKey:ConainerHeight] floatValue];
                                        manager.conainerWidth  = [[inkDict objectForKey:ConainerWidth] floatValue];
                                        manager.resourceHeight = [[inkDict objectForKey:ResourceHeight] floatValue];
                                        manager.resourceWidth = [[inkDict objectForKey:ResourceWidth] floatValue];
                                        manager.inkFileID = [inkDict objectForKey:InkFileID];
                                        manager.sourceFileID = [inkDict objectForKey:SourceFileID];
                                        manager.partLine = partLine ;
                                        
                                        [existHandWritingArray addObject:manager];
                                        
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
            }
            
        }
        
        
        if (existHandWritingContext)
        {
            CGContextRelease(existHandWritingContext);
        }
        
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        existHandWritingContext = CGBitmapContextCreate(nil,existHandWritingManager.resourceWidth,existHandWritingManager.resourceHeight,8,0,colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
        CFRelease(colorSpace);
        
        CGContextSetBlendMode(existHandWritingContext, kCGBlendModeCopy);
        CGContextSetLineJoin(existHandWritingContext, kCGLineJoinRound);
        CGContextSetLineCap(existHandWritingContext, kCGLineCapRound);
        CGContextSetAllowsAntialiasing(existHandWritingContext, YES);
        //坐标轴Y轴变化 并且像Y移动一定距离，要不坐标是反的
        CGContextScaleCTM(existHandWritingContext, 1.0, -1.0);
        CGContextTranslateCTM(existHandWritingContext, 0, 0 - existHandWritingManager.resourceHeight);
        
        //画笔迹，如果有，没有则是空
        for (int i = 0; i < [existHandWritingArray count]; i++)
        {
            KKPartLineManager *manager = [existHandWritingArray objectAtIndex:i];
            
            if ([manager.tid isEqualToString:@"2"])
            {
                
                CGContextSetStrokeColorWithColor(existHandWritingContext, [[self colorFromHexString:manager.partLine.color16] CGColor]);
                
                for (int i = 0; i < [manager.partLine.points count]; i++)
                {
                    KKPoint *p = [manager.partLine.points objectAtIndex:i];
                    KKPoint *pNext ;
                    if (i+1 == [manager.partLine.points count])
                    {
                        
                        pNext = p;
                    }
                    else
                    {
                        pNext = [manager.partLine.points objectAtIndex:i+1];
                    }
                    
                    if (i == 0)
                    {
                        CGContextSetLineWidth(existHandWritingContext, p.lineWidth);
                        CGContextMoveToPoint(existHandWritingContext, p.x, p.y);
                    }
                    else
                    {
                        CGContextAddLineToPoint(existHandWritingContext, pNext.x, pNext.y);
                        CGContextStrokePath(existHandWritingContext);
                        CGContextMoveToPoint(existHandWritingContext, pNext.x, pNext.y);
                        CGContextSetLineWidth(existHandWritingContext, p.lineWidth);
                    }
                    
                }
                
            }
            
        }
        
        CGImageRef image = CGBitmapContextCreateImage(existHandWritingContext);
        UIImage* image2 = [UIImage imageWithCGImage:image];
        CGImageRelease(image);
        CGContextRelease(existHandWritingContext);
        existHandWritingContext = nil ;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.delegate onWillBlendExistHandWritingImage:image2];//[[NSNotificationCenter defaultCenter] postNotificationName:@"opfourInDatakGuid" object:managerArray];
            
        });
        
    }
    
}

- (void)blendExistHandWritingImageWithImage:(UIImage *)originalImage
{
    @autoreleasepool
    {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(_blendExistHandWritingImageWithImage:)
                                                                                 object:originalImage];
        [queue addOperation:operation];
        
    }
    
}

- (void)_blendExistHandWritingImageWithImage:(UIImage *)originalImage
{
    self.bgImage = originalImage ;
    
    if (existHandWritingContext)
    {
        CGContextRelease(existHandWritingContext);
    }
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    existHandWritingContext = CGBitmapContextCreate(nil,existHandWritingManager.resourceWidth,existHandWritingManager.resourceHeight,8,0,colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CFRelease(colorSpace);
    
    CGContextSetBlendMode(existHandWritingContext, kCGBlendModeCopy);
    CGContextSetLineJoin(existHandWritingContext, kCGLineJoinRound);
    CGContextSetLineCap(existHandWritingContext, kCGLineCapRound);
    CGContextSetAllowsAntialiasing(existHandWritingContext, YES);
    if (self.bgImage)
    {
        
        //因为传过来的tranformY已经是进行矩阵变换后的数值，所以不需要再进行变换，但翻转后的Y轴位移是没有进行ctx scale转换的，所以要进行相应的转换，就得到 0-transformY-(ctmY-1.f)*firstManager.resourceHeight 这样的公式  至于为什么，我现在也不知道，但是结果是对的。
        CGContextDrawImage(existHandWritingContext, CGRectMake(transformX , 0-transformY-(ctmY-1.f)*existHandWritingManager.resourceHeight, existHandWritingManager.resourceWidth*ctmX, existHandWritingManager.resourceHeight*ctmY), self.bgImage.CGImage);
        /*
         if (ctmX == 1)
         {
         CGContextDrawImage(context, CGRectMake(transformX   , transformY, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
         }
         else
         {
         
         CGContextDrawImage(context, CGRectMake(transformX   , 0-transformY-(ctmY-1.f)*firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
         
         NSLog(@"the value is %@ ",[NSValue valueWithCGRect:CGRectMake(transformX   , 0-transformY-firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY)]);
         }
         */
        
    }
    CGContextScaleCTM(existHandWritingContext, 1.0, -1.0);
    CGContextTranslateCTM(existHandWritingContext, 0, 0 - existHandWritingManager.resourceHeight);
    
    
    CGContextTranslateCTM(existHandWritingContext, transformX, transformY);
    CGContextScaleCTM(existHandWritingContext, ctmX, ctmY);
    
    
    for (int i = 0; i < [existHandWritingArray count]; i++)
    {
        KKPartLineManager *manager = [existHandWritingArray objectAtIndex:i];
        
        if ([manager.tid isEqualToString:@"2"])
        {
            
            CGContextSetStrokeColorWithColor(existHandWritingContext, [[self colorFromHexString:manager.partLine.color16] CGColor]);
            
            for (int i = 0; i < [manager.partLine.points count]; i++)
            {
                KKPoint *p = [manager.partLine.points objectAtIndex:i];
                KKPoint *pNext ;
                if (i+1 == [manager.partLine.points count])
                {
                    
                    pNext = p;
                }
                else
                {
                    pNext = [manager.partLine.points objectAtIndex:i+1];
                }
                
                if (i == 0)
                {
                    CGContextSetLineWidth(existHandWritingContext, p.lineWidth);
                    CGContextMoveToPoint(existHandWritingContext, p.x, p.y);
                }
                else
                {
                    CGContextAddLineToPoint(existHandWritingContext, pNext.x, pNext.y);
                    CGContextStrokePath(existHandWritingContext);
                    CGContextMoveToPoint(existHandWritingContext, pNext.x, pNext.y);
                    CGContextSetLineWidth(existHandWritingContext, p.lineWidth);
                }
                
            }
            
        }
        
    }
    
    
    CGImageRef image = CGBitmapContextCreateImage(existHandWritingContext);
    UIImage* image2 = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    CGContextRelease(existHandWritingContext);
    existHandWritingContext = nil;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate onBlendedExistHandWritingImage:image2];
    });
}


- (UIColor *)colorFromHexString:(NSString *)hexString
{
    unsigned rgbValue = 0;
    hexString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
	
    [scanner scanHexInt:&rgbValue];
	
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}



- (void)_blendWithImage:(UIImage *)bgImage
{
    self.bgImage = bgImage ;
    
    if (context)
    {
        CGContextRelease(context);
    }
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate(nil,firstManager.conainerWidth,firstManager.conainerHeight,8,0,colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CFRelease(colorSpace);
    
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetAllowsAntialiasing(context, YES);
    if (self.bgImage)
    {
        
        //因为传过来的tranformY已经是进行矩阵变换后的数值，所以不需要再进行变换，但翻转后的Y轴位移是没有进行ctx scale转换的，所以要进行相应的转换，就得到 0-transformY-(ctmY-1.f)*firstManager.resourceHeight 这样的公式  至于为什么，我现在也不知道，但是结果是对的。
        CGContextDrawImage(context, CGRectMake(transformX+dertX(firstManager.conainerWidth, firstManager.resourceWidth)*ctmX   , 0-transformY-(ctmY-1.f)*firstManager.conainerHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
        /*
         if (ctmX == 1)
         {
         CGContextDrawImage(context, CGRectMake(transformX   , transformY, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
         }
         else
         {
         
         CGContextDrawImage(context, CGRectMake(transformX   , 0-transformY-(ctmY-1.f)*firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
         
         NSLog(@"the value is %@ ",[NSValue valueWithCGRect:CGRectMake(transformX   , 0-transformY-firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY)]);
         }
         */
        
    }
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, 0, 0 - firstManager.conainerHeight);
    
    
    CGContextTranslateCTM(context, transformX, transformY);
    CGContextScaleCTM(context, ctmX, ctmY);
    
    
    for (int i = 0; i < [drawDataArray count]; i++)
    {
        KKPartLineManager *manager = [drawDataArray objectAtIndex:i];
        
        if ([manager.tid isEqualToString:@"2"])
        {
            
            CGContextSetStrokeColorWithColor(context, [[self colorFromHexString:manager.partLine.color16] CGColor]);
            
            for (int i = 0; i < [manager.partLine.points count]; i++)
            {
                KKPoint *p = [manager.partLine.points objectAtIndex:i];
                KKPoint *pNext ;
                if (i+1 == [manager.partLine.points count])
                {
                    
                    pNext = p;
                }
                else
                {
                    pNext = [manager.partLine.points objectAtIndex:i+1];
                }
                
                if (i == 0)
                {
                    CGContextSetLineWidth(context, p.lineWidth);
                    CGContextMoveToPoint(context, p.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), p.y);
                }
                else
                {
                    CGContextAddLineToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                    CGContextStrokePath(context);
                    CGContextMoveToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                    CGContextSetLineWidth(context, p.lineWidth);
                }
                
            }
            
        }
        
    }
    
    
    CGImageRef image = CGBitmapContextCreateImage(context);
    UIImage* image2 = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate onStreamDrawDrawing:kStreamDrawStatusProgress blendedImage:image2 whitePaperGuid:nil imageScale:ctmX];
    });
}


- (void)blendWithImage:(UIImage *)bgImage
{
    
    @autoreleasepool
    {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(_blendWithImage:)
                                                                                 object:bgImage];
        [queue addOperation:operation];
        
    }
    
}

- (void)_dealStackData:(NSString *)data
{
    
    
    NSData *dealedData = [[NSData dataWithBase64EncodedString:data] gzipInflate];
    
    NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:dealedData options:NSJSONReadingMutableLeaves error:nil];
    
    NSArray *dataArray;
    
    if ([[[dict objectForKey:TID] stringValue] isEqualToString:@"1"])
    {
        //开始
        KKPartLineManager *manager = [[KKPartLineManager alloc] init];
        
        manager.tid = [[dict objectForKey:TID] stringValue];
        manager.uid = [[dict objectForKey:UID] stringValue];
        manager.conainerHeight = [[dict objectForKey:ConainerHeight] floatValue];
        manager.conainerWidth  = [[dict objectForKey:ConainerWidth] floatValue];
        manager.resourceHeight = [[dict objectForKey:ResourceHeight] floatValue];
        manager.resourceWidth = [[dict objectForKey:ResourceWidth] floatValue];
        manager.inkFileID = [dict objectForKey:InkFileID];
        manager.sourceFileID = [dict objectForKey:SourceFileID];
        manager.rid = [dict objectForKey:RID];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //通知创建context
            [messageQueue setSuspended:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"createcontext" object:manager];
            
        });
        
        
    }
    if ([[[dict objectForKey:TID] stringValue] isEqualToString:@"2"])
    {
        //过程
        
        if ([[[dict objectForKey:OP] stringValue] isEqualToString:@"2"])
        {
            [managerArray removeAllObjects];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //清除所有笔迹 重绘
                [[NSNotificationCenter defaultCenter] postNotificationName:@"clearall" object:nil];
                
            });
            
            
        }
        
        
        if ([[[dict objectForKey:OP] stringValue] isEqualToString:@"1"])
        {
            NSLog(@"the dict is %@ when the op == 1",[dict description]);
            
            NSArray *arr = [NSJSONSerialization JSONObjectWithData:[[dict objectForKey:DATA] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
            
            NSMutableArray *shadowArray = [[NSMutableArray alloc] initWithArray:managerArray];
            
            for (KKPartLineManager *mgr in managerArray)
            {
                
                NSLog(@"the arr count is %ld",[arr count]);
                
                for (int i = 0; i < [arr count]; i++)
                {
                    if (mgr.partLine.index == [[arr objectAtIndex:i] integerValue])
                    {
                        [shadowArray removeObject:mgr];
                        
                    }
                }
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                managerArray = shadowArray ;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"clearsingleline" object:managerArray];
                
            });
            
        }
        if ([[[dict objectForKey:OP] stringValue] isEqualToString:@"0"])
        {
            if (![[dict objectForKey:DATA] isEqualToString:@""] && [dict objectForKey:DATA])
            {
                //解析data数据，可能包括多个data块数据
                dataArray = [NSJSONSerialization JSONObjectWithData:[[dict objectForKey:DATA] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                

                if ([dataArray isKindOfClass:[NSArray class]])
                {
                    // 处理 index 相同的适合，manager的partline唯一，只有一条。
                    
                    for (int i = 0; i < dataArray.count; i++)
                    {
                        //取上一个partlinemanager.partline.points add points
                        
                        NSDictionary *inlinedict = [dataArray objectAtIndex:i];
                        
                        if (messageDealLastIndex == [[inlinedict objectForKey:Index] integerValue])
                        {
                            
                            KKPartLineManager *partLineManager = (KKPartLineManager *)[managerArray lastObject];
                            
                            for (int j = 0; j < [[inlinedict objectForKey:Points] count]; j++)
                            {
                                KKPoint *p = [[KKPoint alloc] init];
                                p.p = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:Pressure] floatValue];
                                p.x = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PX] floatValue];
                                p.y = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PY] floatValue];
                                p.lineWidth = p.p * partLineManager.partLine.thickness ;
                                
                                [partLineManager.partLine.points addObject:p];
                            }
                            
                        }
                        else
                        {
                            
                            KKPartLine *partLine = [[KKPartLine alloc] init];
                            partLine.color16 = [inlinedict objectForKey:Color];
                            partLine.index = [[inlinedict objectForKey:Index] integerValue];
                            partLine.thickness = [[inlinedict objectForKey:Thickness] floatValue];
                            
                            NSMutableArray *pointsArray = [[NSMutableArray alloc] init];
                            
                            for (int j = 0; j < [[inlinedict objectForKey:Points] count]; j++)
                            {
                                KKPoint *p = [[KKPoint alloc] init];
                                p.p = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:Pressure] floatValue];
                                p.x = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PX] floatValue];
                                p.y = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PY] floatValue];
                                p.lineWidth = p.p * partLine.thickness ;
                                
                                [pointsArray addObject:p];
                            }
                            
                            partLine.points = pointsArray ;
                            
                            messageDealLastIndex = [[inlinedict objectForKey:Index] integerValue];//放到里面来回头测试
                            
                            
                            KKPartLineManager *manager = [[KKPartLineManager alloc] init];
                            manager.op = [[dict objectForKey:OP] stringValue];
                            manager.offset = [[dict objectForKey:Offset] stringValue];
                            manager.tcount = [[dict objectForKey:TCount] stringValue];
                            manager.tid = [[dict objectForKey:TID] stringValue];
                            manager.uid = [[dict objectForKey:UID] stringValue];
                            manager.conainerHeight = [[dict objectForKey:ConainerHeight] floatValue];
                            manager.conainerWidth  = [[dict objectForKey:ConainerWidth] floatValue];
                            manager.resourceHeight = [[dict objectForKey:ResourceHeight] floatValue];
                            manager.resourceWidth = [[dict objectForKey:ResourceWidth] floatValue];
                            manager.inkFileID = [dict objectForKey:InkFileID];
                            manager.sourceFileID = [dict objectForKey:SourceFileID];
                            manager.partLine = partLine ;
                            
                            [managerArray addObject:manager];
                            
                            
                        }
                        
                    }
                    
//                    NSLog(@"_________ array is %@________",[dataArray objectAtIndex:0]);
                }
                else
                {
//                    NSLog(@"阿斯科利的话费卢卡斯就法律框架啊收了房间啊数量将开发拉丝机房垃圾时刻法律框架撒福利科技");
                }
                
//                NSLog(@"the data string is %@",[dict objectForKey:DATA]);
//                NSLog(@"the array count is %ld",[dataArray count]);
                
            }
            else
            {
//                NSLog(@"the data string is nil");
            }
            
            
//            NSLog(@"the managerArray count is %li",[managerArray count]);
            
//            NSLog(@"_========== count is %ld",[[[(KKPartLineManager *)[managerArray lastObject] partLine] points] count]);
            
            
            //主线程
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"drawline" object:managerArray];
                
            });
        }
        if ([[[dict objectForKey:OP] stringValue] isEqualToString:@"3"])
        {
            if ([[dict objectForKey:DATA] isEqualToString:@"Identity"])
            {
                KKPartLineManager *manager = [[KKPartLineManager alloc] init];
                manager.op = [[dict objectForKey:OP] stringValue];
                manager.offset = [[dict objectForKey:Offset] stringValue];
                manager.tcount = [[dict objectForKey:TCount] stringValue];
                manager.tid = [[dict objectForKey:TID] stringValue];
                manager.uid = [[dict objectForKey:UID] stringValue];
                manager.ctmX = 1 ;
                manager.ctmY = 1 ;
                manager.transformX = 0 ;
                manager.transformY = 0 ;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ctm" object:manager];
                    
                });
            }
            else
            {
                KKPartLineManager *manager = [[KKPartLineManager alloc] init];
                manager.op = [[dict objectForKey:OP] stringValue];
                manager.offset = [[dict objectForKey:Offset] stringValue];
                manager.tcount = [[dict objectForKey:TCount] stringValue];
                manager.tid = [[dict objectForKey:TID] stringValue];
                manager.uid = [[dict objectForKey:UID] stringValue];
                
                NSString *ctmString = [dict objectForKey:DATA];
                NSArray *arr = [ctmString componentsSeparatedByString:@","];
                manager.ctmX = [[arr objectAtIndex:0] floatValue];
                manager.ctmY = [[arr objectAtIndex:3] floatValue];
                manager.transformX = [[arr objectAtIndex:4] floatValue];
                manager.transformY = [[arr objectAtIndex:5] floatValue];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ctm" object:manager];
                    
                });
            }
            
            
            
            
        }
        
        if ([[[dict objectForKey:OP] stringValue] isEqualToString:@"4"])
        {
            if (![[dict objectForKey:DATA] isEqualToString:@""] && [dict objectForKey:DATA])
            {
                //解析data数据，可能包括多个data块数据
                id data = [NSJSONSerialization JSONObjectWithData:[[dict objectForKey:DATA] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                
                if ([data isKindOfClass:[NSDictionary class]])
                {
                    
                    if (![[data objectForKey:@"InDatakGuid"] isEqualToString:@""])
                    {
                        NSString *urlString = [NSString stringWithFormat:@"%@/%@",[[KKCache sharedCache] HTTPIP],[ICEFileEntity URL_SANDBOX_SEARCH_FILE]];
                        
                        NSString *postString = [NSString stringWithFormat:@"%@=%@",GUID,[data objectForKey:@"InDatakGuid"]];
                        
                        
                        NSData *postData    =  [postString dataUsingEncoding:NSUTF8StringEncoding];
                        
                        NSString *postLength	= [NSString stringWithFormat:@"%ld", [postData length]];
                        
                        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                                               cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                                           timeoutInterval:60.0];
                        [request setHTTPMethod:@"POST"];
                        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                        [request setHTTPBody:postData];
                        
                        
                        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
                        
                        NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:nil];
                        
                        if ([[dict objectForKey:@"code"] integerValue] == 0)
                        {
                            
                            if ([[dict objectForKey:@"msg"] isKindOfClass:[NSArray class]] && [[dict objectForKey:@"msg"] count] != 0)
                            {
                                NSDictionary *resultDict = [[dict objectForKey:@"msg"] objectAtIndex:0];
                                
                                
                                NSString *inkDataString = [[NSString stringWithFormat:@"%@/resource/%@/%@",[[KKCache sharedCache] HTTPIP],[resultDict objectForKey:PackageName],[resultDict objectForKey:FileNameWithExtend]] encodeURL];
                                
                                NSData *inkdata = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:inkDataString]];
                                
//                                NSData *gzipdata = [[[NSData alloc] initWithBase64EncodedData:inkdata options:NSDataBase64DecodingIgnoreUnknownCharacters] gzipInflate];
                                NSData *gzipdata = [[NSData dataWithBase64EncodedString:[[NSString alloc] initWithData:inkdata encoding:NSUTF8StringEncoding]] gzipInflate];
                                //                            NSLog(@"the final string is %@",[[NSString alloc] initWithData:gzipdata encoding:NSUTF8StringEncoding]);
                                
                                
                                NSDictionary *inkDict = [NSJSONSerialization JSONObjectWithData:gzipdata options:NSJSONReadingMutableLeaves error:nil];
                                
                                if ([[[inkDict objectForKey:TID] stringValue] isEqualToString:@"2"])
                                {
                                    
                                    if ([[[inkDict objectForKey:OP] stringValue] isEqualToString:@"0"])
                                    {
                                        if (![[inkDict objectForKey:DATA] isEqualToString:@""] && [inkDict objectForKey:DATA])
                                        {
                                            //解析data数据，可能包括多个data块数据
                                            NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:[[inkDict objectForKey:DATA] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                                            
                                            if ([dataArray isKindOfClass:[NSArray class]])
                                            {
                                                // 处理 index 相同的适合，manager的partline唯一，只有一条。
                                                
                                                for (int i = 0; i < dataArray.count; i++)
                                                {
                                                    //取上一个partlinemanager.partline.points add points
                                                    
                                                    NSDictionary *inlinedict = [dataArray objectAtIndex:i];
                                                    
                                                    if (messageDealLastIndex == [[inlinedict objectForKey:Index] integerValue])
                                                    {
                                                        
                                                        KKPartLineManager *partLineManager = (KKPartLineManager *)[managerArray lastObject];
                                                        
                                                        for (int j = 0; j < [[inlinedict objectForKey:Points] count]; j++)
                                                        {
                                                            KKPoint *p = [[KKPoint alloc] init];
                                                            p.p = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:Pressure] floatValue];
                                                            p.x = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PX] floatValue];
                                                            p.y = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PY] floatValue];
                                                            p.lineWidth = p.p * partLineManager.partLine.thickness ;
                                                            
                                                            [partLineManager.partLine.points addObject:p];
                                                        }
                                                        
                                                    }
                                                    else
                                                    {
                                                        
                                                        KKPartLine *partLine = [[KKPartLine alloc] init];
                                                        partLine.color16 = [inlinedict objectForKey:Color];
                                                        partLine.index = [[inlinedict objectForKey:Index] integerValue];
                                                        partLine.thickness = [[inlinedict objectForKey:Thickness] floatValue];
                                                        
                                                        NSMutableArray *pointsArray = [[NSMutableArray alloc] init];
                                                        
                                                        for (int j = 0; j < [[inlinedict objectForKey:Points] count]; j++)
                                                        {
                                                            KKPoint *p = [[KKPoint alloc] init];
                                                            p.p = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:Pressure] floatValue];
                                                            p.x = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PX] floatValue];
                                                            p.y = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PY] floatValue];
                                                            p.lineWidth = p.p * partLine.thickness ;
                                                            
                                                            [pointsArray addObject:p];
                                                        }
                                                        
                                                        partLine.points = pointsArray ;
                                                        
                                                        messageDealLastIndex = [[inlinedict objectForKey:Index] integerValue];//放到里面来回头测试
                                                        
                                                        
                                                        KKPartLineManager *manager = [[KKPartLineManager alloc] init];
                                                        manager.op = [[inkDict objectForKey:OP] stringValue];
                                                        manager.offset = [[inkDict objectForKey:Offset] stringValue];
                                                        manager.tcount = [[inkDict objectForKey:TCount] stringValue];
                                                        manager.tid = [[inkDict objectForKey:TID] stringValue];
                                                        manager.uid = [[inkDict objectForKey:UID] stringValue];
                                                        manager.conainerHeight = [[inkDict objectForKey:ConainerHeight] floatValue];
                                                        manager.conainerWidth  = [[inkDict objectForKey:ConainerWidth] floatValue];
                                                        manager.resourceHeight = [[inkDict objectForKey:ResourceHeight] floatValue];
                                                        manager.resourceWidth = [[inkDict objectForKey:ResourceWidth] floatValue];
                                                        manager.inkFileID = [inkDict objectForKey:InkFileID];
                                                        manager.sourceFileID = [inkDict objectForKey:SourceFileID];
                                                        manager.partLine = partLine ;
                                                        
                                                        [managerArray addObject:manager];
                                                        
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                            
                                        }
                                        
                                    }
                                    
                                }
                            }
                            
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"opfourInDatakGuid" object:managerArray];
                            
                        });
                    
                    }
                    if (![[data objectForKey:@"OriginalGuid"] isEqualToString:@""])
                    {
                        
                        NSLog(@"the data is %@",data);
                        NSLog(@"the OriginalGuid is %@",[data objectForKey:@"OriginalGuid"]);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"opfourOriginalGuid" object:managerArray userInfo:[NSDictionary dictionaryWithObject:[data objectForKey:@"OriginalGuid"] forKey:@"OriginalGuid"]];
                            
                        });
                    }
                    
                    
                    
                }
                
            }
            
        }
        
    }
    if ([[[dict objectForKey:TID] stringValue] isEqualToString:@"0"])
    {
        //结束
        [managerArray removeAllObjects];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"drawend" object:managerArray];
            
            
        });
        
    }
    
}


- (void)dealStackData:(NSNotification *)n
{
    id data = [n object];
    
    @autoreleasepool
    {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(_dealStackData:)
                                                                                 object:data];
        [messageQueue addOperation:operation];
        
    }
}

- (void)_drawEnd:(id)data
{
    self.bgImage = nil ;
    
    [drawDataArray removeAllObjects];
    [managerArray removeAllObjects];
    
    
    ctmX = 1 ;
    ctmY = 1 ;
    transformX = 0 ;
    transformY = 0 ;
    lastIndex = -1 ;
    messageDealLastIndex = - 1;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate onStreamDrawEnd:kStreamDrawStatusEnd];
    });
}

- (void)drawEnd:(NSNotification *)n
{
    id data = [n object];
    
    [queue cancelAllOperations];
    @autoreleasepool
    {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(_drawEnd:)
                                                                                 object:data];
        [queue addOperation:operation];
        
    }
}


//初始化必要的数据
- (void)initObjects
{
    ctmX = 1 ;
    ctmY = 1 ;
    transformX = 0 ;
    transformY = 0 ;
 
    lastIndex = -1 ;
    messageDealLastIndex = -1 ;
    
    drawDataArray = [[NSMutableArray alloc] init];
    managerArray = [[NSMutableArray alloc] init];
    
    queue = [[NSOperationQueue alloc] init];
    [queue setMaxConcurrentOperationCount:1];
    
    messageQueue = [[NSOperationQueue alloc] init];
    [messageQueue setMaxConcurrentOperationCount:1];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDrawingWithData:) name:@"drawline" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCTM:) name:@"ctm" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createContext:) name:@"createcontext" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearAll:) name:@"clearall" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearSingleLine:) name:@"clearsingleline" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(opfourInDatakGuid:) name:@"opfourInDatakGuid" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(opfourOriginalGuid:) name:@"opfourOriginalGuid" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(drawEnd:) name:@"drawend" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dealStackData:) name:@"dealStackData" object:nil];
    
    
    
}

- (void)_updateCTM:(id)data
{
    
    if (context)
    {
        CGContextRelease(context);
    }
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate(nil,firstManager.conainerWidth,firstManager.conainerHeight,8,0,colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CFRelease(colorSpace);
    
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetAllowsAntialiasing(context, YES);
    
    KKPartLineManager *manager = (KKPartLineManager *)data ;
    
    ctmX = manager.ctmX ;
    ctmY = manager.ctmY ;
    transformX = manager.transformX ;
    transformY = manager.transformY ;
    
    NSLog(@"the ctmx is %f ctmy is %f transformX is %f transformY is %f",manager.ctmX,manager.ctmY,manager.transformX, manager.transformY);
    
    
    if (self.bgImage)
    {
        //因为传过来的tranformY已经是进行矩阵变换后的数值，所以不需要再进行变换，但翻转后的Y轴位移是没有进行ctx scale转换的，所以要进行相应的转换，就得到 0-transformY-(ctmY-1.f)*firstManager.resourceHeight 这样的公式  至于为什么，我现在也不知道，但是结果是对的。
        CGContextDrawImage(context, CGRectMake(transformX+dertX(firstManager.conainerWidth, firstManager.resourceWidth)*ctmX   , 0-transformY-(ctmY-1.f)*firstManager.conainerHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
        /*
        if (ctmX == 1)
        {
            CGContextDrawImage(context, CGRectMake(transformX   , transformY, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
        }
        else
        {

            CGContextDrawImage(context, CGRectMake(transformX   , 0-transformY-(ctmY-1.f)*firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
            
            NSLog(@"the value is %@ ",[NSValue valueWithCGRect:CGRectMake(transformX   , 0-transformY-firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY)]);
        }
         */
        
        
        
    }
    
    
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, 0, 0 - firstManager.conainerHeight);
    
    CGContextTranslateCTM(context, transformX, transformY);
    CGContextScaleCTM(context, ctmX, ctmY);
    
    
    for (int i = 0; i < [drawDataArray count]; i++)
    {
        KKPartLineManager *manager = [drawDataArray objectAtIndex:i];
        
        if ([manager.tid isEqualToString:@"2"])
        {
            
            CGContextSetStrokeColorWithColor(context, [[self colorFromHexString:manager.partLine.color16] CGColor]);
            
            for (int i = 0; i < [manager.partLine.points count]; i++)
            {
                KKPoint *p = [manager.partLine.points objectAtIndex:i];
                KKPoint *pNext ;
                if (i+1 == [manager.partLine.points count])
                {
                    
                    pNext = p;
                }
                else
                {
                    pNext = [manager.partLine.points objectAtIndex:i+1];
                }
                
                if (i == 0)
                {
                    CGContextSetLineWidth(context, p.lineWidth);
                    CGContextMoveToPoint(context, p.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), p.y);
                }
                else
                {
                    CGContextAddLineToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                    CGContextStrokePath(context);
                    CGContextMoveToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                    CGContextSetLineWidth(context, p.lineWidth);
                }
                
            }
            
        }
        
    }
    
    
    CGImageRef image = CGBitmapContextCreateImage(context);
    UIImage* image2 = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate onStreamDrawDrawing:kStreamDrawStatusProgress blendedImage:image2 whitePaperGuid:nil imageScale:ctmX];
        
    });
    
}


- (void)updateCTM:(NSNotification *)n
{
    id data = [n object];
    
    //只考虑一个人写的情况，不考虑多人，如果有多人，那么第二个人只要开始写，那么就清除所有笔迹
    [queue cancelAllOperations];
    
    @autoreleasepool
    {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(_updateCTM:)
                                                                                 object:data];
        [queue addOperation:operation];
        
    }
}


- (void)_createContext:(id)data
{
    
    firstManager = (KKPartLineManager *)data ;
    
    if (context)
    {
        CGContextRelease(context);
    }
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate(nil,firstManager.conainerWidth,firstManager.conainerHeight,8,0,colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CFRelease(colorSpace);
    
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetAllowsAntialiasing(context, YES);
    //坐标轴Y轴变化 并且像Y移动一定距离，要不坐标是反的
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, 0, 0 - firstManager.conainerHeight);
    
    
    //如果inkFileID 不为空，说明之前就存在一段笔迹，去中控请求回来，画上。
    if (![firstManager.inkFileID isEqualToString:@""])
    {
        
        
        //post请求GUID为inkFileID的笔迹数据
        NSString *urlString = [NSString stringWithFormat:@"%@/%@",[[KKCache sharedCache] HTTPIP],[ICEFileEntity URL_SANDBOX_SEARCH_FILE]];
        
        NSString *postString = [NSString stringWithFormat:@"%@=%@",GUID,firstManager.inkFileID];
        
        
        NSData *postData    =  [postString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *postLength	= [NSString stringWithFormat:@"%ld", [postData length]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                           timeoutInterval:60.0];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:nil];
        
        
        //如果 code 不等于0 那么发生错误
        if ([[dict objectForKey:@"code"] integerValue] == 0)
        {
            //判断空情况
            if ([[dict objectForKey:@"msg"] isKindOfClass:[NSArray class]] && [[dict objectForKey:@"msg"] count] != 0)
            {
                NSDictionary *resultDict = [[dict objectForKey:@"msg"] objectAtIndex:0];
                
                
                NSString *inkDataString = [[NSString stringWithFormat:@"%@/resource/%@/%@",[[KKCache sharedCache] HTTPIP],[resultDict objectForKey:PackageName],[resultDict objectForKey:FileNameWithExtend]] encodeURL];
                
                NSData *inkdata = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:inkDataString]];
                
//                NSData *gzipdata = [[[NSData alloc] initWithBase64EncodedData:inkdata options:NSDataBase64DecodingIgnoreUnknownCharacters] gzipInflate];
                NSData *gzipdata = [[NSData dataWithBase64EncodedString:[[NSString alloc] initWithData:inkdata encoding:NSUTF8StringEncoding]] gzipInflate];
                
                NSDictionary *inkDict = [NSJSONSerialization JSONObjectWithData:gzipdata options:NSJSONReadingMutableLeaves error:nil];
                
                
                //TID = 2 时表示过程数据
                if ([[[inkDict objectForKey:TID] stringValue] isEqualToString:@"2"])
                {
                    
                    if ([[[inkDict objectForKey:OP] stringValue] isEqualToString:@"0"])
                    {
                        if (![[inkDict objectForKey:DATA] isEqualToString:@""] && [inkDict objectForKey:DATA])
                        {
                            //解析data数据，可能包括多个data块数据
                            NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:[[inkDict objectForKey:DATA] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                            
                            if ([dataArray isKindOfClass:[NSArray class]])
                            {
                                // 处理 index 相同的适合，manager的partline唯一，只有一条。
                                
                                for (int i = 0; i < dataArray.count; i++)
                                {
                                    //取上一个partlinemanager.partline.points add points
                                    
                                    NSDictionary *inlinedict = [dataArray objectAtIndex:i];
                                    
                                    if (lastIndex == [[inlinedict objectForKey:Index] integerValue])
                                    {
                                        
                                        KKPartLineManager *partLineManager = (KKPartLineManager *)[drawDataArray lastObject];
                                        
                                        for (int j = 0; j < [[inlinedict objectForKey:Points] count]; j++)
                                        {
                                            KKPoint *p = [[KKPoint alloc] init];
                                            p.p = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:Pressure] floatValue];
                                            p.x = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PX] floatValue];
                                            p.y = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PY] floatValue];
                                            p.lineWidth = p.p * partLineManager.partLine.thickness ;
                                            
                                            [partLineManager.partLine.points addObject:p];
                                        }
                                        
                                    }
                                    else
                                    {
                                        
                                        KKPartLine *partLine = [[KKPartLine alloc] init];
                                        partLine.color16 = [inlinedict objectForKey:Color];
                                        partLine.index = [[inlinedict objectForKey:Index] integerValue];
                                        partLine.thickness = [[inlinedict objectForKey:Thickness] floatValue];
                                        
                                        NSMutableArray *pointsArray = [[NSMutableArray alloc] init];
                                        
                                        for (int j = 0; j < [[inlinedict objectForKey:Points] count]; j++)
                                        {
                                            KKPoint *p = [[KKPoint alloc] init];
                                            p.p = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:Pressure] floatValue];
                                            p.x = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PX] floatValue];
                                            p.y = [[[[inlinedict objectForKey:Points] objectAtIndex:j] objectForKey:PY] floatValue];
                                            p.lineWidth = p.p * partLine.thickness ;
                                            
                                            [pointsArray addObject:p];
                                        }
                                        
                                        partLine.points = pointsArray ;
                                        
                                        lastIndex = [[inlinedict objectForKey:Index] integerValue];//放到里面来回头测试
                                        
                                        
                                        KKPartLineManager *manager = [[KKPartLineManager alloc] init];
                                        manager.op = [[inkDict objectForKey:OP] stringValue];
                                        manager.offset = [[inkDict objectForKey:Offset] stringValue];
                                        manager.tcount = [[inkDict objectForKey:TCount] stringValue];
                                        manager.tid = [[inkDict objectForKey:TID] stringValue];
                                        manager.uid = [[inkDict objectForKey:UID] stringValue];
                                        manager.conainerHeight = [[inkDict objectForKey:ConainerHeight] floatValue];
                                        manager.conainerWidth  = [[inkDict objectForKey:ConainerWidth] floatValue];
                                        manager.resourceHeight = [[inkDict objectForKey:ResourceHeight] floatValue];
                                        manager.resourceWidth = [[inkDict objectForKey:ResourceWidth] floatValue];
                                        manager.inkFileID = [inkDict objectForKey:InkFileID];
                                        manager.sourceFileID = [inkDict objectForKey:SourceFileID];
                                        manager.partLine = partLine ;
                                        
                                        [managerArray addObject:manager];
                                    }
                                    
                                }
                                
                            }
                            
                        }

                    }
                    
                }
            }
            
            
        }
        
    }
    
    //画笔迹，如果有，没有则是空
    for (int i = 0; i < [managerArray count]; i++)
    {
        KKPartLineManager *manager = [managerArray objectAtIndex:i];
        
        if ([manager.tid isEqualToString:@"2"])
        {
            
            CGContextSetStrokeColorWithColor(context, [[self colorFromHexString:manager.partLine.color16] CGColor]);
            
            for (int i = 0; i < [manager.partLine.points count]; i++)
            {
                KKPoint *p = [manager.partLine.points objectAtIndex:i];
                KKPoint *pNext ;
                if (i+1 == [manager.partLine.points count])
                {
                    
                    pNext = p;
                }
                else
                {
                    pNext = [manager.partLine.points objectAtIndex:i+1];
                }
                
                if (i == 0)
                {
                    CGContextSetLineWidth(context, p.lineWidth);
                    CGContextMoveToPoint(context, p.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), p.y);
                }
                else
                {
                    CGContextAddLineToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                    CGContextStrokePath(context);
                    CGContextMoveToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                    CGContextSetLineWidth(context, p.lineWidth);
                }
                
            }
            
        }
        
    }
    
    CGImageRef image = CGBitmapContextCreateImage(context);
    UIImage* image2 = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate onStreamDrawBegan:kStreamDrawStatusStart blendedImage:image2 rid:firstManager.rid sourceFileID:firstManager.sourceFileID];
        [messageQueue setSuspended:NO];
    });
    
    
}
- (void)createContext:(NSNotification *)n
{
    
    
    id data = [n object];
    
    //只考虑一个人写的情况，不考虑多人，如果有多人，那么第二个人只要开始写，那么就清除所有笔迹
    [queue cancelAllOperations];
    
    @autoreleasepool
    {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(_createContext:)
                                                                                 object:data];
        [queue addOperation:operation];
        
    }
}


- (void)_clearAll:(id)data
{
    if (context)
    {
        CGContextRelease(context);
    }
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate(nil,firstManager.conainerWidth,firstManager.conainerHeight,8,0,colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CFRelease(colorSpace);
    
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetAllowsAntialiasing(context, YES);
    
    
    if (self.bgImage)
    {
        //因为传过来的tranformY已经是进行矩阵变换后的数值，所以不需要再进行变换，但翻转后的Y轴位移是没有进行ctx scale转换的，所以要进行相应的转换，就得到 0-transformY-(ctmY-1.f)*firstManager.resourceHeight 这样的公式  至于为什么，我现在也不知道，但是结果是对的。
        CGContextDrawImage(context, CGRectMake(transformX+dertX(firstManager.conainerWidth, firstManager.resourceWidth)*ctmX   , 0-transformY-(ctmY-1.f)*firstManager.conainerHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
        /*
         if (ctmX == 1)
         {
         CGContextDrawImage(context, CGRectMake(transformX   , transformY, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
         }
         else
         {
         
         CGContextDrawImage(context, CGRectMake(transformX   , 0-transformY-(ctmY-1.f)*firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
         
         NSLog(@"the value is %@ ",[NSValue valueWithCGRect:CGRectMake(transformX   , 0-transformY-firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY)]);
         }
         */
        
    }
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextTranslateCTM(context, 0, 0 - firstManager.conainerHeight);
    
    
    
    CGContextTranslateCTM(context, transformX, transformY);
    CGContextScaleCTM(context, ctmX, ctmY);
    
    CGImageRef image = CGBitmapContextCreateImage(context);
    UIImage* image2 = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate onStreamDrawDrawing:kStreamDrawStatusProgress blendedImage:image2 whitePaperGuid:nil imageScale:ctmX];
    });
    
}

- (void)clearAll:(NSNotification *)n
{
    id data = [n object];
    [queue cancelAllOperations];
    
    @autoreleasepool
    {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(_clearAll:)
                                                                                 object:data];
        [queue addOperation:operation];
        
    }
    
}

- (void)_clearSingleLine:(id)data
{
    
    drawDataArray = (NSMutableArray *)data ;
    
    if (context)
    {
        CGContextRelease(context);
    }
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate(nil,firstManager.conainerWidth,firstManager.conainerHeight,8,0,colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CFRelease(colorSpace);
    
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetAllowsAntialiasing(context, YES);
    
    if (self.bgImage)
    {
        //因为传过来的tranformY已经是进行矩阵变换后的数值，所以不需要再进行变换，但翻转后的Y轴位移是没有进行ctx scale转换的，所以要进行相应的转换，就得到 0-transformY-(ctmY-1.f)*firstManager.resourceHeight 这样的公式  至于为什么，我现在也不知道，但是结果是对的。
        CGContextDrawImage(context, CGRectMake(transformX+dertX(firstManager.conainerWidth, firstManager.resourceWidth)*ctmX   , 0-transformY-(ctmY-1.f)*firstManager.conainerHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
        /*
         if (ctmX == 1)
         {
         CGContextDrawImage(context, CGRectMake(transformX   , transformY, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
         }
         else
         {
         
         CGContextDrawImage(context, CGRectMake(transformX   , 0-transformY-(ctmY-1.f)*firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
         
         NSLog(@"the value is %@ ",[NSValue valueWithCGRect:CGRectMake(transformX   , 0-transformY-firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY)]);
         }
         */
        
        
        
    }
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, 0, 0 - firstManager.conainerHeight);
    
    
    
    CGContextTranslateCTM(context, transformX, transformY);
    CGContextScaleCTM(context, ctmX, ctmY);
    
    
    for (int i = 0; i < [drawDataArray count]; i++)
    {
        KKPartLineManager *manager = [drawDataArray objectAtIndex:i];
        
        if ([manager.tid isEqualToString:@"2"])
        {
            
            CGContextSetStrokeColorWithColor(context, [[self colorFromHexString:manager.partLine.color16] CGColor]);
            
            for (int i = 0; i < [manager.partLine.points count]; i++)
            {
                KKPoint *p = [manager.partLine.points objectAtIndex:i];
                KKPoint *pNext ;
                if (i+1 == [manager.partLine.points count])
                {
                    
                    pNext = p;
                }
                else
                {
                    pNext = [manager.partLine.points objectAtIndex:i+1];
                }
                
                if (i == 0)
                {
                    CGContextSetLineWidth(context, p.lineWidth);
                    CGContextMoveToPoint(context, p.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), p.y);
                }
                else
                {
                    CGContextAddLineToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                    CGContextStrokePath(context);
                    CGContextMoveToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                    CGContextSetLineWidth(context, p.lineWidth);
                }
                
            }
            
        }
        
    }
    
    
    CGImageRef image = CGBitmapContextCreateImage(context);
    UIImage* image2 = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate onStreamDrawDrawing:kStreamDrawStatusProgress blendedImage:image2 whitePaperGuid:nil imageScale:ctmX];
    });
    
}

- (void)clearSingleLine:(NSNotification *)n
{
    id data = [n object];
    [queue cancelAllOperations];
    @autoreleasepool
    {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(_clearSingleLine:)
                                                                                 object:data];
        [queue addOperation:operation];
        
    }
    
}




- (void)updateDrawing
{
    
    if (drawDataArray.count <= 2)
    {
        for (int i = 0; i < [drawDataArray count]; i++)
        {
            KKPartLineManager *manager = [drawDataArray objectAtIndex:i];
            
            if ([manager.tid isEqualToString:@"2"])
            {
                
                CGContextSetStrokeColorWithColor(context, [[self colorFromHexString:manager.partLine.color16] CGColor]);
                
                for (int i = 0; i < [manager.partLine.points count]; i++)
                {
                    KKPoint *p = [manager.partLine.points objectAtIndex:i];
                    KKPoint *pNext ;
                    if (i+1 == [manager.partLine.points count])
                    {
                        
                        pNext = p;
                    }
                    else
                    {
                        pNext = [manager.partLine.points objectAtIndex:i+1];
                    }
                    
                    if (i == 0)
                    {
                        CGContextSetLineWidth(context, p.lineWidth);
                        CGContextMoveToPoint(context, p.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), p.y);
                    }
                    else
                    {
                        CGContextAddLineToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                        CGContextStrokePath(context);
                        CGContextMoveToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                        CGContextSetLineWidth(context, p.lineWidth);
                    }
                    
                }
                
            }
            
        }
    }
    else
    {
        for (long int i = [drawDataArray count] - 2; i < [drawDataArray count]; i++)
        {
            KKPartLineManager *manager = [drawDataArray objectAtIndex:i];
            
            if ([manager.tid isEqualToString:@"2"])
            {
                
                CGContextSetStrokeColorWithColor(context, [[self colorFromHexString:manager.partLine.color16] CGColor]);
                
                for (int i = 0; i < [manager.partLine.points count]; i++)
                {
                    KKPoint *p = [manager.partLine.points objectAtIndex:i];
                    KKPoint *pNext ;
                    if (i+1 == [manager.partLine.points count])
                    {
                        pNext = p;
                    }
                    else
                    {
                        pNext = [manager.partLine.points objectAtIndex:i+1];
                    }
                    
                    if (i == 0)
                    {
                        CGContextSetLineWidth(context, p.lineWidth);
                        CGContextMoveToPoint(context, p.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), p.y);
                    }
                    else
                    {
                        CGContextAddLineToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                        CGContextStrokePath(context);
                        CGContextMoveToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                        CGContextSetLineWidth(context, p.lineWidth);
                    }
                    
                }
                
            }
            
        }
    }
    
    // convert the context into a CGImageRef
    
    CGImageRef image = CGBitmapContextCreateImage(context);
    UIImage* image2 = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate onStreamDrawDrawing:kStreamDrawStatusProgress blendedImage:image2 whitePaperGuid:nil imageScale:ctmX];
    });
    
}




- (void)updateDrawingWithData:(NSNotification *)n
{
    drawDataArray = [n object] ;
    
    @autoreleasepool
    {
        
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(updateDrawing)
                                                                                 object:nil];
        [queue addOperation:operation];
        
    }
    
}

- (void)_opfourInDatakGuid:(id)data
{
    drawDataArray = (NSMutableArray *)data ;
    
    if (context)
    {
        CGContextRelease(context);
    }
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate(nil,firstManager.conainerWidth,firstManager.conainerHeight,8,0,colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CFRelease(colorSpace);
    
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetAllowsAntialiasing(context, YES);
    
    if (self.bgImage)
    {
        //因为传过来的tranformY已经是进行矩阵变换后的数值，所以不需要再进行变换，但翻转后的Y轴位移是没有进行ctx scale转换的，所以要进行相应的转换，就得到 0-transformY-(ctmY-1.f)*firstManager.resourceHeight 这样的公式  至于为什么，我现在也不知道，但是结果是对的。
        CGContextDrawImage(context, CGRectMake(transformX+dertX(firstManager.conainerWidth, firstManager.resourceWidth)*ctmX  , 0-transformY-(ctmY-1.f)*firstManager.conainerHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
        /*
         if (ctmX == 1)
         {
         CGContextDrawImage(context, CGRectMake(transformX   , transformY, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
         }
         else
         {
         
         CGContextDrawImage(context, CGRectMake(transformX   , 0-transformY-(ctmY-1.f)*firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
         
         NSLog(@"the value is %@ ",[NSValue valueWithCGRect:CGRectMake(transformX   , 0-transformY-firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY)]);
         }
         */
        
        
        
    }
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, 0, 0 - firstManager.conainerHeight);
    
    
    
    CGContextTranslateCTM(context, transformX, transformY);
    CGContextScaleCTM(context, ctmX, ctmY);
    
    
    for (int i = 0; i < [drawDataArray count]; i++)
    {
        KKPartLineManager *manager = [drawDataArray objectAtIndex:i];
        
        if ([manager.tid isEqualToString:@"2"])
        {
            
            CGContextSetStrokeColorWithColor(context, [[self colorFromHexString:manager.partLine.color16] CGColor]);
            
            for (int i = 0; i < [manager.partLine.points count]; i++)
            {
                KKPoint *p = [manager.partLine.points objectAtIndex:i];
                KKPoint *pNext ;
                if (i+1 == [manager.partLine.points count])
                {
                    
                    pNext = p;
                }
                else
                {
                    pNext = [manager.partLine.points objectAtIndex:i+1];
                }
                
                if (i == 0)
                {
                    CGContextSetLineWidth(context, p.lineWidth);
                    CGContextMoveToPoint(context, p.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), p.y);
                }
                else
                {
                    CGContextAddLineToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                    CGContextStrokePath(context);
                    CGContextMoveToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                    CGContextSetLineWidth(context, p.lineWidth);
                }
                
            }
            
        }
        
    }
    
    
    CGImageRef image = CGBitmapContextCreateImage(context);
    UIImage* image2 = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate onStreamDrawDrawing:kStreamDrawStatusProgress blendedImage:image2 whitePaperGuid:nil imageScale:ctmX];//
    });
}

- (void)opfourInDatakGuid:(NSNotification *)n
{
    id data = [n object];
    
    @autoreleasepool
    {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(_opfourInDatakGuid:)
                                                                                 object:data];
        [queue addOperation:operation];
        
    }
}

- (void)_opfourOriginalGuid:(id)data
{
    
    
    drawDataArray = (NSMutableArray *)[data objectForKey:@"array"];
    
    if (context)
    {
        CGContextRelease(context);
    }
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate(nil,firstManager.conainerWidth,firstManager.conainerHeight,8,0,colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CFRelease(colorSpace);
    
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetAllowsAntialiasing(context, YES);
    
    if (self.bgImage)
    {
        //因为传过来的tranformY已经是进行矩阵变换后的数值，所以不需要再进行变换，但翻转后的Y轴位移是没有进行ctx scale转换的，所以要进行相应的转换，就得到 0-transformY-(ctmY-1.f)*firstManager.resourceHeight 这样的公式  至于为什么，我现在也不知道，但是结果是对的。
        CGContextDrawImage(context, CGRectMake(transformX+dertX(firstManager.conainerWidth, firstManager.resourceWidth)*ctmX   , 0-transformY-(ctmY-1.f)*firstManager.conainerHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
        /*
         if (ctmX == 1)
         {
         CGContextDrawImage(context, CGRectMake(transformX   , transformY, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
         }
         else
         {
         
         CGContextDrawImage(context, CGRectMake(transformX   , 0-transformY-(ctmY-1.f)*firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY), self.bgImage.CGImage);
         
         NSLog(@"the value is %@ ",[NSValue valueWithCGRect:CGRectMake(transformX   , 0-transformY-firstManager.resourceHeight, firstManager.resourceWidth*ctmX, firstManager.resourceHeight*ctmY)]);
         }
         */
        
        
        
    }
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, 0, 0 - firstManager.conainerHeight);
    
    
    
    CGContextTranslateCTM(context, transformX, transformY);
    CGContextScaleCTM(context, ctmX, ctmY);
    
    
    for (int i = 0; i < [drawDataArray count]; i++)
    {
        KKPartLineManager *manager = [drawDataArray objectAtIndex:i];
        
        if ([manager.tid isEqualToString:@"2"])
        {
            
            CGContextSetStrokeColorWithColor(context, [[self colorFromHexString:manager.partLine.color16] CGColor]);
            
            for (int i = 0; i < [manager.partLine.points count]; i++)
            {
                KKPoint *p = [manager.partLine.points objectAtIndex:i];
                KKPoint *pNext ;
                if (i+1 == [manager.partLine.points count])
                {
                    
                    pNext = p;
                }
                else
                {
                    pNext = [manager.partLine.points objectAtIndex:i+1];
                }
                
                if (i == 0)
                {
                    CGContextSetLineWidth(context, p.lineWidth);
                    CGContextMoveToPoint(context, p.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), p.y);
                }
                else
                {
                    CGContextAddLineToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                    CGContextStrokePath(context);
                    CGContextMoveToPoint(context, pNext.x+dertX(firstManager.conainerWidth, firstManager.resourceWidth), pNext.y);
                    CGContextSetLineWidth(context, p.lineWidth);
                }
                
            }
            
        }
        
    }
    
    
    CGImageRef image = CGBitmapContextCreateImage(context);
    UIImage* image2 = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate onStreamDrawDrawing:kStreamDrawStatusProgress blendedImage:image2 whitePaperGuid:[data objectForKey:@"OriginalGuid"] imageScale:ctmX];//
    });

}

- (void)opfourOriginalGuid:(NSNotification *)n
{
    id arrData = [n object];
    NSDictionary *dict = [n userInfo];
    
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:arrData,@"array",[dict objectForKey:@"OriginalGuid"],@"OriginalGuid", nil];
    
    
    @autoreleasepool
    {
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self
                                                                               selector:@selector(_opfourOriginalGuid:)
                                                                                 object:data];
        [queue addOperation:operation];
        
    }
    
}



@end
