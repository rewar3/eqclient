//
//  KKPartLineManager.h
//  ICEConnectDemo
//
//  Created by wsw on 13-12-30.
//  Copyright (c) 2013年 wsw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KKPartLine.h"
@interface KKPartLineManager : NSObject

@property (nonatomic , strong) NSString *op ; //operation  0--笔记数据  1--删除数据  2--全部清空  3--放大缩小（针对画布容器）
@property (nonatomic , strong) NSString *offset ; //offset 如果连续表示不丢失，如果不连续，那么线段丢失
@property (nonatomic , strong) NSString *tid ; //type id 表示状态，0--结束，1--开始，2--过程
@property (nonatomic , strong) NSString *rid ; //resource id GUID 表示当前背景图的GUID.
@property (nonatomic , strong) NSString *uid ; //user id = 区分用户笔迹。
@property (nonatomic , strong) NSString *tcount ;
@property (nonatomic , strong) NSString *inkFileID ;//如果之前有一部分笔迹，通过此id去中控取下来接着画
@property (nonatomic , strong) NSString *sourceFileID;
@property (nonatomic , strong) KKPartLine *partLine ;
@property (nonatomic , strong) NSMutableArray *partLineArray ;

@property (nonatomic) float ctmX ;
@property (nonatomic) float ctmY ;
@property (nonatomic) float transformX ;
@property (nonatomic) float transformY ;




@property float conainerHeight ;
@property float conainerWidth ;

@property float resourceHeight ;
@property float resourceWidth ;


@end
