//
//  KKCache.h
//  iPlay
//
//  Created by wsw on 11-3-9.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kISCACHEDUSERINFO           @"iscacheduserinfo"

@interface KKCache : NSObject 
{
    

}


@property (nonatomic , strong) NSString *HTTPIP ;
@property (nonatomic , strong) NSString *iceName ;
@property (nonatomic , strong) NSArray  *canSendDeviceArray ;
@property (nonatomic , strong) NSArray  *canPushDeviceArray ;
@property (nonatomic , strong) NSArray  *currentMainPackageDataArray ;
@property (nonatomic )         BOOL     mirrorLR ;
@property (nonatomic )         BOOL     mirrorUD ;
@property (nonatomic )         BOOL     fromMoreBack ;
@property (nonatomic )         BOOL     isConnectedICE ;
@property (nonatomic )         BOOL     isMatched;
@property (nonatomic , strong) NSString *authorizationColor ;

@property (nonatomic , strong) NSArray  *allPkgArray;

@property (nonatomic )          BOOL     isFollowing ;//跟随
@property (nonatomic )          BOOL     isForceFollowing ;//强制跟随
@property (nonatomic )          BOOL     isForceAnswer ;//强制回答

@property (nonatomic )          BOOL     lastGoIsPush ; //判断最后一个go是否为push指令，方便强制跟随时瞬间更新UI

@property (nonatomic , strong) NSDictionary *lastMainLineGoDict ;
@property (nonatomic , strong) NSDictionary *lastPushGoDict ;
@property (nonatomic , strong) NSString *currentMainLinePackageName ;

@property (nonatomic , strong) NSString *questionGuid;
@property (nonatomic , strong) NSString *questionType;


+(id)sharedCache ;


- (BOOL)isCachedUserInfo ;

- (NSString *)getDateString ;
- (NSString *)getDateStringToSSS;

@end
