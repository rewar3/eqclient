//
//  KKPartLine.h
//  ICEConnectDemo
//
//  Created by wsw on 13-12-30.
//  Copyright (c) 2013年 wsw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KKPartLine : NSObject


@property (nonatomic , strong) NSString *color16 ;

@property (nonatomic ) NSInteger index ;// 笔迹index(每一笔)

@property (nonatomic , strong) NSMutableArray *points ;

@property (nonatomic ) float thickness ;

@end
