//
//  CachedUserInfo.m
//  NewWiFiPlus
//
//  Created by wsw on 23/12/12.
//  Copyright (c) 2012年 天脉聚源传媒科技有限公司. All rights reserved.
//

#import "CachedUserInfo.h"

static CachedUserInfo *userinfo ;

@implementation CachedUserInfo


+(id)shared
{
    @synchronized(self)
    {
        if (userinfo == nil) {
            userinfo = [[CachedUserInfo alloc] init];
            
        }
    }
    
    return userinfo ;
}

+(id) allocWithZone:(NSZone *)zone{
    @synchronized(self)
    {
        if (userinfo == nil)
        {
            userinfo = [super allocWithZone:zone];
            return userinfo;
        }
    }
    return nil;
}


- (void)updateUserInfo
{
    self.tvmid = [[NSUserDefaults standardUserDefaults] objectForKey:@"tvmid"];
    self.tempid = [[NSUserDefaults standardUserDefaults] objectForKey:@"tempid"];
    self.password = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    self.realName = [[NSUserDefaults standardUserDefaults] objectForKey:@"realname"];
    self.email = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
    self.headImage = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"avatar"]];
    self.minecoverImage = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"minecover"]];
    self.token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    self.tel = [[NSUserDefaults standardUserDefaults] objectForKey:@"telephone"];
    self.sex = [[NSUserDefaults standardUserDefaults] objectForKey:@"sex"];
    self.birthday = [[NSUserDefaults standardUserDefaults] objectForKey:@"birthday"];
    self.address = [[NSUserDefaults standardUserDefaults] objectForKey:@"address"];
    self.postcode = [[NSUserDefaults standardUserDefaults] objectForKey:@"postcode"];
    self.company = [[NSUserDefaults standardUserDefaults] objectForKey:@"company"];
    
}


@end
