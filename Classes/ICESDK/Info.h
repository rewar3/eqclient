//
//  Info.h
//  eclassroom
//
//  Created by wsw on 13-4-27.
//  Copyright (c) 2013年 wsw. All rights reserved.
//

#ifndef eclassroom_Info_h
#define eclassroom_Info_h



#define spaceWidth              1024.f
#define spaceHeight             768.f

#define kDrawImageView          10000

#define kMaxImageHeight         2048
#define kMaxImageWidth          2048

#define kItemWidth              180
#define kItemHeight             135

#define kLeftPackageView        1000
#define kLeftPackageChildView   2000
#define kRightPackageChildView  3000





#define kDecelerationRate       0.5

#define KMaskViewAlpha          0.6
#define KDownloadAreaAlpha      1.f
#define KCopyViewAlpha          0.3

#define kButtonScale            1.4

#define QuestionGUID            @"questionguid"

#define headImageSize          CGSizeMake(160,160)
/*********   notice view tag    ******************/
#define kNoticeDownloadTag      10001


/*********   toast type    ******************/
#define kToastPackage          @"toast_pkg"
#define kToastICE              @"toast_cc"
#define kToastShare            @"toast_share"


/*********   notification name    ******************/
#define kAllPackageUpdate               @"allpackageupdate"
#define kDragPackageView                @"dragpackageview"
#define kDragLeftPackageViewChildren    @"dragleftpackageviewchildren"
#define kTapLeftPackageViewChildren     @"tapleftpackageviewchildren"
#define kDragRightPackageViewChildren   @"dragrightpackageviewchildren"
#define kTapRightPackageViewChildren    @"taprightpackageviewchildren"
#define kForceFollowPower               @"forcefollowpower"
#define kForceAnswer                    @"forceanswer"
#define kMainLineGoCommand              @"mainlinegocommand"
#define kPushGoCommand                  @"pushgocommand"
#define kICEFounded                     @"icefounded"
#define kICELoginFinished               @"iceloginfinished"
#define kICELoginError                  @"iceloginerror"
#define kDownloadPackage                @"downloadpackage"
#define kDownloadchildview              @"downloadchildview"
#define kICESearchTimeout               @"icesearchtimeout"
#define kICEStartSearching              @"icestartsearching"
#define kApplicationHomePowerActived    @"applicationhomepoweractived"
#define kNeedCache                      @"needCache"
/*********   config ***************************/
#define kAPPNAME                        @"WiFiPlus"

#define kUpdateProductName              @"eq"

#define EQLibraryName                   @"EQ"

/*********   item property ********************/
#define PackageName                 @"packname"
#define FileType                    @"file_type"
#define FileType_IMAGE              @"IMAGE"
#define FileType_VIDEO              @"VIDEO"
#define GUID                        @"guid"
#define Owner_TVMID                 @"owner_tvmid"
#define ResourceGroupID             @"res_groupid"
#define ThumbGuid                   @"thumb_guid"
#define Description                 @"desc"
#define ExtendName                  @"ext"
#define FileNameWithExtend          @"filename"
#define Height                      @"height"
#define Tag                         @"tag"
#define FileTitle                   @"title"
#define Weight                      @"weight"
#define Width                       @"width"


/*********   request keys & values             **************/

#define TVMID                       @"tvmid"
#define PackageNameSearchFor        @"in_res_pack"
#define OrderBY                     @"order_column"
#define ID                          @"id"

/**********    particular package name         **************/
#define Communication               @"交流"
#define ASC_OR_DESC                 @"is_asc"
#define ASC                         @"1"
#define DESC                        @"0"
#define TeacherResource             @"板书"

/************  Request Image width & height    ***************/
#define WaterflowWidth              @"200"
#define PackageCoverWidth           @"200"
#define PackageCoverHeight          @"150"
#define MainLineItemWidth           @"512"

/************  command key    ***************/
#define ACTION                      @"action"
#define POWER                       @"power"
#define ANSWER                      @"answer"
#define FOLLOW                      @"follow"
#define OFF                         @"off"
#define ON                          @"on"
/************  setting.bundle key    ***************/
#define SettingVersion              @"version_preference"
#define SettingPassword             @"password_preference"
#define SettingGuide                @"guide_preference"
/************  check new version key    ***************/
#define Status                      @"status"
#define Device                      @"device"
#define Product                     @"product"
#define Versionlist                 @"versionlist"
#define Addr                        @"addr"
#define Describe                    @"describe"
#define Isrollback                  @"isrollback"
#define Rule                        @"rule"
#define Version                     @"version"


#define kCheckNewVersionAlertView   1000


/************  answer type    ***************/
#define kAnswerChooseType           0
#define kAnswerDrawType             1

typedef enum
{
    FolderType_PPT                  = 1,
    FolderType_PDF                  = 2,
    FolderType_WORD                 = 3,
    FolderType_VIDEO                = 4,
    FolderType_IMAGE                = 5,
    FolderType_MIX                  = 6,
    FolderType_COMMUNICATION        = 7,
    FolderType_MEETINGRECORD        = 8,
    FolderType_OTHER                = 9,
} FolderType;
//AlertView类型
typedef enum
{
    AlertTypeNoInternet = 100,
    AlertTypeYesInternet = 200,
    AlertTypeTimeOut = 300,
    AlertTypePassword = 400,
    AlertTypeRegistry = 500,
    AlertTypeConnectionTimeOut = 600 ,
    AlertTypeLoginError = 700,
    AlertTypeSearchICETimeOut = 800
} AlertType ;

typedef enum
{
    RequestType_PackageChildren = 100,
    RequestType_AllPackage = 200 ,
    RequestType_Cover = 300 ,
    RequestType_UploadUserInfo = 400,
    RequestType_CreateFolder = 500 ,
    RequestType_UploadChildren = 600,
    RequestType_CCCoverSmall = 700,
    RequestType_CCCoverMedium = 800,
    RequestType_PKGCover    = 900,
    RequestType_SandBox    = 1000,
    RequestType_FollowData    = 1100
} RequestType ;

typedef enum
{
    kSheetTypeFollow = 100,
    kSheetTypeUpload = 200,
    kSheetDownload   = 300,
    kSheetPush       = 400,
    kSheetPushDevice = 500,
    
    kSheetTypeDelete = 600,
    kSheetTypeYellow = 700,
    kSheetTypeRed = 800,
    kSheetTypePKGSync = 900,
    kSheetTypeSelectResource = 1000
} kSheetType;

typedef enum  {
    kConfirmUploadType      = 1000,
    kConfirmDeleteType      = 2000,
    kConfirmDownloadType    = 3000,
    kConfirmPushType        = 4000
} ConfirmType;

typedef enum  {
    kCancelDownloadType    = 1000,
    kCancelSyncType        = 2000
} CancelType;

typedef enum
{
    kFlagForceFollow = 100 ,
    kFlagForceAnswer = 200 ,
    kFlagFollow      = 300
} FlagType;


#define OP              @"OP"
#define Offset          @"Offset"
#define TCount          @"TCount"
#define TID             @"TID"
#define UID             @"UID"
#define DATA            @"Data"
#define Color           @"Color"
#define Index           @"Index"
#define Points          @"Points"
#define Pressure        @"P"
#define PX               @"X"
#define PY               @"Y"
#define Thickness       @"Thickness"
#define ConainerHeight  @"ConainerHeight"
#define ConainerWidth   @"ConainerWidth"
#define ResourceHeight      @"Height"
#define ResourceWidth       @"Width"
#define InkFileID       @"InkFileID"
#define SourceFileID    @"SourceFileID"

#define DealStackData   @"dealstackdata"
#define RID             @"RID"

/************  answer type    ***************/
#define kAnswerChooseType           0
#define kAnswerDrawType             1


#define kImageWidth 1024
#define kImageHight 768
#define kPageWidth 1024

#define kBarHeight 60

#define kMaxPageCount 5

#define FileType_TEXT               @"TEXT"

/************  mode action    ***************/

#define kOpenPaintMode @"kOpenPaintMode"
#define kClosePaintMode @"kClosePaintMode"
#define kOpenTextMode @"kOpenTextMode"
#define kCloseTextMode @"kCloseTextMode"
#define kCloseCameraMode @"kCloseCameraMode"
#define kOpenAnswerMode @"kOpenAnswerMode"
#define kImageDidLoad @"kImageDidLoad"
#define kOpenStreamMode @"kOpenStreamMode"
#define kCloseStreamMode @"kCloseStreamMode"

#define kBubbleViewShow @"kBubbleViewShow"
#define kBubbleViewHide @"kBubbleViewHide"

#define kBubbleViewDidShow @"kBubbleViewDidShow"
#define kBubbleViewDidHide @"kBubbleViewDidHide"

#define kBubbleViewSelect @"kBubbleViewSelect"

#define kHistoryUsers @"kHistoryUsers"
#define kPainterLineColor @"kPainterLineColor"
#define kPainterLineWidth @"kPainterLineWidth"

#define kPassword @"kPassword"
#define kPasswordError @"kPasswordError"

#define kImageRequestDidLoad @"kImageRequestDidLoad"
#define kPageImageReload @"kPageImageReload"

#define kInkImageWillLoad @"kInkImageWillLoad"
#define kOriginalImageDidLoad @"kOriginalImageDidLoad"
#define kSaveInkImage @"kSaveInkImage"
#define kSaveStreamImage @"kSaveStreamImage"

#endif
