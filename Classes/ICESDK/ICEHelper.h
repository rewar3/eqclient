//
//  ICEHelper.h
//  NewWiFiPlus
//
//  Created by wsw on 17/12/12.
//  Copyright (c) 2012年 天脉聚源传媒科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEManager.h"
#import "Info.h"
#import "KKCache.h"
#import "CachedUserInfo.h"
@interface ICEHelper : NSObject <ICEManagerDelegate>
{
    ICEManager *iceManager_ ;
    
    ICELoginEntity       *loginEntity ;
    
    NSArray              *searchResultICELoginEntityArray ;
}

@property (nonatomic , strong) ICEManager *iceManager ;

+ (id)shared ;
- (void)initManager ;

BOOL isCheckNull(id temp) ;

- (void)doConnectICE;

@end
