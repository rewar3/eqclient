//
//  ICEConnectorDao.m
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "ICEConnectorEntity.h"
#import "ICEConnectorDao.h"
#import "SocketHelper.h"
#include "pthread.h"

@implementation ICEConnectorDao
@synthesize connectICE = connectICE_;

static BonjourHelper* bh_=nil;
static UdpHelper* uh_=nil;

+(BonjourHelper*)bh//bonjourHelper
{
    if(bh_ == nil){
        bh_ = [[BonjourHelper alloc]init];
    }

    return bh_;
}

+(UdpHelper*)uh//udpHelper
{
    if (uh_ == nil) {
        uh_ = [[UdpHelper alloc]init];
    }

    return uh_;
}

+(void)udpStartListen
{
    [[ICEConnectorDao uh]startScan];
}

+(void)udpStopListen
{
    [[ICEConnectorDao uh] stopScan];
}

-(id)initWithServerName:(ICELoginEntity *)loginICE//连接
{
    if(self = [super init]){
        connICERole = nil;
        regUserInfo = nil;
#ifdef TCP_CONNECTION
        sh = nil;
#else
        uth = NULL;
#endif
        pthread_mutex_init(&sendMutex, NULL);
        connectHostName = nil;//[[NSString alloc]init];
        connectICE_ = loginICE;
    }

    return self;
}

-(void)dealloc
{
    pthread_mutex_destroy(&sendMutex);
#ifdef TCP_CONNECTION
    [sh disconnect];
#else
    uth->disconnect();
#endif

}

-(ICELoginEntity*)getLoginICE//得到连接的名字
{
    return connectICE_;
}

-(ICELoginEntity*)getConnectServername;//得到连接名
{
    return connectICE_;
}
-(NSString*)getConnectHostname//得到主机名
{
    return connectHostName;
}
-(BOOL)connectByHostname:(NSString *)hostname//通过一个 hostName 进行连接
{
    if(hostname == nil ||
       [hostname length] == 0
    ){
        return NO;
    }

#ifdef TCP_CONNECTION
    sh = [[SocketHelper alloc]initWithIP:hostname port:[ICEConnectorEntity SERVER_PORT]];
    BOOL isAlive =[sh isConnected];
#else
    uth = new UdtHelper([hostname UTF8String], [ICEConnectorEntity SERVER_PORT_UDT]);
    BOOL isAlive = uth->isConnected();
#endif

    if(isAlive){
        connectHostName = hostname;
    }

    return isAlive;
}

-(NSString*)getSocketString//从 socket 得到字符串
{
    if(![self isConnected]){
        @throw [[NSException alloc]initWithName:@"Exception"
                                         reason:@"没有连接，怎么接收 socket"
                                       userInfo:nil
                ];
    }

    NSString *sockStr;
#ifdef TCP_CONNECTION
    sockStr = [sh recv];
#else
    sockStr = uth->recv();
    if (sockStrP == NULL) {
        sockStr = @"";
    }else{
        sockStr = [[NSString alloc]initWithCString:sockStrP encoding:NSUTF8StringEncoding];
    }
#endif
    
    return sockStr;
}

-(void)disconnect//关闭连接
{
#ifdef TCP_CONNECTION
    [sh disconnect];
#else
    uth->disconnect();
#endif
}

-(BOOL)setRawStr:(NSString*)oneStr//送出字符给 socket
{
    int mutexS;
    BOOL sendS;
#ifdef TCP_CONNECTION
    if (![sh isConnected]) {
#else
    if(!uth->isConnected()){
#endif

#ifdef ICE_DEBUG
        NSLog(@"送出字符出问题，没连接");
#endif
        return NO;
    }

    mutexS = pthread_mutex_lock(&sendMutex);
    if(mutexS != 0){
        pthread_mutex_unlock(&sendMutex);
        return NO;
    }

    @try {
#ifdef TCP_CONNECTION
        sendS = [sh send:oneStr];
#else
        sendS = uth->send([oneStr UTF8String]); //[sh send:oneStr];
#endif

    }
    @catch (NSException *exception) {
        pthread_mutex_unlock(&sendMutex);
        @throw [[NSException alloc]initWithName:[exception name] reason:[exception reason] userInfo:[exception userInfo]];
    }

    
    pthread_mutex_unlock(&sendMutex);
    return sendS;
}

-(NSString*)pickupOneHostnameByServername:(NSUInteger)pickType//根据服务名，得到一个主机名
{
    NSString *hostname = nil;
    NSMutableArray *bonjourList = [[ICEConnectorDao uh]getUdpNameList];

    for(NSUInteger i=0;i<[bonjourList count];i++){
        UdpInfoEntity *bie = [bonjourList objectAtIndex:i];
        //NSLog(@"displayName:%@\tconnectSevername:%@\nbie's role:%d\tpicType:%d", [bie displayName], connectServerName, [bie role], pickType);
        if((NSOrderedSame == [[bie displayName]caseInsensitiveCompare:[connectICE_ connectICEName]]) &&
            [bie role] == pickType
        ){
            hostname = [bie ip];
        }
    }

    return hostname;
}

-(BOOL)isConnected//是否连接
{
    BOOL isConn = false;

    @try {
#ifdef TCP_CONNECTION
        isConn = [sh isConnected]; //[sh isConnected];
#else
        isConn = uth->isConnected();
#endif

    }
    @catch (NSException *exception) {
#ifdef ICE_DEBUG
        NSLog(@"ICEConnectorDao::isConnect->%@", [exception name]);
#endif
    }

    return isConn;
}

/* 
 work for Bonjour

+(NSMutableArray*)getAllICENameArray//得到所有中控服务列表
{
    NSMutableArray *bonjourList = [[ICEConnectorDao bh]bonjourNameList];
    NSMutableDictionary *allICEName = [[NSMutableDictionary alloc]init];
    NSMutableArray *allICEString ;

    for (NSUInteger i=0; i<[bonjourList count]; i++) {
        BonjourInfoEntity *bie = [bonjourList objectAtIndex:i];

        if ([bie role] != [BonjourInfoRoleEntity RAW] &&
            nil == [allICEName valueForKey:[bie displayName]]
        ){
            [allICEName setValue:[bie displayName] forKey:[bie displayName]];
        }
    }

    allICEString = [[NSMutableArray alloc]initWithCapacity:[allICEName count]];
    NSEnumerator *allICEValueEnum = [allICEName objectEnumerator];
    NSString *oneIceName;
    while(oneIceName=[allICEValueEnum nextObject]){
        [allICEString addObject:oneIceName];
    }

    //[allICEName release];
    return allICEString;
}
*/

+(NSMutableArray*)getAllICENameArray//得到所有中控服务列表
{
    NSMutableArray *allICEString = [[NSMutableArray alloc]init];
    NSMutableArray *udpList = [[ICEConnectorDao uh]getUdpNameList];
    NSMutableDictionary *allICEName = [[NSMutableDictionary alloc]init];


    @try {

        for (NSUInteger i=0; i<[udpList count]; i++) {

            UdpInfoEntity *bie = [udpList objectAtIndex:i];

            if([bie isEqual:nil]){
                break;
            }


            if ([bie role] != [UdpInfoRoleEntity RAW_UDP] &&
                nil == [allICEName valueForKey:[bie displayName]]
            ){
                [allICEName setValue:[bie loginMethod] forKey:[bie displayName]];

                ICELoginEntity *oneICELogin = [[ICELoginEntity alloc]init:[bie displayName]
                                                           withMethod:[bie loginMethod]];
                //NSLog(@"name:%@\tlogin:%@", [oneICELogin connectICEName], [oneICELogin loginMethod]);
                [allICEString insertObject:oneICELogin atIndex:0];     //addObject:oneICELogin];
            }

        }

    }
    @catch (NSException *exception) {
    }
/*
    allICEString = [[NSMutableArray alloc]initWithCapacity:[allICEName count]];
    NSEnumerator *allICEKeyEnum = [allICEName keyEnumerator];
    NSString *oneIceName;
    while(oneIceName=[allICEKeyEnum nextObject]){
        NSString *oneLoginMethod = [allICEName valueForKey:oneIceName];
        ICELoginEntity *oneICELogin = [[ICELoginEntity alloc]init:oneIceName withMethod:oneLoginMethod];
        //NSLog(@"name:%@\tlogin:%@", [oneICELogin connectICEName], [oneICELogin loginMethod]);
        [allICEString addObject:oneICELogin];
    }
*/
    //[allICEName release];
    return allICEString;
}

-(void)getRecvBroadcaster:(NSThread*)rrthread//轮训得到 UDP 广播
{
    //[uh_ roundFindICE:rrthread];
}

-(ICERoleEntity*)getConnectICERole//得到连接的中控角色
{
    return connICERole;
}
/*
only work for bonjour
+(NSMutableArray*)getAllICEServerArray//得到所有连接主机名
{
    NSMutableArray *bonjourList = [[ICEConnectorDao bh]bonjourNameList];
    NSMutableArray *serverName = [[NSMutableArray alloc]initWithCapacity:[bonjourList count]];

    NSEnumerator *allICEValueEnum = [bonjourList objectEnumerator];
    BonjourInfoEntity *oneICEName;
    while(oneICEName = [allICEValueEnum nextObject]){
        [serverName addObject:[oneICEName fullName]];
    }

    return serverName;
}*/

+(NSMutableArray*)getAllICEServerArray//得到所有连接主机名
{
    NSMutableArray *udpList = [[ICEConnectorDao uh]getUdpNameList];
    NSMutableArray *serverName = [[NSMutableArray alloc]initWithCapacity:[udpList count]];

    NSEnumerator *allICEValueEnum = [udpList objectEnumerator];
    UdpInfoEntity *oneICEName;
    while(oneICEName = [allICEValueEnum nextObject]){
        [serverName addObject:[oneICEName ip]];
    }

    return serverName;
}
@end
