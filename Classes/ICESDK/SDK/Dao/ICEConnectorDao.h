//
//  ICEConnectorDao.h
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"
#ifdef TCP_CONNECTION
#import "SocketHelper.h"
#else
#import "UdtHelper.h"
#endif
#import "BonjourHelper.h"
#import "ICERoleEntity.h"
#import "UserInfoEntity.h"
#import "UdpHelper.h"
#import "ICELoginEntity.h"

@interface ICEConnectorDao : NSObject
{
#ifdef TCP_CONNECTION
    SocketHelper *sh;//Socket 的帮助
#else
    UdtHelper *uth;
#endif

    ICERoleEntity *connICERole;//注册到的中控信息
    UserInfoEntity *regUserInfo;//注册的用户信息
    ICELoginEntity *connectICE_;//连接到服务名字
    NSString *connectHostName;//连接的主机名
    pthread_mutex_t sendMutex;//送出互斥
#ifdef TCP_CONNECTION
#else
    const char *sockStrP;
#endif
}

@property(atomic, strong) ICELoginEntity *connectICE;
+(BonjourHelper*)bh;//bonjourHelper
+(UdpHelper*)uh;//UdpHelper
//-(void)getRecvBroadcaster:(NSThread*)rrthread;//轮训得到 UDP 广播
-(id)initWithServerName:(ICELoginEntity *)serverName;//连接
-(ICELoginEntity*)getConnectServername;//得到连接名
-(NSString*)getConnectHostname;//得到主机名
-(BOOL)connectByHostname:(NSString *)hostname;//通过一个 hostName 进行连接
-(NSString*)getSocketString;//从 socket 得到字符串
-(void)disconnect;//关闭连接
-(BOOL)setRawStr:(NSString*)oneStr;//送出字符给 socket
-(NSString*)pickupOneHostnameByServername:(NSUInteger)pickType;//根据服务名，得到一个主机名
-(BOOL)isConnected;//是否连接
+(NSMutableArray*)getAllICENameArray;//得到所有中控服务列表
-(ICERoleEntity*)getConnectICERole;//得到连接的中控角色
+(NSMutableArray*)getAllICEServerArray;//得到所有连接主机名
-(ICELoginEntity*)getLoginICE;//得到连接的名字
+(void)udpStartListen;//开始 UDP 监听
+(void)udpStopListen;//结束 UDP 监听
@end
