 //
//  ICESDK.m
//  iOS
//
//  Created by hanshu on 10/16/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//
/**
    changeLog:
                2012-12-04  1、如果设备获得权限，则统一返回管理员权限
                            2、增加粘合，宏和拓展三个指令
    
*/
#import "ICESDK.h"

@implementation ICESDK

@synthesize delegate=delegate_;

static NSMutableDictionary *world_ = nil;//单例
static pthread_mutex_t *mutexStarOver = NULL;//重新开始锁住


-(id)init: (ICEConnectorService *)connICS with: (ICECommandParseService*) connICPS//连接
{
    if (self = [super init]) {
        oneICS = connICS;
        oneICPS = connICPS;

        threadCmdParser = [[NSThread alloc]initWithTarget:self selector:@selector(recvRawFromICEToCmdParser) object:nil];
        [threadCmdParser setName:@"命令解析线程"];
        [threadCmdParser start];
    }
    //oneIFS

    return self;
}

+(void)udpStartListen
{
    [ICEConnectorService udpStartListen];
}

+(void)udpStopListen
{
    [ICEConnectorService udpStopListen];
}

-(NSString*)getConectHostname
{
    return [oneICS getConectHostname];
}

-(void)dealloc{
    /*
    oneICS = nil;
    oneICPS = nil;
    threadCmdParser = nil;
    threadSayHello = nil;
    */
}

-(void)doingPeriod//做周期律
{

    threadSayHello = [[NSThread alloc]initWithTarget:self selector:@selector(periodSayHello) object:nil];
    [threadSayHello setName:@"周期心跳线程"];
    [threadSayHello start];
}

+(NSMutableDictionary*)world
{
    if (world_ == nil) {
        world_ = [[NSMutableDictionary alloc]init];
    }
    return world_;
}

+(void)setWorld:(NSMutableDictionary *)world
{
    world_ = world;
}

-(void)isOver//结束
{
    @try{
        if([[ICESDK world]objectForKey:[oneICS getLoginICE]] != nil){

            //ICESDK *oneSDK = [[ICESDK world]objectForKey:[oneICS getLoginICE]];
            [[ICESDK world]removeObjectForKey:[oneICS getLoginICE]];
        }else{
#ifdef ICE_DEBUG
            NSLog(@"断开未清除");
#endif
        }

        [threadSayHello cancel];
        [threadCmdParser cancel];

        [oneICS disconnect];
    }@catch (NSException *e) {
#ifdef ICE_DEBUG
        NSLog(@"切了个糕出了个错:%@", [e reason]);
#endif
    }

}

-(void) starOver
{
    ICELoginEntity *registerICELogin = [oneICS getLoginICE];
//    pthread_mutex_lock(mutexStarOver);
//    @try {
//        if([[ICESDK world]objectForKey:registerICELogin] != nil){
//
//            //ICESDK *oneSDK = [[ICESDK world]objectForKey:[oneICS getLoginICE]];
//            [[ICESDK world]removeObjectForKey:[oneICS getLoginICE]];
//        }else{
//#ifdef ICE_DEBUG
//            NSLog(@"断开重来清了除:%@", world_);
//#endif
//        }
//
//        //[oneICS disconnect];
//    }
//    @catch (NSException *e) {
//#ifdef ICE_DEBUG
//        NSLog(@"切了个糕出了个错：%@", [e reason]);
//#endif
//    }

    //[ICEConnectorService udpStartListen];

    BOOL isSucc = NO;
    [delegate_ connectionWillRetry];
    for(int i=0;i < 3 ;i++)
    {
        @try {
            
            if (![self connectAndRegister:registerICELogin]) {
#ifdef ICE_DEBUG
                NSLog(@"呀没找到，没连接");
#endif
            }else{
#ifdef ICE_DEBUG
                NSLog(@"找到了，哈哈哈哈");
#endif
                [delegate_ connectionRetrySuccess];
                isSucc = YES;
                break;
            }
        }
        @catch (NSException *e) {
#ifdef ICE_DEBUG
            NSLog(@"出了个错误:%@\t%@\t%@", [e name], [e reason], [e callStackSymbols]);
#endif
            pthread_mutex_unlock(mutexStarOver);
        }

        [NSThread sleepForTimeInterval:0.5];
    }

    if (!isSucc) {
        [delegate_ connectionRetryFailed];
    }

    //    pthread_mutex_unlock(mutexStarOver);
    //[ICEConnectorService udpStopListen];
}

-(void)recvRawFromICEToCmdParser//内部线程方法，用来 xxoo 命令
{
    [NSThread sleepForTimeInterval:1];
    
    while (![threadCmdParser isCancelled]) {
      @autoreleasepool{
        @try {
            NSString *rawCntFromSocket = [oneICS recvSockRawCnt];

            if (rawCntFromSocket == nil ||
                [rawCntFromSocket length] == 0
            ) {
#ifdef ICE_DEBUG
                NSLog(@"收到内部轮训空");
#endif
                //[self starOver];
                [NSThread sleepForTimeInterval:1];
                continue;
            }

#ifdef ICE_DEBUG
            //NSLog(@"内部接收轮训:%@", rawCntFromSocket);
#endif
            
            [oneICPS appendRawStr:rawCntFromSocket];
            [oneICPS fromRawToArray];
        }
        @catch (NSException *e) {
#ifdef ICE_DEBUG
            NSLog(@"收到抛出的异常:name:%@ reson:%@ call:%@", [e name], [e reason], [e callStackSymbols]);
#endif
            //[oneICS disconnect];
            [NSThread sleepForTimeInterval:1];
            //[self performSelectorInBackground:@selector(starOver) withObject:nil];
            //break;
        }
      }
    }

#ifdef ICE_DEBUG
    NSLog(@"退出命令解析");
#endif
    

    //[threadCmdParser cancel];
    [NSThread exit];
}
-(void)periodSayHello//周期性的和中控心跳，如果错误，则重连
{
    CommandEntity *echoCmdEntity = [[CommandEntity alloc]init];
    [echoCmdEntity setCmdTYPE:[CommandTypeEntity ECHO]];

    while (![threadSayHello isCancelled]) {
        @try {
            [echoCmdEntity setClientVaildcode:[CommandEntity createUUID]];
            if ([oneICS isConnected]) {
                StatusEntity *sendStatus = [self sendCommmand:echoCmdEntity timeout:5 allowNone:NO];
                if (![[sendStatus status]isEqualToString:[StatusEntity OK]]) {
                    sendStatus = [self sendCommmand:echoCmdEntity timeout:5 allowNone:NO];
                    if (![[sendStatus status]isEqualToString:[StatusEntity OK]]) {
#ifdef ICE_DEBUG
                        NSLog(@"心跳出问题，重连");
#endif
                        [NSThread sleepForTimeInterval:1];
                        [self starOver];
                        continue;
                    }

                    //[oneICS disconnect];
                }
//
//                if([sendStatus status] == [StatusEntity UNDELIVER]){
//#ifdef ICE_DEBUG
//                    NSLog(@"心跳重新连接123");
//#endif
//                    //[oneICS reConnect];
//                    [self starOver];
//                    //[NSThread sleepForTimeInterval:1];
//                    continue;
//                    //break;
//                }
//
//                if ([sendStatus status] == [StatusEntity UNPASER]) {
//#ifdef ICE_DEBUG
//                    NSLog(@"心跳解析出错");
//#endif
//                    [self starOver];
//                    //[NSThread sleepForTimeInterval:1];
//                    continue;
//                    //break
//                }
//
//                if([sendStatus status] == [StatusEntity NNULL]){
//#ifdef ICE_DEBUG
//                    NSLog(@"心跳空");
//#endif
//                    [self starOver];
//                    //[NSThread sleepForTimeInterval:1];
//                    continue;
//                }
            }else{
#ifdef ICE_DEBUG
                NSLog(@"心跳没有连接，连接连接");
#endif 
                //[oneICS connect];
                //oneIFS
                [self starOver];
                //[NSThread sleepForTimeInterval:1];
                continue;
            }

#ifdef ICE_DEBUG
            NSLog(@"心跳好");
#endif
            [NSThread sleepForTimeInterval:5];

        }
        @catch (NSException *e) {
#ifdef ICE_DEBUG
            NSLog(@"心跳里的 name:%@ reson:%@:%@", [e name], [e reason], [e callStackSymbols]);
#endif
            [self starOver];
            //[NSThread sleepForTimeInterval:1];
            continue;
        }
    }

#ifdef ICE_DEBUG
    NSLog(@"退出心跳发送");
#endif
    //[self starOver];
    //[threadSayHello cancel];
    [NSThread exit];
}

-(StatusEntity*)sendCommmand: (CommandEntity *)sendCmd//送出一个命令
{
    StatusEntity *oneSE = [[StatusEntity alloc]init];
    [oneSE setStatus:[StatusEntity UNDELIVER]];
    
#ifdef ICE_DEBUG
    NSLog(@"送出命令1:%@\tbody:%@", [sendCmd CmdTYPE], [sendCmd body]);
#endif
    @try {
        //NSUInteger i = 0;

        if (![[NSThread currentThread]isCancelled]
             &&[oneICS sendSockRawCnt:[sendCmd toRaw]]
             //&& i<3
        ) {
            //while (i<3) {
                StatusEntity *getOneSE = [oneICPS getOneStatusByCMD:sendCmd timeoutSecond:0 allowNone:NO];
                if (getOneSE != nil) {
                    oneSE = getOneSE;
                }

//                if ([[oneSE cmd]compare:[CommandTypeEntity NONE]] != NSOrderedSame ||
//                    [[oneSE status]compare:[StatusEntity NNULL]] != NSOrderedSame
//                ){
//                    break;
//                }

#ifdef ICE_DEBUG
                //NSLog(@"重试接受命令%@=%d:", [sendCmd CmdTYPE], i);
#endif
                //i++;
                //[NSThread sleepForTimeInterval:0.3];

            //}
        }
//
//        if (i < 3) {
//        }else{
//            [[NSThread currentThread]cancel];
//            [NSThread exit];
//        }
    }
    @catch (NSException *e) {
#ifdef ICE_DEBUG
        NSLog(@"送出线程已中断,送出命令:%@, name:%@ reson:%@", [sendCmd CmdTYPE], [e name], [e reason]);
#endif
    }

    return oneSE;
}

-(StatusEntity*)sendCommmand:(CommandEntity *)sendCmd timeout:(NSUInteger)timeout allowNone:(BOOL)allowNone;//送出一个命令
{
    StatusEntity *oneSE = [[StatusEntity alloc]init];
    [oneSE setStatus:[StatusEntity UNDELIVER]];

#ifdef ICE_DEBUG
    NSLog(@"送出命令2:%@\tbody:%@", [sendCmd CmdTYPE], [sendCmd body]);
#endif
    NSUInteger reTryNum = 1;

    @try {
        if ([oneICS sendSockRawCnt:[sendCmd toRaw]]) {
            while (YES) {
                StatusEntity *getOneSE = [oneICPS getOneStatusByCMD:sendCmd timeoutSecond:timeout allowNone:allowNone];
                if(getOneSE != nil){
                    oneSE = getOneSE;
                }

                if([[oneSE cmd]compare:[CommandTypeEntity NONE]] != NSOrderedSame ||
                   [[oneSE status]compare:[StatusEntity NNULL]] != NSOrderedSame ||
                   [[oneSE status]isEqualToString:[StatusEntity UNDELIVER]]
                ){
                    break;
                }

#ifdef ICE_DEBUG
                NSLog(@"重试接受命令2:%d", reTryNum);
#endif
                reTryNum--;
                if (reTryNum <= 0) {
                    break;
                }

                [NSThread sleepForTimeInterval:0.1];
            }
        }else{
#ifdef ICE_DEBUG
            NSLog(@"发出了错，出了命令出错:%@", [sendCmd CmdTYPE]);
#endif
            @throw [[NSException alloc]initWithName:@"Exception" reason:@"送出线程已经中断2" userInfo:nil];
        }
    }
    @catch (NSException *e) {
#ifdef ICE_DEBUG
        NSLog(@"送出线程已经中断2，送出命令出错:%@", [sendCmd CmdTYPE]);
#endif
        @throw [[NSException alloc]initWithName:@"Exception" reason:@"送出线程已经中断2" userInfo:nil];
    }

    return oneSE;
}

-(CommandEntity*)recvCommand//收到一个命令
{
    CommandEntity *oneCmdEntity = [oneICPS getOneCmd:threadSayHello];
    return oneCmdEntity;
}

+(NSMutableArray*)getAllICENameArray//得到所有中控服务列表
{
     NSMutableArray *allICENameArray = [ICEConnectorService getAllICENameArray];
    return allICENameArray;
}
+(NSArray*)getAllICEServerArray//得到所有连接主机名
{
    NSArray *serverName = [ICEConnectorService getAllICEServerArray];
    return serverName;
}

-(void)stayAlone//关闭连接
{
    [oneICS disconnect];
    [threadCmdParser cancel];
    [threadSayHello cancel];
    
}

-(StatusEntity*)registerToICE3:(ICELoginEntity *) loginICE withUserInfo:(UserInfoEntity*)registerUserEntity timeout:(NSUInteger)timeout//注册到 ICE
{
    CommandEntity *registerCmd = [[CommandEntity alloc]init];
    [registerCmd setCmdTYPE:[CommandTypeEntity REGISTER]];

    NSString *body;

    if ([[registerUserEntity type]isEqualToString:[UserTypeEntity USER]]) {
        body = [[NSString alloc]initWithFormat:@"%@,%@,%@,%@",[loginICE loginMethod], [registerUserEntity id], [registerUserEntity type], [loginICE password]];
    }else{
        body =[[NSString alloc]initWithFormat:@"%@,%@,%@,%@",[ICELoginMethodEntity REGISTRY] , [registerUserEntity id], [registerUserEntity type], [loginICE password]];
    }
    
    [registerCmd setBody:body];

    StatusEntity *regStatus = [self sendCommmand:registerCmd timeout:timeout allowNone:NO];
    if ([[regStatus status]compare:[StatusEntity OK]] == NSOrderedSame ||
        [[regStatus status]isEqualToString:[StatusEntity ALREAD_ONLINE]]
    ){
        registerUserInfo = registerUserEntity;
        //registerICELogin = loginICE;
    }

    return regStatus;
}
/*
-(NSArray*)getAllPublicPackName//得到所有公开包的信息
{
    NSLog(@"TODO");
}

-(NSArray*)uploadLocalFile:(NSMutableDictionary *)postData uploadFile:(NSArray*)uploadFile//上传文件
{
    NSLog(@"TODO");
}
-(NSArray*)searchFile:(SearchFileEntity*)searchFileCond//搜索文件
{
    NSLog(@"TODO");
}

-(NSString*)getUrlByFileDetail:(SearchFileDetailStatusEntity*) oneSFDSE//根据一个文件细节，得到 URL
{
    NSLog(@"TODO");
}

-(NSString*)getThumbURLByFileDetail:(SearchFileDetailStatusEntity*)oneSFDSE with:(NSUInteger)width height:(NSUInteger)height//得到缩略图
{
    NSLog(@"TODO");
}
-(NSString*)getThumbURLByFileDetail:(SearchFileDetailStatusEntity*)oneSFDSE with:(NSUInteger)width height:(NSUInteger)height thumbMethod:(fileThumbMethod)fileTM//得 到缩略图
{
    NSLog(@"TODO");
}
-(NSString*)getUrlByUri: (NSString*)uri//从一个 URI 的到 URL
{
    NSLog(@"TODO");
}
*/
-(NSString*)getUserPower{
    if([[registerUserInfo type]isEqualToString:[UserTypeEntity DEVICE]])
    {
        return [GroupTypeEntity ADMINISTRATOR];
    }else{
        return [UserInfoEntity groupId];
    }
}

+(BOOL)trySharedICE //踹一下锁
{
    if(0 == pthread_mutex_trylock(mutexStarOver)){
        return YES;
    }else{
        return NO;
    }
}

-(BOOL)connectAndRegister:(ICELoginEntity*)loginICE{
    pthread_mutex_lock(mutexStarOver);
    //[NSThread sleepForTimeInterval:1];
    //if (![oneICS isConnected]) {
        [oneICS reConnect];
        if (![oneICS isConnected]) {
            pthread_mutex_unlock(mutexStarOver);
            return NO;
        }

        StatusEntity *regStatus;// = [self registerToICE3:loginICE withUserInfo:registerUserInfo];
        @try {
            regStatus = [self registerToICE3:loginICE withUserInfo:registerUserInfo timeout:3];
            pthread_mutex_unlock(mutexStarOver);

            if (regStatus != nil){
                if ([[regStatus status]isEqualToString:[StatusEntity OK]] ||
                    [[regStatus status]isEqualToString:[StatusEntity ALREAD_ONLINE]]
                ){
                    return YES;
                }else{
                    return NO;
                }
            }else{
                return NO;
            }
        }
        @catch (NSException *e)
        {
           pthread_mutex_unlock(mutexStarOver);
#ifdef ICE_DEBUG
            NSLog(@"connectAndRegister:%@\t%@",[e name],[e reason]);
#endif
            return NO;
        }

    //}

    pthread_mutex_unlock(mutexStarOver);
    return YES;
}

+(ICESDK*)sharedICE :(ICELoginEntity*)loginICE userInfo:(UserInfoEntity*)userEntity//连接到一个中控上面去
{
    ICESDK *oneSDK = nil;
    NSMutableDictionary *myWorld = [ICESDK world];

    if (mutexStarOver == NULL) {
        mutexStarOver = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init(mutexStarOver, NULL);
    }

    //pthread_mutex_lock(mutexStarOver);
    if([myWorld objectForKey:loginICE] == nil){
        @try {
            ICEConnectorService *oneConnService = [[ICEConnectorService alloc]initWithServerName:loginICE];
            ICECommandParseService *oneCmdParserService = [[ICECommandParseService alloc]init];

            if ([oneConnService isConnected]) {
                oneSDK = [[ICESDK alloc]init:oneConnService with:oneCmdParserService];

                StatusEntity *regStatus = [oneSDK registerToICE3:loginICE withUserInfo:userEntity timeout:3];
                if (regStatus != nil &&
                    [[regStatus status]compare:[StatusEntity OK]] == NSOrderedSame
                ){
                    [oneSDK doingPeriod];

                    NSEnumerator *msgEnum = [[regStatus msg]objectEnumerator];
                    [UserInfoEntity setIceId:[msgEnum nextObject]];
                    [UserInfoEntity setGroupId:[msgEnum nextObject]];
                    [UserInfoEntity setTvmId:[userEntity name]];
                    [myWorld setObject:oneSDK forKey:loginICE];
                }else{
                    [oneSDK stayAlone];
#ifdef ICE_DEBUG
                    NSLog(@"发送注册信息出错");
#endif
                    //pthread_mutex_unlock(mutexStarOver);
                    if(![[regStatus status]isEqualToString:[StatusEntity UNDELIVER]])
                    {
                        @throw [[NSException alloc]initWithName:[regStatus status] reason:[[[regStatus msg]objectEnumerator]nextObject] userInfo:nil];
                    }
                    return nil;
                }
            }else{
                return nil;
            }
        }
        @catch (NSException *e) {
            @throw [[NSException alloc]initWithName:[e name] reason:[e reason] userInfo:[e userInfo]];
        }
        @finally {
            //pthread_mutex_unlock(mutexStarOver);
        }

    }else{
        oneSDK = [myWorld objectForKey:loginICE];
    }
    //pthread_mutex_unlock(mutexStarOver);

    return oneSDK;
}

-(MyNeighboursEntity*)getMyNeighbours//得到邻居
{
    CommandEntity *myNeighboursCommand = [[CommandEntity alloc]init];
    [myNeighboursCommand setCmdTYPE:[CommandTypeEntity GETMYNEIGHBOUR]];

    MyNeighboursEntity *myNeigh;
    @try{
        //StatusEntity *oneStatus = [self sendCommmand:myNeighboursCommand];
        StatusEntity *oneStatus = [self sendCommmand:myNeighboursCommand timeout:0.5 allowNone:NO];
        if([[oneStatus status]isEqualToString:[StatusEntity OK]] &&
           [[oneStatus cmd]isEqualToString:[myNeighboursCommand CmdTYPE]]
        ){
            myNeigh = [[MyNeighboursEntity alloc]init:[[[oneStatus msg]objectEnumerator]nextObject]];
        }else{
            myNeigh = nil;
        }
    }@catch(NSException *ee) {
#ifdef ICE_DEBUG
        NSLog(@"得到在线列表web错误:%@", [ee callStackSymbols]);
#endif
        myNeigh = nil;
    }@finally {

    }

    return myNeigh;
}

-(MyNeighboursEntity*)getMyNeighboursByWeb//得到邻居
{
    MyNeighboursEntity *myNeigh;
    @try{
        myNeigh = [[MyNeighboursEntity alloc]init:[oneICS getNeighboursListByWeb]];
    }@catch(NSException *ee) {
#ifdef ICE_DEBUG
        NSLog(@"得到在线列表错误:%@", [ee callStackSymbols]);
#endif
        myNeigh = nil;
    }@finally {

    }

    return myNeigh;
}
@end
