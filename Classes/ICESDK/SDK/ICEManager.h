//
//  ICEManager.h
//  iOS
//
//  Created by wsw on 27/11/12.
//  Copyright (c) 2012年 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICELoginEntity.h"
#import "ICELoginMethodEntity.h"
#import "UserInfoEntity.h"
#import "CommandEntity.h"
#import "ICEErrorEntity.h"
#import "ICESDK.h"
#import "ICEDebug.h"
#import "ICEFileEntity.h"
#import "UploadUserInfoEntity.h"
#import "ICEFolderMakerEntity.h"
#import "BLEPeripheralEntity.h"
#import "BLEHelper.h"

//@class ICEManager ;
//
//@protocol ICEManagerDelegate <NSObject>
//
///*
// 搜索中控 Delegate
//*/
//- (void)searchingTimeout:(ICEManager *)manager ;
//- (void)searchingCompletedWithServiceList:(NSArray *)servicelistArr ;
//- (void)searchingUpdatedWithServiceList:(NSArray *)updateServicelistArr ;
//
///*
// 连接中控相关细节 Delegate
//*/
//- (void)connectionTimeout ;
//- (void)connectionFailedWithICEError:(ICEErrorEntity *)error ;
//- (void)connectionSucceedWithTargetServiceIP:(NSString *)ip userPower:(NSString *)power ;
//
//- (void)connectionRetryFailed ;
//- (void)connectionWillRetry ;
//- (void)connectionRetrySuccess ;
//
///*
// 指令回调(中控列表)
//*/
//- (void)receivedCMD:(CommandEntity *)commandEntity ;
//
//
///*
// 蓝牙4 测距相关
// */
///**
// * @brief 不匹配设备
// */
//-(void)bleWrongEquipment;
///**
// * @brief 得到所有扫描到的 xx
// */
//-(void)nearestBLE:(BLEPeripheralEntity*)onePerpheral;
///**
// * @brief 蓝牙设备离开
// */
//-(void)leaveBLE:(BLEPeripheralEntity*)onePerpheral;
///**
// * @brief 没找到
// */
//-(void)foundNothindBLE;
///**
// * @brief 更新中控信息
// */
//-(void)updateICEInfoByICEName:(NSString*)iceId;
//@end

@protocol ICEManagerDelegate;

@interface managerCmdEntity:NSObject{
    NSString *cmd_;
    NSString *body_;
    NSString *obj_;
}
@property(nonatomic, strong)NSString *cmd;
@property(nonatomic, strong)NSString *body;
@property(nonatomic, strong)NSString *obj;
@end

@interface ICEManager : NSObject<BLEScannerDelegate>
{
    __weak id<ICEManagerDelegate> delegate_ ;
    __weak NSMutableArray *allICEName_;//看还有没有
    NSThread *threadUdpListen_;
    NSThread *threadRecvCmd_;
    UserInfoEntity *my;
    ICELoginEntity *loginICE;
    NSUInteger timeout;
    managerCmdEntity *sendManagerCmd;
    ICEErrorEntity *err;
    MyNeighboursEntity *myNeighbourCache;
    BLEHelper *oneBleHelper;

    /**
     *  蓝牙搜索缓存
     */
    NSMutableDictionary *bleSearchCache;
}


@property (nonatomic , weak) id<ICEManagerDelegate> delegate ;

/**
 * 内部轮训，看 UDP 的线程
 */
-(void)_udpLoopFinder;

/*
 开始搜索当前服务列表(中控列表)
 */
- (BOOL)startSearchServiceWithTimeout:(NSUInteger)time;
- (void)_startSearchServiceWithTimeout;
/*
 停止搜索当前服务列表(中控列表)
 */
- (void)stopSearchService ;


/*
 开始连接当前服务列表中某一个中控
 */

- (BOOL)startConnectionICELoginEntity:(ICELoginEntity *)loginEntity withUserInfo:(UserInfoEntity *)userinfo timeout:(NSUInteger)time ;
- (BOOL)_startConnectionICELoginEntity;

/*
 关闭当前连接(永远只保证一个连接,不允许多连接产生)
 */

- (void)closeCurrentConnection ;
/*
 发送指令
 */

-(BOOL)sendCmd:(NSString *)cmdString ;
-(BOOL)sendCmd:(NSString*)cmd withBody:(NSString*)body;
-(BOOL)sendCmd:(NSString *)cmdString  withBody:(NSString*)body withObj:(NSString*)obj;
-(BOOL)sendCmdEntity:(CommandEntity*)oneCmd;
-(void)_sendCmd:(managerCmdEntity*)cmd;
/**
 * 接受指令的线程
 */
-(void)getCmd;

/**
 * 得到在线列表
 */
-(MyNeighboursEntity *)getMyNeighbours;
-(void)_getMyNeighbours;

/**
 * 搜索蓝牙
 */

 /**
 * @brief 开始扫描 BLE
 * @param NSInteger
    0 为最靠近，-100 为最远。
    越靠近 0 越近，越靠近 -100 越远
 */
-(void)startScanBLE:(NSInteger)nearRSSI;


/**
 * @brief 停止扫描 BLE
 */
-(void)stopScanBLE;
@end


