//
//  ICEConnectorService.m
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "ICEConnectorService.h"
#import "ICEConnectorDao.h"
#import "BonjourHelper.h"

#import "ICEFileEntity.h"

@implementation ICEConnectorService
-(id)initWithServerName:(ICELoginEntity*)serverName//连接到一个中控
{
    if(self = [super init]){
        dao = [[ICEConnectorDao alloc]initWithServerName:serverName];
        [self connect];
    }

    return self;
}


-(void)connect//连接
{
    NSString* masterHostname = [dao pickupOneHostnameByServername:[UdpInfoRoleEntity MASTER_UDP]];
    NSString* slaveHostname = [dao pickupOneHostnameByServername:[UdpInfoRoleEntity SLAVE_UDP]];

    if(![dao connectByHostname:masterHostname]){
        [dao connectByHostname:slaveHostname];
    }
}

-(ICELoginEntity*)getLoginICE//得到连接名称
{
    return [dao getLoginICE];
}
-(NSString*)getConectHostname//得到主机名
{
    return [dao getConnectHostname];
}


-(NSString*)getNeighboursListByWeb
{
    //ice3/permission_list.php?new=1&iceid=
    NSString *url = [[NSString alloc]initWithFormat:@"http://%@/%@%@", [dao getConnectHostname],[ICEFileEntity URL_PERMISSION_IP_LIST], [UserInfoEntity iceId]];
    NSData *jData = [[NSData alloc]initWithContentsOfURL:[NSURL URLWithString:url]];
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jData options:NSJSONReadingMutableContainers error:nil];

    
    NSInteger code = [[jsonDict valueForKey:@"code"]intValue];
    
    NSString *onlineRaw;
    if(code == 0){
        onlineRaw = [jsonDict valueForKey:@"msg"];
    }else{
        onlineRaw = @"";
    }

//    NSLog(@"xxoo:%@ ", onlineRaw);
    return onlineRaw;
}

-(void)reConnect//重新连接
{
    if([self isConnected]){
        [self disconnect];
    }

    [self connect];
}

-(void)udpRecvBroadcaster:(NSThread*)rrthread//轮训得到 UDP 广播
{
    //[dao getRecvBroadcaster:rrthread];
}
-(BOOL)isConnected//是否连接
{
    return [dao isConnected];
}

-(ICERoleEntity*)getConnectICERole//得到连接的角色
{
    ICERoleEntity *connICERole = [dao getConnectICERole];
    return connICERole;
}
-(BOOL)sendSockRawCnt:(NSString*) sendStr//原始字符串送给 socket
{
    return [dao setRawStr:sendStr];
}

-(void)disconnect//关闭连接
{
    [dao disconnect];
}

-(NSString*)recvSockRawCnt//得到 socket 的内容
{
    NSString *rawCntStr = [dao getSocketString];
    return rawCntStr;
}

+(NSMutableArray*)getAllICENameArray//得到所有中控服务列表
{
    NSMutableArray* allICEString = [ICEConnectorDao getAllICENameArray];
    return allICEString;
}

+(NSMutableArray*)getAllICEServerArray//得到所有连接主机名
{
    NSMutableArray* serverName = [ICEConnectorDao getAllICEServerArray];
    return serverName;
}

+(void)udpStartListen
{
    [ICEConnectorDao udpStartListen];
}

+(void)udpStopListen
{
    [ICEConnectorDao udpStopListen];
}


@end
