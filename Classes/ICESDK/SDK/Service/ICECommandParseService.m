//
//  ICECommandParseService.m
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "ICECommandParseService.h"
#include "pthread.h"

@implementation ICECommandParseService
@synthesize rrawFromServer=rawFromServer;
-(id)init
{
    if(self = [super init]){
        rawFromServer = [[NSString alloc]init];
        commandArray = [[NSMutableArray alloc]init];
        statusArray = [[NSMutableArray alloc]init];

        //mutexRawFromServer = PTHREAD_MUTEX_INITIALIZER;//
        pthread_mutex_init(&mutexRawFromServer, NULL);
        //mutexCommandArray = PTHREAD_MUTEX_INITIALIZER;//
        pthread_mutex_init(&mutexCommandArray, NULL);
        //mutexStatusArray = PTHREAD_MUTEX_INITIALIZER;//
        pthread_mutex_init(&mutexStatusArray, NULL);

        //condCommandArray = PTHREAD_COND_INITIALIZER;
        pthread_cond_init(&condCommandArray, NULL);
        //condStatusArray = PTHREAD_COND_INITIALIZER;
        pthread_cond_init(&condStatusArray, NULL);
    }

    return self;
}

-(void)dealloc
{
    pthread_cond_destroy(&condCommandArray);
    pthread_cond_destroy(&condStatusArray);

    pthread_mutex_destroy(&mutexRawFromServer);
    pthread_mutex_destroy(&mutexCommandArray);
    pthread_mutex_destroy(&mutexStatusArray);
}

-(CommandEntity*)getOneCmd:(NSThread*)periodThread//得到一个命令
{
    CommandEntity *oneCmdEntity = nil;//[[CommandEntity alloc]init];
    //return oneCmdEntity;

    @try {
        int mutexS;

        while (![periodThread isCancelled]) {
            mutexS = pthread_mutex_lock(&mutexCommandArray);

            if (mutexS == EINVAL) {
                //@throw [[NSException alloc]initWithName:@"Exception" reason:@"得到命令解锁失败" userInfo:nil];
                return oneCmdEntity;
            }

            if(mutexS != 0){
                //usleep(200000);//如果忙，则睡眠 0.2 秒
                continue;
            }

            if([commandArray count] != 0){
                break;
            }

            mutexS = pthread_cond_wait(&condCommandArray, &mutexCommandArray);

            if(mutexS != 0){
                //usleep(200000);//如果忙，则睡眠 0.2 秒
                pthread_mutex_unlock(&mutexCommandArray);
                continue;
            }

            if([commandArray count] != 0){
                break;
            }

            //usleep(200000);//如果忙，则睡眠 0.2 秒
        }

        if ([commandArray count]>0){//[periodThread isCancelled]) {
            oneCmdEntity = [commandArray objectAtIndex:0];
            [commandArray removeObjectAtIndex:0];
        }

        pthread_mutex_unlock(&mutexCommandArray);
        //pthread_mutex_unlock(&mutexCommandArray);

    }
    @catch (NSException *exception) {
        pthread_mutex_unlock(&mutexCommandArray);
#ifdef ICE_DEBUG
        NSLog(@"xxoo:%@\n%@\n%@", commandArray, [exception reason],[exception callStackSymbols]);
#endif
        @throw [[NSException alloc]initWithName:@"Exception" reason:@"得到命令出错锁" userInfo:nil];
    }

    return oneCmdEntity;
}

-(StatusEntity*)getOneStatus//得到一个状态
{
    @try {
        StatusEntity *oneStatusEntity = nil;
        int mutexS;

        while (YES) {
            mutexS = pthread_mutex_lock(&mutexStatusArray);

            if(mutexS == EINVAL){
                @throw [[NSException alloc]initWithName:@"Exception" reason:@"得到状唧唧态拉出错" userInfo:nil];
            }

            if(mutexS != 0){
                continue;
            }


            if ([statusArray count]!=0) {
                break;
            }

            pthread_cond_wait(&condStatusArray, &mutexStatusArray);
            if (mutexS != 0) {
                mutexS = pthread_mutex_unlock(&mutexStatusArray);

                continue;
            }


            if ([statusArray count]!=0) {
                break;
            }
        }

        oneStatusEntity = [statusArray objectAtIndex:0];
        [statusArray removeObjectAtIndex:0];
//        pthread_mutex_unlock(&mutexStatusArray);

        mutexS = pthread_mutex_unlock(&mutexStatusArray);

        return oneStatusEntity;
    }
    @catch (NSException *exception) {
        pthread_mutex_unlock(&mutexStatusArray);
        @throw [[NSException alloc]initWithName:@"Exception" reason:@"得到状态出错" userInfo:nil];
    }
}

-(StatusEntity*)getOneStatusByCMD:(CommandEntity*) cmd timeoutSecond:(NSUInteger) timeoutSecond allowNone:(BOOL) allowNone//根据命令，得到一个状态
{
    BOOL isForever = (timeoutSecond == 0) ? YES : NO;
    
    @try {
        StatusEntity *retStatusEntity = nil;
        int mutexS;
        
        while(YES){
#ifdef ICE_DEBUG
            NSLog(@"getOneStatusByCMD:%u\t%d",timeoutSecond, isForever);
#endif

            mutexS = pthread_mutex_lock(&mutexStatusArray);

            if (mutexS == EINVAL) {
                pthread_mutex_unlock(&mutexStatusArray);
                return retStatusEntity;
            }
            
            if(mutexS != 0){
                //usleep(200000);
                //timeoutSecond--;
                continue;
            }

            if ([statusArray count] > 0) {
                for (NSUInteger i=0; i<[statusArray count]; i++) {
                    StatusEntity *oneStatusEntity = [statusArray objectAtIndex:i];
                    if (oneStatusEntity == NULL ||
                        oneStatusEntity == nil
                    ){
                        [statusArray removeObjectAtIndex:i];
                        continue;
                    }

                    if([[oneStatusEntity cmd]isEqualToString:[cmd CmdTYPE]]){
                        goto outside_in_GOSB;
                    }
                }
            }

            if(isForever){
                pthread_cond_wait(&condStatusArray, &mutexStatusArray);
            }else{
                gettimeofday(&mutexCondTv, NULL);
                mutexCondTs.tv_sec = mutexCondTv.tv_sec + timeoutSecond;
                pthread_cond_timedwait(&condStatusArray, &mutexStatusArray, &mutexCondTs);

                gettimeofday(&mutexCondTvNow, NULL);
                timeoutSecond = mutexCondTv.tv_sec - mutexCondTvNow.tv_sec - 1;

                if (timeoutSecond <= 0) {
                    pthread_mutex_unlock(&mutexStatusArray);

                    return retStatusEntity;
                }
            }

            if ([statusArray count] == 0) {
                for (NSUInteger i=0; i<[statusArray count]; i++) {
                    StatusEntity *oneStatusEntity = [statusArray objectAtIndex:i];
                    if (oneStatusEntity == NULL ||
                        oneStatusEntity == nil
                        ){
                        [statusArray removeObjectAtIndex:i];
                        continue;
                    }

                    if([[oneStatusEntity cmd]isEqualToString:[cmd CmdTYPE]]){
                        goto outside_in_GOSB;
                    }
                }
            }

            usleep(10000);
            pthread_mutex_unlock(&mutexStatusArray);
        }

outside_in_GOSB:
        for (NSUInteger i=0; i < [statusArray count]; i++) {
            StatusEntity *oneStatusEntity = [statusArray objectAtIndex:i];
            if (oneStatusEntity == NULL ||
                oneStatusEntity == nil
            ) {
                //[statusArray removeObjectAtIndex:i];
                continue;
            }

            if([[oneStatusEntity cmd]isEqualToString:[cmd CmdTYPE]]){
                retStatusEntity = oneStatusEntity;
                [statusArray removeObjectAtIndex:i];
                break;
            }
        }

        if(retStatusEntity == nil &&
           allowNone
        ){
            for (NSUInteger i=0; i < [statusArray count]; i++) {
                StatusEntity *oneStatusEntity = [statusArray objectAtIndex:i];
                if(NSOrderedSame == [[oneStatusEntity cmd]caseInsensitiveCompare:[CommandTypeEntity NNULL]]){
                    retStatusEntity = oneStatusEntity;
                    [statusArray removeObjectAtIndex:i];
                    break;
                }
            }
        }

        pthread_mutex_unlock(&mutexStatusArray);
        
        return retStatusEntity;
    }
    @catch (NSException *exception) {
        pthread_mutex_unlock(&mutexStatusArray);
        @throw [[NSException alloc]initWithName:@"Exception" reason:@"得到状态出错锁与关键字" userInfo:nil];
    }
}

-(void)appendRawStr:(NSString*)onePieceRawString//添加到指令缓存里
{
    int mutexS;

    mutexS = pthread_mutex_lock(&mutexRawFromServer);

    if(mutexS != 0){
        return;
    }

    rawFromServer = [rawFromServer stringByAppendingString:onePieceRawString];

    pthread_mutex_unlock(&mutexRawFromServer);
}
-(void)fromRawToArray//从原始的字符里，分析出命令来
{
    if ([rawFromServer length] == 0) {
        return;
    }

    //NSMutableArray *commandEntityList = [[NSMutableArray alloc]init];

    NSRange startPos = NSMakeRange(0, [rawFromServer length]);

    NSRange responseHeadOutPos;
    NSRange responseBodyOutPos;

    NSRange jsonOutPos;

    int mutexS;

    mutexS = pthread_mutex_lock(&mutexRawFromServer);
    if(mutexS != 0){
        return ;
    }

    @try {
        while (YES) {
            startPos.location = 0;
            startPos.length = [rawFromServer length];

            NSRange jsonInPos = [rawFromServer rangeOfString:[ICECommandParseService JSON_START_STRING] options:NSLiteralSearch range:startPos];
            NSRange responseInPos = [rawFromServer rangeOfString:[ICECommandParseService RESPONSE_START_STRING] options:NSLiteralSearch range:startPos];
            jsonInPos.length = [rawFromServer length]-jsonInPos.location;
            responseInPos.length = [rawFromServer length]-responseInPos.location;
            
            if (jsonInPos.location != 0 &&
                responseInPos.location != 0
            ) {
                if([self filterRawCmd]){
                    continue;
                }else{
                    break;
                }
            }
            
            if(jsonInPos.location == NSNotFound ||
               responseInPos.location == NSNotFound
            ){
                if(jsonInPos.location == NSNotFound &&
                   responseInPos.location == NSNotFound
                ){
                    break;
                }

                if(responseInPos.location != NSNotFound){
                    if(![self parseResponseFromRaw]){
                        break;
                    }
                    continue;
                }

                if(jsonInPos.location != NSNotFound){
                    [self parseJsonFromRaw];
                    continue;
                }

                if ([rawFromServer length] == 0) {
                    
                    break;
                }else{
                    continue;
                }
            }

            if(responseInPos.location != NSNotFound &&
               responseInPos.location < jsonInPos.location
            ){
                responseHeadOutPos = [rawFromServer rangeOfString:[ICECommandParseService RESPONSE_TAIL_STRING] options:NSLiteralSearch range:startPos];
                responseBodyOutPos = [rawFromServer rangeOfString:[ICECommandParseService RESPONSE_TAIL_STRING] options:NSLiteralSearch range:NSMakeRange(responseHeadOutPos.location, [rawFromServer length] - responseHeadOutPos.location)];

                if(responseBodyOutPos.location == NSNotFound){
                    if(responseHeadOutPos.location == NSNotFound){
                        startPos.location = responseHeadOutPos.location;
                        startPos.length = [rawFromServer length] - responseHeadOutPos.location;
                    }

                    continue;
                }

                if(![self parseResponseFromRaw]){
                    break;
                }
                
                if ([rawFromServer length] == 0) {
                    break;
                }else{
                    continue;
                }
            }

            if(jsonInPos.location != NSNotFound &&
               jsonInPos.location < responseInPos.location
            ){
                jsonOutPos = [rawFromServer rangeOfString:[ICECommandParseService JSON_TAIL_STRING] options:NSLiteralSearch range:jsonInPos];

                if(jsonOutPos.location == NSNotFound){
                    continue;
                }

                [self parseJsonFromRaw];

                if ([rawFromServer length] == 0) {
                    break;
                }
            }
        }

    }@catch (NSException *e) {
#ifdef ICE_DEBUG
        NSLog(@"解析出现一般性错误 name:%@\treason:%@(%@)", [e name], [e reason], [e callStackSymbols]);
#endif
    }
    pthread_mutex_unlock(&mutexRawFromServer);

}

-(BOOL) filterRawCmd{
    NSRange startPos = NSMakeRange(0, [rawFromServer length]);
    NSRange jsonInPos = [rawFromServer rangeOfString:[ICECommandParseService JSON_START_STRING] options:NSLiteralSearch range:startPos];
    NSRange responseInPos = [rawFromServer rangeOfString:[ICECommandParseService RESPONSE_START_STRING] options:NSLiteralSearch range:startPos];
    jsonInPos.length = [rawFromServer length]-jsonInPos.location;
    responseInPos.length = [rawFromServer length]-responseInPos.location;

    if (jsonInPos.location == NSNotFound &&
        responseInPos.location == NSNotFound
    ){
        return NO;
    }

    if(jsonInPos.location == 0 ||
       responseInPos.location == 0
    ){
        return YES;
    }

    if (jsonInPos.location == NSNotFound) {
        [self filterRawResponse];
        return YES;
    }

    if (responseInPos.location == NSNotFound) {
        [self filterRawJson];
        return YES;
    }

    if (jsonInPos.location > responseInPos.location) {
        [self filterRawResponse];
        return YES;
    }

    if (responseInPos.location > jsonInPos.location) {
        [self filterRawJson];
        return YES;
    }

    return YES;
}

-(void)filterRawResponse{
    NSRange startPos = NSMakeRange(0, [rawFromServer length]);
    NSRange responseInPos = [rawFromServer rangeOfString:[ICECommandParseService RESPONSE_START_STRING] options:NSLiteralSearch range:startPos];

    rawFromServer = [rawFromServer substringFromIndex:responseInPos.location];
}

-(void)filterRawJson{
    NSRange startPos = NSMakeRange(0, [rawFromServer length]);
    NSRange jsonInPos = [rawFromServer rangeOfString:[ICECommandParseService JSON_START_STRING] options:NSLiteralSearch range:startPos];

    rawFromServer = [rawFromServer substringFromIndex:jsonInPos.location];
}

-(void)parseJsonFromRaw{//分析 JSON 从原始
    NSRange jsonInPos = [rawFromServer rangeOfString:[ICECommandParseService JSON_START_STRING]];
    NSRange jsonOutPos = [rawFromServer rangeOfString:[ICECommandParseService JSON_TAIL_STRING] options:NSLiteralSearch range:NSMakeRange(jsonInPos.location+1, ([rawFromServer length]-jsonInPos.location-1))];

    
    jsonOutPos.location += [[ICECommandParseService JSON_START_STRING]length];
#ifdef ICE_DEBUG
        NSLog(@"rawFromServer1:(size:%u)>%@<\nloc:%u\tlen:%u", [rawFromServer length],rawFromServer, jsonInPos.location, (jsonOutPos.location-jsonInPos.location));
#endif
    NSString *oneString = [rawFromServer substringWithRange:NSMakeRange(jsonInPos.location, (jsonOutPos.location-jsonInPos.location))];
    [self parseOneJson:oneString];

    oneString = nil;
    if(jsonInPos.location > 0){
        oneString = [rawFromServer substringWithRange:NSMakeRange(0, jsonInPos.location)];
#ifdef ICE_DEBUG
        NSLog(@"rawFromServer2:%@\noneStr:%@\nloc:0\tlen:%u", rawFromServer, oneString, jsonInPos.location);
#endif
    }else{
        oneString = [[NSString alloc]init];
    }


    NSString *lastPartString = [oneString stringByAppendingString:[rawFromServer substringFromIndex:(jsonOutPos.location)]];
    rawFromServer = lastPartString;
#ifdef ICE_DEBUG
        NSLog(@"rawFromServer5:{size:%d}>%@<", [rawFromServer length], rawFromServer);
#endif

}

-(BOOL)parseResponseFromRaw{//分析应答从原始内容
#ifdef ICE_DEBUG
        NSLog(@"rawFromServer3:(size:%d)>>%@<<", [rawFromServer length], rawFromServer);
#endif

    NSRange responseInPos = [rawFromServer rangeOfString:[ICECommandParseService RESPONSE_START_STRING]];
    NSRange responseHeadOutPos = [rawFromServer rangeOfString:[ICECommandParseService RESPONSE_TAIL_STRING]];    
    NSRange responseHeadOutPosNext = NSMakeRange((responseHeadOutPos.location+[[ICECommandParseService RESPONSE_TAIL_STRING]length]), ([rawFromServer length]-responseHeadOutPos.location-[[ICECommandParseService RESPONSE_TAIL_STRING]length]));
    //    NSRange responseHeadOutPosNext = NSMakeRange((responseHeadOutPos.location+[[ICECommandParseService RESPONSE_TAIL_STRING]length]), ([rawFromServer length]-responseHeadOutPos.location));

    if (responseHeadOutPos.location == NSNotFound ||
        responseHeadOutPosNext.location == NSNotFound
    ){
        return NO;
    }

    NSRange responseBodyOutPos = [rawFromServer rangeOfString:[ICECommandParseService RESPONSE_TAIL_STRING] options:NSLiteralSearch range:responseHeadOutPosNext];
    
    if (responseBodyOutPos.location == NSNotFound
    ){
        return NO;
    }
    
    NSString *oneString = [rawFromServer substringWithRange:NSMakeRange(responseInPos.location, (responseBodyOutPos.location - responseInPos.location + responseBodyOutPos.length))];

    
    [self parseOneResponse:oneString];

    if(responseInPos.location != NSNotFound){
        oneString = [rawFromServer substringToIndex:(responseInPos.location)];
    }else{
        oneString = [[NSString alloc]init];
    }

    rawFromServer = [oneString stringByAppendingString:[rawFromServer substringFromIndex:(responseBodyOutPos.location + responseBodyOutPos.length)]];
    
    return YES;
}

-(void)parseOneResponse:(NSString*)response{//分析一个答复
    NSString *splitBorder = @"\r\n";
    NSArray *responseArray = [response componentsSeparatedByString:splitBorder];

    NSString *oneLineSplitBorder = @":";

    if([responseArray count] < 3){
        return;
    }


    BOOL justHead = NO;
    if ([[[responseArray objectAtIndex:([responseArray count]-2)]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]length] != 0)
    {
        justHead = YES;
    }

    CommandEntity *oneCmdEntity = [[CommandEntity alloc]init];
    if (justHead) {
        [oneCmdEntity setBody:@""];
    }else{
        [oneCmdEntity setBody:[[responseArray objectAtIndex:([responseArray count]-3)] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }


    NSEnumerator *responseValuesEnum = [responseArray objectEnumerator];
    NSString *oneResponseLine;
    while(oneResponseLine = [responseValuesEnum nextObject]){
        if([[oneResponseLine stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]length] == 0){
            break;
        }

        NSMutableArray *oneResponseArray = [[NSMutableArray alloc]initWithArray:[oneResponseLine componentsSeparatedByString:oneLineSplitBorder]];
        if([oneResponseArray count]<2){
            continue;
        }

        NSString *propertyKey = [[oneResponseArray objectAtIndex:0]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [oneResponseArray removeObjectAtIndex:0];

        NSString *propertyValue = [oneResponseArray componentsJoinedByString:@""];


        if(NSOrderedSame == [[propertyKey uppercaseString]compare:@"CMD"]){
            [oneCmdEntity setCmdTYPE:propertyValue];
        }

        if(NSOrderedSame == [[propertyKey uppercaseString]compare:@"MSGID"]){
            [oneCmdEntity setMSGID:propertyValue];
        }

        if(NSOrderedSame == [[propertyKey uppercaseString]compare:@"PREMSGID"]){
            [oneCmdEntity setPREMSGID:propertyValue];
        }


        if(NSOrderedSame == [[propertyKey uppercaseString]compare:@"FROM"]){
            [oneCmdEntity setFROM:propertyValue];
        }

        if(NSOrderedSame == [[propertyKey uppercaseString]compare:@"OBJ"]){
            [[oneCmdEntity OBJ]addObject:propertyValue];
            //[oneCmdEntity setOBJ:propertyValue];
        }

        if(NSOrderedSame == [[propertyKey uppercaseString]compare:@"RES"]){
            [oneCmdEntity setRES:propertyValue];
        }

        if(NSOrderedSame == [[propertyKey uppercaseString]compare:@"CLIENTVAILDCODE"]){
            [oneCmdEntity setClientVaildcode:propertyValue];
        }

        
        [oneCmdEntity setCMD_HEAD_DICTKey:propertyKey value:propertyValue];

    }

    responseValuesEnum = nil;
    responseArray = nil;

    int mutexS;
    mutexS = pthread_mutex_lock(&mutexCommandArray);

    if(mutexS != 0){
#ifdef ICE_DEBUG
        NSLog(@"在命令解析服务里，所出错了啊，出错");
#endif
        return;
    }

    BOOL isDuplicated = NO;
    NSEnumerator *commandArrayValueEnum = [commandArray objectEnumerator];
    CommandEntity *oneCmdEInA;
    while(oneCmdEInA = [commandArrayValueEnum nextObject]){
        if([[oneCmdEInA MSGID]isEqualToString:[oneCmdEntity MSGID]]){
            isDuplicated = YES;
        }
    }
    commandArrayValueEnum = nil;
    oneCmdEInA = nil;

    if(!isDuplicated){
        //if (![[oneCmdEntity CmdTYPE]isEqualToString:[CommandTypeEntity CANCEL]]) {
            [commandArray addObject:oneCmdEntity];
            oneCmdEntity = nil;
        //}
    }

    //mutexS =
    pthread_mutex_unlock(&mutexCommandArray);
    pthread_cond_broadcast(&condCommandArray);
}
-(void)parseOneJson:(NSString*)json{//分析一个 json
    int mutexS;
    mutexS = pthread_mutex_lock(&mutexStatusArray);

    if(mutexS != 0){
        return;
    }

    StatusEntity *se = [[StatusEntity alloc]initWithJSONData:[json dataUsingEncoding:NSUTF8StringEncoding]];
    [statusArray addObject:se];
    pthread_mutex_unlock(&mutexStatusArray);
    pthread_cond_broadcast(&condStatusArray);
}

+(NSString*)RESPONSE_TAIL_STRING//应答结束字符
{
    return @"\r\n\r\n";
}
+(NSString*)RESPONSE_START_STRING//应答开始字符串
{
    return @"CMD:";
}
+(NSString*)JSON_TAIL_STRING//JSON 的结束字符串
{
    return @"}";
}
+(NSString*)JSON_START_STRING//JSON 的开始字符串
{
    return @"{";
}

@end
