//
//  ICEConnectorService.h
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEConnectorDao.h"
#import "ICELoginEntity.h"
#import "ICEDebug.h"

@interface ICEConnectorService : NSObject
{
    ICEConnectorDao* dao;
}

-(id)initWithServerName:(ICELoginEntity*)serverName;//连接到一个中控
-(void)connect;//连接
-(NSString*)getConectHostname;//得到主机名
-(void)reConnect;//重新连接
-(ICELoginEntity*)getLoginICE;//得到连接名称
-(BOOL)isConnected;//是否连接
-(ICERoleEntity*)getConnectICERole;//得到连接的角色
-(BOOL)sendSockRawCnt:(NSString*) sendStr;//原始字符串送给 socket
-(void)disconnect;//关闭连接
-(NSString*)recvSockRawCnt;//得到 socket 的内容
//-(void)udpRecvBroadcaster: (NSThread*)rrthread;//轮训得到 UDP 广播
+(NSMutableArray*)getAllICENameArray;//得到所有中控服务列表
+(NSMutableArray*)getAllICEServerArray;//得到所有连接主机名
+(void)udpStartListen;//开始监听
+(void)udpStopListen;//结束监听
-(NSString*)getNeighboursListByWeb;//得到邻居
@end
