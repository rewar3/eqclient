//
//  ICECommandParseService.h
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

//TODO: 修改结构

#import <Foundation/Foundation.h>
#import "CommandEntity.h"
#import "StatusEntity.h"
#import "ICEDebug.h"

#include <sys/time.h>

@interface ICECommandParseService : NSObject
{
    pthread_mutex_t mutexRawFromServer;
    pthread_mutex_t mutexCommandArray;
    pthread_mutex_t mutexStatusArray;

	pthread_cond_t condCommandArray;
    pthread_cond_t condStatusArray;

    NSString *rawFromServer;

    NSMutableArray *commandArray;
    NSMutableArray *statusArray;

    struct timeval mutexCondTv;
    struct timeval mutexCondTvNow;
    struct timespec mutexCondTs;
}

@property(atomic) NSString* rrawFromServer;

-(CommandEntity*)getOneCmd:(NSThread*)periodThread;//得到一个命令
-(StatusEntity*)getOneStatus;//得到一个状态
-(StatusEntity*)getOneStatusByCMD:(CommandEntity*) cmd timeoutSecond:(NSUInteger) timeoutSecond allowNone:(BOOL) allowNone;//根据命令，得到一个状态
-(void)appendRawStr:(NSString*)onePieceRawString;//添加到指令缓存里
-(void)fromRawToArray;//从原始的字符里，分析出命令来
-(void)parseJsonFromRaw;//分析 JSON 从原始
-(BOOL)parseResponseFromRaw;//分析应答从原始内容
-(void)parseOneResponse:(NSString*)response;//分析一个答复
-(void)parseOneJson:(NSString*)json;//分析一个 json
-(BOOL)filterRawCmd;
-(void)filterRawResponse;
-(void)filterRawJson;

+(NSString*)RESPONSE_TAIL_STRING;//应答结束字符
+(NSString*)RESPONSE_START_STRING;//应答开始字符串
+(NSString*)JSON_TAIL_STRING;//JSON 的结束字符串
+(NSString*)JSON_START_STRING;//JSON 的开始字符串

@end
