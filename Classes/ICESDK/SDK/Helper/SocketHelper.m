//
//  SocketHelper.m
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "SocketHelper.h"

@implementation SocketHelper
-(id)initWithIP:(NSString*)ip port:(NSUInteger)port//连接到一个 host
{
    if(self = [super init]){
        int status;

        bzero(&servAddr, sizeof(struct sockaddr_in));
        servAddr.sin_family = AF_INET;
        servAddr.sin_port = htons(port);
        inet_pton(AF_INET, [ip UTF8String], &servAddr.sin_addr);

        sockfd = socket(PF_INET, SOCK_STREAM, 0);
        if(sockfd == -1){
            perror("连不上");
            @throw [[NSException alloc]initWithName:@"Exception" reason:@"SocketHelper 取得 socket 之出错:" userInfo:nil];
            return self;
        }

        int set = 1;
        //int connectTimeout = 5;
        struct linger noLinger;
        noLinger.l_onoff = 1;
        noLinger.l_linger = 1;

        //int recvBuf = 65536;
        //int sendBuf = 40960;

        int ttl = 42;

        setsockopt(sockfd, SOL_SOCKET, SO_NOSIGPIPE, (const void *)&set, sizeof(int));
        setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const void*)&set, sizeof(int));
        setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, (const void*)&set, sizeof(int));
        setsockopt(sockfd, SOL_SOCKET, SO_LINGER,    (const void*)&noLinger, sizeof(struct linger));
        //setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF,    (const void*)&recvBuf, sizeof(int));
        //setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF,    (const void*)&sendBuf, sizeof(int));
        //setsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, (const void*)&set, sizeof(int));
        setsockopt(sockfd, IPPROTO_IP, IP_TTL,       (const void*)&ttl, sizeof(int));
        setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (const void*)&set, sizeof(int));
        //setsockopt(sockfd, IPPROTO_TCP, TCP_KEEPALIVE, (const void*)&set, sizeof(int));
        //setsockopt(sockfd, IPPROTO_TCP, TCP_CONNECTIONTIMEOUT, (const void*)&connectTimeout, sizeof(int));

#ifdef ICE_DEBUG
        NSLog(@"sock :%@", ip);
#endif
        status = connect(sockfd, (const struct sockaddr *)&servAddr, sizeof(servAddr));
        if(status == -1){
            perror("连不上");
            //@throw [[NSException alloc]initWithName:@"Exception" reason:@"connect 服务器出错" userInfo:nil];
            sockfd = -1;
            return self;
        }

        buf = (char *)malloc(65536);
    }

    return self;
}

-(BOOL)isConnected{
    if(sockfd == -1){
        return NO;
    }else{
        //return YES;
    }
    /*
    struct timeval to={0,0};

    fd_set eSet;
    fd_set wSet;
    fd_set rSet;
    FD_ZERO(&eSet);
    FD_ZERO(&wSet);
    FD_ZERO(&rSet);

    FD_SET(sockfd, &eSet);
    FD_SET(sockfd, &wSet);
    FD_SET(sockfd, &rSet);

    int status = select((sockfd+1), &rSet, &wSet, &eSet, &to);

    if(status < 0){
        return NO;
    }

    if(FD_ISSET(sockfd, &rSet)){
#ifdef ICE_DEBUG
//        NSLog(@"不能读");
#endif
    }


    if(FD_ISSET(sockfd, &wSet)){
#ifdef ICE_DEBUG
//        NSLog(@"不能写");
#endif
    }

    if(FD_ISSET(sockfd, &eSet)){
        [self disconnect];
        return NO;
    }
     */

    return YES;
}

-(BOOL)send:(NSString*)msg{
    size_t nleft;
    ssize_t nwritten;
    const char *ptr;

    ptr = [msg UTF8String];
    nleft = strlen(ptr);
    signal(SIGPIPE, SIG_IGN);
#ifdef ICE_DEBUG
    NSLog(@"send:%@", msg);
#endif
    while (nleft > 0) {
        if((nwritten = write(sockfd, ptr, nleft)) <= 0){
            if(nwritten < 0 && errno == EINTR){
                nwritten = 0;
            }else{
#ifdef ICE_DEBUG
                NSLog(@"没送成功");
                perror("write error:");
#endif
                //[self disconnect];
                if (sockfd > 0) {
                    close(sockfd);
                }
                sockfd = -1;

                @throw [[NSException alloc]initWithName:@"NSException" reason:@"送出 socket 出错" userInfo:nil];
            }
        }
        signal(SIGPIPE, SIG_DFL);

        nleft -= nwritten;
        ptr += nwritten;
    }

    return YES;
}

-(NSString*)recv
{
    fd_set rSet;
    FD_ZERO(&rSet);
    FD_SET(sockfd, &rSet);
    int status=0;


//    status = select((sockfd+1), &rSet, NULL, NULL, NULL);
//    if (status == 0) {
//        return @"";
//    }
//
//
//    if(!FD_ISSET(sockfd, &rSet)){
//        return nil;
//    }

    
    bzero(buf, 65536);

    status = recv(sockfd, buf, 65535, 0);
#ifdef ICE_DEBUG
    NSLog(@"status read.status:%d", status);
#endif

    NSString *readStr;
    if(status <= 0){
#ifdef ICE_DEBUG
        perror("recv error:");
#endif
        //@throw [[NSException alloc]initWithName:@"Exception" reason:@"接收 socket 原始出错2" userInfo:nil];
        //[self disconnect];
        if (sockfd > 0) {
            close(sockfd);
        }
        sockfd = -1;
        readStr = @"";
    }else{
        readStr = [[NSString alloc]initWithCString:buf encoding:NSUTF8StringEncoding];
    }
    //free(buf);
#ifdef ICE_DEBUG
    //NSLog(@"recv:\n%@", readStr);
#endif
    return readStr;
}

-(void)disconnect{
#ifdef ICE_DEBUG
    NSLog(@"关闭连接");
#endif
    if (sockfd < 0) {
        return;
    }
    close(sockfd);
    sockfd = -1;
}

-(void)dealloc
{
    free(buf);
}
@end
