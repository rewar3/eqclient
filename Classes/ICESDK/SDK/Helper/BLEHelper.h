//
//  BLEHelper.h
//  iOS
//
//  Created by hanshu on 3/7/13.
//  Copyright (c) 2013 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreBluetooth/CBService.h>

#import "BLEPeripheralEntity.h"


@protocol BLEScannerDelegate
@required
/*
 蓝牙4 测距相关
 */
/**
 * @brief 不匹配设备
 */
-(void)wrongBLEEquipment;
/**
 * @brief 得到所有扫描到的 xx
 */
-(void)nearestPeripheral:(BLEPeripheralEntity*)onePerpheral;
/**
 * @brief 没找到
 */
-(void)foundNothindPeripheral;
@optional
/**
 * @brief 扫描到一个设备
 */
-(void) scanIt:(NSString*)iceName with:(NSNumber*)rssi;
/**
 * @brief 更新中控信息
 */
-(void)updateICEInfoByICEName:(NSString*)iceId;
/**
 * @brief 离开
 */
-(void)leaveBLE:(BLEPeripheralEntity*)onePerpheral;
@end;


@interface BLEHelper : NSObject<CBCentralManagerDelegate, CBPeripheralDelegate>
{
    CBCentralManager *ccm;
    CBPeripheral *actPeripheral;
    NSMutableDictionary *blePerpheral;
    NSMutableArray *foundPerpheral;

    int isContinue;
    BOOL isNotify;
    BLEPeripheralEntity *nearestBLE;
//    BLEPeripheralEntity *lastNearestBLE;

    NSString *nearestBLEName;
    NSString *lastNearestBLEName;
    NSNumber *minRSSI;
    BOOL autoRestart;
    BOOL isWrongEqipment;

    NSInteger nearRSSI_;
    NSInteger leaveRSSI_;

    BOOL isLeave;
}

@property (nonatomic,assign) NSObject <BLEScannerDelegate> *delegate;
/**
 * 0 为最靠近，-100 为最远。
 * 越靠近 0 越近，越靠近 -100 越远
 */
@property (nonatomic,assign) NSInteger nearRSSI;
@property (nonatomic,assign) NSInteger leaveRSSI;

@property (nonatomic,strong) NSString *httpIP;
-(void)starScanBLE;//:(NSTimeInterval) timeout;
-(void)stopScanBLE;
-(void)updatePeripheralInfo:(NSString*)ICEID;
@end
