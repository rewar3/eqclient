//
//  BonjourHelper.m
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "BonjourHelper.h"

@implementation BonjourInfoRoleEntity
+(NSUInteger)MASTER
{
    return 1;
}

+(NSUInteger)SLAVE;
{
    return 2;
}

+(NSUInteger)RAW;
{
    return 3;
}
@end

@implementation BonjourInfoEntity
@synthesize fullName = fullName_;
@synthesize hostName = hostName_;
@synthesize port = port_;
@synthesize serverName = serverName_;
@synthesize displayName = displayName_;
@synthesize hostIp = hostIp_;
@end

@implementation BonjourHelper
@synthesize bonjourNameList=bonjourNameList_;

-(id)init{
    if(self = [super init]){
        bonjourNameList_ = [[NSMutableArray alloc]init];

        browser = [[NSNetServiceBrowser alloc]init];
        [browser setDelegate:self];
        [browser searchForServicesOfType:CC3_NET_TYPE
                                inDomain:CC3_NET_DOMAIN];
    }

    return self;
}

-(void)netServiceBrowser: (NSNetServiceBrowser *) aNetServiceBrowser
          didFindService: (NSNetService *) aNetService
              moreComing: (BOOL)moreComing
{
    NSLog(@"didFindService:%@", [aNetService name]);
    isFound = YES;

        //如果找到中控，则通知它
    if([[aNetService name] hasPrefix:CC3_NAME_PREFIX]){
        [aNetService setDelegate:self];
        [aNetService resolveWithTimeout:5];
    }
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser
         didRemoveService:(NSNetService *)aNetService
               moreComing:(BOOL)moreComing
{
    NSLog(@"didRemoveService:%@", [aNetService name]);
    isFound = NO;

    if([[aNetService name] hasPrefix:CC3_NAME_PREFIX]){
        [aNetService setDelegate:self];
        [aNetService resolveWithTimeout:5];
    }

    /*
    BonjourInfoEntity *oneBIE;
    NSMutableArray *tmpBNL = [[NSMutableArray alloc]init];

    for (NSUInteger i=0; i < [bonjourNameList_ count]; i++) {
        oneBIE = [bonjourNameList_ objectAtIndex:i];

        if([[oneBIE serverName]caseInsensitiveCompare:[aNetService name]] == NSOrderedSame){
            [tmpBNL removeObjectAtIndex:i];
        }
    }
     
    [bonjourNameList_ release];
     bonjourNameList_ = tmpBNL;
     */

}


-(void) netServiceDidResolveAddress: (NSNetService *) currentService{
    NSLog(@"netServiceDidResolveAddress:%@\t%d", [currentService name], isFound?1:0);
    NSArray *addresses = [currentService addresses];

    NSData *address = [addresses objectAtIndex:0];
    struct sockaddr_in iceAddr;
    [address getBytes:&iceAddr length:[address length]];

    char* hostIp = inet_ntoa(iceAddr.sin_addr);

    NSLog(@"ip:%s\thostname:%@\tname:%@", hostIp, [currentService name],[currentService hostName]);

    BonjourInfoEntity *oneBIE = [[BonjourInfoEntity alloc]init];
    [oneBIE setFullName:[currentService hostName]];
    [oneBIE setHostName:[currentService name]];
    [oneBIE setPort:[currentService port]];
    [oneBIE setHostIp:[[NSString alloc]initWithCString:hostIp encoding:NSUTF8StringEncoding]];

    NSArray *fullNameArrray = [[currentService hostName]componentsSeparatedByString:@"."];
    [oneBIE setServerName:[fullNameArrray objectAtIndex:0]];


    NSArray *logicNameArray = [[currentService name]componentsSeparatedByString:@"_"];
    if ([logicNameArray count] >= 3) {
        [oneBIE setDisplayName:[logicNameArray objectAtIndex:2]];
    }else{
        [oneBIE setDisplayName:[currentService name]];
    }

    NSString *role = [logicNameArray objectAtIndex:1];
    if([role caseInsensitiveCompare:@"master"] == NSOrderedSame){
        [oneBIE setRole:[BonjourInfoRoleEntity MASTER]];
    }else if([role caseInsensitiveCompare:@"slave"] == NSOrderedSame){
        [oneBIE setRole:[BonjourInfoRoleEntity SLAVE]];
    }else{
        [oneBIE setRole:[BonjourInfoRoleEntity RAW]];
    }

    BOOL wasFound = NO;
    for (NSUInteger i=0; i< [bonjourNameList_  count]; i++) {
        BonjourInfoEntity *oneListBIE = [bonjourNameList_ objectAtIndex:i];
        
        if([[oneListBIE serverName]caseInsensitiveCompare:[oneBIE serverName]] == NSOrderedSame &&
           [oneListBIE role] == [oneBIE role]
        ){
            if(!isFound){
                [bonjourNameList_ removeObjectAtIndex:i];
            }

            wasFound = true;
            break;
        }
    }

    if(!wasFound &&
       isFound
    ){
        [bonjourNameList_ addObject:oneBIE];
    }

    //NSLog(@"bonjournamelist:xx:%@", bonjourNameList_);
    
}


-(void) netService: (NSNetService *) sender
     didNotResolve:(NSDictionary *)errorDict
{
    NSLog(@"didNotResolve:%@", [sender name]);
}

@end