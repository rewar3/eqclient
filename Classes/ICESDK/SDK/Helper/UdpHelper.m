//
//  UdpHelper.m
//  iOS
//
//  Created by hanshu on 11/1/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "UdpHelper.h"
@implementation UdpInfoRoleEntity
+(NSUInteger)MASTER_UDP{
    return 1;
}
+(NSUInteger)SLAVE_UDP{
    return 2;
}
+(NSUInteger)RAW_UDP{
    return 3;
}
@end


@implementation UdpInfoEntity
@synthesize displayName=displayName_;
@synthesize role=role_;
@synthesize ip=ip_;
@synthesize expireTime=expireTime_;
@synthesize loginMethod=loginMethod_;
@end

@implementation UdpHelper
//@synthesize udpNameList=udpNameList_;
@synthesize threadRoundFindICE = threadRoundFindICE_;

-(id)init
{
    if (self = [super init]) {
        udpNameList_ = [[NSMutableArray alloc]init];
        struct sockaddr_in sin;

        sockRecver = socket(PF_INET, SOCK_DGRAM, 0);
        int set = 1;
        setsockopt(sockRecver, SOL_SOCKET, SO_REUSEADDR, (const void*)&set, sizeof(int));
        setsockopt(sockRecver, SOL_SOCKET, SO_REUSEPORT, (const void*)&set, sizeof(int));

        sin.sin_family = PF_INET;
        sin.sin_port = htons([UdpHelper PORT]);
        sin.sin_addr.s_addr = htonl(INADDR_ANY);

        if(bind(sockRecver,(struct sockaddr*)&sin,sizeof(struct sockaddr)) != 0)
        {
            perror("bind");
            sockRecver = -1;
        }

        //设置非阻塞
        unsigned long noneblock = fcntl(sockRecver, F_GETFL, NULL);
        noneblock |= O_NONBLOCK;
        noneblock = fcntl(sockRecver, F_SETFL, noneblock);
        if (noneblock == -1) {
            perror("fcntl");
            sockRecver = -1;
        }


        //[self performSelectorInBackground:@selector(roundFindICE) withObject:nil];

        threadRoundFindICE_ = [[NSThread alloc]initWithTarget:self selector:@selector(roundFindICE) object:nil];
        [threadRoundFindICE_ setName:@"轮询接收 UDP"];
    }

    return self;
}

-(void)startScan
{
    if ([threadRoundFindICE_ isExecuting]) {
        return;
    }

    [udpNameList_ removeAllObjects];
    threadRoundFindICE_ = [[NSThread alloc]initWithTarget:self selector:@selector(roundFindICE) object:nil];
    [threadRoundFindICE_ setName:@"轮询接收 UDP"];
    [threadRoundFindICE_ start];
}

-(void)stopScan
{
    if ([threadRoundFindICE_ isExecuting]) {
        [threadRoundFindICE_ cancel];
    }

#ifdef ICE_DEBUG
    NSLog(@"stopScan:%d", [threadRoundFindICE_ isCancelled]);
#endif
    [udpNameList_ removeAllObjects];
    [NSThread exit];
}

-(NSMutableArray*)getUdpNameList{
    return udpNameList_;
}

-(BOOL)_roundFindICE_Adapter
{
    struct sockaddr_in saClient;
    int size;
    char buff[256];
    char *ipAddr;
    BOOL isFound;
    __weak UdpInfoEntity *oneUdpInfo;
    const char *oneIpAddr;

    if (sockRecver <= 0) {
#ifdef ICE_DEBUG
        NSLog(@"初始化 udp 出错");
#endif
        return false;
    }

    NSUInteger currTimestamp ;
    currTimestamp = time(NULL);
    for (NSUInteger i=0; i<[udpNameList_ count]; i++) {
        if ([[udpNameList_ objectAtIndex:i]expireTime] < currTimestamp) {
            [udpNameList_ removeObjectAtIndex:i];
        }
    }

    bzero(buff, 256);
    size = sizeof(saClient);
#ifdef ICE_DEBUG
    //NSLog(@"begin to receive\n");
#endif
    //cc3_master_hanshuDevice

    if (recvfrom(sockRecver,buff,256,0,(struct sockaddr *)&saClient,(unsigned int*)&size) == -1){
        //非阻塞，没收到
        if (errno == EAGAIN) {
            sleep(1);
            return true;
        }else{
#ifdef ICE_DEBUG
            NSLog(@"UDP receive error");
#endif
        }
        return true;
    }

    ipAddr = inet_ntoa(saClient.sin_addr);
    isFound = NO;
    for (NSUInteger i=0; i<[udpNameList_ count]; i++) {
        oneUdpInfo = [udpNameList_ objectAtIndex:i];
        if(oneUdpInfo == NULL){
            continue;
        }

        oneIpAddr = [[oneUdpInfo ip]UTF8String];

        if (strcmp(oneIpAddr, (const char*)ipAddr) == 0) {
            isFound = YES;

            [[udpNameList_ objectAtIndex:i]setExpireTime:currTimestamp+10];
            break;
        }
    }

    if (!isFound) {
        NSString *rawBroadtext;
        NSArray *broadInfoArray;

        rawBroadtext = [[NSString alloc]initWithCString:buff encoding:NSUTF8StringEncoding];
        broadInfoArray = [rawBroadtext componentsSeparatedByString:@"_"];//cc3_master_hanshuDevice_passowrds

        if([broadInfoArray count] < 4 ||
           [[broadInfoArray objectAtIndex:0]caseInsensitiveCompare:@"cc3"] != NSOrderedSame
           ){
            broadInfoArray = nil;
            rawBroadtext = nil;

            return true;
        }


        UdpInfoEntity *newUdpInfo = [[UdpInfoEntity alloc]init];
        [newUdpInfo setDisplayName:[broadInfoArray objectAtIndex:2]];

        if ([[broadInfoArray objectAtIndex:1]caseInsensitiveCompare:@"master"] == NSOrderedSame) {
            [newUdpInfo setRole:[UdpInfoRoleEntity MASTER_UDP]];
        }else if ([[broadInfoArray objectAtIndex:1]caseInsensitiveCompare:@"slave"] == NSOrderedSame){
            [newUdpInfo setRole:[UdpInfoRoleEntity SLAVE_UDP]];
        }else{
            [newUdpInfo setRole:[UdpInfoRoleEntity RAW_UDP]];
        }

        if ([[broadInfoArray objectAtIndex:3]caseInsensitiveCompare:[ICELoginMethodEntity PASSWORD]] == NSOrderedSame) {
            [newUdpInfo setLoginMethod:[ICELoginMethodEntity PASSWORD]];
        }else if ([[broadInfoArray objectAtIndex:3]caseInsensitiveCompare:[ICELoginMethodEntity REGISTRY]] == NSOrderedSame) {
            [newUdpInfo setLoginMethod:[ICELoginMethodEntity REGISTRY]];
        }else{
            [newUdpInfo setLoginMethod:[ICELoginMethodEntity DEFAULT]];
        }

        NSUInteger expireT = currTimestamp+10;
        [newUdpInfo setIp:[[NSString alloc]initWithCString:ipAddr encoding:NSUTF8StringEncoding]];
        [newUdpInfo setExpireTime:expireT];

        [udpNameList_ addObject:newUdpInfo];

        newUdpInfo = nil;
        broadInfoArray = nil;
        rawBroadtext = nil;
    }

    return true;
}

-(void)roundFindICE
{
    while (![threadRoundFindICE_ isCancelled])
    {
        struct sockaddr_in saClient;
        int size;
        char buff[256];
        char *ipAddr;
        BOOL isFound;
        __weak UdpInfoEntity *oneUdpInfo;
        const char *oneIpAddr;
        
        if (sockRecver <= 0) {
#ifdef ICE_DEBUG
            NSLog(@"初始化 udp 出错");
#endif
            continue;
        }
        
        NSUInteger currTimestamp ;
        currTimestamp = time(NULL);
        for (NSUInteger i=0; i<[udpNameList_ count]; i++) {
            if ([[udpNameList_ objectAtIndex:i]expireTime] < currTimestamp) {
                [udpNameList_ removeObjectAtIndex:i];
            }
        }
        
        bzero(buff, 256);
        size = sizeof(saClient);
#ifdef ICE_DEBUG
        //NSLog(@"begin to receive\n");
#endif
        //cc3_master_hanshuDevice
        
        if (recvfrom(sockRecver,buff,256,0,(struct sockaddr *)&saClient,(unsigned int*)&size) == -1){
            //非阻塞，没收到
            if (errno == EAGAIN) {
                sleep(1);
                continue;
            }else{
#ifdef ICE_DEBUG
                NSLog(@"UDP receive error");
#endif
            }
            continue;
        }
        
        ipAddr = inet_ntoa(saClient.sin_addr);
        isFound = NO;
        for (NSUInteger i=0; i<[udpNameList_ count]; i++) {
            oneUdpInfo = [udpNameList_ objectAtIndex:i];
            if(oneUdpInfo == NULL){
                continue;
            }
            
            oneIpAddr = [[oneUdpInfo ip]UTF8String];
            
            if (strcmp(oneIpAddr, (const char*)ipAddr) == 0) {
                isFound = YES;
                
                [[udpNameList_ objectAtIndex:i]setExpireTime:currTimestamp+10];
                break;
            }
        }
        
        if (!isFound) {
            NSString *rawBroadtext;
            NSArray *broadInfoArray;
            
            rawBroadtext = [[NSString alloc]initWithCString:buff encoding:NSUTF8StringEncoding];
            broadInfoArray = [rawBroadtext componentsSeparatedByString:@"_"];//cc3_master_hanshuDevice_passowrds
            
            if([broadInfoArray count] < 4 ||
               [[broadInfoArray objectAtIndex:0]caseInsensitiveCompare:@"cc3"] != NSOrderedSame
               ){
                broadInfoArray = nil;
                rawBroadtext = nil;
                
                continue;
            }
            
            
            UdpInfoEntity *newUdpInfo = [[UdpInfoEntity alloc]init];
            [newUdpInfo setDisplayName:[broadInfoArray objectAtIndex:2]];
            
            if ([[broadInfoArray objectAtIndex:1]caseInsensitiveCompare:@"master"] == NSOrderedSame) {
                [newUdpInfo setRole:[UdpInfoRoleEntity MASTER_UDP]];
            }else if ([[broadInfoArray objectAtIndex:1]caseInsensitiveCompare:@"slave"] == NSOrderedSame){
                [newUdpInfo setRole:[UdpInfoRoleEntity SLAVE_UDP]];
            }else{
                [newUdpInfo setRole:[UdpInfoRoleEntity RAW_UDP]];
            }
            
            if ([[broadInfoArray objectAtIndex:3]caseInsensitiveCompare:[ICELoginMethodEntity PASSWORD]] == NSOrderedSame) {
                [newUdpInfo setLoginMethod:[ICELoginMethodEntity PASSWORD]];
            }else if ([[broadInfoArray objectAtIndex:3]caseInsensitiveCompare:[ICELoginMethodEntity REGISTRY]] == NSOrderedSame) {
                [newUdpInfo setLoginMethod:[ICELoginMethodEntity REGISTRY]];
            }else{
                [newUdpInfo setLoginMethod:[ICELoginMethodEntity DEFAULT]];
            }
            
            NSUInteger expireT = currTimestamp+10;
            [newUdpInfo setIp:[[NSString alloc]initWithCString:ipAddr encoding:NSUTF8StringEncoding]];
            [newUdpInfo setExpireTime:expireT];
            
            [udpNameList_ addObject:newUdpInfo];
            
            newUdpInfo = nil;
            broadInfoArray = nil;
            rawBroadtext = nil;
        }
        
        sleep(1);
    }
}


+(NSInteger)PORT{
    return 8883;
}
@end
