//
//  BLEHelper.m
//  iOS
//
//  Created by hanshu on 3/7/13.
//  Copyright (c) 2013 hanshu. All rights reserved.
//

#import "BLEHelper.h"

@implementation BLEHelper
@synthesize delegate;
@synthesize nearRSSI=nearRSSI_;
@synthesize leaveRSSI=leaveRSSI_;
-(id)init
{
    if(self = [super init]){
        ccm = [[CBCentralManager alloc]initWithDelegate:self queue:nil];
        actPeripheral = nil;//[[CBPeripheral alloc]init];
        blePerpheral = [[NSMutableDictionary alloc]init];
        foundPerpheral = [[NSMutableArray alloc]init];
        //NSLog(@"ccm:%d", [ccm state]);
        isContinue = 0;
        isNotify = NO;
        nearestBLEName = nil;
        lastNearestBLEName = nil;
        minRSSI = [[NSNumber alloc]initWithInt:-100];
        autoRestart = NO;
        nearRSSI_ = -60;//[[NSNumber alloc]initWithInt:-50];
        leaveRSSI_ = -75;//
        isWrongEqipment = NO;
        isLeave = YES;
        nearestBLE = [[BLEPeripheralEntity alloc]init];
    }

    return self;
}


- (NSString*) centralManagerStateToString: (int)state{
    switch(state) {
        case CBCentralManagerStateUnknown:
            return @"未知错误 (CBCentralManagerStateUnknown)";
        case CBCentralManagerStateResetting:
            return @"充值 (CBCentralManagerStateResetting)";
        case CBCentralManagerStateUnsupported:
            return @"系统不支持 (CBCentralManagerStateUnsupported)";
        case CBCentralManagerStateUnauthorized:
            return @"状态未授权 (CBCentralManagerStateUnauthorized)";
        case CBCentralManagerStatePoweredOff:
            return @"BLE 设备没供电 (CBCentralManagerStatePoweredOff)";
        case CBCentralManagerStatePoweredOn:
            return @"状态好 (CBCentralManagerStatePoweredOn)";
        default:
            return @"未知状态";
    }
    return @"未知状态";
}


- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
#ifdef ICE_DEBUG
    NSLog(@"第一步：蓝牙开启：%@",[self centralManagerStateToString:[central state]]);
#endif
    if ([central state] == CBCentralManagerStateUnsupported ||
        [central state] == CBCentralManagerStateUnauthorized 
    ) {
        isWrongEqipment = YES;
        return;
    }

    isWrongEqipment = NO;
    if ([central state] != CBCentralManagerStatePoweredOn) {
        [self stopScanBLE];
        autoRestart = YES;
    }else{
        if (autoRestart) {
            [self starScanBLE];//:10];
        }
    }
    //[self findBLEPeripherals:5];
}


/*!
 *  @method findBLEPeripherals:
 *
 *  @param timeout timeout in seconds to search for BLE peripherals
 *
 *  @return 0 (Success), -1 (Fault)
 *
 *  @discussion findBLEPeripherals searches for BLE peripherals and sets a timeout when scanning is stopped
 *
 */
- (int) findBLEPeripherals:(NSTimeInterval) timeout {
    if ([ccm state]  != CBCentralManagerStatePoweredOn){
#ifdef ICE_DEBUG
        NSLog(@"需要相应设备\r\n");
        NSLog(@"状态 = %d (%@)\r\n",[ccm state],[self centralManagerStateToString:[ccm state]]);
#endif
        if([ccm state] == CBCentralManagerStateUnsupported){
            isWrongEqipment = YES;
            [delegate wrongBLEEquipment];
        }

        return -1;
    }

    [blePerpheral removeAllObjects];
    [foundPerpheral removeAllObjects];
    isContinue = 0;
    isNotify = NO;

    //[NSTimer scheduledTimerWithTimeInterval:timeout target:self selector:@selector(scanTimer:) userInfo:nil repeats:NO];

    [ccm scanForPeripheralsWithServices:nil options:0]; // Start scanning

    return 0; // Started scanning OK !
}

-(BOOL)hasPeripheralUUID:(NSString*)uuid{
    NSUInteger c = [foundPerpheral count];

    BOOL has = NO;
    for (NSUInteger i=0; i<c; i++) {
        if([[foundPerpheral objectAtIndex:i]isEqualToString:uuid]){
            has = YES;
            break;
        }
    }

    return has;
}

/**
 * @param 寻找最大的 RSSI 蓝牙设备的 UUID
 */
-(BLEPeripheralEntity*)nearestPerpheral
{
    if ([blePerpheral count]==0) {
        [delegate foundNothindPeripheral];
        return nil;
    }

    NSNumber *maxRssi = minRSSI;

    NSEnumerator *keyEnumerator = [blePerpheral keyEnumerator];
    BLEPeripheralEntity *maxBLE = nil;
    NSString *uuid;

    while ((uuid = [keyEnumerator nextObject])) {
        [delegate scanIt:[[blePerpheral objectForKey:uuid]name] with:[ (BLEPeripheralEntity*)[blePerpheral objectForKey:uuid]rssi]];
#ifdef ICE_DEBUG
        NSLog(@"最大的名字 rssi:%@ :%@", [[blePerpheral objectForKey:uuid]rssi], [[blePerpheral objectForKey:uuid]name]);
#endif
        if (([[(BLEPeripheralEntity*)[blePerpheral objectForKey:uuid]rssi]floatValue] > [maxRssi floatValue]) &&
            [[[blePerpheral objectForKey:uuid]name]hasPrefix:@"ICE_"]
        ) {
            maxRssi = [(BLEPeripheralEntity*)[blePerpheral objectForKey:uuid]rssi];
            maxBLE = [blePerpheral objectForKey:uuid];
#ifdef ICE_DEBUG
            NSLog(@"有最大啊有最大");
#endif
        }

        //[[blePerpheral objectForKey:uuid]setRssi:minRSSI];
        //NSLog(@"blePerpheral:%@", [[blePerpheral objectForKey:uuid]rssi]);
    }
    

//#ifdef ICE_DEBUG
    NSLog(@"maxBLE:%@", maxBLE);
//#endif
    return maxBLE;
}

/**
 * @param 检查最近的蓝牙设备
 */
-(void)checkNearestPerpheral
{
    BLEPeripheralEntity* nearestPeripheral = [self nearestPerpheral];
    nearestBLEName = [nearestPeripheral name];
    
    if (lastNearestBLEName == nil ||
        ![lastNearestBLEName isEqualToString:nearestBLEName]
    ) {
#ifdef ICE_DEBUG
        NSLog(@"通知最大");
#endif
        [delegate nearestPeripheral:nearestPeripheral];
        lastNearestBLEName = nearestBLEName;
        
    }else{

#ifdef ICE_DEBUG
        NSLog(@"没有最大");
#endif
    }

}


/*!
 *  @method scanTimer:
 *
 *  @param timer Backpointer to timer
 *
 *  @discussion scanTimer is called when findBLEPeripherals has timed out, it stops the CentralManager from scanning further and prints out information about known peripherals
 *
 */
- (void) scanTimer:(NSTimer *)timer {
    [self stopScanBLE];
#ifdef ICE_DEBUG
    NSLog(@"Stopped Scanning\r\n");
#endif
    if (isNotify){
        return;
    }
    isNotify = YES;


    if ([blePerpheral count]==0) {
        [delegate foundNothindPeripheral];
        return;
    }

    
    BLEPeripheralEntity *maxBLE = [self nearestPerpheral];
    
    if (maxBLE == nil) {
        [delegate foundNothindPeripheral];
    }
#ifdef ICE_DEBUG
    NSLog(@"maxBLE:%@", maxBLE);
#endif
    [delegate nearestPeripheral:maxBLE];
}



-(void)starScanBLE//:(NSTimeInterval) timeout
{
    if (isWrongEqipment) {
        [delegate wrongBLEEquipment];
        return;
    }

    [self stopScanBLE];
    autoRestart = YES;
    isLeave = YES;

    [self findBLEPeripherals:10];
}

-(void)stopScanBLE
{
    if (isWrongEqipment) {
        [delegate wrongBLEEquipment];
        return;
    }

    if(actPeripheral != nil &&
       [actPeripheral isConnected]
       ){
        [ccm cancelPeripheralConnection:actPeripheral];
    }

    [ccm stopScan];
    isNotify = YES;
    nearestBLEName = nil;
    lastNearestBLEName = nil;
    autoRestart = NO;
}

-(void)updatePeripheralInfo:(NSString*)ICEID
{
    [delegate updateICEInfoByICEName:ICEID];
}

/*
 *  @method compareCBUUID
 *
 *  @param UUID1 UUID 1 to compare
 *  @param UUID2 UUID 2 to compare
 *
 *  @returns 1 (equal) 0 (not equal)
 *
 *  @discussion compareCBUUID compares two CBUUID's to each other and returns 1 if they are equal and 0 if they are not
 *
 */

-(int) compareCBUUID:(CBUUID *) UUID1 UUID2:(CBUUID *)UUID2 {
    char b1[16];
    char b2[16];
    [UUID1.data getBytes:b1];
    [UUID2.data getBytes:b2];
    if (memcmp(b1, b2, UUID1.data.length) == 0)return 1;
    else return 0;
}


/*
 *  @method CBUUIDToString
 *
 *  @param UUID UUID to convert to string
 *
 *  @returns Pointer to a character buffer containing UUID in string representation
 *
 *  @discussion CBUUIDToString converts the data of a CBUUID class to a character pointer for easy printout using printf()
 *
 */
-(const char *) CBUUIDToString:(CBUUID *) UUID {
    return [[UUID.data description] cStringUsingEncoding:NSStringEncodingConversionAllowLossy];
}


/*
 *  @method getAllCharacteristicsFromKeyfob
 *
 *  @param p Peripheral to scan
 *
 *
 *  @discussion getAllCharacteristicsFromKeyfob starts a characteristics discovery on a peripheral
 *  pointed to by p
 *
 */
-(void) getAllCharacteristicsFromKeyfob:(CBPeripheral *)p{

}

/*
 *  @method UUIDToString
 *
 *  @param UUID UUID to convert to string
 *
 *  @returns Pointer to a character buffer containing UUID in string representation
 *
 *  @discussion UUIDToString converts the data of a CFUUIDRef class to a character pointer for easy printout using printf()
 *
 */
-(const char *) UUIDToString:(CFUUIDRef)UUID {
    if (!UUID) return "NULL";
    CFStringRef s = CFUUIDCreateString(NULL, UUID);
    return CFStringGetCStringPtr(s, 0);

}


/*
 *  @method UUIDSAreEqual:
 *
 *  @param u1 CFUUIDRef 1 to compare
 *  @param u2 CFUUIDRef 2 to compare
 *
 *  @returns 1 (equal) 0 (not equal)
 *
 *  @discussion compares two CFUUIDRef's
 *
 */

- (int) UUIDSAreEqual:(CFUUIDRef)u1 u2:(CFUUIDRef)u2 {
    CFUUIDBytes b1 = CFUUIDGetUUIDBytes(u1);
    CFUUIDBytes b2 = CFUUIDGetUUIDBytes(u2);
    if (memcmp(&b1, &b2, 16) == 0) {
        return 1;
    }
    else return 0;
}



//----------------------------------------------------------------------------------------------------
//
//
//
//
//CBCentralManagerDelegate protocol methods beneeth here
// Documented in CoreBluetooth documentation
//
//
//
//
//----------------------------------------------------------------------------------------------------

/*!
 *  @method centralManagerDidUpdateState:
 *
 *  @param central  The central manager whose state has changed.
 *
 *  @discussion     Invoked whenever the central manager's state has been updated. Commands should only be issued when the state is
 *                  <code>CBCentralManagerStatePoweredOn</code>. A state below <code>CBCentralManagerStatePoweredOn</code>
 *                  implies that scanning has stopped and any connected peripherals have been disconnected. If the state moves below
 *                  <code>CBCentralManagerStatePoweredOff</code>, all <code>CBPeripheral</code> objects obtained from this central
 *                  manager become invalid and must be retrieved or discovered again.
 *
 *  @see            state
 *
 */


/*!
 *  @method centralManager:didRetrievePeripherals:
 *
 *  @param central      The central manager providing this information.
 *  @param peripherals  A list of <code>CBPeripheral</code> objects.
 *
 *  @discussion         This method returns the result of a @link retrievePeripherals @/link call, with the peripheral(s) that the central manager was
 *                      able to match to the provided UUID(s).
 *
 */
- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals
{
    NSLog(@"BLE1");
}

/*!
 *  @method centralManager:didRetrieveConnectedPeripherals:
 *
 *  @param central      The central manager providing this information.
 *  @param peripherals  A list of <code>CBPeripheral</code> objects representing all peripherals currently connected to the system.
 *
 *  @discussion         This method returns the result of a @link retrieveConnectedPeripherals @/link call.
 *
 */
- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals

{
    NSLog(@"BLE2");
}

/*!
 *  @method centralManager:didDiscoverPeripheral:advertisementData:RSSI:
 *
 *  @param central              The central manager providing this update.
 *  @param peripheral           A <code>CBPeripheral</code> object.
 *  @param advertisementData    A dictionary containing any advertisement and scan response data.
 *  @param RSSI                 The current RSSI of <i>peripheral</i>, in decibels.
 *
 *  @discussion                 This method is invoked while scanning, upon the discovery of <i>peripheral</i> by <i>central</i>. Any advertisement/scan response
 *                              data stored in <i>advertisementData</i> can be accessed via the <code>CBAdvertisementData</code> keys. A discovered peripheral must
 *                              be retained in order to use it; otherwise, it is assumed to not be of interest and will be cleaned up by the central manager.
 *
 *  @seealso                    CBAdvertisementData.h
 *
 */
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI

{
    if (isNotify) {
#ifdef ICE_DEBUG
        NSLog(@"通知有了，退出");
#endif
        return;
    }

#ifdef ICE_DEBUG
    NSLog(@"第二步：didDiscoverPeripheral：%@  [%@](rssi:%@)\r\n", [peripheral name],peripheral,RSSI);
#endif

    
     if ([advertisementData objectForKey:@"kCBAdvDataLocalName"] != nil) {
#ifdef ICE_DEBUG
         NSLog(@"%@", [advertisementData objectForKey:@"kCBAdvDataLocalName"]);
#endif
         [delegate scanIt:[advertisementData objectForKey:@"kCBAdvDataLocalName"] with:RSSI];
     }else{
#ifdef ICE_DEBUG
         NSLog(@"没得到值啊值");
#endif
         return;
     }

    NSString *uuid = [advertisementData objectForKey:@"kCBAdvDataLocalName"];
   
    //RSSI
#ifdef ICE_DEBUG
    NSLog(@"第二步：didDiscoverPeripheral：%@  [%@](rssi:%@)   %@\r\n", [peripheral name],uuid,RSSI, advertisementData);
#endif


    if ([uuid length] > 0) {
        if (nil == [blePerpheral objectForKey:uuid]) {
            BLEPeripheralEntity *oneBlePeripheral = [[BLEPeripheralEntity alloc]init];
            [[oneBlePeripheral scanGround]addObject:RSSI];
            
            [oneBlePeripheral setName:[advertisementData objectForKey:@"kCBAdvDataLocalName"]];
            [oneBlePeripheral setRssi:RSSI];
            [oneBlePeripheral setUuid:uuid];
            [oneBlePeripheral setNeighbourInfo:[[NeighbourEntity alloc]init]];
            NSArray *oneBleNameArray = [[oneBlePeripheral name]componentsSeparatedByString:@"_"];
            if ([oneBleNameArray count] != 3) {
                return;
            }

            //ICE3_中控号_天脉号
            NSString *iceId = [oneBleNameArray objectAtIndex:1];
            [oneBlePeripheral setICEID:iceId];
            [blePerpheral setValue:oneBlePeripheral forKey:uuid];
            [self performSelectorInBackground:@selector(updatePeripheralInfo:) withObject:iceId];
        }else{
            [[blePerpheral objectForKey:uuid]setName:[advertisementData objectForKey:@"kCBAdvDataLocalName"]];
            [[blePerpheral objectForKey:uuid]setRssi:RSSI];
            
#ifdef ICE_DEBUG
            NSLog(@"%@\tblePerpheral:%d nearRSSI:%d", uuid, [[[blePerpheral objectForKey:uuid]rssi]integerValue], nearRSSI_);
#endif
            if ([lastNearestBLEName isEqualToString:uuid] &&
                [[(BLEPeripheralEntity*)[blePerpheral objectForKey:uuid]rssi]integerValue] <= leaveRSSI_ &&
                !isLeave
            ){
                [delegate leaveBLE:[blePerpheral objectForKey:uuid]];
                isLeave = YES;
                lastNearestBLEName = nil;
            }

            if ([[(BLEPeripheralEntity*)[blePerpheral objectForKey:uuid]rssi]integerValue] >= nearRSSI_) {
                NSEnumerator *keyEnumberator = [blePerpheral keyEnumerator];
                NSString *oneUuid;
                NSInteger maxRSSI = [minRSSI integerValue];
                NSString *maxUuid = nil;

                while ((oneUuid = [keyEnumberator nextObject])) {
                    if ([[(BLEPeripheralEntity*)[blePerpheral objectForKey:oneUuid]rssi]integerValue] >= nearRSSI_) {
                        maxRSSI  = [[(BLEPeripheralEntity*)[blePerpheral objectForKey:oneUuid]rssi]integerValue];
                        maxUuid = oneUuid;
                    }

                    [[blePerpheral objectForKey:oneUuid]setRssi:minRSSI];
                }


                if (![lastNearestBLEName isEqualToString:maxUuid] &&
                    isLeave
                ){
                    [[blePerpheral objectForKey:maxUuid]setRssi:[[NSNumber alloc]initWithInteger:maxRSSI]];
                    lastNearestBLEName = [[blePerpheral objectForKey:maxUuid]name];
                    [delegate nearestPeripheral:[blePerpheral objectForKey:maxUuid]];
                    isLeave = NO;
                }
             }
        }
    }


//            [[[blePerpheral objectForKey:uuid] scanGround]addObject:RSSI];
//
//            if([[[blePerpheral objectForKey:uuid]scanGround]count] >= 3)
//            {
//                //排序
//                NSArray *orderedScanGround = [[[blePerpheral objectForKey:uuid]scanGround]sortedArrayUsingComparator:^(id obj1, id obj2){
//                    if ([obj1 integerValue] > [obj2 integerValue]) {
//                        return (NSComparisonResult)NSOrderedDescending;
//                    }
//
//
//                    if ([obj1 integerValue] < [obj2 integerValue]) {
//                        return (NSComparisonResult)NSOrderedAscending;
//                    }
//                    
//                    return (NSComparisonResult)NSOrderedSame;
//                }];
//                
//                //计算平均
//                NSInteger averageRSSI = 0;
//                NSInteger i;
//                for (i=1; (i+1) < [orderedScanGround count]; i++) {
//                    averageRSSI += [[orderedScanGround objectAtIndex:i]integerValue];
//#ifdef ICE_DEBUG
//                    NSLog(@"averageRSSI += %d ", [[orderedScanGround objectAtIndex:i]integerValue]);
//#endif
//                }
//
//                i -= 1;
//                averageRSSI /= i;
//#ifdef ICE_DEBUG
//                NSLog(@"%@\taverageRSSI:%d/%d >= nearRSSI:%d\tleaveRSSI:%d", [[blePerpheral objectForKey:uuid]name], averageRSSI,i,nearRSSI_,leaveRSSI_);
//#endif
//                
//                //移除所有
//                [[[blePerpheral objectForKey:uuid]scanGround]removeAllObjects];
//
//                if ([lastNearestBLEName isEqualToString:[[blePerpheral objectForKey:uuid]name]] &&
//                    averageRSSI <= leaveRSSI_ &&
//                    !isLeave
//                ){
//                    [delegate leaveBLE:[blePerpheral objectForKey:uuid]];
//                    isLeave = YES;
//                    lastNearestBLEName = nil;
//                }
//
//
//                if (averageRSSI >= nearRSSI_ &&
//                    ![lastNearestBLEName isEqualToString:[[blePerpheral objectForKey:uuid]name]] &&
//                    isLeave
//                ){
//                    lastNearestBLEName = [[blePerpheral objectForKey:uuid]name];
//                    [[blePerpheral objectForKey:uuid]setRssi:[[NSNumber alloc] initWithInteger:averageRSSI]];
//                    [delegate nearestPeripheral:[blePerpheral objectForKey:uuid]];
//                    isLeave = NO;
//                }
//
//            }
//            [[blePerpheral objectForKey:uuid]setUuid:uuid];
//        }
//    }


    [ccm stopScan];
    [ccm scanForPeripheralsWithServices:nil options:0];
    //[NSThread sleepForTimeInterval:1];
    return;

//
//    actPeripheral = peripheral;
//    NSString *uuid;
//    if ([peripheral UUID] != NULL) {
//        uuid = CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault,[peripheral UUID]));
//    }else{
//        uuid = @"";
//    }
//
//    //RSSI
//#ifdef ICE_DEBUG
//    NSLog(@"第二步：didDiscoverPeripheral：%@  [%@](rssi:%@)\r\n", [peripheral name],uuid,RSSI);
//#endif
//
//#ifdef ICE_DEBUG
//    NSLog(@"xxxxxxxx:%@\n%@\n",blePerpheral,foundPerpheral);
//#endif
//    if ([uuid length] > 0) {
//        if (nil == [blePerpheral objectForKey:uuid]) {
//            BLEPeripheralEntity *oneBlePeripheral = [[BLEPeripheralEntity alloc]init];
//            [oneBlePeripheral setName:[advertisementData objectForKey:@"kCBAdvDataLocalName"]];
//            [oneBlePeripheral setRssi:RSSI];
//            [oneBlePeripheral setUuid:uuid];
//
//            [blePerpheral setValue:oneBlePeripheral forKey:uuid];
//        }else{
//            [[blePerpheral objectForKey:uuid]setName:[advertisementData objectForKey:@"kCBAdvDataLocalName"]];
//            [[blePerpheral objectForKey:uuid]setRssi:RSSI];
//            //[[blePerpheral objectForKey:uuid]setUuid:uuid];
//        }
//    }
//
//
//    if(![self hasPeripheralUUID:uuid]){
//        if (isContinue == 0) {
//            isContinue++;
//        }else{
//#ifdef ICE_DEBUG
//            NSLog(@"有了一个有②");
//#endif
//        }
//
//        [ccm connectPeripheral:peripheral options:nil];
//
//    }else{
////        if ([foundPerpheral count] ==  [blePerpheral count]) {
////            [self scanTimer:0];
////            [ccm stopScan];
////            [ccm scanForPeripheralsWithServices:nil options:0]; // Start scanning
////        }
//
//#ifdef ICE_DEBUG
//        NSLog(@"有一个 UUID 被找到:%@", uuid);
//#endif
//        if ([foundPerpheral count] ==  [blePerpheral count]) {
//            [self checkNearestPerpheral];
//            [foundPerpheral removeAllObjects];
//            [blePerpheral removeAllObjects];
//        }
//        
//        [NSThread sleepForTimeInterval:1];
//        [ccm stopScan];
//        [ccm scanForPeripheralsWithServices:nil options:0];
//    }
}

/*!
 *  @method centralManager:didConnectPeripheral:
 *
 *  @param central      The central manager providing this information.
 *  @param peripheral   The <code>CBPeripheral</code> that has connected.
 *
 *  @discussion         This method is invoked when a connection initiated by @link connectPeripheral:options: @/link has succeeded.
 *
 */
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    actPeripheral = peripheral;

    NSString *uuid = CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault,[peripheral UUID]));
    if (![self hasPeripheralUUID:uuid]) {
        [foundPerpheral addObject:uuid];
    }
    
    //[self stopScanBLE];
    [ccm cancelPeripheralConnection:peripheral];
    [ccm stopScan];
    isContinue--;
    [ccm scanForPeripheralsWithServices:nil options:0];
    [delegate scanIt:@"第三步:didConnectPeripheral" with:[peripheral RSSI]];

#ifdef ICE_DEBUG
    NSLog(@"第三步:didConnectPeripheral:%@\tisContinue:%d", peripheral,isContinue);//, [peripheral _name], [peripheral _RSSI]);
#endif
    return;


    [peripheral setDelegate:self];
    [peripheral discoverServices:nil];
}

/*!
 *  @method centralManager:didFailToConnectPeripheral:error:
 *
 *  @param central      The central manager providing this information.
 *  @param peripheral   The <code>CBPeripheral</code> that has failed to connect.
 *  @param error        The cause of the failure.
 *
 *  @discussion         This method is invoked when a connection initiated by @link connectPeripheral:options: @/link has failed to complete. As connection attempts do not
 *                      timeout, the failure of a connection is atypical and usually indicative of a transient issue.
 *
 */
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
#ifdef ICE_DEBUG
    NSLog(@"BLE5");
#endif
}

/*!
 *  @method centralManager:didDisconnectPeripheral:error:
 *
 *  @param central      The central manager providing this information.
 *  @param peripheral   The <code>CBPeripheral</code> that has disconnected.
 *  @param error        If an error occurred, the cause of the failure.
 *
 *  @discussion         This method is invoked upon the disconnection of a peripheral that was connected by @link connectPeripheral:options: @/link. If the disconnection
 *                      was not initiated by @link cancelPeripheralConnection @/link, the cause will be detailed in the <i>error</i> parameter. Once this method has been
 *                      called, no more methods will be invoked on <i>peripheral</i>'s <code>CBPeripheralDelegate</code>.
 *
 */
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
#ifdef ICE_DEBUG
    NSLog(@"断了个断，重新来:%@\t%@\t:%d",peripheral, error, isContinue);
#endif
    [delegate scanIt:@"x" with:[peripheral RSSI]];

    NSString *uuid = CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault,[peripheral UUID]));
    if (![self hasPeripheralUUID:uuid]) {
        [foundPerpheral addObject:uuid];
    }



    [ccm cancelPeripheralConnection:peripheral];
    [ccm stopScan];
    isContinue--;
    [ccm scanForPeripheralsWithServices:nil options:0];
    return;


}

//----------------------------------------------------------------------------------------------------
//
//
//
//
//
//CBPeripheralDelegate protocol methods beneeth here
//
//
//
//
//
//----------------------------------------------------------------------------------------------------


/*
 *  @method didDiscoverCharacteristicsForService
 *
 *  @param peripheral Pheripheral that got updated
 *  @param service Service that characteristics where found on
 *  @error error Error message if something went wrong
 *
 *  @discussion didDiscoverCharacteristicsForService is called when CoreBluetooth has discovered
 *  characteristics on a service, on a peripheral after the discoverCharacteristics routine has been called on the service
 *
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    isContinue--;
    actPeripheral = peripheral;

    if (!error) {
#ifdef ICE_DEBUG
        NSLog(@"第五步:didDiscoverCharacteristicsForService 找到服务：%s",[self CBUUIDToString:service.UUID]);
#endif
        [delegate scanIt:@"第五步:didDiscoverCharacteristicsForService 找到服务" with:[peripheral RSSI]];
        NSString *uuid = CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault,[peripheral UUID]));
        [foundPerpheral addObject:uuid];

        [ccm stopScan];
        [ccm scanForPeripheralsWithServices:nil options:0]; // Start scanning

        //for(int i=0; i < [[service characteristics]count]; i++) {
        //CBCharacteristic *c = [[service characteristics]objectAtIndex:0];
        //NSLog(@"\t要发现特征 %s",[self CBUUIDToString:c.UUID]);
        //[delegate scanIt:@"\t要发现特征" with:[peripheral RSSI]];

        //CBService *s = [[peripheral services] objectAtIndex:[[peripheral services]count] - 1];
        //if([self compareCBUUID:service.UUID UUID2:s.UUID]) {
        //    NSLog(@"\t\t发现特征完毕 %s",[self CBUUIDToString:c.UUID]);
        //    [delegate scanIt:@"\t\t发现特征完毕" with:[peripheral RSSI]];
        //}
        //}
    }
    else {
#ifdef ICE_DEBUG
        NSLog(@"Characteristic discorvery unsuccessfull !\r\n");
#endif
    }

}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {

}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error {
}

/*
 *  @method didDiscoverServices
 *
 *  @param peripheral Pheripheral that got updated
 *  @error error Error message if something went wrong
 *
 *  @discussion didDiscoverServices is called when CoreBluetooth has discovered services on a
 *  peripheral after the discoverServices routine has been called on the peripheral
 *
 */

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    actPeripheral = peripheral;
#ifdef ICE_DEBUG
    NSLog(@"第四步:didDiscoverServices:%@", peripheral);
#endif
    [delegate scanIt:@"第四步:didDiscoverServices" with:[peripheral RSSI]];

    NSString *uuid = CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault,[peripheral UUID]));
    [foundPerpheral addObject:uuid];
    [self stopScanBLE];
    isContinue--;
    [ccm scanForPeripheralsWithServices:nil options:0];
    return;

    if (!error) {
        //for (int i=0; i < [[peripheral services]count]; i++) {
        CBService *s = [[peripheral services] objectAtIndex:0];
#ifdef ICE_DEBUG
        NSLog(@"\t 要发现服务 %s",[self CBUUIDToString:[s UUID]]);
#endif
        //[delegate scanIt:@"\t 要发现服务" with:[peripheral RSSI]];
        [peripheral discoverCharacteristics:nil forService:s];
        //}
    }
    else {
#ifdef ICE_DEBUG
        NSLog(@"Service discovery was unsuccessfull !\r\n");
#endif
        isContinue = 0;
    }
}

/*
 *  @method didUpdateNotificationStateForCharacteristic
 *
 *  @param peripheral Pheripheral that got updated
 *  @param characteristic Characteristic that got updated
 *  @error error Error message if something went wrong
 *
 *  @discussion didUpdateNotificationStateForCharacteristic is called when CoreBluetooth has updated a
 *  notification state for a characteristic
 *
 */

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (!error) {
        //printf("Updated notification state for characteristic with UUID %s on service with  UUID %s on peripheral with UUID %s\r\n",[self CBUUIDToString:characteristic.UUID],[self CBUUIDToString:characteristic.service.UUID],[self UUIDToString:peripheral.UUID]);
    }
    else {
        //printf("Error in setting notification state for characteristic with UUID %s on service with  UUID %s on peripheral with UUID %s\r\n",[self CBUUIDToString:characteristic.UUID],[self CBUUIDToString:characteristic.service.UUID],[self UUIDToString:peripheral.UUID]);
        //printf("Error code was %s\r\n",[[error description] cStringUsingEncoding:NSStringEncodingConversionAllowLossy]);
    }

}

/*
 *  @method didUpdateValueForCharacteristic
 *
 *  @param peripheral Pheripheral that got updated
 *  @param characteristic Characteristic that got updated
 *  @error error Error message if something went wrong
 *
 *  @discussion didUpdateValueForCharacteristic is called when CoreBluetooth has updated a
 *  characteristic for a peripheral. All reads and notifications come here to be processed.
 *
 */

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"BLE1.1");
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"BLE1.2");
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"BLE1.3");
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"BLE1.4");
}

- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"BLE1.5");
}


@end
