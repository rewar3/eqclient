//
//  SocketHelper.h
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <signal.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

#import "ICEDebug.h"

@interface SocketHelper : NSObject
{
    int sockfd;
    struct sockaddr_in servAddr;
    char *buf;// = (char *)malloc(65536);
}

-(id)initWithIP:(NSString*)ip port:(NSUInteger)port;//连接到一个 host
-(BOOL)isConnected;
-(BOOL)send:(NSString*)msg;
-(NSString*)recv;
-(void)disconnect;
@end
