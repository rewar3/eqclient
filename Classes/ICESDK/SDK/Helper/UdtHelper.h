//
//  UdtHelper.h
//  iOS
//
//  Created by hanshu on 4/12/13.
//  Copyright (c) 2013 hanshu. All rights reserved.
//
#include "ICEDebug.h"

#ifndef __iOS__UdtHelper__
#define __iOS__UdtHelper__

#ifndef TCP_CONNECTION


#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <netdb.h>

#include <iostream>
#include "udt.h"

#include "arpa/inet.h"


class UdtHelper
{
public:
        UdtHelper(const char* ip,unsigned int port);
        bool isConnected();
        bool send(const char*msg);
        const char* recv();
        void disconnect();
        ~UdtHelper();

private:
        UDTSOCKET sockHandle;
        timeval tv;
        bool isConnect;
        char buff[65536];
        size_t nleft;
        int nwr;

};

#endif

#endif /* defined(__iOS__UdtHelper__) */
