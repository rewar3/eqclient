//
//  UdpHelper.h
//  iOS
//
//  Created by hanshu on 11/1/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ICELoginMethodEntity.h"

#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<sys/socket.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include <arpa/inet.h>
#include<netdb.h>
#import "ICEDebug.h"

@interface UdpInfoRoleEntity:NSObject
+(NSUInteger)MASTER_UDP;
+(NSUInteger)SLAVE_UDP;
+(NSUInteger)RAW_UDP;
@end

@interface UdpInfoEntity:NSObject
{
    NSString *displayName_;//显示名
    NSUInteger role_;//角色
    NSString *ip_;//ip地址
    NSUInteger expireTime_;//过期时间
    NSString *loginMethod_;
}


@property(atomic, strong) NSString *displayName;
@property(atomic) NSUInteger role;
@property(atomic, strong)NSString *ip;
@property NSUInteger expireTime;
@property(atomic, strong)NSString *loginMethod;
@end

@interface UdpHelper:NSObject
{
    int sockRecver;
    NSMutableArray *udpNameList_;
    NSThread *threadRoundFindICE_;//轮训取的 xx
}
@property(atomic, strong)NSThread *threadRoundFindICE;

-(NSMutableArray*)getUdpNameList;
-(void)roundFindICE;
+(NSInteger)PORT;

-(void)startScan;
-(void)stopScan;
@end