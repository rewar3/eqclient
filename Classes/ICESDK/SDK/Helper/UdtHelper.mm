//
//  UdtHelper.cpp
//  iOS
//
//  Created by hanshu on 4/12/13.
//  Copyright (c) 2013 hanshu. All rights reserved.
//
#include "UdtHelper.h"

#ifndef TCP_CONNECTION

using namespace std;

UdtHelper::UdtHelper(const char* ip,unsigned int port)
{
    //ip = "10.0.1.80";
    //port = 9000;
    sockHandle = UDT::socket(AF_INET, SOCK_STREAM, 0);
    sockaddr_in servAddr;
    servAddr.sin_family = AF_INET;
    servAddr.sin_port = htons(port);
    inet_pton(AF_INET, ip, &servAddr.sin_addr);

    bzero(&(servAddr.sin_zero), 8);

    if (UDT::ERROR == UDT::connect(sockHandle, (sockaddr*)&servAddr, sizeof(servAddr))) {
#ifdef ICE_DEBUG
        cout << "\r\nconnect 错误 " <<ip<<"  "<<UDT::getlasterror().getErrorMessage() << endl;
#endif
        isConnect = false;
        return ;
    }

    isConnect = true;
}


bool UdtHelper::isConnected()
{
    return isConnect;
}

bool UdtHelper::send(const char*msg)
{

#ifdef ICE_DEBUG
    cout<<"send:"<<msg;
#endif

    nleft = strlen(msg);
    nwr = 0;

    while (nleft > 0) {
        nwr = UDT::send(sockHandle, msg, nleft, NULL);
        if(nwr == UDT::ERROR){

#ifdef ICE_DEBUG
            cout<< "没送abc成功" <<UDT::getlasterror().getErrorMessage() << endl;
#endif
            //[self disconnect];
            if (isConnect) {
                disconnect();
            }

            //throw exception();
            return false;
        }

        nleft -= nwr;
        msg += nwr;
    }
    
    return true;

}

const char* UdtHelper::recv()
{
    bzero(buff, 65536);

    nwr = UDT::recv(sockHandle, buff, 65535, NULL);

    if (nwr == UDT::ERROR) {
#ifdef ICE_DEBUG
        cout<< "没送123成功" <<UDT::getlasterror().getErrorMessage() << endl;
#endif
        if (isConnect) {
            disconnect();
        }
        
        return NULL;
    }


#ifdef ICE_DEBUG
    //printf(@"接收到:%s", buff);
#endif

    return buff;

}

void UdtHelper::disconnect()
{
    UDT::close(sockHandle);
    isConnect = false;
}

UdtHelper::~UdtHelper(){
    disconnect();
}
#endif