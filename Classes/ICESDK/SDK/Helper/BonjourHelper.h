//
//  BonjourHelper.h
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/types.h>
#import <sys/socket.h>
#include <arpa/inet.h>

#define CC3_NET_DOMAIN @"local."
#define CC3_NET_TYPE @"_cc3._tcp."
#define CC3_NAME_PREFIX @"cc3_"

@interface BonjourInfoRoleEntity:NSObject
+(NSUInteger)MASTER;
+(NSUInteger)SLAVE;
+(NSUInteger)RAW;
@end

@interface BonjourInfoEntity:NSObject
{
    NSString *fullName_;//全名
    NSString *hostName_;//主机名
    NSUInteger port_;//服务的端口
    NSString *serverName_;//服务名
    NSString *displayName_;//显示名
    NSUInteger role_;//角色
    NSString *hostIp_;//ip地址
}

@property(atomic, strong) NSString *fullName;
@property(atomic, strong) NSString *hostName;
@property(atomic) NSUInteger port;
@property(atomic, strong) NSString *serverName;
@property(atomic, strong) NSString *displayName;
@property(atomic) NSUInteger role;
@property(atomic, strong)NSString *hostIp;
@end

@interface BonjourHelper : NSObject<NSStreamDelegate,NSNetServiceBrowserDelegate,NSNetServiceDelegate>
{
    NSMutableArray *bonjourNameList_;
    BOOL isFound;
    NSNetServiceBrowser *browser;
}
@property(atomic,strong) NSMutableArray *bonjourNameList;
@end
