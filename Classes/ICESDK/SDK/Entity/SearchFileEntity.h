//
//  SearchFileEntity.h
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GroupTypeEntity.h"
#import "UserInfoEntity.h"
#import "ICEDebug.h"

typedef enum {
    SearchFileOrderDESC=0,
    SearchFileOrderASC=1
}SearchFileOrderMethod;

@interface SearchFileEntity : NSObject
{
    //搜索的 guid
    NSString *guid_;

    //标题包含
    NSString *titleContain_;

    //描述包含
    NSString *descContain_;

    //标签包含
    NSString *tag_;

    //在那个资源包中
    NSString *inPack_;

    //搜索方法
    SearchFileOrderMethod orderMethod_;

    //排序的栏
    NSString *orderColumn_;

    //开始时间
    NSString *beginTime_;

    //结束时间
    NSString *finishTime_;

    //提示权限组
    NSString *suggestGroupId_;

    //天脉号
    NSString *tvmid_;
}

@property(atomic, strong)NSString *guid;
@property(atomic, strong)NSString *titleContain;
@property(atomic, strong)NSString *descContain;
@property(atomic, strong)NSString *tag;
@property(atomic, strong)NSString *inPack;
@property(atomic)SearchFileOrderMethod orderMethod;
@property(atomic, strong)NSString *orderColumn;
@property(atomic, strong)NSString *beginTime;
@property(atomic, strong)NSString *finishTime;
@property(atomic, strong)NSString *suggestGroupId;
@property(atomic, strong)NSString *tvmid;
-(NSDictionary*)convertToHttpCond;
@end
