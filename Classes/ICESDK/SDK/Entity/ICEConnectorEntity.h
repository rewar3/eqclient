//
//  ICEConnectorEntity.h
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

@interface ICEConnectorEntity : NSObject
+(NSUInteger)SERVER_PORT;
+(unsigned int) SERVER_PORT_UDT;
@end
