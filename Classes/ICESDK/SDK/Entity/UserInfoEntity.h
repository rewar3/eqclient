//
//  UserInfoEntity.h
//  iOS
//
//  Created by hanshu on 10/16/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserTypeEntity.h"
#import "GroupTypeEntity.h"
#import "ICEDebug.h"

//用户信息的实体
@interface UserInfoEntity : NSObject
{
    NSString *name_;//用户名
    NSString *id_;//天脉号
    NSString *ip_;//用户本地 IP
    NSString *type_;//用户类型
    NSString *key_;//用户信息的 key
    NSString *value_;//用户信息的 value
}

@property(atomic, strong) NSString *name;
@property(atomic, strong) NSString *id;
@property(atomic, strong) NSString *ip;
@property(atomic, strong) NSString *type;
//初始化方法
-(id)init;
-(id)init: (NSString*) userType; //WithGroup:(NSString*)groupType;


-(NSString*)getPrivatePack;//得到私有包

+(void)setIceId:(NSString*)icesID;//设置中控ID
+(NSString*)iceId;//得到中控ID

+(void)setGroupId:(NSString*)groupsId;//设置组ID
+(NSString*)groupId;//得到组ID

+(void)setTvmId:(NSString*)tvmid;
+(NSString*)tvmId;

/**
 * @brief 根据天脉号搜用户信息
 */
+(NSString*)URL_SEARCH_USERINFO_TVMID;

/**
 * @brief 根据中控号设置用户信息
 */
+(NSString*)URL_SEARCH_USERINFO_ICEID;


@end
