//
//  SearchFileStatusEntity.h
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

typedef enum {
    fileThumbMethodResample = 0,
    fileThumbMethodCrop = 1
}fileThumbMethod;


@interface SearchFileDetailStatusEntity : NSObject
{
    //序列号，每个资源有一个不重复的，无意义
    NSUInteger id_;

    //guid
    NSString *guid_;

    //用户号
    NSString *userId_;

    //是否共享
    NSUInteger isShare_;

    //文件名
    NSString *filename_;

    //包名称
    NSString *packname_;

    //描述
    NSString *desc_;

    //标题
    NSString *title_;

    //标签
    NSString *tag_;

    //提交时间
    NSString *submit_date_;
}

@property(atomic) NSUInteger id;
@property(atomic, strong)NSString *guid;
@property(atomic, strong)NSString *userId;
@property(atomic, strong)NSString *filename;
@property(atomic, strong)NSString *packname;
@property(atomic, strong)NSString *desc;
@property(atomic, strong)NSString *title;
@property(atomic, strong)NSString *tag;
@property(atomic, strong)NSString *submit_date;

-(NSString*)getFileURI;//得到文件的 URI
-(NSString*)getThumbURI:(NSUInteger)width height:(NSUInteger)height;//得到缩略图
-(NSString*)getThumbURI:(NSUInteger)width height:(NSUInteger)height method:(fileThumbMethod)method;//得到缩略图
+(NSString*)STORAGE_PREFIX;
@end



@interface SearchFileStatusEntity : NSObject
{
    NSInteger code_;
    NSDictionary *msg_;
}

@property(atomic) NSInteger code;
@property(atomic, strong) NSDictionary *msg;

//-(id)initWithJSONData:(NSData*)jData;
+(NSInteger)FAILED_CODE;
+(NSUInteger)SUCC_CODE;
@end
