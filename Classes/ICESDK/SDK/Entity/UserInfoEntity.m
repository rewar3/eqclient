//
//  UserInfoEntity.m
//  iOS
//
//  Created by hanshu on 10/16/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "UserInfoEntity.h"

@implementation UserInfoEntity
static NSString *groupId_ = @"";
static NSString *iceId_   = @"";
static NSString *tvmid_ = @"";

@synthesize name=name_;
@synthesize id=id_;
@synthesize ip=ip_;
@synthesize type=type_;

-(id)init
{
    if((self = [super init]))
    {
        /*
        name_ = @"";
        id_ = @"";
        ip_ = @"";
        */
        type_ = [UserTypeEntity USER];
        [UserInfoEntity setGroupId:[GroupTypeEntity ANONYMOUS]];
    }

    return self;
}

-(id)init: (NSString*) userType
{
    if((self = [super init]))
    {
        type_ = userType;
        //[UserInfoEntity setGroupId:[GroupTypeEntity ANONYMOUS]];
    }

    return self;
}



-(NSString*)getPrivatePack
{
    NSString *privatePack = [[NSString alloc]initWithFormat:@"private_pack_%@", id_] ;
    return privatePack;
}


+(void)setIceId:(NSString*)icesID//设置中控ID
{
    iceId_ = icesID;
}
+(NSString*)iceId//得到中控ID
{
    return iceId_;
}

+(void)setGroupId:(NSString*)groupsId//设置组ID
{
    groupId_ = groupsId;
}
+(NSString*)groupId;//得到组ID
{
    return groupId_;
}
+(void)setTvmId:(NSString*)tvmid
{
    tvmid_ = tvmid;
}
+(NSString*)tvmId
{
    return tvmid_;
}

+(NSString*)URL_SEARCH_USERINFO_TVMID
{
    return @"ice3/search_userinfo.php?tvmid=";
}

+(NSString*)URL_SEARCH_USERINFO_ICEID
{
    return @"ice3/search_userinfo.php?iceid=";
}
@end
