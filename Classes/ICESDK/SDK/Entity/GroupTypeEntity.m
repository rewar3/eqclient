//
//  GroupTypeEntity.m
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "GroupTypeEntity.h"

@implementation GroupTypeEntity
+(NSString*)ANONYMOUS
{
    return @"1";
}

+(NSString*)PRIVILEGES
{
    return @"2";
}

+(NSString*)ADMINISTRATOR
{
    return @"3";
}

+(NSString*)ALL_CAN_SEE{
    return @"";
}

+(NSString*)CmdObjToAdministrator{
    return @"group-3";
}

+(NSString*)CmdObjToPrivilege{
    return @"group-2";
}

+(NSString*)CmdObjToAnonymous{
    return @"group-1";
}
@end
