//
//  StatusEntity.m
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "StatusEntity.h"

@implementation StatusEntity
@synthesize cmd = cmd_;
@synthesize status = status_;
@synthesize msg = msg_;

+(NSString *)OK//成功代码
{
    return @"0";
}

+(NSString *)FAIL//失败代码
{
    return @"-1";
}

+(NSString *)UNDELIVER//未抵达
{
    return @"-2";
}

+(NSString *)UNPASER//未解析
{
    return @"-3";
}


+(NSString *)ALREAD_ONLINE//已经在
{
    return @"-4";
}

+(NSString *)NNULL//空
{
    return @"-44";
}

-(id)initWithJSONData:(NSData*)jData;
{
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jData options:NSJSONReadingMutableContainers error:nil];

    StatusEntity *oneStatus =  [[StatusEntity alloc]init];
    if(jsonDict == nil){
        [oneStatus setCmd:[CommandTypeEntity NONE]];
        [oneStatus setStatus:[StatusEntity UNPASER]];
    }else{
        [oneStatus setCmd:[jsonDict objectForKey:@"cmd"]];
        [oneStatus setStatus:[jsonDict objectForKey:@"status"]];
        [oneStatus setMsg:[jsonDict objectForKey:@"msg"]];
    }

    return oneStatus;
}
@end
