//
//  SearchFileEntity.m
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "SearchFileEntity.h"

@implementation SearchFileEntity
@synthesize guid=guid_;
@synthesize titleContain=titleContain_;
@synthesize descContain=descContain_;
@synthesize tag=tag_;
@synthesize inPack=inPack_;
@synthesize orderColumn=orderColumn_;
@synthesize orderMethod=orderMethod_;
@synthesize beginTime=beginTime_;
@synthesize finishTime=finishTime_;
@synthesize tvmid=tvmid_;
@synthesize suggestGroupId=suggestGroupId_;
-(id)init
{
    if(self = [super init])
    {
        guid_=@"";
        titleContain_=@"";
        descContain_=@"";
        tag_=@"";
        inPack_=@"";
        orderColumn_=@"";
        orderMethod_ = SearchFileOrderASC;
        beginTime_=@"";
        finishTime_=@"";
        suggestGroupId_ = [GroupTypeEntity ALL_CAN_SEE];
    }

    return self;
}

-(NSDictionary*)convertToHttpCond{
    NSDictionary *postCond = [[NSDictionary alloc]init];

    [postCond setValue:guid_ forKey:@"guid"];
    [postCond setValue:titleContain_ forKey:@"title_contain"];
    [postCond setValue:descContain_ forKey:@"desc_contain"];
    [postCond setValue:tag_ forKey:@"tag"];
    [postCond setValue:inPack_ forKey:@"in_res_pack"];
    [postCond setValue:[UserInfoEntity tvmId] forKey:@"tvmid"];
    [postCond setValue:suggestGroupId_ forKey:@"suggest_groupid"];

    if(orderMethod_ == SearchFileOrderASC){
        [postCond setValue:@"1" forKey:@"is_asc"];
    }else{
        [postCond setValue:@"0" forKey:@"is_asc"];
    }

    if([orderColumn_ isEqualToString:@"id"] ||
       [orderColumn_ isEqualToString:@"title"] ||
       [orderColumn_ isEqualToString:@"submit_date"]
    ){
        [postCond setValue:orderColumn_ forKey:@"order_column"];
    }else{
        [postCond setValue:@"id" forKey:@"order_column"];
    }

    [postCond setValue:beginTime_ forKey:@"begin_time"];
    [postCond setValue:finishTime_ forKey:@"finish_time"];

    return postCond;
}
@end
