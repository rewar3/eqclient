//
//  ICEFileEntity.h
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

@interface ICEFileEntity : NSObject
+(NSString*)URL_GET_ALL_PACK;
+(NSString*)URL_UPLOAD_FILE;
+(NSString*)URL_SEARCH_FILE;
+(NSString*)URL_SANDBOX_SEARCH_FILE;
+(NSString*)URL_PERMISSION_IP_LIST;
+(NSString*)URL_SEARCH_PACK_EXTINFO;
+(NSString*)URL_REMOVE_PACK;
+(NSString*)URL_UPDATE_PACK_WEIGHT;
+(NSString*)URL_UPDATE_FILE_INFO;
+(NSString*)URL_RMEOVE_FILE;
@end
