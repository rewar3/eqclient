//
//  CommandTypeEntity.m
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "CommandTypeEntity.h"

@implementation CommandTypeEntity
+(NSString*)convertWithString:(NSString*)cmdStr{
    if ([cmdStr isEqualToString:[self GETMYNEIGHBOUR]]) {
        return [self GETMYNEIGHBOUR];
    }

    if ([cmdStr isEqualToString:[self VALUECHANGE]]) {
        return [self GETMYNEIGHBOUR];
    }
    
    if ([cmdStr isEqualToString:[self STARTOVER]]) {
        return [self STARTOVER];
    }

    if ([cmdStr isEqualToString:[self DELPACK]]) {
        return [self DELPACK];
    }

    if ([cmdStr isEqualToString:[self DELFILE]]) {
        return [self DELFILE];
    }
    
    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self GETONLINE]]){
        return [self GETONLINE];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self REGISTER]]){
        return [self REGISTER];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self RESET]]){
        return [self RESET];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self DOWN]]){
        return [self DOWN];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self UP]]){
        return [self UP];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self LEFT]]){
        return [self LEFT];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self RIGHT]]){
        return [self RIGHT];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self FIRST]]){
        return [self FIRST];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self LAST]]){
        return [self LAST];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self FORWARD]]){
        return [self FORWARD];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self BACKWARD]]){
        return [self BACKWARD];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self FUNCTION1]]){
        return [self FUNCTION1];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self FUNCTION2]]){
        return [self FUNCTION2];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self ECHO]]){
        return [self ECHO];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self NEWFILE]]){
        return [self NEWFILE];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self NEWPACK]]){
        return [self NEWPACK];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self GO]]){
        return [self GO];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self PUSH]]){
        return [self PUSH];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self PLAY]]){
        return [self PLAY];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self SYNC]]){
        return [self SYNC];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self CANCEL]]){
        return [self CANCEL];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self OK]]){
        return [self OK];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self ZOOMIN]]){
        return [self ZOOMIN];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self ZOOMOUT]]){
        return [self ZOOMOUT];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self STICKY]]){
        return [self STICKY];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self MARCO]]){
        return [self MARCO];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self EXT]]){
        return [self EXT];
    }
    
    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self CLEANALL]]){
        return [self CLEANALL];
    }
    
    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self TOUCH]]){
        return [self TOUCH];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self TALKTO]]){
        return [self TALKTO];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self NEARBY]]){
        return [self NEARBY];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self FORCE]]){
        return [self FORCE];
    }

    if(NSOrderedSame == [cmdStr caseInsensitiveCompare:[self LOCK]]){
        return [self LOCK];
    }

    return [self NONE];
}

+(NSString*)GETMYNEIGHBOUR{
    return @"getpermissionlist";
}
+(NSString*)GETONLINE{
    return @"getonline";
}
+(NSString*)REGISTER{
    return @"register";
}
+(NSString*)RESET{
    return @"reset";
}
+(NSString*)UP{
    return @"up";
}
+(NSString*)DOWN{
    return @"down";
}
+(NSString*)LEFT{
    return @"left";
}
+(NSString*)RIGHT{
    return @"right";
}
+(NSString*)FIRST{
    return @"first";
}
+(NSString*)LAST{
    return @"last";
}
+(NSString*)GO{
    return @"go";
}
+(NSString*)FORWARD;{
    return @"forward";
}
+(NSString*)BACKWARD{
    return @"backward";
}
+(NSString*)FUNCTION1{
    return @"function1";
}
+(NSString*)FUNCTION2{
    return @"function2";
}
+(NSString*)NONE{
    return @"none";
}
+(NSString*)ECHO{
    return @"echo";
}
+(NSString*)NEWFILE{
    return @"newfile";
}
+(NSString*)NEWPACK{
    return @"newpack";
}
+(NSString*)NNULL{
    return @"null";
}
+(NSString*)PLAY//play
{
    return @"play";
}
+(NSString*)PUSH//push
{
    return @"push";
}
+(NSString*)SYNC//sync
{
    return @"sync";
}
+(NSString*)CANCEL//
{
    return @"cancel";
}
+(NSString*)CACHE//
{
    return @"cache";
}
+(NSString*)OK
{
    return @"ok";
}
+(NSString*)ZOOMIN
{
    return @"zoomin";
}
+(NSString*)ZOOMOUT
{
    return @"zoomout";
}
+(NSString*)STICKY
{
    return @"sticky";
}
+(NSString*)MARCO
{
    return @"marco";
}
+(NSString*)EXT
{
    return @"ext";
}
+(NSString*)CLEANALL
{
    return @"cleanall";
}

+(NSString*)VALUECHANGE
{
    return @"valuechange";
}

+(NSString*)STARTOVER
{
    return @"startover";
}

+(NSString*)DELPACK
{
    return @"delpack";
}

+(NSString*)DELFILE
{
    return @"delfile";
}
+(NSString*)ICERESTART{
    return @"icerestart";
}
+(NSString*)TOUCH{
    return @"touch";
}
+(NSString*)TALKTO{
    return @"talkto";
}
+(NSString*)NEARBY{
    return @"nearby";
}
+(NSString*)LOCK{
    return @"lock";
}
+(NSString*)FORCE{
    return @"force";
}

+(NSString*)MENU
{
    return @"menu";
}

+(NSString*)HOME
{
    return @"home";
}

@end

