//
//  UserTypeEntity.h
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

@interface UserTypeEntity : NSObject
{

}
+(NSString*)USER;//得到用户类型
+(NSString*)DEVICE;//得到设备类型
@end
