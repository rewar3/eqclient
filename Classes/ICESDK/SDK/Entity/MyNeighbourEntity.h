//
//  MyNeighbourEntity.h
//  iOS
//
//  Created by hanshu on 1/14/13.
//  Copyright (c) 2013 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfoEntity.h"
#import "GroupTypeEntity.h"

@interface NeighbourEntity : NSObject
{
    NSString *iceId_;
    NSString *tvmId_;
    NSString *groupId_;
    NSString *userType_;
    NSString *CmdObjToMy_;
    NSString *ip_;
}

@property(atomic, strong) NSString *iceId;
@property(atomic, strong) NSString *tvmId;
@property(atomic, strong) NSString *groupId;
@property(atomic, strong) NSString *userType;
@property(atomic, strong) NSString *CmdObjToMy;
@property(atomic, strong) NSString *ip;
@end

@interface MyNeighboursEntity : NSObject
{
    NSArray *canSendArray_;
    NSArray *canReadArray_;
}

@property(atomic, strong) NSArray *canSendArray;
@property(atomic, strong) NSArray *canReadArray;

-(id) init:(NSString *) rawStr;
@end