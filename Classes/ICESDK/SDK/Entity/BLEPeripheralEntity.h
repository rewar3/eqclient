//
//  BLEPeripheralEntity.h
//  iOS
//
//  Created by hanshu on 3/7/13.
//  Copyright (c) 2013 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyNeighbourEntity.h"

@interface BLEPeripheralEntity : NSObject
{
    //    NSString *uuid_;
    //    NSString *name_;
    //    NSNumber *rssi_;

    //扫描的次数
    NSMutableArray* scanGround_;
    NSString* ICEID_;
}

@property(nonatomic, strong) NSString *uuid;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSNumber *rssi;
@property(nonatomic, strong) NSString *key;
@property(nonatomic, strong) NSString *value;
@property(nonatomic, strong) NSMutableArray* scanGround;
@property(nonatomic, strong) NSString *ICEID;

@property(nonatomic, strong) NeighbourEntity *neighbourInfo;
@end
