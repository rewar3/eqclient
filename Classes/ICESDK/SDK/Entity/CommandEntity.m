//
//  CommandEntity.m
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "CommandEntity.h"

@implementation CommandEntity

@synthesize CmdTYPE=CmdTYPE_;
@synthesize MSGID=MSGID_;
@synthesize PREMSGID=PREMSGID_;
@synthesize FROM=FROM_;
@synthesize OBJ=OBJ_;
@synthesize RES=RES_;
@synthesize ClientVaildcode=ClientVaildcode_;
@synthesize CMD_HEAD_DICT=CMD_HEAD_DICT_;
@synthesize body=body_;

-(id)init
{
    if(self = [super init])
    {

        CmdTYPE_ = [CommandTypeEntity NONE];
        
        MSGID_ = @"";
        PREMSGID_ = @"";
        FROM_ = @"";
        OBJ_ = [[NSMutableArray alloc]init];
        RES_ = @"";
        
        ClientVaildcode_ = @"";//[CommandEntity createUUID];
        CMD_HEAD_DICT_ = [[NSMutableDictionary alloc]init];
        body_ = @"";
    }

    return self;
}


-(void)setCMD_HEAD_DICTKey: (NSString*)key value:(NSString*)value{
    [CMD_HEAD_DICT_ setObject:value forKey:key];
}

+(NSString *)createUUID
{
    /**
    CFUUIDRef uuid_ref = CFUUIDCreate(NULL);
    CFStringRef uuid_string_ref= CFUUIDCreateString(NULL, uuid_ref);
    
    NSString *uuid = [NSString stringWithString:[[NSString alloc]initWithCString:uuid_string_ref encoding:NSUTF8StringEncoding]];

    CFRelease(uuid_ref);
    CFRelease(uuid_string_ref);
    */
    NSString *uuid = [[NSString alloc]initWithFormat:@"%u-%u", arc4random(), arc4random()];
    return uuid;
}

-(NSString*)toRaw
{
    NSString *bodyEncode;
    NSString *objString = @"";
    if ([OBJ_ count] > 0) {
        objString = [OBJ_ componentsJoinedByString:@","];
    }

    if([CmdTYPE_ isEqualToString:[CommandTypeEntity REGISTER]]){
        bodyEncode = body_;
    }else{
        bodyEncode = (__bridge NSString*)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                        (__bridge CFStringRef)body_,
                                                                        NULL,
                                                                        CFSTR(":/?#[]@!$&’()*+,;="), 
                                                                        kCFStringEncodingUTF8);
    }

    NSString *rawStr = [[NSString alloc]initWithFormat:@"CMD:%@\r\nMSGID:00000000\r\nPREMSGID:00000000\r\nFROM:%@\r\nOBJ:%@\r\nRES:%@\r\nClientVaildcode:%@\r\n\r\n%@\r\n\r\n", CmdTYPE_, [UserInfoEntity iceId], objString, RES_, [CommandEntity createUUID], bodyEncode];

    CFRelease(CFBridgingRetain(bodyEncode));
    [OBJ_ removeAllObjects];
    return rawStr;
}
@end
