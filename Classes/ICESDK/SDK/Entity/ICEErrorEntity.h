//
//  ICEConnectErrorEntity.h
//  iOS
//
//  Created by hanshu on 11/28/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

@interface ICEErrorEntity : NSObject
{
    NSInteger code_;
    NSString *msg_;
}

@property(atomic) NSInteger code;
@property(atomic, strong) NSString *msg;
@end
