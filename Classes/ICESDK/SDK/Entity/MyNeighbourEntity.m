//
//  MyNeighbourEntity.m
//  iOS
//
//  Created by hanshu on 1/14/13.
//  Copyright (c) 2013 hanshu. All rights reserved.
//

#import "MyNeighbourEntity.h"

@implementation NeighbourEntity
@synthesize iceId = iceId_;
@synthesize tvmId = tvmId_;
@synthesize groupId = groupId_;
@synthesize userType = userType_;
@synthesize CmdObjToMy = CmdObjToMy_;
@synthesize ip = ip_;
@end

@implementation MyNeighboursEntity
@synthesize canSendArray = canSendArray_;
@synthesize canReadArray = canReadArray_;
-(id)init:(NSString *)rawStr
{
    if(self = [super init]){

        if([rawStr length] < 5)
        {
            canSendArray_ = [[NSArray alloc]init];
            canReadArray_ = [[NSArray alloc]init];

            return self;
        }
        

        NSArray *twoPiece = [rawStr componentsSeparatedByString:@";"];
        if([twoPiece count] != 2){
            return self;
        }

        NSArray *sendPiece = [[twoPiece objectAtIndex:0]componentsSeparatedByString:@","];
        NSArray *recvPiece = [[twoPiece objectAtIndex:1]componentsSeparatedByString:@","];

        canSendArray_ = [self parsePiece:sendPiece];
        canReadArray_ = [self parsePiece:recvPiece];

    }

    return self;
}


/**
 * @brief 分析一片，得到在线数组
 * @param 数组 片数组
 * @return 数组 分析片数组
 */
-(NSArray *)parsePiece: (NSArray *)piece{
    NSMutableArray *neigList = [[NSMutableArray alloc]initWithCapacity:16];

    if([piece count] == 0){
        return neigList;
    }

    for(int i=0;i < [piece count];i++){
        NSArray *oneChunk = [[piece objectAtIndex:i]componentsSeparatedByString:@":"];

        if([oneChunk count] < 3){
            continue;
        }

        if([[oneChunk objectAtIndex:0]isEqualToString:[UserInfoEntity iceId]]){
            continue;
        }

        NeighbourEntity *newNeigh = [[NeighbourEntity alloc]init];
        [newNeigh setIceId:[oneChunk objectAtIndex:0]];
        [newNeigh setTvmId:[oneChunk objectAtIndex:1]];
        
        if([[oneChunk objectAtIndex:2]intValue] > [[GroupTypeEntity ADMINISTRATOR]intValue]){
            [newNeigh setUserType:[UserTypeEntity DEVICE]];
        }else{
            [newNeigh setUserType:[UserTypeEntity USER]];
        }

        if([[newNeigh userType]isEqualToString:[UserTypeEntity DEVICE]]){
            [newNeigh setGroupId:[GroupTypeEntity ADMINISTRATOR]];
        }else{
            [newNeigh setGroupId:[oneChunk objectAtIndex:2]];
        }

        if ([oneChunk count] > 4) {
            [newNeigh setIp:[oneChunk objectAtIndex:3]];
        }else{
            [newNeigh setIp:@""];
        }

        [newNeigh setCmdObjToMy:[NSString stringWithFormat:@"dev-%@", [newNeigh iceId]]];
        [neigList addObject:newNeigh];
    }

    return neigList;
}
@end