//
//  uploadFileEntity.m
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "UploadFileEntity.h"

@implementation UploadFileEntity
@synthesize title = title_;
@synthesize desc = desc_;
@synthesize tag = tag_;
@synthesize guid = guid_;
@synthesize filePath = filePath_;

-(id)initWithTitle:(NSString*)t desc:(NSString*)d tag:(NSString*)ta guid:(NSString*)gu filePath:(NSString*)fp//初始化
{
    if(self = [super init]){
        title_ = t;
        desc_ = d;
        tag_ = ta;
        guid_ = gu;
        filePath_ = fp;
    }

    return self;
}

-(NSString*)getFileName//返回文件名
{
    return filePath_;
}

-(NSString*)getMimeType//返回实体的 mimetype 值
{
    return @"";
}

+(NSString *)UPDATE_PACK_KEY//更新包的 key
{
    return @"onto_res_pack";
}
+(NSString *)UPDATE_USER_ID_KEY//上传者的 id
{
    return @"owner_tvmid";
}
+(NSString *)SUGGEST_PACK_KEY//提示权限组
{
    return @"suggest_groupid";
}
@end
