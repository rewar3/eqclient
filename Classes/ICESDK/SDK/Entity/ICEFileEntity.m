//
//  ICEFileEntity.m
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "ICEFileEntity.h"
#import "UserInfoEntity.h"

@implementation ICEFileEntity
/**
 * 搜索文件夹的路径，user_tvmid= 后面跟天脉号。会得到当前用户符合权限的列表
 * 
 * 返回 JSON，解开后，得到如下数组
 Array
 (
    [code] => 0
    [msg] => Array
                ([0] => Array([id] => 1
                             [owner_tvmid] => red
                             [packname] => 交流
                             [res_groupid] => 1
                             [submit_date] => 2012-12-13 12:35:45
                        )

                 [1] => Array([id] => 2
                              [owner_tvmid] => 141
                              [packname] => 天脉产品
                              [res_groupid] => 3
                              [submit_date] => 2012-12-13 12:37:31
                         )

                  [2] => Array(
                               .....
                         )
                )
 )
 

 id: 主键，没有实际意义
 owner_tvmid: 创建者的天脉号
 packname:  包名
 res_groupid: 包权限(和 GroupTypeEntity:: 相应权限做 isEqualToString)
 *
 */
+(NSString*)URL_GET_ALL_PACK
{
    return @"ice3/search_folder.php?user_tvmid=";
}

/**
 *  上传文件的。以 POST 方式提交以下字段
        
        onto_res_pack   上传到那个包中
   (必填)owner_tvmid     上传者的天脉号
        request_guid    上传过程的 guid（如果有这个选项，则自动忽略 onto_res_pack 选项）
        
    每个上传的实体文件信息如下(有 N 个上传文件，既有 N 个信息，信息可以为空)
        title[] 文件标题
        tag[]   标签
        file_type[] 文件类型
        guid[]  文件的 guid

    上传的实体文件，以 POST 方式上传，文件名为 file[]
 
 
 
    提交后，返回 JSON
        code = 0
        msg 中是上传文件的信息，和 2.75 的结构类似
 */
+(NSString*)URL_UPLOAD_FILE
{
    return @"ice3/folder_upload.php";
}

/**
 * 搜索文件。以 POST 提交以下字段。
         guid      文件的 GUID，如果查询多个，以逗号隔开，比如 guid1,guid2 就是搜索 guid1 和 guid2 两个 guid
   (必须) tvmid    提交者的天脉号
         begin_time     开始时间
         finish_time    结束时间
         in_res_pack    搜索包。可以搜索多个包，以逗号隔开，比如 包1,包2  就是搜索 包1 和 包2 两个包的内容
         title_contain  标题包含什么字符
         tag            搜索 tag
         desc_contain   描述包含
         is_asc         1 是正序排序，0 是逆序排列
         order_column   以那个字段排序
         suggest_groupid    需要制定搜素那个权限的文件(以 GropuTypeEntity:: 想对应的值)
         owner_tvmid    文件所述 id。如果有多个，以逗号隔开。
 */
+(NSString*)URL_SEARCH_FILE
{
    return @"ice3/search_file.php";
}

+(NSString*)URL_SANDBOX_SEARCH_FILE
{
    return @"ice3/search_sandbox.php";
}

+(NSString*)URL_PERMISSION_IP_LIST
{
    return @"ice3/permission_list.php?new=1&iceid=";
}

/**
 * 搜索文件夹的拓展信息。以 POST 提交以下字段。
 *  (必须) tvmid    提交者的天脉号
 *  (必须) packname    需要计算的包名
 *
 * 返回一个 JSON，解开以后为一个数组:
 *
 *  array(code => 0 
          msg => array(
                         size = 包长度
                         name = 包名称
          )
    )
 *
 * 如果 code < 0 则为出错
 *
*/ 
+(NSString*)URL_SEARCH_PACK_EXTINFO
{
    return @"ice3/search_extpackinfo.php";
}


/**
 *  删除包的接口。以 POST 方式提交以下信息
 *  (必须) res_pack 包名
 *  (必须) user_tvmid 用户的天脉号
 *  
 *  返回一个 json，解开后为一个数组:
    
    array(code => 0
          msg => 提示文字
    )

 *  如果 code < 0 则为提交错误，提示文字在 msg 里
 */
+(NSString*)URL_REMOVE_PACK
{
    return @"ice3/remove_pack.php";
}

/**
 * 更新包权重。以 POST 方式提交以下信息。
 *
 * 字段名:
 * (必须) res_pack 包名
 * (必须) owner_tvmid 用户的天脉号。比如是红色用户或设备才可以修改包的权限
 * (必须) weight 包的权重。默认是最下 9999，最大是 1
 */
+(NSString*)URL_UPDATE_PACK_WEIGHT
{
    return @"ice3/folder_maker.php";
}

/**
 * 更新文件信息。以 POST 方式提交以下信息
 * 
 * 字段名:
 *         (必须) owner_tvmid 用户天脉号。必须是红色用户或者设备才有可以修改文件信息的权限
 *         (必须) res_pack 要更新文件的包名

            更新一个文件信息，就有一对以下信息：
 *                weight[] 要更新文件的排序顺序。以正序排列
 *                guid[] 要修改文件的 guid
            
            比如用户/设备 ICE 要更新包名是 xxoo 中 guid 是 a 和 b 的权重为 1 和 2，则 POST 提交:

            ----------------
            owner_tvmid: ICE
            res_pack: xxoo

            guid[]: a
            weight[]: 1

            guid[]: b
            weight[]: 2
            ----------------

 */
+(NSString*)URL_UPDATE_FILE_INFO
{
    return @"ice3/update_file_info.php";
}

/**
 * 删除文件的接口
 * 
 * 字段名:
 *         (必须) guids 要删除的文件的 guid，如果有多个，则以逗号隔开，比如 'guid1,guid2'
 *         (必须) owner_tvmid 提交者的 guid，必须要是正确的 tvmid
 *
 *
 * 如果提交成功，则返回一个 json，解开后为一个数组:
 *
 *          array('code'=>0,
                  'msg' => array(
                                 'status'=>'ok',                  //如果失败则为 fail
                                 'guid'  =>'删除的 guid',
                                 'msg' => ''                      //如果失败，则为失败原因
                           )
                           ...
                  )
 */
+(NSString*)URL_RMEOVE_FILE
{
    return @"ice3/remove_file.php";    
}

@end
