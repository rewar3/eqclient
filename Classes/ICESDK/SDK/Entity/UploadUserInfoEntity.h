//
//  UploadUserInfoEntity.h
//  iOS
//
//  Created by hanshu on 12/21/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

@interface UploadUserInfoEntity : NSObject
+(NSString*) URI_UPLOAD_USERINFO;
@end
