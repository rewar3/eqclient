//
//  UploadFileStatusEntity.m
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "UploadFileStatusEntity.h"

@implementation UploadFileStatusEntity : NSObject
@synthesize code=code_;
@synthesize msg=msg_;
@end

@implementation UploadFileDetailStatusEntity : NSObject
@synthesize status=status_;
@synthesize filename=filename_;
@synthesize msg=msg_;
@synthesize guid=guid_;
@end
