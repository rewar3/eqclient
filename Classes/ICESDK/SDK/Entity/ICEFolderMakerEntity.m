//
//  ICEFolderMakerEntity.m
//  iOS
//
//  Created by hanshu on 1/16/13.
//  Copyright (c) 2013 hanshu. All rights reserved.
//

#import "ICEFolderMakerEntity.h"

@implementation ICEFolderMakerEntity
/**
 * 创建一个文件夹。以 POST 方式提交以下内容：
 * 
 *      res_pack    要创建的资源包名称
 *      suggest_groupid     要创建的资源包权限
                            1 是绿色
                            2 是黄色
                            3 是红色
                            空 是用户当前权限
        owner_tvmid  创建者的天脉号，必须为红色权限或者是设备
        pack_type    包类型
        request_guid     请求的 guid
        upload_file_num  上传文件的数量
 */
+(NSString*) URI_MAKER{
    return @"ice3/folder_maker.php";
}
@end
