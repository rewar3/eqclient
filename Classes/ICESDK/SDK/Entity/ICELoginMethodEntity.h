//
//  ICELoginMethodEntity.h
//  iOS
//
//  Created by hanshu on 11/23/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

@interface ICELoginMethodEntity : NSObject
+(NSString *)DEFAULT;
+(NSString *)REGISTRY;
+(NSString *)PASSWORD;
@end
