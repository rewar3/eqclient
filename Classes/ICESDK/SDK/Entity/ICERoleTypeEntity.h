//
//  ICERoleTypeEntity.h
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

@interface ICERoleTypeEntity : NSObject
+(NSUInteger)MASTER;
+(NSUInteger)SLAVE;
@end
