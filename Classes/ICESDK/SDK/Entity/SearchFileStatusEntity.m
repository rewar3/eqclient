//
//  SearchFileStatusEntity.m
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "SearchFileStatusEntity.h"

@implementation SearchFileDetailStatusEntity : NSObject
@synthesize id=id_;
@synthesize guid=guid_;
@synthesize userId=userId_;
@synthesize filename=filename_;
@synthesize packname=packname_;
@synthesize desc=desc_;
@synthesize title=title_;
@synthesize tag=tag_;
@synthesize submit_date=submit_date_;

-(id)init
{
    if(self = [super init]){
        id_ = 0;
        guid_ = @"";
        userId_ = @"";
        isShare_ = 0;
        filename_ = @"";
        packname_ = @"";
        desc_ = @"";
        title_ = @"";
        tag_ = @"";
    }

    return self;
}

+(NSString*)STORAGE_PREFIX
{
    return @"resource";
}

-(NSString*)getFileURI//得到文件的 URI
{
    NSString *uri = [[NSString alloc]initWithFormat:@"/%@/%@/%@",
                            [SearchFileDetailStatusEntity STORAGE_PREFIX],
                            [packname_ stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                            filename_
                    ];

    return uri;
}

-(NSString*)getThumbURI:(NSUInteger)width height:(NSUInteger)height//得到缩略图
{
    NSString *uri = [[NSString alloc]initWithFormat:@"/%@/%@/%@.jpg_%u_%u.jpg",
                            [SearchFileDetailStatusEntity STORAGE_PREFIX],
                            [packname_ stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                            guid_,
                            width,
                            height
                     ];

    return uri;
}

-(NSString*)getThumbURI:(NSUInteger)width height:(NSUInteger)height method:(fileThumbMethod)method//得到缩略图
{
    NSString *thumbMethod = @"_";
    if(method == fileThumbMethodResample){
        thumbMethod = @"x";
    }

    NSString *uri = [[NSString alloc]initWithFormat:@"/%@/%@/%@.jpg%@%u_%u.jpg",
                                     [SearchFileDetailStatusEntity STORAGE_PREFIX],
                                     [packname_ stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                                     guid_,
                                     thumbMethod,
                                     width,
                                     height
                     ];

    return uri;
}
@end


@implementation SearchFileStatusEntity
@synthesize code = code_;
@synthesize msg = msg_;

+(NSInteger)FAILED_CODE
{
    return -1;
}
+(NSUInteger)SUCC_CODE
{
    return 0;
}
@end
