//
//  CommandTypeEntity.h
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

@interface CommandTypeEntity : NSObject
{
}

+(NSString*)convertWithString:(NSString*)cmdStr;
+(NSString*)GETMYNEIGHBOUR;
+(NSString*)GETONLINE;// = "getonline";
+(NSString*)REGISTER;// = "register";
+(NSString*)RESET;// = "reset";
+(NSString*)UP;//  = "up";
+(NSString*)DOWN;// = "down";
+(NSString*)LEFT;// = "left";
+(NSString*)RIGHT;// = "right";
+(NSString*)FIRST;// = "first";
+(NSString*)LAST;// = "last";
+(NSString*)GO;// = "go";
+(NSString*)FORWARD;// = "forward";
+(NSString*)BACKWARD;// = "backward";
+(NSString*)FUNCTION1;// = "function1";
+(NSString*)FUNCTION2;// = "function2";
+(NSString*)NONE;// = "none";
+(NSString*)ECHO;// = "echo";
+(NSString*)NEWFILE;// = "newfile";
+(NSString*)NEWPACK;// = "newpack";
+(NSString*)NNULL;// = "null";
+(NSString*)PLAY;//play
+(NSString*)PUSH;//push
+(NSString*)SYNC;//sync
+(NSString*)CANCEL;//
+(NSString*)OK;
+(NSString*)ZOOMIN;
+(NSString*)ZOOMOUT;
+(NSString*)STICKY;
+(NSString*)MARCO;
+(NSString*)EXT;
+(NSString*)CLEANALL;
+(NSString*)STARTOVER;
+(NSString*)DELPACK;
+(NSString*)DELFILE;
+(NSString*)VALUECHANGE;
+(NSString*)ICERESTART;
+(NSString*)TOUCH;
+(NSString*)TALKTO;
+(NSString*)NEARBY;
+(NSString*)LOCK;
+(NSString*)FORCE;
+(NSString*)MENU;
+(NSString*)HOME;
+(NSString*)CACHE;
@end

