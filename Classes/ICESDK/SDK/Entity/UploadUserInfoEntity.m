//
//  UploadUserInfoEntity.m
//  iOS
//
//  Created by hanshu on 12/21/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "UploadUserInfoEntity.h"

@implementation UploadUserInfoEntity

/**
 * 上传用户信息, POST 提交
 * 变量：
        (必须)    owner_tvmid   用户的天脉号
                 company        公司名
                 appname        应用名
                 email          电子邮件
                 mobile         手机
 
    POST 上传文件实体文件名
                face
 返回 JSON，解开后是一个数组，Array('code'=>0 msg=>'[]'), 当 code==0 时正确
 */
+(NSString*) URI_UPLOAD_USERINFO
{
    return @"/ice3/upload_userinfo.php";
}

@end
