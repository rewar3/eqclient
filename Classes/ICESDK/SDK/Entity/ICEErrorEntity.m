//
//  ICEConnectErrorEntity.m
//  iOS
//
//  Created by hanshu on 11/28/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//




#import "ICEErrorEntity.h"

@implementation ICEErrorEntity
@synthesize msg=msg_;
@synthesize code=code_;


/* 转发器
 错误码	错误信息	含义
 0       OK	正确
 -1      Message Format Error!       指令格式不符合规定
 -2      Invalid Registration Name!	注册信息的格式不符合规定
 -3      Command No Exists!          该命令字不存在
 -4      endPoint Already Register!	1.	自己重复注册 2.	自己被其他端踢掉
 -5      Invalid Command word!       命令字段的格式不符合规定
 -6      endPoint No Exists!         以设备身份注册时，注册表事先不存在该设备
 -7      Permission Denied!          1.	没有注册就开始发送其他指令   2.	8881端口的客户端进行注册
 -8      Unknown Error!              未知错误
 -9      Invalid Password!           密码错误，密码不存在
 -10     Wrong endPoint Type!        1.	用户试图以设备身份登录 2.	设备试图以用户身份登录
 -11     Wrong Login Mode!           1.	设备以非注册表方式登录     2.	以注册表方式登录时，事先不存在该设备
 -12     Exceed Packet Size Limit!	数据包超过4K限制
 -13
 */

@end
