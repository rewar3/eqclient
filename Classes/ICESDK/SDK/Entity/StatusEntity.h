//
//  StatusEntity.h
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommandTypeEntity.h"
#import "ICEDebug.h"

@interface StatusEntity : NSObject
{
    NSString *cmd_;
    NSString *status_;
    NSDictionary *msg_;
}

@property(atomic, strong) NSString *cmd;
@property(atomic, strong) NSString *status;
@property(atomic, strong) NSDictionary *msg;

-(id)initWithJSONData:(NSData*)jData;

+(NSString *)FAIL;//失败代码
+(NSString *)OK;//成功代码
+(NSString *)UNDELIVER;//未抵达
+(NSString *)UNPASER;//未解析
+(NSString *)ALREAD_ONLINE;//已经在
+(NSString *)NNULL;//空
@end
