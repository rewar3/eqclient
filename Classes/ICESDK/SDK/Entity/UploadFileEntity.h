//
//  uploadFileEntity.h
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

@interface UploadFileEntity : NSObject
{
    NSString *title_;
    NSString *desc_;
    NSString *tag_;
    NSString *guid_;
    NSString *filePath_;
}

@property(atomic, strong) NSString *title;
@property(atomic, strong) NSString *desc;
@property(atomic, strong) NSString *tag;
@property(atomic, strong) NSString *guid;
@property(atomic, strong) NSString *filePath;

-(id)initWithTitle:(NSString*)t desc:(NSString*)d tag:(NSString*)t guid:(NSString*)gu filePath:(NSString*)fp;//初始化
-(NSString*)getFileName;//返回文件名
-(NSString*)getMimeType;//返回实体的 mimetype 值

+(NSString *)UPDATE_PACK_KEY;//更新包的 key
+(NSString *)UPDATE_USER_ID_KEY;//上传者的 id
+(NSString *)SUGGEST_PACK_KEY;//提示权限组
@end
