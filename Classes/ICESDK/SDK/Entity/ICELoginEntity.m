//
//  ICELoginEntity.m
//  iOS
//
//  Created by hanshu on 11/23/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import "ICELoginEntity.h"

@implementation ICELoginEntity
@synthesize connectICEName = connectICEName_;
@synthesize loginMethod = loginMethod_;
@synthesize password = password_;

-(id)init:(NSString*)connIceN withMethod:(NSString*)loginM
{
    if (self = [super init])
    {
        connectICEName_ = connIceN;
        loginMethod_ = loginM;
        password_ = @"";
    }

    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}
@end
