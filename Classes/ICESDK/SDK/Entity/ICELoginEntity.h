  //
//  ICELoginEntity.h
//  iOS
//
//  Created by hanshu on 11/23/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

@interface ICELoginEntity : NSObject<NSCopying>
{
    NSString *connectICEName_;
    NSString *loginMethod_;
    NSString *password_;
}

@property(atomic, strong) NSString *connectICEName;
@property(atomic, strong) NSString *loginMethod;
@property(atomic, strong) NSString *password;

-(id)init:(NSString*)connIceName withMethod:(NSString*)loginM;

@end
