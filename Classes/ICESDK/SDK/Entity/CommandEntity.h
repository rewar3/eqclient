//
//  CommandEntity.h
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommandTypeEntity.h"
#import "UserInfoEntity.h"
#import "ICEDebug.h"

@interface CommandEntity : NSObject
{
    NSString *CmdTYPE_;
    NSString *MSGID_;
    NSString *PREMSGID_;
    NSString *FROM_;
    NSMutableArray *OBJ_;
    NSString *RES_;
    NSString *ClientVaildcode_;
    NSMutableDictionary *CMD_HEAD_DICT_;

    NSString *body_;
}

@property(atomic, strong) NSString *CmdTYPE;
@property(atomic, strong) NSString *MSGID;
@property(atomic, strong) NSString *PREMSGID;
@property(atomic, strong) NSString *FROM;
@property(atomic, strong) NSMutableArray *OBJ;
@property(atomic, strong) NSString *RES;
@property(atomic, strong) NSString *ClientVaildcode;
@property(atomic, strong) NSMutableDictionary *CMD_HEAD_DICT;

@property(atomic, strong) NSString *body;

+(NSString *)createUUID;
-(NSString*)toRaw;
-(void)setCMD_HEAD_DICTKey: (NSString*)key value:(NSString*)value;
@end
