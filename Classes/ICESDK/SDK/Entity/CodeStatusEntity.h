//
//  CodeStatusEntity.h
//  iOS
//
//  Created by hanshu on 10/18/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

@interface CodeStatusEntity : NSObject
{
    NSInteger code_;//状态码
    NSString *status_;//状态值
    NSDictionary *msg;//具体内容
}

@property(atomic) NSInteger code;
@property(atomic, strong) NSString *status;
@property(atomic, strong) NSDictionary *msg;

-(id)initWithJSONData:(NSData*)jsonData;
@end
