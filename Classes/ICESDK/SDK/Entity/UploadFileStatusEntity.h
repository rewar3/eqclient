//
//  UploadFileStatusEntity.h
//  iOS
//
//  Created by hanshu on 10/19/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICEDebug.h"

@interface UploadFileStatusEntity : NSObject
{
    NSInteger code_;
    NSArray *msg_;
}

@property(atomic) NSInteger code;
@property(atomic, strong)NSArray *msg;
@end

@interface UploadFileDetailStatusEntity : NSObject
{
    //状态
    NSString *status_;

    //文件名
    NSString *filename_;

    //信息体
    NSString *msg_;

    //guid
    NSString *guid_;
}

@property(atomic, strong)NSString *status;
@property(atomic, strong)NSString *filename;
@property(atomic, strong)NSString *msg;
@property(atomic, strong)NSString *guid;
@end