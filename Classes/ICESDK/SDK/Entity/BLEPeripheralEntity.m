//
//  BLEPeripheralEntity.m
//  iOS
//
//  Created by hanshu on 3/7/13.
//  Copyright (c) 2013 hanshu. All rights reserved.
//

#import "BLEPeripheralEntity.h"

@implementation BLEPeripheralEntity
@synthesize rssi;
@synthesize name;
@synthesize uuid;
@synthesize key;
@synthesize value;
@synthesize neighbourInfo;
@synthesize scanGround=scanGround_;
@synthesize ICEID=ICEID_;

-(id)init
{
    if (self = [super init]) {
        scanGround_ = [[NSMutableArray alloc]initWithCapacity:12];
    }

    return self;
}
@end
