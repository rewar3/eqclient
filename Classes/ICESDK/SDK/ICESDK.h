//
//  ICESDK.h
//  iOS
//
//  Created by hanshu on 10/16/12.
//  Copyright (c) 2012 hanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfoEntity.h"
#import "ICEConnectorService.h"
#import "ICECommandParseService.h"
#import "SearchFileEntity.h"
#import "SearchFileStatusEntity.h"
#import "UserTypeEntity.h"
#import "GroupTypeEntity.h"
#import "ICELoginEntity.h"
#import "GroupTypeEntity.h"
#import "Entity/ICELoginMethodEntity.h"
#import "ICEDebug.h"
#import "Entity/MyNeighbourEntity.h"
#import "BLEPeripheralEntity.h"
#import "ICEManager.h"

#include <pthread.h>


@class ICEManager ;
@protocol ICEManagerDelegate <NSObject>

/*
 搜索中控 Delegate

 */

- (void)searchingTimeout:(ICEManager *)manager ;
- (void)searchingCompletedWithServiceList:(__weak NSArray *)servicelistArr ;
- (void)searchingUpdatedWithServiceList:(__weak NSArray *)updateServicelistArr ;
/*
 连接中控相关细节 Delegate

 */
- (void)connectionTimeout ;
- (void)connectionFailedWithICEError:(ICEErrorEntity *)error ;
- (void)connectionSucceedWithTargetServiceIP:(NSString *)ip userPower:(NSString *)power ;

- (void)connectionRetryFailed ;
- (void)connectionWillRetry ;
- (void)connectionRetrySuccess ;

/*
 指令回调(中控列表)
 */
- (void)receivedCMD:(CommandEntity *)commandEntity ;


/*
 蓝牙4 测距相关
 */
/**
 * @brief 不匹配设备
 */
-(void)bleWrongEquipment;
/**
 * @brief 得到所有扫描到的 xx
 */
-(void)nearestBLE:(BLEPeripheralEntity*)onePerpheral;
/**
 * @brief 没找到
 */
-(void)foundNothindBLE;
/**
 * @brief 蓝牙设备离开
 */
-(void)leaveBLE:(BLEPeripheralEntity*)onePerpheral;
@end


@interface ICESDK : NSObject
{
    //最后送出的用户
    NSMutableArray *lastSendUserArray;

    //送出的命令
    NSMutableArray *cmdDeliveredArray;

    /// <summary>
    /// 连接了的 ICE
    /// </summary>
    ICEConnectorService *oneICS;

    /// <summary>
    /// 一个命令解析服务
    /// </summary>
    ICECommandParseService *oneICPS;

    /// <summary>
    /// 一个文件上传服务
    /// </summary>
    //ICEFileService *oneIFS;

    /// <summary>
    /// 分析命令的线程
    /// </summary>
    NSThread* threadCmdParser;

    /// <summary>
    /// 心跳线程
    /// </summary>
    NSThread* threadSayHello;

    /// <summary>
    /// 注册用户的信息
    /// </summary>
    UserInfoEntity *registerUserInfo;

    //登陆的中空信息
    __weak id<ICEManagerDelegate> delegate_ ;
}

@property (nonatomic , weak) id<ICEManagerDelegate> delegate ;

-(id)init: (ICEConnectorService *)connICS with: (ICECommandParseService*) connICPS;//连接
-(void)doingPeriod;//做周期律
-(void)isOver;//结束
-(void)starOver;//重来
-(void)recvRawFromICEToCmdParser;//内部线程方法，用来 xxoo 命令
-(void)periodSayHello;//周期性的和中控心跳，如果错误，则重连
//-(void)periodFindICE;//通过 UDP 找中控
-(StatusEntity*)sendCommmand: (CommandEntity *)sendCmd;//送出一个命令
-(StatusEntity*)sendCommmand:(CommandEntity *)sendCmd timeout:(NSUInteger)timeout allowNone:(BOOL)allowNone;//送出一个命令
-(CommandEntity*)recvCommand;//收到一个命令
+(NSMutableArray*)getAllICENameArray;//得到所有中控服务列表
+(NSArray*)getAllICEServerArray;//得到所有连接主机名
-(void)stayAlone;//关闭连接
//-(StatusEntity*)registerToICE3:(ICELoginEntity *) loginICE withUserInfo:(UserInfoEntity*)registerUserEntity;//注册到 ICE
-(StatusEntity*)registerToICE3:(ICELoginEntity *) loginICE withUserInfo:(UserInfoEntity*)registerUserEntity timeout:(NSUInteger)timeout;
//-(NSArray*)getAllPublicPackName;//得到所有公开包的信息
//-(NSArray*)uploadLocalFile:(NSMutableDictionary *)postData uploadFile:(NSArray*)uploadFile;//上传文件
//-(NSArray*)searchFile:(SearchFileEntity*)searchFileCond;//搜索文件
//-(NSString*)getUrlByFileDetail:(SearchFileDetailStatusEntity*) oneSFDSE;//根据一个文件细节，得到 URL
//-(NSString*)getThumbURLByFileDetail:(SearchFileDetailStatusEntity*)oneSFDSE with:(NSUInteger)width height:(NSUInteger)height;//得到缩略图
//-(NSString*)getThumbURLByFileDetail:(SearchFileDetailStatusEntity*)oneSFDSE with:(NSUInteger)width height:(NSUInteger)height thumbMethod:(fileThumbMethod)fileTM;//得到缩略图
//-(NSString*)getUrlByUri: (NSString*)uri;//从一个 URI 的到 URL
+(BOOL)trySharedICE; //踹一下锁
+(ICESDK*)sharedICE :(ICELoginEntity*)connect userInfo:(UserInfoEntity*)userEntity;//连接到一个中控上面去
+(NSMutableDictionary*)world;//得到世界
//+(pthread_mutex_t)mutexStarOver;//重新开始锁住
-(NSString*)getUserPower;//得到用户权限
+(void)udpStartListen;//开始监听
+(void)udpStopListen;//结束监听
+(void)setWorld:(NSMutableDictionary*)world;
-(NSString*)getConectHostname;//得到连接的主机
-(MyNeighboursEntity*)getMyNeighbours;//得到邻居
-(MyNeighboursEntity*)getMyNeighboursByWeb;//得到邻居
@end
