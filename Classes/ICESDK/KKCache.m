//
//  KKCache.m
//  iPlay
//
//  Created by wsw on 11-3-9.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "KKCache.h"

static KKCache *myCache = nil;

@implementation KKCache
@synthesize HTTPIP ;
@synthesize canPushDeviceArray ;
@synthesize canSendDeviceArray ;
@synthesize mirrorLR ;
@synthesize mirrorUD ;
@synthesize questionGuid;
@synthesize questionType;
+(id)sharedCache
{
    @synchronized(self)
    {
        if (myCache == nil) {
            myCache = [[KKCache alloc] init]; 
        }
    }
    
    return myCache ;
}

+(id) allocWithZone:(NSZone *)zone{
    @synchronized(self)
    {
        if (myCache == nil) 
        {
            myCache = [super allocWithZone:zone];
            return myCache;
        }
    }
    return nil;
}

- (BOOL)isCachedUserInfo
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kISCACHEDUSERINFO] boolValue];
}

- (NSString *)getDateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    return [dateFormatter stringFromDate:[NSDate date]];
}

- (NSString *)getDateStringToSSS
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    //[dateFormatter setDateFormat:@"hh:mm:ss"]
    [dateFormatter setDateFormat:@"yyyyMMddHHmmssSSS"];
    return [dateFormatter stringFromDate:[NSDate date]];
}

@end
