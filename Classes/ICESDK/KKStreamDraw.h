//
//  KKStreamDraw.h
//  ICEConnectDemo
//
//  Created by wsw on 14-1-14.
//  Copyright (c) 2014年 wsw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KKPartLineManager.h"
#import "KKPartLine.h"
#import "KKPoint.h"
#import "NSDataAdditions.h"
#import "NSString+.h"
#import "KKCache.h"
#import "Info.h"
#import "ICEHelper.h"

typedef enum
{
    kStreamDrawStatusStart = 100 ,
    kStreamDrawStatusProgress = 200 ,
    kStreamDrawStatusEnd      = 300 ,
    kStreamDrawStatusClearAll      = 400 ,
    kStreamDrawStatusClearSingleLine      = 500
} StreamDrawStatus;



@class KKStreamDraw ;

@protocol KKStreamDrawDelegate  <NSObject>


//开始写的时候 有可能之前有笔迹，那么就会有blendedImage ,bgImageNoDrawingGuid 为任何时候都没有笔迹合成的图片（通常作为背景），resourceImageMaybeWithDrawingGuid 有可能是合成过笔迹的图片（通常在push后，接着继续操作时会使用到）
- (void)onStreamDrawBegan:(StreamDrawStatus)status blendedImage:(UIImage *)blendedImage rid:(NSString *)rid sourceFileID:(NSString *)sourceFileID;

//过程中不断渲染时会调用，只有blendedImage。
- (void)onStreamDrawDrawing:(StreamDrawStatus)status blendedImage:(UIImage *)blendedImage whitePaperGuid:(NSString *)whitePaperGuid imageScale:(float)scale;

//渲染结束时调用，通常处理UI的一些事件。
- (void)onStreamDrawEnd:(StreamDrawStatus)status ;


//从板书拖出来，没有加载原图片渲染的透明Image
- (void)onWillBlendExistHandWritingImage:(UIImage *)image ;

//
- (void)onBlendedExistHandWritingImage:(UIImage *)image ;

- (void)onCurrentFrameImageSavedWithImage:(UIImage *)image ;

@end


@interface KKStreamDraw : NSObject
{
    NSMutableArray *drawDataArray ;
    
    NSMutableArray *managerArray;
    
    NSMutableArray *existHandWritingArray;
    
    UIImageView *drawImageView ;
    
    float       ctmX ;
    float       ctmY ;
    float       transformX ;
    float       transformY ;
    
    CGContextRef context ;
    
    CGContextRef existHandWritingContext ;//板书情况特殊，单独context
    
    CGContextRef saveContext ;
    
    KKPartLineManager *firstManager ;
    
    KKPartLineManager *existHandWritingManager ;//板书情况特殊，单独manager
    
    
    NSOperationQueue *queue ;
    
    NSOperationQueue *messageQueue ;
    
    
    NSInteger  lastIndex ;//记录最后一个Index 区别是不是一笔，是否需要创建新的manager。
    NSInteger  messageDealLastIndex ;
    NSInteger  existHandWritingLastIndex ;
}

@property (nonatomic , weak) id <KKStreamDrawDelegate> delegate ;
@property (nonatomic , strong) UIImage *bgImage ;


- (void)initObjects ;//初始化变量

- (void)updateDrawingWithData:(id)data ;//书写过程刷新

- (void)updateCTM:(id)data ;//放大缩小时矩阵变化时刷新

- (void)createContext:(id)data;//开始时创建context

- (void)clearAll:(id)data;//清楚所有笔迹

- (void)clearSingleLine:(id)data ;//清楚单个笔迹

//- (void)opfour:(id)data;//处理特殊情况op==4的情况

//UIcolor 16进制颜色值转换
- (UIColor *)colorFromHexString:(NSString *)hexString ;

- (void)blendWithImage:(UIImage *)bgImage ;



- (void)getExistHandWritingImageWithGuid:(NSString *)guid ;
- (void)blendExistHandWritingImageWithImage:(UIImage *)originalImage ;

- (void)saveCurrentFrameImage ;

@end
