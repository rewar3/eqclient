//
//  CachedUserInfo.h
//  NewWiFiPlus
//
//  Created by wsw on 23/12/12.
//  Copyright (c) 2012年 天脉聚源传媒科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface CachedUserInfo : NSObject

@property (nonatomic , strong) NSString *tvmid ;
@property (nonatomic , strong) NSString *tempid ;
@property (nonatomic , strong) NSString *token ;
@property (nonatomic , strong) NSString *password ;
@property (nonatomic , strong) UIImage *headImage ;
@property (nonatomic , strong) UIImage *minecoverImage ;
@property (nonatomic , strong) NSString *realName ;
@property (nonatomic , strong) NSString *email ;
@property (nonatomic , strong) NSString *birthday ;
@property (nonatomic , strong) NSString *sex ;
@property (nonatomic , strong) NSString *tel ;
@property (nonatomic , strong) NSString *company ;
@property (nonatomic , strong) NSString *address ;
@property (nonatomic , strong) NSString *postcode ;

@property (nonatomic , strong) NSString *userPower ;
@property (nonatomic , strong) NSString *uploadPower ;

+ (id)shared ;

- (void)updateUserInfo ;

@end
