//
//  HistoryCell.h
//  EQClient
//
//  Created by rewar on 13-12-20.
//  Copyright (c) 2013年 xumeng. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HistoryCellDelegate;
@interface HistoryCell : UIView

@property (nonatomic,strong)NSString *userName;
@property (nonatomic,strong)NSString *avatarUrl;

@property (nonatomic,strong)UIImageView *imgBG;
@property (nonatomic,strong)UIImageView *imgAvatar;
@property (nonatomic,strong)UILabel *lblName;
@property (nonatomic,strong)UIButton *btnDelete;
@property (nonatomic,weak)id<HistoryCellDelegate> delegate;

- (id)initWithBGImage:(UIImage *)image userName:(NSString *)name avatarUrl:(NSString *)url;

@end
@protocol HistoryCellDelegate <NSObject>

- (void)cellClick:(HistoryCell *) cell;
- (void)cellDelete:(HistoryCell *) cell;

@end

