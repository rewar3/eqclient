//
//  ImagePageContainer.m
//  EQClient
//
//  Created by xumeng on 12/23/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import "ImagePageContainer.h"
#import "Page.h"

@interface ImagePageContainer()
{
    BOOL _exists;
    BOOL _userAction;
}
@end

@implementation ImagePageContainer
@synthesize scrollView;
@synthesize pages;
@synthesize currentPage;
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initScrollView];
    }
    return self;
}

- (void)initScrollView
{
    
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kImageHight)];
    scrollView.pagingEnabled = YES;
    scrollView.clipsToBounds = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.alwaysBounceVertical = NO;
    scrollView.delegate = self;
    [self addSubview:scrollView];
    pages = [[NSMutableArray alloc] init];
    _userAction = NO;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(openStreamModeHandler) name:kOpenStreamMode object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(closeStreamModeHandler) name:kCloseStreamMode object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(imageRequestDidLoadHandler:) name:kImageRequestDidLoad object:nil];
}

- (void)imageRequestDidLoadHandler:(NSNotification *)n
{
    NSDictionary *dic = n.object;
    if(dic)
    {
        NSString *guid = [dic objectForKey:GUID];
        ScrollPageView *sp = [self getPageWithGUID:guid];
        if(sp)
        {
            [sp imageRequestDidLoad:dic];
            [sp setPageVO:[Page getPageWithGUID:guid]];
        }
    }
}

- (void)addPageWithGUID:(NSString *)guid animated:(BOOL) animation isStream:(BOOL)isStream
{
    ScrollPageView *contentView;
    contentView = [self getPageWithGUID:guid];
    if(contentView)
    {
        [pages removeObject:contentView];
        [pages addObject:contentView];
        
        [scrollView setContentSize:CGSizeMake((kPageWidth) * (pages.count+1), scrollView.frame.size.height)];
        [contentView setFrame:CGRectMake(kPageWidth*pages.count, 0, kImageWidth, kImageHight)];
        
        _exists = YES;
    }
    else
    {
        
        contentView = [[ScrollPageView alloc]initWithGUID:guid];
        [contentView setIsAction:YES];
        [contentView setDelegate:self];
        [scrollView addSubview:contentView];
        [pages addObject:contentView];
        
        [scrollView setContentSize:CGSizeMake((kPageWidth) * pages.count, scrollView.frame.size.height)];
        [contentView setFrame:CGRectMake((kPageWidth)*(pages.count-1), 0, kImageWidth, kImageHight)];
        
        _exists = NO;
        [delegate addPageCount:pages.count];
        
    }
    if(!isStream)
    {
        [self turnToPageWithGUID:guid animated:animation];
    }
    
    if(!animation)//无动画移除多余page
    {
        if(_exists)//page重用
        {
            ScrollPageView *contentView;
            for (int i=0; i<pages.count; i++) {
                contentView =[pages objectAtIndex:i];
                [contentView setFrame:CGRectMake((kPageWidth)*(i), contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height)];
            }
            [self.scrollView setContentSize:CGSizeMake((kPageWidth) * (pages.count), self.scrollView.frame.size.height)];
            [self turnToPageWithGUID:guid animated:animation];
            _exists = NO;
        }
        else
        {
            if(pages.count>kMaxPageCount && !isStream)
            {
                ScrollPageView *contentView;
                NSInteger count = pages.count - kMaxPageCount;
                for(int i=0;i<count;i++)
                {
                    contentView = [pages objectAtIndex:i];
                    [contentView removeFromSuperview];
                    [pages removeObject:contentView];
                }
                
                for (int i=0; i<pages.count; i++) {
                    contentView =[pages objectAtIndex:i];
                    [contentView setFrame:CGRectMake(contentView.frame.origin.x-kPageWidth, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height)];
                }
                
                
                [self.scrollView setContentSize:CGSizeMake((kPageWidth) * kMaxPageCount, self.scrollView.frame.size.height)];
            }
        }
        currentPage = [pages lastObject];
        
        if(!isStream)
        {
            [self turnToPageWithGUID:guid animated:animation];
        }
    }
    
    
    if(animation&&pages.count>1)
    {
        [delegate imagePageContainerWillBeginScrolling];
    }
    
    if(pages.count ==1)
    {
        currentPage = contentView;
        [delegate changePage:0 page:currentPage];
    }
}

- (void)turnToPageWithGUID:(NSString *)guid animated:(BOOL) animation
{
    CGPoint offset;
    int index;
    ScrollPageView *contentView;
    for (int i=0; i<pages.count; i++) {
        ScrollPageView *view = [pages objectAtIndex:i];
        if([view.pageVO.guid isEqualToString:guid])
        {
            contentView = view;
            index = i;
        }
    }
    _userAction = NO;
    [self reloadPageImageWithIndex:index];
    
    offset = CGPointMake(contentView.frame.origin.x, contentView.frame.origin.y);
    [self.scrollView setContentOffset:offset animated:animation];
    [delegate changePage:index page:contentView];
}

- (void)endPage
{
    if(pages.count>kMaxPageCount)
    {
        ScrollPageView *contentView;
        
        NSInteger count = pages.count - kMaxPageCount;
        for(int i=0;i<count;i++)
        {
            contentView = [pages objectAtIndex:i];
            [contentView removeFromSuperview];
        }
        
        [pages removeObjectsInRange:NSMakeRange(0, count)];
        
        for (int i=0; i<pages.count; i++) {
            contentView =[pages objectAtIndex:i];
            [contentView setFrame:CGRectMake(kPageWidth *i, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height)];
        }
        
        [self.scrollView setContentOffset:CGPointMake((kMaxPageCount-1)*kPageWidth, 0) animated:NO];
        [self.scrollView setContentSize:CGSizeMake((kPageWidth) * kMaxPageCount, self.scrollView.frame.size.height)];
        
        contentView = [pages lastObject];
        [self turnToPageWithGUID:contentView.pageVO.guid animated:NO];
    }
}

- (void)reloadPageImageWithIndex:(int)index
{
    for (int i=0; i<pages.count; i++) {
        
        ScrollPageView *view = [pages objectAtIndex:i];
        
        if(i==index || i==index-1 || i==index+1)
        {
            if(!view.isAction)
            {
                [view loadImage];
            }
        }
        else
        {
            view.originalImage = nil;
            view.imageView.imageView.image = nil;
            view.isAction = NO;
        }
    }
}

- (ScrollPageView *)getPageWithGUID:(NSString *)guid
{
    ScrollPageView *contentView;
    for (int i=0; i<pages.count; i++) {
        ScrollPageView *view = [pages objectAtIndex:i];
        if([view.pageVO.guid isEqualToString:guid])
        {
            contentView = view;
        }
    }
    return contentView;
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int index = fabs(self.scrollView.contentOffset.x) / self.scrollView.frame.size.width;
    if(index>=pages.count-1)
    {
        _userAction = NO;
        [delegate changePage:index page:currentPage];
        return;
    }
//    if(_userAction)
//    {
        [self reloadPageImageWithIndex:index];
//    }
    _userAction = NO;
    currentPage = [pages objectAtIndex:index];
    [delegate changePage:index page:currentPage];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [delegate imagePageContainerWillBeginDragging];
    _userAction = YES;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    
    int index = fabs(self.scrollView.contentOffset.x) / self.scrollView.frame.size.width;
    if(_exists)
    {
        ScrollPageView *contentView;
        for (int i=0; i<pages.count; i++) {
            contentView =[pages objectAtIndex:i];
            [contentView setFrame:CGRectMake((kPageWidth)*(i), contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height)];
        }
        [self.scrollView setContentSize:CGSizeMake((kPageWidth) * (pages.count), self.scrollView.frame.size.height)];
        [self.scrollView setContentOffset:CGPointMake((pages.count-1)*kPageWidth, 0) animated:NO];
        _exists = NO;
        
        currentPage = [pages lastObject];
    }
    else
    {
        if(pages.count>kMaxPageCount&&index>=kMaxPageCount)//动画结束处理位置
        {
            ScrollPageView *contentView;
            NSInteger count = pages.count - kMaxPageCount;
            for(int i=0;i<count;i++)
            {
                contentView = [pages objectAtIndex:i];
                [contentView removeFromSuperview];
            }
            [pages removeObjectsInRange:NSMakeRange(0, count)];
            
            for (int i=0; i<pages.count; i++) {
                contentView =[pages objectAtIndex:i];
                [contentView setFrame:CGRectMake(kPageWidth *i, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height)];
            }
            
            [self.scrollView setContentOffset:CGPointMake((kMaxPageCount-1)*kPageWidth, 0) animated:NO];
            [self.scrollView setContentSize:CGSizeMake((kPageWidth) * kMaxPageCount, self.scrollView.frame.size.height)];
            
        }
        if(index>=kMaxPageCount)index=kMaxPageCount-1;
        currentPage = [pages objectAtIndex:index];
    }
    
    [delegate imagePageContainerWillEndScrolling];
    
    [delegate changePage:index page:currentPage];
}

- (void)openStreamModeHandler
{
    [scrollView setScrollEnabled:NO];
}

- (void)closeStreamModeHandler
{
    [scrollView setScrollEnabled:YES];
}

- (void)ScrollPage:(ScrollPageView *)scrollPage playVideoWithUrl:(NSString *)url
{
    [delegate playVideo:url guid:scrollPage.pageVO.guid];
}

@end
