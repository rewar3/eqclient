//
//  ImagePageContainer.h
//  EQClient
//
//  Created by xumeng on 12/23/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollPageView.h"
@protocol ImagePageContainerDelegate;
@interface ImagePageContainer : UIView<UIScrollViewDelegate,ScrollPageViewDelegate>
@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) NSMutableArray *pages;
@property (nonatomic,strong) ScrollPageView *currentPage;
@property (nonatomic,weak) id<ImagePageContainerDelegate> delegate;

- (void)addPageWithGUID:(NSString *)guid animated:(BOOL) animation isStream:(BOOL)isStream;

- (void)turnToPageWithGUID:(NSString *)guid animated:(BOOL) animation;

- (ScrollPageView *)getPageWithGUID:(NSString *)guid;

- (void)endPage;
@end

@protocol ImagePageContainerDelegate <NSObject>

- (void)changePage:(int)index page:(ScrollPageView *)pageView;

- (void)addPageCount:(NSInteger)count;

- (void)playVideo:(NSString *) resUrl guid:(NSString *)guidStr;

- (void)imagePageContainerWillBeginScrolling;

- (void)imagePageContainerWillEndScrolling;

- (void)imagePageContainerWillBeginDragging;
@end
