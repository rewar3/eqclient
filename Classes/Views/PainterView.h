//
//  PainterView.h
//  Painter
//
//  Created by  ibokan on 10-9-7.
//  Copyright 2010 tencent.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Squiggle.h"

@class PainterView ;

@protocol PainterViewDelegate <NSObject>

- (void)willDraw:(PainterView *)view ;

@end


@interface PainterView : UIView 
{
	//正在绘的曲线
	NSMutableDictionary *squigglesDic;
	//已经完成的曲线
	NSMutableArray *finishSquiggles;
    NSMutableArray *redoSquiggles ;
    
    CGPoint currentPoint;
    CGPoint previousPoint1;
    CGPoint previousPoint2;
    
	UIColor *color;
	float lineWidth;
    float eraseWidth ;
    BOOL  eraseStatus ;
    
    __weak id <PainterViewDelegate> delegate_ ;
}
@property (nonatomic, retain) NSMutableDictionary *squigglesDic;
@property (nonatomic, retain) NSMutableArray *finishSquiggles;
@property (nonatomic, retain) UIColor *color;
@property (nonatomic, weak) id <PainterViewDelegate> delegate ;
@property float lineWidth;
@property float eraseWidth;
@property BOOL eraseStatus;

- (void)drawSquiggle:(Squiggle *)squiggle inContext:(CGContextRef)context;

- (void)resetView;
- (void)erase;

- (void)undo ;
- (void)redo ;

CGPoint midPoint(CGPoint p1, CGPoint p2);

CGFloat distance(CGPoint point1,CGPoint point2);

@end
