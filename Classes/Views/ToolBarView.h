//
//  ToolBarView.h
//  EQClient
//
//  Created by xumeng on 12/11/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ToolBarType) {
    ToolBarTypeIndex            = 1,
    ToolBarTypePaint            = 2,
    ToolBarTypeAnswer           = 3
};

@protocol ToolBarViewDelegate;

@interface ToolBarView : UIView<UITextFieldDelegate>
{
}
@property (nonatomic,strong)UIImageView *imgBG;

@property (nonatomic)ToolBarType currentType;

//indexMode
@property (nonatomic,strong)UIButton *btnPaint;
@property (nonatomic,strong)UIImageView *imgTextBG;
@property (nonatomic,strong)UIButton *btnPostText;
@property (nonatomic,strong)UIButton *btnMore;
@property (nonatomic,strong)UITextField *txtMessage;
@property (nonatomic,strong)UILabel *lblTip;

//paintMode

@property (nonatomic,strong)UIButton *btnBack;
@property (nonatomic,strong)UIButton *btnChooseColor;
@property (nonatomic,strong)UIButton *btnEraser;
@property (nonatomic,strong)UIButton *btnLineWidth;
@property (nonatomic,strong)UILabel *lblThin;
@property (nonatomic,strong)UILabel *lblThick;
@property (nonatomic,strong)UIButton *btnDelete;
@property (nonatomic,strong)UIButton *btnPostImage;
@property (nonatomic)BOOL isEraser;

//answerMode

@property (nonatomic,strong)NSMutableArray *btnAry;
@property (nonatomic,strong)UIButton *btnPostAnswer;



@property (nonatomic,weak)id<ToolBarViewDelegate> delegate;

- (instancetype)initWithToolBarType:(ToolBarType) type;

- (void)changeModeWithToolBarType:(ToolBarType) type completion:(void (^)(BOOL finished))completion;


@end

@protocol ToolBarViewDelegate <NSObject>

- (void)postText:(NSString *)message;

- (void)openEraser;

- (void)closeEraser;

- (void)postImage;

- (void)postAnswer:(NSString *)answer;

@end
