//
//  CustomPageControl.h
//  EQClient
//
//  Created by xumeng on 12/12/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPageControl : UIView
{
    NSMutableArray* _indicators;
    UIImage * _normalImage;
    UIImage * _selectedImage;
}

@property(nonatomic) NSInteger numberOfPages;
@property(nonatomic) NSInteger currentPage;

- (void)addPage;
@end
