//
//  BubbleView.h
//  EQClient
//
//  Created by rewar on 13-12-16.
//  Copyright (c) 2013年 xumeng. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, BubbleViewType) {
    BubbleViewTypeCamera          = 0,
    BubbleViewTypeColor           = 1,
    BubbleViewTypeLineWidth       = 2,
    BubbleViewTypeDelete          = 3,
    BubbleViewTypeIndex           = 4
};

@protocol BubbleViewDelegate;
@interface BubbleView : UIView
@property (nonatomic,strong)UIImageView *imgBG;
@property (nonatomic)BubbleViewType bubbleType;
@property (nonatomic)NSMutableArray *buttonAry;
@property (nonatomic,weak)id<BubbleViewDelegate> delegate;

+ (BubbleView *)bubbleViewWithType:(BubbleViewType) type;

- (id)initWithBubbleViewType:(BubbleViewType) type;

- (void)setLineColor:(NSInteger) color;

- (void)setLineWidth:(NSInteger) width;

@end

@protocol BubbleViewDelegate <NSObject>

- (void)bubbleViewButtonDidClick:(NSUInteger) buttonIndex bubbleType:(BubbleViewType) type;

@end
