//
//  HistoryCell.m
//  EQClient
//
//  Created by rewar on 13-12-20.
//  Copyright (c) 2013年 xumeng. All rights reserved.
//

#import "HistoryCell.h"
@implementation HistoryCell

@synthesize userName;
@synthesize avatarUrl;
@synthesize imgBG;
@synthesize imgAvatar;
@synthesize lblName;
@synthesize btnDelete;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
    }
    return self;
}

- (id)initWithBGImage:(UIImage *)image userName:(NSString *)name avatarUrl:(NSString *)url
{
    self = [super init];
    
    if(self)
    {
        [self setFrame:CGRectMake(0, 0, 280, 54)];
        self.imgBG = [[UIImageView alloc]initWithImage:image];
        [self.imgBG setFrame:CGRectMake(0, 0, 280, 54)];
        [self addSubview:self.imgBG];
        
        self.userName = name;
        self.avatarUrl = url;
        
        self.imgAvatar = [[UIImageView alloc]initWithFrame:CGRectMake(4, 4, 45, 45)];
        
        [self addSubview:self.imgAvatar];
        UIImage *image = [UIImage imageWithContentsOfFile:url];
        [self.imgAvatar setImage:image];
        
        self.lblName = [[UILabel alloc]init];
        [self.lblName setFrame:CGRectMake(55, 7, 160, 40)];
        [self.lblName setFont:[UIFont systemFontOfSize:22]];
        [self.lblName setTextAlignment:NSTextAlignmentLeft];
        [self addSubview:self.lblName];
        [self.lblName setText:name];
        
        self.btnDelete = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.btnDelete setFrame:CGRectMake(235, 5, 44, 44)];
        [self.btnDelete setImage:ImageWithFile(@"删除行.png") forState:UIControlStateNormal];
        [self.btnDelete setImage:ImageWithFile(@"删除行_点击.png") forState:UIControlStateHighlighted];
        [self.btnDelete addTarget:self action:@selector(btnDeleteHandler) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnDelete];
        
        [self setUserInteractionEnabled:YES];
        UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickHandler:)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}

- (void)btnDeleteHandler
{
    [delegate cellDelete:self];
}

- (void)clickHandler:(UITapGestureRecognizer *)gesture
{
    [delegate cellClick:self];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
