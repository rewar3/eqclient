//
//  CustomPageControl.m
//  EQClient
//
//  Created by xumeng on 12/12/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import "CustomPageControl.h"
@implementation CustomPageControl

- (void)setNumberOfPages:(NSInteger)numberOfPages
{
    _numberOfPages = numberOfPages;
    [self addPage];
}

- (void)setCurrentPage:(NSInteger)currentPage
{
    _currentPage = currentPage;
    [self changePage];
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _normalImage = ImageWithFile(@"默认.png");
        _selectedImage = ImageWithFile(@"当前.png");
        _indicators = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)addPage
{
    if(_indicators.count==_numberOfPages||_numberOfPages>kMaxPageCount)return;
    UIImageView *dot = [[UIImageView alloc]initWithImage:_normalImage];
    [dot setFrame:CGRectMake(35*_numberOfPages, 0, 30, 5)];
    [self addSubview:dot];
    [_indicators addObject:dot];
}

- (void)changePage
{
    if(_currentPage>kMaxPageCount-1)return;
    if(_currentPage<_numberOfPages)
    {
        UIImageView *dot;
        
        for (int i=0; i<_indicators.count;i++ ) {
            dot =[_indicators objectAtIndex:i];
            [dot setImage:_normalImage];
        }
        
        dot =[_indicators objectAtIndex:_currentPage];
        [dot setImage:_selectedImage];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
