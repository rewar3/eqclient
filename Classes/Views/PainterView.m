//
//  PainterView.m
//  Painter
//
//  Created by  ibokan on 10-9-7.
//  Copyright 2010 tencent.com. All rights reserved.
//

#import "PainterView.h"
#import "Squiggle.h"

@implementation PainterView
@synthesize color;
@synthesize lineWidth;
@synthesize squigglesDic;
@synthesize finishSquiggles ;
@synthesize eraseWidth ;
@synthesize eraseStatus ;
@synthesize delegate = delegate_ ;

CGPoint midPoint(CGPoint p1, CGPoint p2)
{
    return CGPointMake((p1.x + p2.x) * 0.5, (p1.y + p2.y) * 0.5);
}
CGFloat distance(CGPoint point1,CGPoint point2)
{
    CGFloat dx = point2.x - point1.x;
    CGFloat dy = point2.y - point1.y;
    return sqrt(dx*dx + dy*dy );
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
	{
        
        eraseStatus = NO ;
		squigglesDic = [[NSMutableDictionary alloc] init];
		finishSquiggles = [[NSMutableArray alloc] init];
		color = [[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:1];
        //set default lineWith
		lineWidth = 3;
        eraseWidth = 3 ;
        
    }
    
    return self;
}
- (void)awakeFromNib
{
    eraseStatus = NO ;
    squigglesDic = [[NSMutableDictionary alloc] init];
    finishSquiggles = [[NSMutableArray alloc] init];
    redoSquiggles = [[NSMutableArray alloc] init];
    color = [[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:1];
    //set default lineWith
    lineWidth = 8;
    eraseWidth = 8 ;
}
- (void)drawRect:(CGRect)rect
{
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	for (Squiggle *squiggle in finishSquiggles)
	{
		[self drawSquiggle:squiggle inContext:context];
	}
	
	for (NSString *key in squigglesDic)
	{
		Squiggle *squiggle = [squigglesDic valueForKey:key];
		[self drawSquiggle:squiggle inContext:context];
	}
    
}

- (void)drawSquiggle:(Squiggle *)squiggle inContext:(CGContextRef)context
{
    
    NSMutableArray *pointArray1 = [squiggle pointsArray];
    
    UIColor *squiggleColor = squiggle.strokeColor;
    CGColorRef colorRef = [squiggleColor CGColor];
    CGContextSetStrokeColorWithColor(context, colorRef);
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    
    CGContextSetAllowsAntialiasing(context, YES);
    /*
    //复制第1个元素值到firstPoint点中
    CGPoint firstPoint = [(NSValue *)[[pointArray1 objectAtIndex:0] objectForKey:@"point"] CGPointValue];
    CGContextSetLineWidth(context, [(NSNumber *)[[pointArray1 objectAtIndex:0] objectForKey:@"width"] floatValue]);
    //	[[pointArray1 objectAtIndex:0] getValue:&firstPoint];
    CGContextMoveToPoint(context, firstPoint.x, firstPoint.y);
    for (int i = 1; i < [pointArray1 count]; i++)
    {
        //将下一个点复制值,然后再在这两点之间画线
        CGPoint point = [(NSValue *)[[pointArray1 objectAtIndex:i] objectForKey:@"point"] CGPointValue];;
        CGContextSetLineWidth(context, [(NSNumber *)[[pointArray1 objectAtIndex:0] objectForKey:@"width"] floatValue]);
        CGContextAddLineToPoint(context, point.x, point.y);
    }
    
    CGContextStrokePath(context);
    */

    if ([pointArray1 count] >= 3) {
        
        for (int i = 0; i < [pointArray1 count]; i++)
        {
            
            //将下一个点复制值,然后再在这两点之间画线
            if (i == 0)
            {
                CGPoint point0 = [(NSValue *)[[pointArray1 objectAtIndex:i] objectForKey:@"point"] CGPointValue];
                
                CGContextSetLineWidth(context, [(NSNumber *)[[pointArray1 objectAtIndex:0] objectForKey:@"width"] floatValue]);
                CGContextMoveToPoint(context, point0.x, point0.y);
                
            }
            else
            {                                
                if (i == 1)
                {
                    CGContextAddLineToPoint(context, midPoint([(NSValue *)[[pointArray1 objectAtIndex:0] objectForKey:@"point"] CGPointValue], [(NSValue *)[[pointArray1 objectAtIndex:1] objectForKey:@"point"] CGPointValue]).x, midPoint([(NSValue *)[[pointArray1 objectAtIndex:0] objectForKey:@"point"] CGPointValue], [(NSValue *)[[pointArray1 objectAtIndex:1] objectForKey:@"point"] CGPointValue]).y);
                    CGContextSetLineWidth(context, [(NSNumber *)[[pointArray1 objectAtIndex:i] objectForKey:@"width"] floatValue]);
                }
                else
                {   
                    CGContextMoveToPoint(context, midPoint([(NSValue *)[[pointArray1 objectAtIndex:i-2] objectForKey:@"point"] CGPointValue], [(NSValue *)[[pointArray1 objectAtIndex:i-1] objectForKey:@"point"] CGPointValue]).x, midPoint([(NSValue *)[[pointArray1 objectAtIndex:i-2] objectForKey:@"point"] CGPointValue], [(NSValue *)[[pointArray1 objectAtIndex:i-1] objectForKey:@"point"] CGPointValue]).y);
                    
                    
                    CGContextAddQuadCurveToPoint(context, [(NSValue *)[[pointArray1 objectAtIndex:i-1] objectForKey:@"point"] CGPointValue].x, [(NSValue *)[[pointArray1 objectAtIndex:i-1] objectForKey:@"point"] CGPointValue].y, midPoint([(NSValue *)[[pointArray1 objectAtIndex:i-1] objectForKey:@"point"] CGPointValue], [(NSValue *)[[pointArray1 objectAtIndex:i] objectForKey:@"point"] CGPointValue]).x, midPoint([(NSValue *)[[pointArray1 objectAtIndex:i-1] objectForKey:@"point"] CGPointValue], [(NSValue *)[[pointArray1 objectAtIndex:i] objectForKey:@"point"] CGPointValue]).y );
                    CGContextSetLineWidth(context, [(NSNumber *)[[pointArray1 objectAtIndex:i] objectForKey:@"width"] floatValue]);
                }
            
            }
        }
    }
    CGContextStrokePath(context);
    
	
}

//触摸点开始记录,一移动再记录,这样就记录无数多个点便成了线
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [delegate_ willDraw:self];
    
	NSArray *array = [touches allObjects];
	
	for (UITouch *touch in array)
	{
        previousPoint1 = [touch previousLocationInView:self];
        previousPoint2 = [touch previousLocationInView:self];
        currentPoint = [touch locationInView:self];
        
		Squiggle *squiggle = [[Squiggle alloc] init];
        squiggle.isErase = eraseStatus ;
        if (eraseStatus == NO) 
        {
            [squiggle setLineWidth:lineWidth];  // 设置线宽
            //self  指当前这次触摸事件
            [squiggle addPoint:[touch locationInView:self] withLineWidth:lineWidth];
        }
        else
        {
            [squiggle setLineWidth:eraseWidth];  // 设置橡皮擦线宽
            //self  指当前这次触摸事件
            [squiggle addPoint:[touch locationInView:self] withLineWidth:eraseWidth];
        }
		
		[squiggle setStrokeColor:color];    // 设置画笔颜色
		
        
		
		//因为每次触摸事件的 内存地址唯一,所以将其作为 squiggle的关键字key
		//将内存地址转换为对象
		NSValue *touchValue = [NSValue valueWithPointer:(__bridge const void *)(touch)];
		NSString *key = [NSString stringWithFormat:@"%@", touchValue];
		
		[squigglesDic setValue:squiggle forKey:key];

		
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSArray *array = [touches allObjects];
	
	for(UITouch *touch in array)
	{
        previousPoint2  = previousPoint1;
        previousPoint1  = [touch previousLocationInView:self];
        currentPoint    = [touch locationInView:self];
        
        CGPoint mid1    = midPoint(previousPoint1, previousPoint2);
        CGPoint mid2    = midPoint(currentPoint, previousPoint1);
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:mid1];
        [path addQuadCurveToPoint:mid2 controlPoint:previousPoint1];
        
		//这里因为在前面 touchBegan中已经存好了相应的key,每一次触摸都有一关键字对应,这里只是在触摸的基础上移动画点
		NSValue *touchValue = [NSValue valueWithPointer:(__bridge const void *)(touch)];
		
		Squiggle *squiggle = [squigglesDic
							  valueForKey:[NSString stringWithFormat:@"%@",touchValue]];
		CGPoint current = [touch locationInView:self];
//		CGPoint previous = [touch previousLocationInView:self];
		//一直将当前点加进pointsArray中
        if (eraseStatus == YES) 
        {
            [squiggle addPoint:current withLineWidth:eraseWidth];
        }
        else
        {
            [squiggle addPoint:current withLineWidth:lineWidth];
        }
		
		
		/*更新后,得声明重新执行下 drawRect方法,系统不会自动执行
		CGPoint lower,higher;
		lower.x = (previous.x > current.x ? current.x : previous.x);
		lower.y = (previous.y > current.y ? current.y : previous.y);
		higher.x = (previous.x < current.x ? current.x : previous.x);
		higher.y = (previous.y < current.y ? current.y : previous.y);
		*/
		// redraw the screen in the required region
        
        if (eraseStatus == NO) 
        {
            CGRect drawBox = path.bounds;
            drawBox.origin.x        -= self.lineWidth * 2;
            drawBox.origin.y        -= self.lineWidth * 2;
            drawBox.size.width      += self.lineWidth * 4;
            drawBox.size.height     += self.lineWidth * 4;
            [self setNeedsDisplayInRect:drawBox];
            /*
            [self setNeedsDisplayInRect:CGRectMake(lower.x-2*lineWidth,
                                                   lower.y-2*lineWidth, higher.x - lower.x + lineWidth*3,
                                                   higher.y - lower.y + lineWidth * 3)];
            */
        }
        else
        {
//            [self setNeedsDisplayInRect:CGRectMake(lower.x-2*eraseWidth,
//                                                   lower.y-2*eraseWidth, higher.x - lower.x + eraseWidth*3,
//                                                   higher.y - lower.y + eraseWidth * 3)];

        }
        
		
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	for(UITouch *touch in touches)
	{
		NSValue *touchValue = [NSValue valueWithPointer:(__bridge const void *)(touch)];
		NSString *key = [NSString stringWithFormat:@"%@", touchValue];
		Squiggle *squiggle = [squigglesDic valueForKey:key];
		if (!squiggle) {
            return ;
        }
		[finishSquiggles addObject:squiggle];
		[squigglesDic removeObjectForKey:key];
        NSLog(@"%s",__FUNCTION__);
	}
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"%s",__FUNCTION__);
}

- (void)resetView
{
	[squigglesDic removeAllObjects];
	[finishSquiggles removeAllObjects];
	[self setNeedsDisplay];
}

- (void)erase
{
	self.color = [UIColor clearColor];
//    self.eraseStatus = !self.eraseStatus ;
}

- (void)undo
{
    if ([finishSquiggles count] != 0) 
    {
        [redoSquiggles addObject:[finishSquiggles lastObject]];
        [finishSquiggles removeLastObject];
    }
    
    [self setNeedsDisplay];
}
- (void)redo 
{
    if ([redoSquiggles count] != 0) 
    {
        [finishSquiggles addObject:[redoSquiggles lastObject]];
        [redoSquiggles removeLastObject];
    }
    [self setNeedsDisplay];
    
}

- (void)dealloc
{
	squigglesDic = nil;
	finishSquiggles = nil;
	color = nil;

}

@end
