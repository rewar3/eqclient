//
//  BubbleView.m
//  EQClient
//
//  Created by rewar on 13-12-16.
//  Copyright (c) 2013年 xumeng. All rights reserved.
//

#import "BubbleView.h"
@interface BubbleView()
{
    @private
    UIImageView *_imgArrow;
}

@end
@implementation BubbleView
@synthesize imgBG;
@synthesize bubbleType;
@synthesize buttonAry;
@synthesize delegate;
+ (BubbleView *)bubbleViewWithType:(BubbleViewType) type
{
    BubbleView *bubbleView = [[BubbleView alloc]initWithBubbleViewType:type];
    return bubbleView;
}

- (id)initWithBubbleViewType:(BubbleViewType) type
{
    self = [super init];
    if(self)
    {
        
        bubbleType = type;
        switch (type) {
            case BubbleViewTypeCamera:
            {
                self.imgBG = [[UIImageView alloc]initWithImage:ImageWithFile(@"相机框.png")];
                [self.imgBG setFrame:CGRectMake(0, 0, 90, 217)];
                [self addSubview:self.imgBG];
                [self setFrame:CGRectMake(0, 0, self.imgBG.frame.size.width, self.imgBG.frame.size.height)];
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setFrame:CGRectMake(16, 16, 58, 44)];
                [button setImage:ImageWithFile(@"新建.png") forState:UIControlStateNormal];
                [button setImage:ImageWithFile(@"新建_点击.png") forState:UIControlStateHighlighted];
                [button setTag:0];
                [button addTarget:self action:@selector(buttonHandler:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:button];
                
                
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setFrame:CGRectMake(16, 84, 58, 44)];
                [button setImage:ImageWithFile(@"拍照.png") forState:UIControlStateNormal];
                [button setImage:ImageWithFile(@"拍照_点击.png") forState:UIControlStateHighlighted];
                [button setTag:1];
                [button addTarget:self action:@selector(buttonHandler:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:button];
                
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setFrame:CGRectMake(16, 152, 58, 44)];
                [button setImage:ImageWithFile(@"相册.png") forState:UIControlStateNormal];
                [button setImage:ImageWithFile(@"相册_点击.png") forState:UIControlStateHighlighted];
                [button setTag:2];
                [button addTarget:self action:@selector(buttonHandler:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:button];
                
                UIView *line = [[UIView alloc]initWithFrame:CGRectMake(6, 72, 78, 1)];
                [line setBackgroundColor:RGBCOLOR(44, 44, 44)];
                [self addSubview:line];
                
                line = [[UIView alloc]initWithFrame:CGRectMake(6, 136, 78, 1)];
                [line setBackgroundColor:RGBCOLOR(44, 44, 44)];
                [self addSubview:line];
                
            }
                break;
            case BubbleViewTypeColor:
            {
                self.imgBG = [[UIImageView alloc]initWithImage:ImageWithFile(@"颜色框背景.png")];
                [self.imgBG setFrame:CGRectMake(0, 0, 60, 272)];
                [self addSubview:self.imgBG];
                [self setFrame:CGRectMake(0, 0, self.imgBG.frame.size.width, self.imgBG.frame.size.height)];
                
                _imgArrow = [[UIImageView alloc]initWithImage:ImageWithFile(@"颜色选中.png")];
                [_imgArrow setFrame:CGRectMake(0, 0, 60, 44)];
                [self addSubview:_imgArrow];
                
                UIButton *button;
                for(int i = 0;i<6;i++)
                {
                    button = [UIButton buttonWithType:UIButtonTypeCustom];
                    [button setFrame:CGRectMake(12, 41 * i + 12, 36, 36)];
                    NSString *path =[NSString stringWithFormat:@"c_%d.png",i];
                    [button setImage:ImageWithFile(path) forState:UIControlStateNormal];
                    [button setImage:ImageWithFile(path) forState:UIControlStateSelected];
                    [button setTag:i];
                    [button addTarget:self action:@selector(buttonHandler:) forControlEvents:UIControlEventTouchUpInside];
                    [self addSubview:button];
                }
                
                
                
            }
                break;
            case BubbleViewTypeLineWidth:
            {
                buttonAry = [[NSMutableArray alloc]init];
                self.imgBG = [[UIImageView alloc]initWithImage:ImageWithFile(@"颜色框新.png")];
                [self.imgBG setFrame:CGRectMake(0, 0, 90, 142)];
                [self addSubview:self.imgBG];
                [self setFrame:CGRectMake(0, 0, self.imgBG.frame.size.width, self.imgBG.frame.size.height)];
                UIButton *button;
                for(int i = 0;i<3;i++)
                {
                    button = [UIButton buttonWithType:UIButtonTypeCustom];
                    [button setFrame:CGRectMake(15, i*43+7, 58, 36)];
                   
                    NSString *path =[NSString stringWithFormat:@"l_%d.png",i];
                    [button setImage:ImageWithFile(path) forState:UIControlStateNormal];
                    path =[NSString stringWithFormat:@"l_%ds.png",i];
                    [button setImage:ImageWithFile(path) forState:UIControlStateSelected];
                    [button setTag:i];
                    [button addTarget:self action:@selector(buttonHandler:) forControlEvents:UIControlEventTouchUpInside];
                    [self addSubview:button];
                    [self.buttonAry addObject:button];
                }
                
            }
                break;
            case BubbleViewTypeDelete:
            {
                self.imgBG = [[UIImageView alloc]initWithImage:ImageWithFile(@"颜色框.png")];
                [self.imgBG setFrame:CGRectMake(0, 0, 150, 107)];
                [self addSubview:self.imgBG];
                [self setFrame:CGRectMake(0, 0, self.imgBG.frame.size.width, self.imgBG.frame.size.height)];
                UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(22, 15, 120, 15)];
                [lblTitle setText:@"是否全部擦除？"];
                [lblTitle setBackgroundColor:[UIColor clearColor]];
                [lblTitle setTextColor:[UIColor whiteColor]];
                [lblTitle setFont:[UIFont systemFontOfSize:16]];
                [self addSubview:lblTitle];
                
                UIView *line = [[UIView alloc]initWithFrame:CGRectMake(75, 40, 1, 40)];
                [line setBackgroundColor:[UIColor blackColor]];
                [line setAlpha:0.5];
                [self addSubview:line];
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setFrame:CGRectMake(20, 40, 40, 40)];
                [button setTitle:@"是" forState:UIControlStateNormal];
                [button setTitle:@"是" forState:UIControlStateSelected];
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [button setTitleColor:RGBCOLOR(161, 235, 52) forState:UIControlStateSelected];
                [button setTag:0];
                [button addTarget:self action:@selector(buttonHandler:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:button];
                
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setFrame:CGRectMake(90, 40, 40, 40)];
                [button setTitle:@"否" forState:UIControlStateNormal];
                [button setTitle:@"否" forState:UIControlStateSelected];
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [button setTitleColor:RGBCOLOR(161, 235, 52) forState:UIControlStateSelected];
                [button setTag:1];
                [button addTarget:self action:@selector(buttonHandler:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:button];
                
            }
                break;
            case BubbleViewTypeIndex:
            {
                self.imgBG = [[UIImageView alloc]initWithImage:ImageWithFile(@"弹出.png")];
                [self.imgBG setFrame:CGRectMake(0, 0, 90, 147)];
                [self addSubview:self.imgBG];
                [self setFrame:CGRectMake(0, 0, self.imgBG.frame.size.width, self.imgBG.frame.size.height)];
                
                UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setFrame:CGRectMake(16, 16, 58, 44)];
                [button setImage:ImageWithFile(@"拍照.png") forState:UIControlStateNormal];
                [button setImage:ImageWithFile(@"拍照_点击.png") forState:UIControlStateHighlighted];
                [button setTag:0];
                [button addTarget:self action:@selector(buttonHandler:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:button];
                
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setFrame:CGRectMake(16, 84, 58, 44)];
                [button setImage:ImageWithFile(@"相册.png") forState:UIControlStateNormal];
                [button setImage:ImageWithFile(@"相册_点击.png") forState:UIControlStateHighlighted];
                [button setTag:1];
                [button addTarget:self action:@selector(buttonHandler:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:button];
                
                UIView *line = [[UIView alloc]initWithFrame:CGRectMake(6, 72, 78, 1)];
                [line setBackgroundColor:RGBCOLOR(44, 44, 44)];
                [self addSubview:line];
                
            }
                break;
            default:
                break;
        }
        
    }
    return self;
}

- (void)buttonHandler:(id)sender
{
    UIButton *button = (UIButton *)sender;
    [delegate bubbleViewButtonDidClick:button.tag bubbleType:bubbleType];
    if(bubbleType==BubbleViewTypeColor)
    {
        [self setLineColor:button.tag];
    }
    if(bubbleType == BubbleViewTypeLineWidth)
    {
        [self setLineWidth:button.tag];
    }
}

- (void)setLineColor:(NSInteger) color
{
    
    [_imgArrow setFrame:CGRectMake(0, color*41+8, 60, 44)];
    
}

- (void)setLineWidth:(NSInteger) width
{
    for (int i = 0; i<self.buttonAry.count; i++) {
        UIButton *button = [self.buttonAry objectAtIndex:i];
        if(i==width)
        {
            [button setSelected:YES];
        }
        else
        {
            [button setSelected:NO];
        }
        
    }
}

@end

