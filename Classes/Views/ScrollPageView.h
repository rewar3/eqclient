//
//  ScrollPageView.h
//  EQClient
//
//  Created by xumeng on 12/17/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageVO.h"
#import "PZPhotoView.h"
@protocol ScrollPageViewDelegate;
@interface ScrollPageView : UIView

@property (nonatomic,strong)PageVO *pageVO;
@property (nonatomic,strong)PZPhotoView *imageView;
@property (nonatomic,strong)UIImage *originalImage;
@property (nonatomic,strong)UIView *loadingView;
@property (nonatomic)BOOL isAction;
@property (nonatomic,weak)id<ScrollPageViewDelegate> delegate;

- (instancetype)initWithGUID:(NSString *)guidStr;

- (void)imageRequestDidLoad:(NSDictionary *)dic;

- (void)loadImage;
@end

@protocol ScrollPageViewDelegate <NSObject>

- (void)ScrollPage:(ScrollPageView *)scrollPage playVideoWithUrl:(NSString *)url;

@end