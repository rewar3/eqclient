//
//  ToolBarView.m
//  EQClient
//
//  Created by xumeng on 12/11/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import "ToolBarView.h"
#import "NSObject+Utils.h"
#import "BubbleView.h"

#define charAry @[ @"A",@"B",@"C",@"D",@"E" ]

@interface ToolBarView()
{
    @private
    BOOL _isBubbleShow;
    
    BOOL _bubColorShow;
    BOOL _bubLineWidthShow;
    BOOL _bubDeleteShow;
    
    UIView *_indexHolder;
    UIView *_paintHolder;
    UIView *_answerHolder;
    
}
@end

@implementation ToolBarView

@synthesize imgBG;
@synthesize currentType;

@synthesize btnPaint;
@synthesize imgTextBG;
@synthesize btnPostText;
@synthesize btnMore;
@synthesize txtMessage;
@synthesize delegate;
@synthesize lblTip;

@synthesize btnBack;
@synthesize btnChooseColor;
@synthesize btnEraser;
@synthesize btnLineWidth;
@synthesize lblThin;
@synthesize lblThick;
@synthesize btnDelete;
@synthesize btnPostImage;
@synthesize isEraser;

@synthesize btnAry;
@synthesize btnPostAnswer;

- (instancetype)initWithToolBarType:(ToolBarType) type
{
    self = [super init];
    if(self)
    {
        
        imgBG = [[UIImageView alloc]initWithImage:ImageWithFile(@"工具栏.png")];
        [imgBG setFrame:CGRectMake(0, 0, 1024, kBarHeight)];
        [self addSubview:imgBG];
        
     //index mode
        _indexHolder = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kBarHeight)];
        [_indexHolder setHidden:YES];
        [self addSubview:_indexHolder];
        
        btnPaint = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnPaint setFrame:CGRectMake(201, 12, 36, 36)];
        [btnPaint setImage:ImageWithFile(@"画笔.png") forState:UIControlStateNormal];
        [btnPaint setImage:ImageWithFile(@"画笔_点击.png") forState:UIControlStateHighlighted];
        [btnPaint addTarget:self action:@selector(btnPaintHandler) forControlEvents:UIControlEventTouchUpInside];
        [_indexHolder addSubview:btnPaint];
        
        imgTextBG = [[UIImageView alloc]initWithImage:ImageWithFile(@"输入框.png")];
        [imgTextBG setFrame:CGRectMake(267, 8, 490, 44)];
        [_indexHolder addSubview:imgTextBG];
        
        txtMessage = [[UITextField alloc]initWithFrame:CGRectMake(269, 10, 486, 40)];
        [txtMessage setTextColor:[UIColor whiteColor]];
        [txtMessage setBackgroundColor:[UIColor clearColor]];
        [txtMessage setFont:[UIFont boldSystemFontOfSize:18]];
        [txtMessage setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [txtMessage setBorderStyle:UITextBorderStyleNone];
        [txtMessage setReturnKeyType:UIReturnKeyDone];
        [txtMessage addTarget:self action:@selector(txtMessageFinished:) forControlEvents:UIControlEventEditingDidEndOnExit];
        
        [txtMessage setDelegate:self];
        [txtMessage setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"请输入文字参与互动" attributes:@{NSForegroundColorAttributeName: RGBCOLOR(64, 64, 64)}]];
        [_indexHolder addSubview:txtMessage];
        
        btnPostText = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnPostText setFrame:CGRectMake(782, 13, 80, 34)];
        [btnPostText setBackgroundImage:ImageWithFile(@"发送按钮.png") forState:UIControlStateNormal];
        [btnPostText setBackgroundImage:ImageWithFile(@"发送按钮_点击.png") forState:UIControlStateHighlighted];
        [btnPostText setBackgroundImage:ImageWithFile(@"发送按钮_灰色.png") forState:UIControlStateDisabled];
        [btnPostText setTitle:@"发送" forState:UIControlStateNormal];
        [btnPostText setTitle:@"发送" forState:UIControlStateHighlighted];
        [btnPostText setTitle:@"发送" forState:UIControlStateDisabled];
        [btnPostText setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnPostText setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [btnPostText setTitleColor:RGBCOLOR(214, 214, 214) forState:UIControlStateDisabled];
        [btnPostText.titleLabel setFont:[UIFont systemFontOfSize:20]];
        [btnPostText addTarget:self action:@selector(btnPostTextHandler) forControlEvents:UIControlEventTouchUpInside];
        [btnPostText setEnabled:NO];
        [_indexHolder addSubview:btnPostText];
        
        btnMore = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnMore setFrame:CGRectMake(886, 12, 36, 36)];
        [btnMore setImage:ImageWithFile(@"更多.png") forState:UIControlStateNormal];
        [btnMore setImage:ImageWithFile(@"更多_点击.png") forState:UIControlStateHighlighted];
        [btnMore setImage:ImageWithFile(@"更多_点击.png") forState:UIControlStateSelected];
        [btnMore addTarget:self action:@selector(btnMoreHandler) forControlEvents:UIControlEventTouchUpInside];
        [_indexHolder addSubview:btnMore];
        
        
        _isBubbleShow = NO;
    
    //paint mode
        
        _paintHolder = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kBarHeight)];
        [_paintHolder setHidden:YES];
        [self addSubview:_paintHolder];
        

        btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setFrame:CGRectMake(200, 12, 36, 36)];//360
        [btnBack setImage:ImageWithFile(@"关闭.png") forState:UIControlStateNormal];
        [btnBack setImage:ImageWithFile(@"关闭_点击.png") forState:UIControlStateHighlighted];
        [btnBack addTarget:self action:@selector(btnBackHandler) forControlEvents:UIControlEventTouchUpInside];
        [_paintHolder addSubview:btnBack];
        
        btnChooseColor = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnChooseColor setFrame:CGRectMake(360, 12, 36, 36)];
        [btnChooseColor addTarget:self action:@selector(btnChooseColorHandler) forControlEvents:UIControlEventTouchUpInside];
        [_paintHolder addSubview:btnChooseColor];
        
        isEraser = NO;
//        btnEraser = [UIButton buttonWithType:UIButtonTypeCustom];
//        [btnEraser setFrame:CGRectMake(434+offset, 12, 36, 36)];
//        [btnEraser setImage:ImageWithFile(@"橡皮.png") forState:UIControlStateNormal];
//        [btnEraser setImage:ImageWithFile(@"橡皮_选中.png") forState:UIControlStateHighlighted];
//        [btnEraser setImage:ImageWithFile(@"橡皮_选中.png") forState:UIControlStateSelected];
//        [btnEraser addTarget:self action:@selector(btnEraserHandler) forControlEvents:UIControlEventTouchUpInside];
//        [_paintHolder addSubview:btnEraser];
        
        btnLineWidth = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnLineWidth setFrame:CGRectMake(480, 12, 58, 36)];
        [btnLineWidth setImage:ImageWithFile(@"l_1.png") forState:UIControlStateNormal];
        [btnLineWidth setImage:ImageWithFile(@"l_1.png") forState:UIControlStateHighlighted];
        [btnLineWidth setImage:ImageWithFile(@"l_1.png") forState:UIControlStateSelected];
        [btnLineWidth addTarget:self action:@selector(btnLineWidthHandler) forControlEvents:UIControlEventTouchUpInside];
        [_paintHolder addSubview:btnLineWidth];
        
        
        btnDelete = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDelete setFrame:CGRectMake(627, 12, 36, 36)];
        [btnDelete setImage:ImageWithFile(@"删除.png") forState:UIControlStateNormal];
        [btnDelete setImage:ImageWithFile(@"删除_点击.png") forState:UIControlStateHighlighted];
        [btnDelete addTarget:self action:@selector(btnDeleteHandler) forControlEvents:UIControlEventTouchUpInside];
        [_paintHolder addSubview:btnDelete];
        
        btnPostImage = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnPostImage setFrame:CGRectMake(782, 12, 80, 34)];//660
        [btnPostImage setBackgroundImage:ImageWithFile(@"发送按钮.png") forState:UIControlStateNormal];
        [btnPostImage setBackgroundImage:ImageWithFile(@"发送按钮_点击.png") forState:UIControlStateHighlighted];
        [btnPostImage setBackgroundImage:ImageWithFile(@"发送按钮_灰色.png") forState:UIControlStateDisabled];
        [btnPostImage setTitle:@"发送" forState:UIControlStateNormal];
        [btnPostImage setTitle:@"发送" forState:UIControlStateHighlighted];
        [btnPostImage setTitle:@"发送" forState:UIControlStateDisabled];
        [btnPostImage setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnPostImage setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [btnPostImage setTitleColor:RGBCOLOR(214, 214, 214) forState:UIControlStateDisabled];
        [btnPostImage.titleLabel setFont:[UIFont systemFontOfSize:20]];
        [btnPostImage addTarget:self action:@selector(btnPostImageHandler) forControlEvents:UIControlEventTouchUpInside];
        [_paintHolder addSubview:btnPostImage];
        
        NSNumber *colorIndex = GetObjectUserDefault(kPainterLineColor);
        NSInteger index;
        if(colorIndex)
        {
            index = [colorIndex integerValue];
            NSString *imageName = [NSString stringWithFormat:@"c_%ld.png",(long)index];
            [btnChooseColor setImage:ImageWithFile(imageName) forState:UIControlStateNormal];
            [btnChooseColor setImage:ImageWithFile(imageName) forState:UIControlStateSelected];
        }
        else
        {
            [btnChooseColor setImage:ImageWithFile(@"c_3.png") forState:UIControlStateNormal];
            [btnChooseColor setImage:ImageWithFile(@"c_3.png") forState:UIControlStateSelected];
            SetObjectUserDefault([NSNumber numberWithInt:LineColorRed], kPainterLineColor);
        }
        
        _bubColorShow = NO;
        _bubDeleteShow = NO;
        _bubLineWidthShow = NO;
    
    //answer mode
        _answerHolder = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kBarHeight)];
        [_answerHolder setHidden:YES];
        [self addSubview:_answerHolder];
        
        
        btnAry = [[NSMutableArray alloc]init];
        UIButton *button;
        for (int i=0; i<charAry.count; i++) {
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setFrame:CGRectMake(i*(10+70)+300, 5, 70, 50)];
            
            [button setBackgroundImage:ImageWithFile(@"选项.png") forState:UIControlStateNormal];
            [button setBackgroundImage:ImageWithFile(@"选项_选中.png") forState:UIControlStateSelected];
            
            [button setTitle:[charAry objectAtIndex:i] forState:UIControlStateNormal];
            [button setTitle:[charAry objectAtIndex:i] forState:UIControlStateSelected];
            
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button setTitleColor:RGBCOLOR(50, 50, 50) forState:UIControlStateSelected];
            
            [button setTag:i];
            [button addTarget:self action:@selector(btnAnswerHandler:) forControlEvents:UIControlEventTouchUpInside];
            
            [btnAry addObject:button];
            [_answerHolder addSubview:button];
        }
        
        btnPostAnswer = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnPostAnswer setFrame:CGRectMake(782, 5, 70, 50)];
        
        [btnPostAnswer setBackgroundImage:ImageWithFile(@"提交.png") forState:UIControlStateNormal];
        [btnPostAnswer setBackgroundImage:ImageWithFile(@"提交_点击.png") forState:UIControlStateSelected];
        
        [btnPostAnswer setTitle:@"发送" forState:UIControlStateNormal];
        [btnPostAnswer setTitle:@"发送" forState:UIControlStateSelected];
        
        [btnPostAnswer setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnPostAnswer setTitleColor:RGBCOLOR(50, 50, 50) forState:UIControlStateSelected];
        [btnPostAnswer addTarget:self action:@selector(btnPostAnswerHandler) forControlEvents:UIControlEventTouchUpInside];
        
        [_answerHolder addSubview:btnPostAnswer];
        
        
        lblTip = [[UILabel alloc]initWithFrame:CGRectMake(450, -37, 100, 20)];
        [lblTip setBackgroundColor:[UIColor clearColor]];
        [lblTip setText:@"已发送"];
        [lblTip setHidden:YES];
        [lblTip setFont:[UIFont boldSystemFontOfSize:20]];
        [lblTip setTextColor:[UIColor whiteColor]];
        [self addSubview:lblTip];
        
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(bubbleViewDidShowHandler:) name:kBubbleViewDidShow object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(bubbleViewDidHideHandler:) name:kBubbleViewDidHide object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(bubbleViewSelectHandler:) name:kBubbleViewSelect object:nil];
        
        [self changeModeWithToolBarType:type completion:nil];
        
    }
    
    
    
    return self;
}

- (void)changeModeWithToolBarType:(ToolBarType) type completion:(void (^)(BOOL finished))completion
{
    if(currentType==type)return;
    UIView *view = [self getViewWithToolBarType:currentType];
    if(currentType!=0)
    {
        switch (type) {
            case ToolBarTypeIndex:
            {
                [_indexHolder setHidden:NO];
                [_indexHolder setAlpha:0];
                [_indexHolder setFrame:CGRectMake(0, kBarHeight, SCREEN_WIDTH, kBarHeight)];
                [UIView animateWithDuration:0.15 animations:^{
                    [view setFrame:CGRectMake(0, kBarHeight, SCREEN_WIDTH, kBarHeight)];
                    [view setAlpha:0];
                    [_indexHolder setFrame:CGRectMake(0, 0, SCREEN_WIDTH, kBarHeight)];
                    [_indexHolder setAlpha:1];
                } completion:^(BOOL finished) {
                    [view setHidden:YES];
                    if(completion)completion(YES);
                }];
            }
                break;
            case ToolBarTypePaint:
            {
                [_paintHolder setHidden:NO];
                [_paintHolder setAlpha:0];
                [_paintHolder setFrame:CGRectMake(0, kBarHeight, SCREEN_WIDTH, kBarHeight)];
                [UIView animateWithDuration:0.15 animations:^{
                    [view setFrame:CGRectMake(0, kBarHeight, SCREEN_WIDTH, kBarHeight)];
                    [view setAlpha:0];
                    [_paintHolder setFrame:CGRectMake(0, 0, SCREEN_WIDTH, kBarHeight)];
                    [_paintHolder setAlpha:1];
                } completion:^(BOOL finished) {
                    [view setHidden:YES];
                    if(completion)completion(YES);
                }];
            }
                break;
            case ToolBarTypeAnswer:
            {
                [self resetButton];
                [_answerHolder setHidden:NO];
                [_answerHolder setAlpha:0];
                [_answerHolder setFrame:CGRectMake(0, kBarHeight, SCREEN_WIDTH, kBarHeight)];
                [UIView animateWithDuration:0.15 animations:^{
                    [view setFrame:CGRectMake(0, kBarHeight, SCREEN_WIDTH, kBarHeight)];
                    [view setAlpha:0];
                    [_answerHolder setFrame:CGRectMake(0, 0, SCREEN_WIDTH, kBarHeight)];
                    [_answerHolder setAlpha:1];
                } completion:^(BOOL finished) {
                    [view setHidden:YES];
                    if(completion)completion(YES);
                }];
            }
                break;
            default:
                break;
        }
        
    }
    else
    {
        [_indexHolder setHidden:NO];
        if(completion)completion(YES);
    }
    currentType = type;
    
}

- (UIView *)getViewWithToolBarType:(ToolBarType)type
{
    UIView *view;
    
    switch (type) {
        case ToolBarTypeIndex:
            view = _indexHolder;
            break;
        case ToolBarTypePaint:
            view = _paintHolder;
            break;
        case ToolBarTypeAnswer:
            view = _answerHolder;
            break;
        default:
            view = _indexHolder;
            break;
    }
    return view;
}

- (void)showTip
{
    [lblTip setHidden:NO];
    [lblTip setAlpha:0];
    [lblTip setFrame:CGRectMake(450, -17, 100, 20)];
    [UIView animateWithDuration:0.5 animations:^{
        [lblTip setAlpha:1];
        [lblTip setFrame:CGRectMake(450, -37, 100, 20)];
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.5 delay:1.5 options:UIViewAnimationOptionCurveLinear animations:^{
            [lblTip setAlpha:0];
            [lblTip setFrame:CGRectMake(450, -47, 100, 20)];
        } completion:^(BOOL finished) {
            [lblTip setHidden:YES];
        }];
    }];
}

- (void)btnMoreHandler
{
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:0],@"type",[NSNumber numberWithInteger:0],@"index", nil];
    if(_isBubbleShow)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewHide object:dic];
    }
    else
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewShow object:dic];
    }
}

- (void)txtMessageFinished:(id)sender
{
    [delegate postText:txtMessage.text];
    [txtMessage setText:@""];
    [txtMessage resignFirstResponder];
    [btnPostText setEnabled:NO];
    [self showTip];
}

- (void)btnPostTextHandler
{
    [delegate postText:txtMessage.text];
    [txtMessage setText:@""];
    [txtMessage resignFirstResponder];
    [btnPostText setEnabled:NO];
    [self showTip];
}

- (void)btnPaintHandler
{
    [[NSNotificationCenter defaultCenter]postNotificationName:kOpenPaintMode object:nil];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter]postNotificationName:kOpenTextMode object:nil];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter]postNotificationName:kCloseTextMode object:nil];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *tmpStr = @"" ;
    tmpStr = textField.text ;
    tmpStr = [tmpStr stringByReplacingCharactersInRange:range withString:string];
    
    if([tmpStr length]>0)
    {
        [btnPostText setEnabled:YES];
    }
    else
    {
        [btnPostText setEnabled:NO];
    }
    
    return YES;
}

- (void)bubbleViewDidShowHandler:(NSNotification *)n
{
    [btnMore setSelected:YES];
    _isBubbleShow = YES;
   
    //paint mode
    NSInteger type = [n.object integerValue];
    if(type==BubbleViewTypeColor)
    {
        _bubColorShow = YES;
    }
    if(type==BubbleViewTypeLineWidth)
    {
        _bubLineWidthShow = YES;
        [btnLineWidth setSelected:YES];
    }
    if(type==BubbleViewTypeDelete)
    {
        _bubDeleteShow = YES;
        [btnDelete setSelected:YES];
    }
}

- (void)bubbleViewDidHideHandler:(NSNotification *)n
{
    [btnMore setSelected:NO];
    _isBubbleShow = NO;
    
    //paint mode
    NSInteger type = [n.object integerValue];
    if(type==BubbleViewTypeColor)
    {
        _bubColorShow = NO;
    }
    if(type==BubbleViewTypeLineWidth)
    {
        _bubLineWidthShow = NO;
        [btnLineWidth setSelected:NO];
    }
    if(type==BubbleViewTypeDelete)
    {
        _bubDeleteShow = NO;
        [btnDelete setSelected:NO];
    }
}







- (void)btnBackHandler
{
    if(isEraser)
    {
        isEraser = NO;
        [delegate closeEraser];
        [btnEraser setSelected:NO];
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:kClosePaintMode object:nil];
}

- (void)btnPostImageHandler
{
    if(isEraser)
    {
        isEraser = NO;
        [delegate closeEraser];
        [btnEraser setSelected:NO];
    }
    [delegate postImage];
    [self showTip];
}

- (void)btnLineWidthHandler
{
    NSNumber *lineWidth = GetObjectUserDefault(kPainterLineWidth);
    NSInteger index;
    if(lineWidth)
    {
        index = [lineWidth integerValue];
    }
    else
    {
        index = 0;
        SetObjectUserDefault([NSNumber numberWithInt:0], kPainterLineWidth);
    }
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:2],@"type",[NSNumber numberWithInteger:index],@"index", nil];
    if(_bubLineWidthShow)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewHide object:dic];
    }
    else
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewShow object:dic];
    }
    
}

- (void)btnDeleteHandler
{
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:3],@"type",[NSNumber numberWithInteger:0],@"index", nil];
    if(_bubDeleteShow)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewHide object:dic];
    }
    else
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewShow object:dic];
    }
    
}

- (void)btnEraserHandler
{
    if(!isEraser)
    {
        isEraser = YES;
        [delegate openEraser];
        [btnEraser setSelected:YES];
    }
    else
    {
        isEraser = NO;
        [delegate closeEraser];
        [btnEraser setSelected:NO];
    }
}

- (void)btnChooseColorHandler
{
    
    NSNumber *colorIndex = GetObjectUserDefault(kPainterLineColor);
    NSInteger index;
    if(colorIndex)
    {
        index = [colorIndex integerValue];
        NSString *imageName = [NSString stringWithFormat:@"c_%ld.png",(long)index];
        [btnChooseColor setImage:ImageWithFile(imageName) forState:UIControlStateNormal];
        [btnChooseColor setImage:ImageWithFile(imageName) forState:UIControlStateSelected];
    }
    else
    {
        index = LineColorRed;
        [btnChooseColor setImage:ImageWithFile(@"c_2.png") forState:UIControlStateNormal];
        [btnChooseColor setImage:ImageWithFile(@"c_2.png") forState:UIControlStateSelected];
        SetObjectUserDefault([NSNumber numberWithInt:LineColorRed], kPainterLineColor);
    }
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:1],@"type",[NSNumber numberWithInteger:index],@"index", nil];
    if(_bubColorShow)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewHide object:dic];
    }
    else
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewShow object:dic];
    }
}

- (void)bubbleViewSelectHandler:(NSNotification *)n
{
    NSDictionary *dic = n.object;
    NSInteger type = [[dic objectForKey:@"type"] integerValue];
    NSInteger index = [[dic objectForKey:@"index"] integerValue];
    if(type==BubbleViewTypeColor)
    {
        NSString *imageName = [NSString stringWithFormat:@"c_%lu.png",(unsigned long)index];
        [btnChooseColor setImage:ImageWithFile(imageName) forState:UIControlStateNormal];
        [btnChooseColor setImage:ImageWithFile(imageName) forState:UIControlStateSelected];
    }
    
}


- (void)btnPostAnswerHandler
{
    NSString *answer = @"";
    for (int i=0; i<btnAry.count; i++) {
        UIButton *btn = [btnAry objectAtIndex:i];
        if(btn.isSelected)
        {
            answer = [answer stringByAppendingString:[charAry objectAtIndex:i]];
        }
    }
    
    [delegate postAnswer:answer];
    [self showTip];
}

- (void)btnAnswerHandler:(id)sender
{
    UIButton *button = sender;
    if(button.isSelected)
    {
        [button setSelected:NO];
    }
    else
    {
        [button setSelected:YES];
    }
}

- (void)resetButton
{
    for (int i=0; i<btnAry.count; i++) {
        UIButton *btn = [btnAry objectAtIndex:i];
        [btn setSelected:NO];
    }
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
