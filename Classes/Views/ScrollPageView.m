//
//  ScrollPageView.m
//  EQClient
//
//  Created by xumeng on 12/17/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import "ScrollPageView.h"
#import "AppDelegate.h"
#import "NSString+.h"
#import "NSObject+Utils.h"
#import "Page.h"


@interface ScrollPageView()
{
    @private
    int errorCount;
}
@end

@implementation ScrollPageView
@synthesize imageView;
@synthesize originalImage;
@synthesize pageVO;
@synthesize isAction;
@synthesize loadingView;
@synthesize delegate;

- (instancetype)initWithGUID:(NSString *)guidStr
{
    self = [super init];
    if(self)
    {
        PageVO *page =  [Page getPageWithGUID:guidStr];
        if(page)
        {
            self.pageVO = page;
        }
        else
        {
            self.pageVO = [Page createPageWithGUID:guidStr];
        }
        
        
        self.loadingView = [[UIView alloc]init];
        [self.loadingView setFrame:CGRectMake((kImageWidth-70)/2, (kImageHight-100)/2, 70, 90)];
        [self addSubview:loadingView];
        
        UIImage *image = ImageWithFile(@"加载.png");
        UIImageView *imgLoading = [[UIImageView alloc]initWithImage:image];
        [imgLoading setFrame:CGRectMake(8, 0, 54, 54)];
        [self.loadingView addSubview:imgLoading];
        
        UILabel *lblLoading = [[UILabel alloc]initWithFrame:CGRectMake(0, 59, 70, 20)];
        [lblLoading setText:@"正在加载.."];
        [lblLoading setFont:[UIFont systemFontOfSize:14]];
        [lblLoading setTextColor:RGBCOLOR(0, 192, 255)];
        [lblLoading setTextAlignment:NSTextAlignmentCenter];
        [lblLoading setBackgroundColor:[UIColor clearColor]];
        [self.loadingView addSubview:lblLoading];
        
        CABasicAnimation* rotationAnimation;
        rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
        rotationAnimation.duration = 3;
        rotationAnimation.cumulative = YES;
        rotationAnimation.repeatCount = INT_MAX;
        
        [imgLoading.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
        
        self.imageView = [[PZPhotoView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
        [self.imageView displayImage:nil];
        [self addSubview:imageView];
        
    }
    return self;
}

- (void)imageRequestDidLoad:(NSDictionary *)dic
{
    NSString *guidStr = [dic objectForKey:GUID];
    
    if([self.pageVO.guid isEqualToString:guidStr])
    {
        self.pageVO = [Page getPageWithGUID:guidStr];
        
        if(!self.isAction)return;
        
        UIImage *image = [dic objectForKey:@"viewImage"];
        
        DLog(@"页图片加载成功%@",guidStr);
        [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
        self.originalImage =image;
        
        NSString *isInk = [dic objectForKey:@"isInk"];
        if([isInk isEqualToString:@"yes"])
        {
            [imageView setFrame:CGRectMake(0, 0, kImageWidth, kImageHight)];
            [imageView prepareForReuse];
            [imageView displayImage:nil];
        }
        else
        {
            [self opreationImage:originalImage];
        }
        
        
        if([self.pageVO.fileType isEqualToString:FileType_VIDEO])
        {
            [self.imageView setUserInteractionEnabled:NO];
            
            UIImage *image = ImageWithFile(@"视频.png");
            UIButton *btnPlay = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnPlay setImage:image forState:UIControlStateNormal];
            [btnPlay setImage:image forState:UIControlStateSelected];
            [btnPlay setFrame:CGRectMake((kImageWidth-60)/2, (kImageHight-60)/2, 60, 60)];
            [self addSubview:btnPlay];
            
            [btnPlay addTarget:self action:@selector(btnPlayHandler) forControlEvents:UIControlEventTouchUpInside];
            
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kImageDidLoad object:nil];
    }
    [self.loadingView removeFromSuperview];
}

- (void)btnPlayHandler
{
    [delegate ScrollPage:self playVideoWithUrl:self.pageVO.resPath];
}

- (void)loadImage
{
    self.isAction = YES;
    UIImage *image = [self getImageFromCacheWithGUID:self.pageVO.guid];
    if(image)
    {
        self.originalImage =image;
        [self opreationImage:originalImage];
    }
    else
    {
        NSDictionary *dic;
        if([self isBlankString:self.pageVO.imageUrl])
        {
            dic = [NSDictionary dictionaryWithObjectsAndKeys:self.pageVO.guid,GUID,@"info",@"type", nil];
        }
        else
        {
            dic = [NSDictionary dictionaryWithObjectsAndKeys:self.pageVO,@"page",@"image",@"type", nil];
        }
        
        [[NSNotificationCenter defaultCenter]postNotificationName:kPageImageReload object:dic];
    }
    
}

- (void)opreationImage:(UIImage *)image
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [imageView setFrame:CGRectMake(0, 0, kImageWidth, kImageHight)];
        [imageView prepareForReuse];
        [imageView displayImage:image];
    });
}

@end
