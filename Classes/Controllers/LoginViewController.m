//
//  LoginViewController.m
//  EQClient
//
//  Created by xumeng on 12/10/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import "LoginViewController.h"
#import "IndexViewController.h"
#import "ICEHelper.h"
#import "UIDevice+IdentifierAddition.h"
#import "NSString+.h"
#import "CachedUserInfo.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"



CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};

@interface LoginViewController ()
{
@private
    BOOL _showHistory;
    BOOL _showBub;
    BOOL _hasAvatar;
    NSString *_avatarPath;
    BOOL _showAlert;
}
@property (nonatomic,strong)UIImageView *imgBGView;
@property (nonatomic,strong)UIImageView *imgNameBG;
@property (nonatomic,strong)UIButton *imgAvatar;
@property (nonatomic,strong)UIButton *btnHistory;
@property (nonatomic,strong)UITextField *txtName;
@property (nonatomic,strong)UIButton *btnSubmit;
@property (nonatomic,strong)UIActivityIndicatorView *activityIndicator;
@property (nonatomic,strong)UIActivityIndicatorView *activityIndicatorSubmit;
@property (nonatomic,strong)UILabel *lblTip;
@property (nonatomic,strong)BubbleView *bubbleView;
@property (nonatomic,strong)UIPopoverController *popover;

@property (nonatomic,strong)UIImageView *wechatCodeBG;
@property (nonatomic,strong)UIWebView *wechatCode;
@property (nonatomic,strong)UIImageView *wechatIcon;

@property (nonatomic,strong)UIButton *btnSetting;
@property (nonatomic,strong)UIView *alertBG;
@property (nonatomic,strong)UIView *textFieldBG;
@property (nonatomic,strong)UITextField *txtPassword;
@property (nonatomic,strong)UILabel *lblAlert;
@property (nonatomic,strong)UIButton *btnOK;
@property (nonatomic,strong)UIImageView *imgAlertIcon;

@property (nonatomic,strong)UIView *operateView;
@property (nonatomic,strong)UIView *alertView;

@property (nonatomic,strong)NSMutableArray *historyCells;
@property (nonatomic,strong)NSMutableArray *historyUser;

@property (nonatomic,strong)UILabel *lblVersion;

@end


@implementation LoginViewController
@synthesize imgBGView;
@synthesize imgNameBG;
@synthesize imgAvatar;
@synthesize btnHistory;
@synthesize txtName;
@synthesize btnSubmit;
@synthesize operateView;
@synthesize activityIndicator;
@synthesize lblTip;
@synthesize bubbleView;
@synthesize historyCells;
@synthesize historyUser;
@synthesize btnSetting;
@synthesize alertBG;
@synthesize txtPassword;
@synthesize lblAlert;
@synthesize btnOK;
@synthesize textFieldBG;
@synthesize alertView;
@synthesize imgAlertIcon;
@synthesize activityIndicatorSubmit;
@synthesize wechatCode;
@synthesize wechatIcon;
@synthesize wechatCodeBG;
@synthesize popover;
@synthesize lblVersion;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self initUI];
    [self initAlertView];
    
    //注册服务监听事件
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doICELoginError:) name:kICELoginError object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doICEFounded:) name:kICEFounded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doICELoginFinished:) name:kICELoginFinished object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doICESearchTimeout:) name:kICESearchTimeout object:nil];
    
    //初始化ICEHelper
    [[ICEHelper shared] initManager];
    [[[ICEHelper shared] iceManager] startSearchServiceWithTimeout:10];
    if(!GetObjectUserDefault(kPassword))
    {
        SetObjectUserDefault(@"xx", kPassword);
    }
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(kPasswordErrorHandler) name:kPasswordError object:nil];
    
}

- (void)initUI
{
    imgBGView = [[UIImageView alloc]initWithImage:ImageWithFile(@"底1.png")];
    [imgBGView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view addSubview:imgBGView];
    
    bubbleView = [BubbleView bubbleViewWithType:BubbleViewTypeIndex];
    [bubbleView setFrame:CGRectMake((SCREEN_WIDTH-280)/2-19, (SCREEN_HEIGHT-128)/2-35, 90, 147)];
    [bubbleView setHidden:YES];
    [bubbleView setDelegate:self];
    [self.view addSubview:bubbleView];
    _showBub = NO;
    
    operateView = [[UIView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH-280)/2, (SCREEN_HEIGHT-128)/2-35, 280, 128)];
    [self.view addSubview:operateView];
    
    imgNameBG = [[UIImageView alloc]initWithImage:ImageWithFile(@"登录框.png")];
    [imgNameBG setFrame:CGRectMake(0, 0, 280, 54)];
    [operateView addSubview:imgNameBG];
    
    imgAvatar = [UIButton buttonWithType:UIButtonTypeCustom];
    [imgAvatar setFrame:CGRectMake(4, 4, 45, 45)];
    [imgAvatar setImage:ImageWithFile(@"默认头像.png") forState:UIControlStateNormal];
    [imgAvatar setImage:ImageWithFile(@"默认头像.png") forState:UIControlStateSelected];
    [imgAvatar addTarget:self action:@selector(imgAvatarHandler) forControlEvents:UIControlEventTouchUpInside];
    [operateView addSubview:imgAvatar];
    _hasAvatar = NO;
    
    txtName = [[UITextField alloc]initWithFrame:CGRectMake(55, 7, 160, 40)];
    [txtName setDelegate:self];
    [txtName setFont:[UIFont systemFontOfSize:22]];
    [txtName setTextAlignment:NSTextAlignmentLeft];
    [txtName setSpellCheckingType:UITextSpellCheckingTypeNo];
    [txtName setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [txtName setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [txtName setReturnKeyType:UIReturnKeyDone];
    [txtName addTarget:self action:@selector(txtNameFinished:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [operateView addSubview:txtName];
    
    
    btnHistory = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnHistory setFrame:CGRectMake(235, 5, 44, 44)];
    [btnHistory setImage:ImageWithFile(@"下拉.png") forState:UIControlStateNormal];
    [btnHistory setImage:ImageWithFile(@"下拉.png") forState:UIControlStateSelected];
    [btnHistory setImage:ImageWithFile(@"下拉_不可用.png") forState:UIControlStateDisabled];
    [btnHistory addTarget:self action:@selector(btnHistoryHandler) forControlEvents:UIControlEventTouchUpInside];
    [operateView addSubview:btnHistory];
    _showHistory = NO;
    
    btnSubmit = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSubmit setFrame:CGRectMake(0, 74, 280, 54)];
    [btnSubmit setBackgroundImage:ImageWithFile(@"登录.png") forState:UIControlStateNormal];
    [btnSubmit setBackgroundImage:ImageWithFile(@"登录_点击.png") forState:UIControlStateHighlighted];
    [btnSubmit setTitle:@"登录" forState:UIControlStateNormal];
    [btnSubmit setTitle:@"登录" forState:UIControlStateHighlighted];
    [btnSubmit setTitleColor:RGBCOLOR(3, 118, 141) forState:UIControlStateNormal];
    [btnSubmit setTitleColor:RGBCOLOR(3, 118, 141) forState:UIControlStateHighlighted];
    [btnSubmit.titleLabel setFont:[UIFont systemFontOfSize:22]];
    [btnSubmit addTarget:self action:@selector(btnSubmitHandler) forControlEvents:UIControlEventTouchUpInside];
    [btnSubmit setEnabled:NO];
    [btnSubmit setAlpha:0.5];
    [operateView addSubview:btnSubmit];
    
    activityIndicatorSubmit = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
    [activityIndicatorSubmit setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [activityIndicatorSubmit setCenter:CGPointMake(btnSubmit.frame.size.width/2, btnSubmit.frame.size.height/2)];
    [btnSubmit addSubview:activityIndicatorSubmit];
    
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicator setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-62)];
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    
    lblTip = [[UILabel alloc]initWithFrame:CGRectMake((SCREEN_WIDTH-150)/2, (SCREEN_HEIGHT-42.5), 150, 25)];
    [lblTip setBackgroundColor:[UIColor clearColor]];
    [lblTip setFont:[UIFont systemFontOfSize:15]];
    [lblTip setTextAlignment:NSTextAlignmentCenter];
    [lblTip setTextColor:RGBCOLOR(17, 131, 158)];
    [lblTip setText:@"正在连接中控..."];
    [self.view addSubview:lblTip];
    
    historyUser = GetObjectUserDefault(kHistoryUsers);
    historyCells = [[NSMutableArray alloc]init];
    [self updateHistoryView];
    
    btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setFrame:CGRectMake(SCREEN_WIDTH - 36 -20, 20 , 36, 36)];
    [btnSetting setBackgroundImage:ImageWithFile(@"设置.png") forState:UIControlStateNormal];
    [btnSetting setBackgroundImage:ImageWithFile(@"设置.png") forState:UIControlStateSelected];
    [btnSetting addTarget:self action:@selector(btnSettingHandler) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnSetting];
    
    UIImage *image = ImageWithFile(@"二维码底框.png");
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)];
    wechatCodeBG = [[UIImageView alloc]initWithImage:image];
    [wechatCodeBG setFrame:CGRectMake((SCREEN_WIDTH-236)/2, 440, 236, 236)];
    [wechatCodeBG setContentMode:UIViewContentModeScaleToFill];
    [wechatCodeBG setHidden:YES];
    [self.view addSubview:wechatCodeBG];
    
    
    wechatCode = [[UIWebView alloc]initWithFrame:CGRectMake(10, 10, 216,216)];
    [wechatCode.scrollView setScrollEnabled:NO];
    [wechatCode setDelegate:self];
    [wechatCodeBG addSubview:wechatCode];
    
    wechatIcon = [[UIImageView alloc]initWithImage:ImageWithFile(@"微信文字.png")];
    [wechatIcon setFrame:CGRectMake((SCREEN_WIDTH-295)/2, 700, 295, 12)];
    [wechatIcon setHidden:YES];
    [self.view addSubview:wechatIcon];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    lblVersion = [[UILabel alloc]initWithFrame:CGRectMake(5, (SCREEN_HEIGHT-25), 150, 25)];
    [lblVersion setBackgroundColor:[UIColor clearColor]];
    [lblVersion setFont:[UIFont systemFontOfSize:14]];
    [lblVersion setTextAlignment:NSTextAlignmentLeft];
    [lblVersion setTextColor:RGBCOLOR(0, 130, 150)];
    [lblVersion setText:[NSString stringWithFormat:@"v %@",version]];
    [self.view addSubview:lblVersion];
}

- (void)txtNameFinished:(id)sender
{
    [self btnSubmitHandler];
}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL * url = [request URL];
    NSString *nickName;
    NSString *avatarUrl;
    NSString *fakeId;
    if([[url scheme] isEqualToString:@"imageReady"])
    {
        [wechatCodeBG setAlpha:0];
        [wechatCodeBG setHidden:NO];
        [wechatIcon setAlpha:0];
        [wechatIcon setHidden:NO];
        [UIView animateWithDuration:0.3 animations:^{
            [wechatCodeBG setAlpha:1];
            [wechatIcon setAlpha:1];
        }];
        
    }
    if([[url scheme] isEqualToString:@"wechatInfo"])
    {
        NSString *urlresource = url.resourceSpecifier;
        
        NSArray *compent = [urlresource componentsSeparatedByString:@"&"];
        for (int i=0; i<compent.count; i++)
        {
            NSString *onestring = [compent objectAtIndex:i];
            if ([onestring hasPrefix:@"//nickName"])
            {
                NSArray   *onearray = [onestring componentsSeparatedByString:@"="];
                
                nickName =[onearray lastObject];
            }
            if ([onestring hasPrefix:@"fakeId"])
            {
                NSArray   *onearray = [onestring componentsSeparatedByString:@"="];
                
                fakeId =[onearray lastObject];
                [[CachedUserInfo shared] setTvmid:fakeId];
            }
            if ([onestring hasPrefix:@"avatarUrl"])
            {
                NSArray   *onearray = [onestring componentsSeparatedByString:@"="];
                
                avatarUrl =[onearray lastObject];
            }
        }
        
        nickName = [nickName stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [txtName setText:nickName];
        [self imageFromURL:avatarUrl];
        
    }
    
    return YES;
}


- (void)initAlertView
{
    _showAlert = NO;
    alertView = [[UIView alloc]initWithFrame:CGRectMake(345,280-20, 335, 180)];
    [alertView setHidden:YES];
    [self.view addSubview:alertView];
    
    alertBG = [[UIView alloc]initWithFrame:CGRectMake(0,0, 335, 180)];
    [alertBG setBackgroundColor:[UIColor blackColor]];
    [alertBG setAlpha:0.8];
    alertBG.layer.cornerRadius = 5;
    [alertView addSubview:alertBG];
    
    textFieldBG = [[UIView alloc]initWithFrame:CGRectMake(27,100, 280, 54)];
    [textFieldBG setBackgroundColor:[UIColor whiteColor]];
    textFieldBG.layer.cornerRadius = 3;
    [alertView addSubview:textFieldBG];
    
    btnOK = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnOK setFrame:CGRectMake(258, 105, 44, 44)];
    [btnOK setImage:ImageWithFile(@"确定.png") forState:UIControlStateNormal];
    [btnOK setImage:ImageWithFile(@"确定_点击.png") forState:UIControlStateHighlighted];
    [btnOK addTarget:self action:@selector(btnOKHandler) forControlEvents:UIControlEventTouchUpInside];
    [alertView addSubview:btnOK];
    
    lblAlert = [[UILabel alloc]initWithFrame:CGRectMake(65, 33, 200, 40)];
    [lblAlert setBackgroundColor:[UIColor clearColor]];
    [lblAlert setFont:[UIFont boldSystemFontOfSize:20]];
    [lblAlert setTextAlignment:NSTextAlignmentLeft];
    [lblAlert setTextColor:[UIColor whiteColor]];
    [lblAlert setText:@"请输入登录密码"];
    [alertView addSubview:lblAlert];
    
    imgAlertIcon = [[UIImageView alloc]initWithImage:ImageWithFile(@"叹号.png")];
    [imgAlertIcon setFrame:CGRectMake(30, 41, 24, 24)];
    [alertView addSubview:imgAlertIcon];
    
    txtPassword = [[UITextField alloc]initWithFrame:CGRectMake(45, 107, 160, 40)];
    [txtPassword setSecureTextEntry:YES];
    [txtPassword setFont:[UIFont systemFontOfSize:22]];
    [txtPassword setTextAlignment:NSTextAlignmentLeft];
    [txtPassword setSpellCheckingType:UITextSpellCheckingTypeNo];
    [txtPassword setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [alertView addSubview:txtPassword];
    
}

- (void)btnSettingHandler
{
    if(_showAlert)
    {
        [txtPassword resignFirstResponder];
        [UIView animateWithDuration:0.3 animations:^{
            [alertView setAlpha:0];
        } completion:^(BOOL finished) {
            _showAlert = NO;
            [alertView setHidden:YES];
        }];
    }
    else
    {
        NSString *pwd =GetObjectUserDefault(kPassword);
        if(pwd)
        {
            [txtPassword setText:pwd];
        }
        [lblAlert setText:@"请输入登录密码"];
        [alertView setHidden:NO];
        [alertView setAlpha:0];
        [UIView animateWithDuration:0.3 animations:^{
            [alertView setAlpha:1];
        } completion:^(BOOL finished) {
            _showAlert = YES;
        }];
        
    }
    
}

- (void)btnOKHandler
{
    [txtPassword resignFirstResponder];
    if([txtPassword.text length]>0)
    {
        NSString *pwd =txtPassword.text;
        SetObjectUserDefault(pwd, kPassword);
        [UIView animateWithDuration:0.3 animations:^{
            [alertView setAlpha:0];
        } completion:^(BOOL finished) {
            _showAlert = NO;
            [alertView setHidden:YES];
        }];
    }
    else
    {
        ALERT_TITLE(@"提示", @"密码不能为空");
    }
}

- (void)kPasswordErrorHandler
{
    NSString *pwd =GetObjectUserDefault(kPassword);
    if(pwd)
    {
        [txtPassword setText:pwd];
    }
    [lblAlert setText:@"密码错误，请重新输入"];
    [alertView setHidden:NO];
    [alertView setAlpha:0];
    
    [btnSubmit setEnabled:YES];
    [activityIndicatorSubmit stopAnimating];
    
    [UIView animateWithDuration:0.3 animations:^{
        [alertView setAlpha:1];
    } completion:^(BOOL finished) {
        _showAlert = YES;
    }];
}

- (void)updateHistoryView
{
    if([historyUser count]>0)
    {
        HistoryCell *cell;
        for (int i=0; i<historyCells.count; i++) {
            cell = [historyCells objectAtIndex:i];
            [cell removeFromSuperview];
        }
        [historyCells removeAllObjects];
        
        for (int i=0; i<historyUser.count; i++) {
            
            NSDictionary *userDic = [historyUser objectAtIndex:i];
            
            if(i==historyUser.count-1)
            {
                cell = [[HistoryCell alloc]initWithBGImage:ImageWithFile(@"下拉底.png") userName:[userDic objectForKey:@"username"] avatarUrl:[userDic objectForKey:@"avatarpath"]];
                
            }
            else
            {
                cell = [[HistoryCell alloc]initWithBGImage:ImageWithFile(@"下拉中.png") userName:[userDic objectForKey:@"username"] avatarUrl:[userDic objectForKey:@"avatarpath"]];
            }
            [cell setDelegate:self];
            [cell setTag:i];
            [cell setFrame:CGRectMake((SCREEN_WIDTH-280)/2, (SCREEN_HEIGHT-128)/2+54*i+54-35, 280, 54)];
            [cell setHidden:YES];
            [historyCells addObject:cell];
            [self.view addSubview:cell];
        }
    }
    else
    {
        [btnHistory setEnabled:NO];
    }
}

- (void)cellClick:(HistoryCell *) cell
{
    NSDictionary *userDic;
    for (int i = 0; i<historyUser.count; i++) {
        if([cell.userName isEqualToString:[[historyUser objectAtIndex:i] objectForKey:@"username"]])
        {
            userDic =[historyUser objectAtIndex:i];
        }
    }
    [txtName setText:[userDic objectForKey:@"username"]];
    UIImage *image = [UIImage imageWithContentsOfFile:[userDic objectForKey:@"avatarpath"]];
    [imgAvatar setImage:image forState:UIControlStateNormal];
    [imgAvatar setImage:image forState:UIControlStateSelected];
    _hasAvatar = YES;
    _avatarPath = [userDic objectForKey:@"avatarpath"];
    [self hideHistoryView];
}

- (void)cellDelete:(HistoryCell *) cell
{
    [historyCells removeObject:cell];
    [cell removeFromSuperview];
    for (int i = 0; i<historyUser.count; i++) {
        NSDictionary *userDic = [historyUser objectAtIndex:i];
        if([cell.userName isEqualToString:[userDic objectForKey:@"username"]])
        {
            [historyUser removeObjectAtIndex:i];
        }
    }
    for (int i=0; i<historyCells.count; i++) {
        
        HistoryCell *c = [historyCells objectAtIndex:i];
        [c setFrame:CGRectMake((SCREEN_WIDTH-280)/2, (SCREEN_HEIGHT-128)/2+54*i+54-35, 280, 54)];
    }
    SetObjectUserDefault(historyUser, kHistoryUsers);
    
    
    if(historyCells.count==0)
    {
        [self hideHistoryView];
        [btnHistory setEnabled:NO];
    }
}

- (void)showHistoryView
{
    HistoryCell *cell;
    for (int i= 0; i<historyCells.count;i++) {
        cell = [historyCells objectAtIndex:i];
        [cell setHidden:NO];
        [cell setAlpha:0];
        [UIView animateWithDuration:0.05 delay:i*0.05 options:UIViewAnimationOptionCurveLinear animations:^{
            [cell setAlpha:1];
        } completion:^(BOOL finished) {
            
        }];
    }
    UIImage *image = ImageWithFile(@"下拉.png");
    image = [self imageRotatedByDegrees:180 image:image];
    [btnHistory setImage:image forState:UIControlStateNormal];
    [btnHistory setImage:image forState:UIControlStateSelected];
    _showHistory = YES;
}

- (void)hideHistoryView
{
    HistoryCell *cell;
    for (int i= 0; i<historyCells.count;i++) {
        cell = [historyCells objectAtIndex:historyCells.count-i-1];
        [UIView animateWithDuration:0.05 delay:i*0.05 options:UIViewAnimationOptionCurveLinear animations:^{
            [cell setAlpha:0];
        } completion:^(BOOL finished) {
            [cell setHidden:YES];
        }];
    }
    UIImage *image = ImageWithFile(@"下拉.png");
    [btnHistory setImage:image forState:UIControlStateNormal];
    [btnHistory setImage:image forState:UIControlStateSelected];
    _showHistory = NO;
    
}

- (void)imgAvatarHandler
{
    if(!_showBub)
    {
        [self showBub];
    }
    else
    {
        [self hideBub];
    }
    
}

- (void)showBub
{
    if(!_showBub)
    {
        [bubbleView setHidden:NO];
        [bubbleView setAlpha:0];
        [bubbleView setFrame:CGRectMake((SCREEN_WIDTH-280)/2-19, (SCREEN_HEIGHT-128)/2-157-35, 90, 147)];
        [UIView animateWithDuration:0.3 animations:^{
            [bubbleView setFrame:CGRectMake((SCREEN_WIDTH-280)/2-19, (SCREEN_HEIGHT-128)/2-147-35, 90, 147)];
            [bubbleView setAlpha:1];
        } completion:^(BOOL finished) {
            _showBub = YES;
        }];
        
    }
}

- (void)hideBub
{
    if(_showBub)
    {
        [UIView animateWithDuration:0.3 animations:^{
            [bubbleView setFrame:CGRectMake((SCREEN_WIDTH-280)/2-19, (SCREEN_HEIGHT-128)/2-157-35, 90, 147)];
            [bubbleView setAlpha:0];
        } completion:^(BOOL finished) {
            _showBub = NO;
            [bubbleView setHidden:YES];
        }];
    }
}

- (void)btnSubmitHandler
{
    [txtName resignFirstResponder];
    if([txtName.text length]==0)
    {
        ALERT_TITLE(@"提示",@"昵称不能为空");
        return;
        
    }
    if(!_hasAvatar)
    {
        ALERT_TITLE(@"提示",@"头像不能为空");
        return;
    }
    
    [activityIndicatorSubmit startAnimating];
    [btnSubmit setEnabled:NO];
    
    [[ICEHelper shared] doConnectICE];
    
}

- (void)btnHistoryHandler
{
    if(!_showHistory)
    {
        [self showHistoryView];
    }
    else
    {
        [self hideHistoryView];
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (toBeString.length > 10) {
        textField.text = [toBeString substringToIndex:10];
        return NO;
    }
    return YES;
}

- (void)bubbleViewButtonDidClick:(NSUInteger) buttonIndex bubbleType:(BubbleViewType) type
{
    if(buttonIndex==0)
    {
        [self openCamera];
    }
    if(buttonIndex==1)
    {
        [self openAlbum];
    }
}

- (void)openCamera
{
    UIImagePickerController * picker = [[UIImagePickerController alloc]init];
    
    picker.delegate=self;
    
    picker.allowsEditing=YES;
    
    picker.sourceType=UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:^{
        
    }];
   
}

- (void)openAlbum
{
    UIImagePickerController * picker = [[UIImagePickerController alloc]init];
    
    picker.delegate=self;
    
    picker.allowsEditing=YES;
    
    picker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    
    popover = [[UIPopoverController alloc] initWithContentViewController:picker];
    [popover setDelegate:self];
    [popover presentPopoverFromRect:CGRectMake(-50.0, 0.0, 500.0, 500.0)
                             inView:self.view
           permittedArrowDirections:UIPopoverArrowDirectionAny
                           animated:YES];
    
    
    
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    if(picker.sourceType==UIImagePickerControllerSourceTypeSavedPhotosAlbum)
    {
        if(image)
        {
            [self pickImageHandler:image];
        }
        [popover dismissPopoverAnimated:YES];
    }
    else
    {
        [picker dismissViewControllerAnimated:YES completion:^{
            if(image)
            {
                [self pickImageHandler:image];
            }
        }];
    }

    
}

- (void)pickImageHandler:(UIImage *)image
{
    NSString *imagePath = DocumentPath;
    imagePath = [imagePath stringByAppendingPathComponent:@"avatar"];
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL existed = [fileManager fileExistsAtPath:imagePath isDirectory:&isDir];
    if ( !(isDir == YES && existed == YES) )
    {
        [fileManager createDirectoryAtPath:imagePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString *uuid =gen_uuid();
    imagePath = [imagePath stringByAppendingPathComponent:uuid];
    imagePath = [imagePath stringByAppendingPathExtension:@"jpg"];
    NSData *imageData = nil;
    imageData = UIImageJPEGRepresentation(image, 0.5);
    [imageData writeToFile:imagePath atomically:YES];
    
    [imgAvatar setImage:image forState:UIControlStateNormal];
    [imgAvatar setImage:image forState:UIControlStateSelected];
    _hasAvatar = YES;
    _avatarPath = imagePath;
    if(_showBub)
    {
        [self hideBub];
    }
}

-(void)imagePickerControllerDIdCancel:(UIImagePickerController*)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        if(_showBub)
        {
            [self hideBub];
        }
    }];
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    if(_showBub)
    {
        [self hideBub];
    }
    return YES;
}

-(void)imageFromURL:(NSString *)urlStr
{
    [AppDelegate sharedHttpRequestManager].responseSerializer = [AFImageResponseSerializer serializer];
    [[AppDelegate sharedHttpRequestManager] GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        UIImage *image = (UIImage *)responseObject;
        if(image)
        {
            
            NSString *imagePath = DocumentPath;
            imagePath = [imagePath stringByAppendingPathComponent:@"avatar"];
            BOOL isDir = NO;
            NSFileManager *fileManager = [NSFileManager defaultManager];
            BOOL existed = [fileManager fileExistsAtPath:imagePath isDirectory:&isDir];
            if ( !(isDir == YES && existed == YES) )
            {
                [fileManager createDirectoryAtPath:imagePath withIntermediateDirectories:YES attributes:nil error:nil];
            }
            
            NSString *uuid =gen_uuid();
            imagePath = [imagePath stringByAppendingPathComponent:uuid];
            imagePath = [imagePath stringByAppendingPathExtension:@"jpg"];
            NSData *imageData = nil;
            imageData = UIImageJPEGRepresentation(image, 0.5);
            [imageData writeToFile:imagePath atomically:YES];
            
            [imgAvatar setImage:image forState:UIControlStateNormal];
            [imgAvatar setImage:image forState:UIControlStateSelected];
            _hasAvatar = YES;
            _avatarPath = imagePath;
            if(_showBub)
            {
                [self hideBub];
            }
            [self btnSubmitHandler];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}


NSString * gen_uuid()
{
    CFUUIDRef uuid_ref = CFUUIDCreate(NULL);
    CFStringRef uuid_string_ref= CFUUIDCreateString(NULL, uuid_ref);
    
    CFRelease(uuid_ref);
    NSString *uuid = [NSString stringWithString:(__bridge NSString*)uuid_string_ref];
    
    CFRelease(uuid_string_ref);
    return uuid;
}


- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees image:(UIImage *)image
{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,image.size.width, image.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(DegreesToRadians(degrees));
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, DegreesToRadians(degrees));
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-image.size.width / 2, -image.size.height / 2, image.size.width, image.size.height), [image CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}

- (NSString*)stringWithIdentifierForVendor
{
    NSString* resultString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    resultString = [resultString stringByAppendingString:bundleIdentifier];
    
    NSString* identifier = [resultString stringFromMD5];
    
    return identifier;
}


#pragma mark Notification Methods
- (void)doICELoginError:(NSNotification *)n
{
    NSString *pwd =GetObjectUserDefault(kPassword);
    if(pwd)
    {
        [txtPassword setText:pwd];
    }
    [lblAlert setText:@"密码权限错误！"];
    [alertView setHidden:NO];
    [alertView setAlpha:0];
    
    [btnSubmit setEnabled:YES];
    [activityIndicatorSubmit stopAnimating];
    
    [[[ICEHelper shared] iceManager] closeCurrentConnection];
    [UIView animateWithDuration:0.3 animations:^{
        [alertView setAlpha:1];
    } completion:^(BOOL finished) {
        _showAlert = YES;
    }];
}


- (void)doICESearchTimeout:(NSNotification *)n
{
    [lblTip setText:@"连接超时，重新连接。"];
    //停止搜索，继续
    [[[ICEHelper shared] iceManager] stopSearchService];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:2]];
    
    [[[ICEHelper shared] iceManager] startSearchServiceWithTimeout:10];
    [[NSNotificationCenter defaultCenter] postNotificationName:kICEStartSearching object:nil];
    [lblTip setText:@"正在连接中控..."];
}

- (void)doICELoginFinished:(NSNotification *)n
{
    //添加用户 判断是否需要保存用户信息 如果是从历史用户里选取的则不用保存到历史登录信息里。
    NSLog(@"login finished");
    
    NSDictionary *userDic = [NSDictionary dictionaryWithObjectsAndKeys:txtName.text,@"username",_avatarPath,@"avatarpath",nil];
    BOOL exist = NO;
    if(historyUser)
    {
        if(historyUser.count==3)
        {
            [historyUser removeObjectAtIndex:0];
        }
        for (int i = 0; i<historyUser.count; i++) {
            NSDictionary *userDic = [historyUser objectAtIndex:i];
            if([txtName.text isEqualToString:[userDic objectForKey:@"username"]])
            {
                exist = YES;
            }
        }
        if(!exist)[historyUser addObject:userDic];
    }
    else
    {
        historyUser = [[NSMutableArray alloc]init];
        [historyUser addObject:userDic];
    }
    SetObjectUserDefault(historyUser, kHistoryUsers);
    
    
    [[CachedUserInfo shared] setRealName:txtName.text];
    [[CachedUserInfo shared] setHeadImage:imgAvatar.imageView.image];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:[[CachedUserInfo shared] tvmid] forKey:@"owner_tvmid"];
    [dic setObject:[[CachedUserInfo shared] realName] forKey:@"nickname"];
    [dic setObject:@"eClassRoom" forKey:@"appname"];
    AFJSONResponseSerializer *serializer =[AFJSONResponseSerializer serializer];
    [serializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil]];
    [AppDelegate sharedHttpRequestManager].responseSerializer =serializer;
    
    NSURL *filePath = [NSURL fileURLWithPath:_avatarPath];
    [[AppDelegate sharedHttpRequestManager] POST:[NSString stringWithFormat:@"%@%@",[[KKCache sharedCache] HTTPIP],[UploadUserInfoEntity URI_UPLOAD_USERINFO]] parameters:dic constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:filePath name:@"face" error:nil];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"登录信息上传成功%@",responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"登录信息上传错误error%@",error);
    }];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate loginViewChange];
    
}

- (void)doICEFounded:(NSNotification *)n
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        [[CachedUserInfo shared] setTvmid:[self stringWithIdentifierForVendor]];
    }
    else
    {
        [[CachedUserInfo shared] setTvmid:[[UIDevice currentDevice] uniqueDeviceIdentifier]];
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        [activityIndicator setAlpha:0];
        [lblTip setAlpha:0];
        [btnSubmit setAlpha:1];
    } completion:^(BOOL finished) {
        [activityIndicator removeFromSuperview];
        [lblTip removeFromSuperview];
        activityIndicator  = nil;
        lblTip = nil;
        
        NSURL *url = [NSURL URLWithString:@"http://hudong-demo.tvmining.com/loginEQ/login.html"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [wechatCode loadRequest:request];
        
        [btnSubmit setEnabled:YES];
    }];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
