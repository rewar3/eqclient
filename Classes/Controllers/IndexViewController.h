//
//  IndexViewController.h
//  EQClient
//
//  Created by xumeng on 12/10/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToolBarView.h"
#import "ImagePageContainer.h"
#import "BubbleView.h"
#import "KKStreamDraw.h"

typedef NS_ENUM(NSInteger, PageStateType) {
    PageStateTypeNone               =0,
    PageStateTypePaint              =1,
    PageStateTypeText               =2,
    PageStateTypeCamera             =3,
    PageStateTypeUserDragging       =4,
    PageStateTypePlayVideo          =5
    
};

@class DMPagingScrollView,CustomPageControl;
@interface IndexViewController : UIViewController<ToolBarViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ImagePageContainerDelegate,BubbleViewDelegate,UIPopoverControllerDelegate,KKStreamDrawDelegate>
{
    BOOL _isAnswer;
    
    PageStateType _pageStateType;
    KKStreamDraw *_lineManager;
}
@property (nonatomic,strong)CustomPageControl *pageControl;
@property (nonatomic,strong)ToolBarView *toolBarView;
@property (nonatomic)PageStateType pageStateType;

@property (nonatomic)BOOL isAnswer;


- (void)initScrollView;
@end
