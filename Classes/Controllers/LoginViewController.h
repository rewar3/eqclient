//
//  LoginViewController.h
//  EQClient
//
//  Created by xumeng on 12/10/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BubbleView.h"
#import "HistoryCell.h"
@interface LoginViewController : UIViewController<UITextFieldDelegate,BubbleViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,HistoryCellDelegate,UIWebViewDelegate,UIPopoverControllerDelegate>

@end
