//
//  IndexViewController.m
//  EQClient
//
//  Created by xumeng on 12/10/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import "IndexViewController.h"
#import "CustomPageControl.h"
#import "AFHTTPRequestOperation.h"
#import "PainterView.h"
#import "ICEHelper.h"
#import "NSString+JSONSerialization.h"
#import "ICEManager.h"
#import "ComposeImage.h"
#import "NSString+.h"
#import "ScrollPageView.h"
#import <MediaPlayer/MediaPlayer.h>
#import "NSObject+Utils.h"
#import "AppDelegate.h"
#import "PageRequestManager.h"
#import "Page.h"

@interface IndexViewController ()
{
@private
    
    BOOL _isScrolling;
    
    UIColor *_currentLineColor;
    int _currentLineWidth;
    
    BOOL _answerType;//yes:客观 no:主观
    BOOL _modeClose;//答题模式下是否需要关闭之前状态
    
    NSString *questionGUID;
    
    BOOL _isStreamMode;
    BOOL _isInkPaper;
    BOOL _isWhitePaper;
    NSString * _sourceFileID;
    
    UIImage *noDrawImage;
    UIImage *_drawingImage;
    NSString *_drawingGUID;
    NSString *_whitePaperGUID;
    UIImage *_oldImage;
    ScrollPageView *streamPage;

    BOOL _inkLoaded;
}
@property (nonatomic)UIImage *editImage;

@property (nonatomic,strong)ImagePageContainer *pageContainer;
@property (nonatomic,strong)PainterView *painterView;
@property (nonatomic,strong)UIImagePickerController *picker;
@property (nonatomic,strong)UIImageView *imgAnswerTip;
@property (nonatomic,strong)UIPopoverController *popover;

@property (nonatomic,strong)BubbleView *bubble;
@property (nonatomic,strong)UIButton *bubbleMask;//其他区域遮罩

@property (nonatomic,strong)NSMutableArray *contentQueue;
@property (nonatomic,strong)NSTimer *timer;//用户操作结束计时器

@property (nonatomic)BOOL isScrolling;

@property (nonatomic,strong)UIImageView *imgPreview;
@property (nonatomic,strong)PZPhotoView *imgWhitePaper;

@end

@implementation IndexViewController
@synthesize pageControl;
@synthesize toolBarView;
@synthesize painterView;
@synthesize contentQueue;
@synthesize editImage;
@synthesize timer;
@synthesize imgAnswerTip;
@synthesize pageContainer;
@synthesize picker;
@synthesize bubble;
@synthesize bubbleMask;
@synthesize popover;
@synthesize imgPreview;
@synthesize imgWhitePaper;

- (void)setPageStateType:(PageStateType)pageStateType
{
    _pageStateType = pageStateType;
    if(_pageStateType==PageStateTypeNone)
    {
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:bubble.bubbleType],@"type",[NSNumber numberWithInteger:0],@"index", nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewHide object:dic];
        
        [self contentQueueHandler];
    }
    else
    {
        [timer invalidate];
    }
}

- (PageStateType)pageStateType
{
    return _pageStateType;
}

- (void)setIsAnswer:(BOOL)isAnswer
{
    _isAnswer = isAnswer;
    if(!_isAnswer)
    {
        [imgAnswerTip setHidden:YES];
    }
    else
    {
        [imgAnswerTip setHidden:NO];
    }
}

- (BOOL)isAnswer
{
    return _isAnswer;
}

- (void)setIsScrolling:(BOOL)isScrolling
{
    _isScrolling = isScrolling;
    
    if(!_isScrolling)
    {
        [self contentQueueHandler];
    }
    
}

- (BOOL)isScrolling
{
    return _isScrolling;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIImageView *imageBG = [[UIImageView alloc]initWithImage:ImageWithFile(@"底.png")];
    [imageBG setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view addSubview:imageBG];
    
    [self initScrollView];
    
    self.imgWhitePaper = [[PZPhotoView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.imgWhitePaper setHidden:YES];
    [self.imgWhitePaper displayImage:nil];
    [self.view addSubview:self.imgWhitePaper];
    
    self.imgPreview = [[UIImageView alloc]initWithImage:editImage];
    [self.imgPreview setHidden:YES];
    [self.view addSubview:self.imgPreview];
    
    [self initPainterView];
    
    pageControl = [[CustomPageControl alloc]initWithFrame:CGRectMake((SCREEN_WIDTH-190)/2, SCREEN_HEIGHT-75, 190, 5)];
    [self.view addSubview:pageControl];
    
    bubbleMask = [UIButton buttonWithType:UIButtonTypeCustom];
    [bubbleMask setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [bubbleMask addTarget:self action:@selector(bubbleMaskHandler) forControlEvents:UIControlEventTouchUpInside];
    [bubbleMask setAlpha:0.1];
    [bubbleMask setHidden:YES];
    [self.view addSubview:bubbleMask];
    
    toolBarView = [[ToolBarView alloc]initWithToolBarType:ToolBarTypeIndex];
    [toolBarView setFrame:CGRectMake(0, SCREEN_HEIGHT-kBarHeight, SCREEN_WIDTH, kBarHeight)];
    [toolBarView setDelegate:self];
    [self.view addSubview:toolBarView];
    
    imgAnswerTip = [[UIImageView alloc]initWithImage:ImageWithFile(@"应答中.png")];
    [imgAnswerTip setFrame:CGRectMake(0, 50, 75, 30)];
    [imgAnswerTip setHidden:YES];
    [self.view addSubview:imgAnswerTip];
    
    _lineManager = [[KKStreamDraw alloc]init];
    [_lineManager initObjects];
    [_lineManager setDelegate:self];
    
    
    _pageStateType = PageStateTypeNone;
    
    _isAnswer = NO;
    _isScrolling = NO;
    _isStreamMode = NO;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(goCommandHandler) name:kPushGoCommand object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(forceAnswerCommandHandler:) name:kForceAnswer object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(openPaintModeHandler:) name:kOpenPaintMode object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(closePaintModeHandler) name:kClosePaintMode object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(openTextModeHandler) name:kOpenTextMode object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(closeTextModeHandler) name:kCloseTextMode object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(closeCameraModeHandler) name:kCloseCameraMode object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(openAnswerModeHandler) name:kOpenAnswerMode object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(bubbleViewShowHandler:) name:kBubbleViewShow object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(bubbleViewHideHandler:) name:kBubbleViewHide object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(inkImageWillLoadHandler:) name:kInkImageWillLoad object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(saveStreamImageHandler:) name:kSaveStreamImage object:nil];
    
}

- (void)initScrollView
{
    
    pageContainer = [[ImagePageContainer alloc]initWithFrame:CGRectMake(0, 0, 1024, 768)];
    [pageContainer setDelegate:self];
    [self.view addSubview:pageContainer];
    contentQueue = [[NSMutableArray alloc] init];
}

- (void)initPainterView
{
    painterView = [[PainterView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [painterView setBackgroundColor:[UIColor clearColor]];
    
    [painterView setHidden:YES];
    [self.view addSubview:painterView];
    NSNumber *colorIndex = GetObjectUserDefault(kPainterLineColor);
    if(colorIndex)
    {
        int index = [colorIndex intValue];
        [painterView setColor:[self getColorWithIndex:index]];
    }
    else
    {
        [painterView setColor:[UIColor redColor]];
    }
    
    NSNumber *lineWidthIndex = GetObjectUserDefault(kPainterLineWidth);
    if(lineWidthIndex)
    {
        int index = [lineWidthIndex intValue];
        [painterView setLineWidth:[self getWidthWithIndex:index]];
    }
    else
    {
        [painterView setLineWidth:5];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    
    if(pageContainer.currentPage)[pageContainer turnToPageWithGUID:pageContainer.currentPage.pageVO.guid animated:NO];
    
    [UIApplication sharedApplication].statusBarHidden = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    if(self.pageStateType == PageStateTypePlayVideo)
    {
        self.pageStateType = PageStateTypeNone;
    }
}


#pragma mark UIKeyboardWillChangeFrameNotification

- (void)keyboardWillChangeFrame:(NSNotification *)n
{
    
    NSDictionary *dic = n.userInfo;
    NSValue* aValue = [dic objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect rect =[aValue CGRectValue];
    [UIView animateWithDuration:0.1 animations:^{
        [toolBarView setFrame:CGRectMake(0, SCREEN_HEIGHT-(rect.size.width+kBarHeight), SCREEN_WIDTH, kBarHeight)];
    }];
    
}

#pragma mark kForceAnswer

- (void)forceAnswerCommandHandler:(NSNotification *)n
{
    
    if([[KKCache sharedCache] isForceAnswer])//开启答题
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:kOpenAnswerMode object:nil];
        [self.pageContainer.scrollView setScrollEnabled:NO];
    }
    else//关闭答题
    {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:kCloseTextMode object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:kClosePaintMode object:nil];
        self.isAnswer = NO;
        
        if(_answerType)//客观
        {
            [self.toolBarView changeModeWithToolBarType:ToolBarTypeIndex completion:nil];
        }
        [toolBarView.btnMore setHidden:NO];
        [self.pageContainer.scrollView setScrollEnabled:YES];
    }
    
}

- (void)imageDidLoadHandler
{
    ScrollPageView *contentView = [pageContainer getPageWithGUID:questionGUID];
    UIImage *image;
    image = contentView.originalImage;
    
    if(image)
    {
        editImage = image;
    }
    else
    {
        editImage = ImageWithFile(@"blank.png");
    }
    
    
    if(_answerType)//客观
    {
        [self.toolBarView changeModeWithToolBarType:ToolBarTypeAnswer completion:nil];
    }
    else//主观
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:kOpenPaintMode object:editImage];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kImageDidLoad object:nil];
}

- (void)openAnswerModeHandler
{
    if([[[KKCache sharedCache] questionType] isEqualToString:@"0"])
    {
        _answerType = YES;
        [self.toolBarView changeModeWithToolBarType:ToolBarTypeAnswer completion:nil];
        
    }
    else
    {
        _answerType = NO;
        
    }
    
    if (![self isBlankString:[[KKCache sharedCache] questionGuid]])//有图答题
    {
        questionGUID = [[KKCache sharedCache] questionGuid];
        ScrollPageView *sp = [self.pageContainer getPageWithGUID:questionGUID];
        if(!sp)
        {
//            NSDictionary *dic =[NSDictionary dictionaryWithObjectsAndKeys:[[KKCache sharedCache] questionGuid],GUID, nil];
            
//            if(!self.isScrolling&&self.pageStateType==PageStateTypeNone)
//            {
//                [pageContainer addPageWithGUID:[dic objectForKey:GUID] animated:YES isStream:NO];
//                [[PageRequestManager defaultManager]goCommand:[dic objectForKey:GUID]];
                
//                [pageContainer addPageWithGUID:[dic objectForKey:GUID] animated:YES isStream:NO];
//                [[PageRequestManager defaultManager]goCommand:[dic objectForKey:GUID]];
//                [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(imageDidLoadHandler) name:kImageDidLoad object:nil];
//            }
        }
        else
        {
            UIImage *image;
            image = sp.originalImage;
            if(image)
            {
                editImage = image;
            }
            else
            {
                editImage = ImageWithFile(@"blank.png");
            }
            
            if(_answerType)//客观
            {
                [self.toolBarView changeModeWithToolBarType:ToolBarTypeAnswer completion:nil];
            }
            else
            {
//                [[NSNotificationCenter defaultCenter]postNotificationName:kOpenPaintMode object:image];
            }
            
        }
        [self closeAllState];
        self.isAnswer = YES;
        [pageContainer turnToPageWithGUID:questionGUID animated:YES];
    }
    else//无图答题
    {
        self.isAnswer = YES;
        [self imageDidLoadHandler];
    }
    //不能更多
    [toolBarView.btnMore setHidden:YES];
}

- (void)closeAllState
{
    switch (self.pageStateType) {
        case PageStateTypeUserDragging:
        {
            self.pageStateType = PageStateTypeNone;
            if(_isStreamMode)
            {
                [self.pageContainer turnToPageWithGUID:streamPage.pageVO.guid animated:YES];
            }
        }
            break;
        case PageStateTypePaint:
        {
            _modeClose = YES;
            [[NSNotificationCenter defaultCenter]postNotificationName:kClosePaintMode object:nil];
        }
            break;
        case PageStateTypeText:
        {
            _modeClose = YES;
            [[NSNotificationCenter defaultCenter]postNotificationName:kCloseTextMode object:nil];
        }
            break;
        case PageStateTypeCamera:
        {
            _modeClose = YES;
            [[NSNotificationCenter defaultCenter]postNotificationName:kCloseCameraMode object:nil];
        }
            break;
            
        default:
            break;
    }
    if(self.pageContainer.currentPage)
    {
        [self.pageContainer.currentPage.imageView updateZoomScale:self.pageContainer.currentPage.imageView.minimumZoomScale];
    }
}

#pragma mark kPushGoCommand
- (void)goCommandHandler
{
    NSDictionary *dic = [[KKCache sharedCache] lastPushGoDict];
    if(!self.isScrolling&&self.pageStateType==PageStateTypeNone)
    {
        [pageContainer addPageWithGUID:[dic objectForKey:GUID] animated:YES isStream:NO];
        [[PageRequestManager defaultManager]goCommand:[dic objectForKey:GUID]];
    }
    else [self addContentQueue:dic];//在任何其他状态加入队列
    
}

- (void)addContentQueue:(NSDictionary *)dic
{
    [contentQueue addObject:dic];
    DLog(@"添加GO执行队列数量:%lu",(unsigned long)contentQueue.count);
}

- (void)contentQueueHandler
{
    NSDictionary *dic;
    
    DLog(@"开始执行GO队列:%lu",(unsigned long)contentQueue.count);
    if(contentQueue.count>5)
    {
        NSInteger num =contentQueue.count-5;
        [contentQueue removeObjectsInRange:NSMakeRange(0, num)];
        
    }
    
    for (int i = 0;i<contentQueue.count;i++)
    {
        dic = [contentQueue objectAtIndex:i];
        if(i==contentQueue.count-1)
        {
            [pageContainer addPageWithGUID:[dic objectForKey:GUID] animated:YES isStream:NO];
            [[PageRequestManager defaultManager]goCommand:[dic objectForKey:GUID]];
            
        }
        else
        {
            [pageContainer addPageWithGUID:[dic objectForKey:GUID] animated:NO isStream:NO];
            [[PageRequestManager defaultManager]goCommand:[dic objectForKey:GUID]];
        }
    }
    [contentQueue removeAllObjects];
    DLog(@"结束执行GO队列:%lu",(unsigned long)contentQueue.count);
}

- (void)userOpreation
{
    self.pageStateType = PageStateTypeUserDragging;
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(timerFired:) userInfo:nil repeats:NO];
    DLog(@"暂停响应GO");
}

- (void)timerFired:(NSTimer *)t
{
    self.pageStateType = PageStateTypeNone;
    DLog(@"继续响应GO");
}

#pragma mark ImagePageContainerDelegate

- (void)changePage:(int)index page:(ScrollPageView *)pageView
{
    [pageControl setCurrentPage:index];
    if([pageView.pageVO.fileType isEqualToString:FileType_VIDEO])
    {
        [self.toolBarView.btnPaint setHidden:YES];
    }
    else
    {
        [self.toolBarView.btnPaint setHidden:NO];
    }
    
}

- (void)addPageCount:(NSInteger)count
{
    [pageControl setNumberOfPages:count];
}

- (void)playVideo:(NSString *) resUrl guid:(NSString *)guidStr
{
    MPMoviePlayerViewController *player = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:resUrl]];
    
    [self presentMoviePlayerViewControllerAnimated:player];
    self.pageStateType = PageStateTypePlayVideo;
    
}

- (void)imagePageContainerWillBeginScrolling
{
    self.isScrolling = YES;
}

- (void)imagePageContainerWillEndScrolling
{
    self.isScrolling = NO;
}

- (void)imagePageContainerWillBeginDragging
{
    [self userOpreation];
}

#pragma mark ToolBarViewDelegate

- (void)openCamera
{
    picker = [[UIImagePickerController alloc]init];
    
    picker.delegate=self;
    
    picker.allowsEditing=NO;
    
    picker.sourceType=UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:^{
        
    }];
    self.pageStateType = PageStateTypeCamera;
}

- (void)openAlbum
{
    picker = [[UIImagePickerController alloc]init];
    
    picker.delegate=self;
    
    picker.allowsEditing=NO;
    
    picker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    
    popover = [[UIPopoverController alloc] initWithContentViewController:picker];
    [popover setDelegate:self];
    [popover presentPopoverFromRect:CGRectMake(875.0, 700.0, 50.0, 50.0)
                             inView:self.view
           permittedArrowDirections:UIPopoverArrowDirectionAny
                           animated:YES];
    
    self.pageStateType = PageStateTypeCamera;
}

- (void)postText:(NSString *)message
{
    [self sendTextMessage:message];
    self.pageStateType = PageStateTypeNone;
    
    [[NSNotificationCenter defaultCenter]postNotificationName:kCloseTextMode object:nil];
}

- (void)sendTextMessage:(NSString *)msg
{
    if([self isBlankString:msg])return;
    
    char* utf8Replace = "\xe2\x80\x86\0";
    NSData* data = [NSData dataWithBytes:utf8Replace length:strlen(utf8Replace)];
    NSString* utf8_str_format = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSMutableString* mutableAblumName = [NSMutableString stringWithString:msg];
    NSString* strAblum =  [mutableAblumName stringByReplacingOccurrencesOfString:utf8_str_format withString:@""];
    
    NSDictionary *dic;
    if(pageContainer.currentPage.pageVO.guid == nil)
    {
        dic = [NSDictionary dictionaryWithObjectsAndKeys:strAblum,@"symbol", nil];
    }
    else
    {
        dic = [NSDictionary dictionaryWithObjectsAndKeys:strAblum,@"symbol",@"questionguid",pageContainer.currentPage.pageVO.guid, nil];
    }
    
    CommandEntity *commandEntity = [[CommandEntity alloc]init];
    commandEntity.CmdTYPE =[CommandTypeEntity MARCO];
    commandEntity.body = [dic JSONString];
    [[commandEntity OBJ] addObject:[GroupTypeEntity ALL_CAN_SEE]];
    [[[ICEHelper shared] iceManager] sendCmdEntity:commandEntity];
    
}


#pragma mark PaintBarViewDelegate

- (void)changeColor:(UIColor *)color
{
    if(self.toolBarView.isEraser)
    {
        self.toolBarView.isEraser = NO;
        [self.toolBarView.btnEraser setSelected:NO];
        [self closeEraser];
    }
    [painterView setColor:color];
}

- (void)openEraser
{
    _currentLineColor = [painterView color];
    _currentLineWidth = [painterView lineWidth];
    [painterView setColor:[UIColor clearColor]];
    [painterView setLineWidth:20];
}

- (void)closeEraser
{
    [painterView setColor:_currentLineColor];
    [painterView setLineWidth:_currentLineWidth];
}

- (void)changeLineWidth:(CGFloat)width
{
    if(self.toolBarView.isEraser)
    {
        self.toolBarView.isEraser = NO;
        [self.toolBarView.btnEraser setSelected:NO];
        [self closeEraser];
    }
    [painterView setLineWidth:width];
}

- (void)deleteAll
{
    [painterView resetView];
}

- (void)postImage
{
    ComposeImage *composeImage = [[ComposeImage alloc]initWithImage:editImage painterView:painterView];
    
    [self uploadFileWithName:[composeImage createDoneImage] mediaType:@"IMAGE"];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:kClosePaintMode object:nil];
    editImage = nil;
}

#pragma mark AnswerBarViewDelegate

- (void)postAnswer:(NSString *)answer
{
    [self sendTextMessage:answer];
}


#pragma mark mode action Notification handler

- (void)openPaintModeHandler:(NSNotification *)n
{
    if(n.object)
    {
        editImage = (UIImage *)n.object;
        CGSize newSize = CGSizeMake(editImage.size.width*2, editImage.size.height*2);
        CGSize originalSize = [self getImageNewSizeWithFrame:CGSizeMake(kImageWidth, kImageHight) originalSize:newSize];
        
        
        [self.imgPreview setFrame:CGRectMake((kImageWidth-originalSize.width)/2+(SCREEN_WIDTH-kImageWidth)/2, (kImageHight-originalSize.height)/2+(SCREEN_HEIGHT-kImageHight)/2, originalSize.width, originalSize.height)];
        [self.imgPreview setHidden:NO];
        [self.imgPreview setImage:editImage];
        
        [toolBarView changeModeWithToolBarType:ToolBarTypePaint completion:^(BOOL finished) {
            [painterView setFrame:CGRectMake((kImageWidth-originalSize.width)/2+(SCREEN_WIDTH-kImageWidth)/2, (kImageHight-originalSize.height)/2+(SCREEN_HEIGHT-kImageHight)/2, originalSize.width, originalSize.height)];
            [painterView setAlpha:1];
            [painterView setHidden:NO];
        }];
        
    }
    else
    {
        if(_isWhitePaper)
        {
            editImage = _drawingImage;
            CGSize newSize = CGSizeMake(editImage.size.width*2, editImage.size.height*2);
            CGSize originalSize = [self getImageNewSizeWithFrame:CGSizeMake(kImageWidth, kImageHight) originalSize:newSize];
            [self.imgPreview setFrame:CGRectMake((kImageWidth-originalSize.width)/2+(SCREEN_WIDTH-kImageWidth)/2, (kImageHight-originalSize.height)/2+(SCREEN_HEIGHT-kImageHight)/2, originalSize.width, originalSize.height)];
            [self.imgPreview setHidden:NO];
            [self.imgPreview setImage:editImage];
            
            [toolBarView changeModeWithToolBarType:ToolBarTypePaint completion:^(BOOL finished) {
                [painterView setFrame:CGRectMake((kImageWidth-originalSize.width)/2+(SCREEN_WIDTH-kImageWidth)/2, (kImageHight-originalSize.height)/2+(SCREEN_HEIGHT-kImageHight)/2, originalSize.width, originalSize.height)];
                [painterView setAlpha:1];
                [painterView setHidden:NO];
            }];
        }
        else
        {
            if(pageContainer.currentPage)
            {
                if(pageContainer.currentPage.originalImage)
                {
                    if(_isStreamMode)
                    {
                        editImage = _drawingImage;
                    }
                    else
                    {
                        editImage = pageContainer.currentPage.originalImage;
                    }
                    [pageContainer.currentPage.imageView updateZoomScale:pageContainer.currentPage.imageView.minimumZoomScale];
                    
                    [toolBarView changeModeWithToolBarType:ToolBarTypePaint completion:^(BOOL finished) {
                        [painterView setFrame:pageContainer.currentPage.imageView.imageView.frame];
                        [painterView setAlpha:1];
                        [painterView setHidden:NO];
                    }];
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }
        
        
    }
    self.pageStateType = PageStateTypePaint;
    
}

- (void)closePaintModeHandler
{
    if(editImage)
    {
        [self.imgPreview setImage:nil];
        [self.imgPreview setHidden:YES];
        
        [toolBarView changeModeWithToolBarType:ToolBarTypeIndex completion:^(BOOL finished) {
            [painterView resetView];
            [painterView setHidden:YES];
            self.pageStateType = PageStateTypeNone;
            [self endModeCloseHandler];
            editImage = nil;
            if(_oldImage&&streamPage)
            {
                [streamPage.imageView.imageView setImage:_oldImage];
                _oldImage = nil;
            }
        }];
        
    }
}

- (void)openTextModeHandler
{
    self.pageStateType = PageStateTypeText;
    [toolBarView.btnPaint setHidden:YES];
    [toolBarView.btnMore setHidden:YES];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到当前view
    [self.pageContainer addGestureRecognizer:tapGestureRecognizer];
}

- (void)closeTextModeHandler
{
    self.pageStateType = PageStateTypeNone;
    if(self.isAnswer)
    {
        [toolBarView.txtMessage endEditing:YES];
    }
    else
    {
        [toolBarView.btnMore setHidden:NO];
    }
    [toolBarView.btnPaint setHidden:NO];
    
    
    [UIView animateWithDuration:0.1 animations:^{
        [toolBarView setFrame:CGRectMake(0, SCREEN_HEIGHT-kBarHeight, SCREEN_WIDTH, kBarHeight)];
    }];
    
    [self endModeCloseHandler];
}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    [self.toolBarView.txtMessage resignFirstResponder];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    [self.pageContainer removeGestureRecognizer:tapGestureRecognizer];
    
}

- (void)closeCameraModeHandler
{
    [self.picker dismissViewControllerAnimated:YES completion:^{
        self.pageStateType = PageStateTypeNone;
        [self endModeCloseHandler];
    }];
}

- (void)endModeCloseHandler
{
    if(self.isAnswer)
    {
        if(_modeClose)
        {
            if(_answerType)//客观题
            {
                [self.toolBarView changeModeWithToolBarType:ToolBarTypeAnswer completion:nil];
            }
            
            _modeClose = NO;
            return;
        }
        
    }
    
    if(_isStreamMode)
    {
        [self.pageContainer turnToPageWithGUID:streamPage.pageVO.guid animated:YES];
        if(!_isWhitePaper)
        {
            [_lineManager blendWithImage:streamPage.originalImage];
        }
        return;
        
    }
}

#pragma mark KKStreamDrawDelegate

//开始写的时候 有可能之前有笔迹，那么就会有blendedImage ,bgImageNoDrawingGuid 为任何时候都没有笔迹合成的图片（通常作为背景），resourceImageMaybeWithDrawingGuid 有可能是合成过笔迹的图片（通常在push后，接着继续操作时会使用到）
- (void)onStreamDrawBegan:(StreamDrawStatus)status blendedImage:(UIImage *)blendedImage rid:(NSString *)rid sourceFileID:(NSString *)sourceFileID
{
    //rid 当前图GUID sourceFileID 原图GUID
    streamPage =  [self.pageContainer getPageWithGUID:rid];
    
    
    if(!_isInkPaper)
    {
        noDrawImage = nil;
        _oldImage = streamPage.originalImage;
    }
    
    
    _isStreamMode = YES;
    
    if(self.pageStateType==PageStateTypeUserDragging)
    {
        [self.pageContainer turnToPageWithGUID:streamPage.pageVO.guid animated:YES];
        [_lineManager blendWithImage:streamPage.originalImage];
    }
    
    if(![self isBlankString:rid]&&[self isBlankString:sourceFileID])//原图第一次书写
    {
        noDrawImage = streamPage.originalImage;
        _sourceFileID = rid;
    }
    if(![self isBlankString:rid]&&![self isBlankString:sourceFileID])//板书图书写
    {
        if(!noDrawImage)
        {
            noDrawImage = [self getImageFromCacheWithGUID:sourceFileID];
        }
        _sourceFileID = rid;//笔记图id
        if(self.pageStateType==PageStateTypeNone)[self.pageContainer turnToPageWithGUID:rid animated:YES];
    }
    if([self isBlankString:rid]&&![self isBlankString:sourceFileID])//白板书写
    {
        _isWhitePaper = YES;
        _whitePaperGUID = sourceFileID;
        noDrawImage = [self getImageFromCacheWithGUID:sourceFileID];
        if(!noDrawImage)
        {
            noDrawImage = ImageWithFile(@"blank.png");
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(whitePaperDidLoadHandler:) name:kImageRequestDidLoad object:nil];
            [Page createPageWithGUID:sourceFileID];//请求白板图片，显示白板层
            [[PageRequestManager defaultManager]goCommand:sourceFileID];
        }
        else
        {
            _oldImage = noDrawImage;
            [_lineManager blendWithImage:_oldImage];
        }
        [imgWhitePaper setHidden:NO];
        [imgWhitePaper setFrame:CGRectMake(0, 0, kImageWidth, kImageHight)];
        [imgWhitePaper prepareForReuse];
        [imgWhitePaper displayImage:noDrawImage];
        [self.pageContainer setHidden:YES];
    }

    
    [_lineManager blendWithImage:noDrawImage];
    [[NSNotificationCenter defaultCenter]postNotificationName:kOpenStreamMode object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(noDrawImageDidLoadHandler) name:kImageDidLoad object:nil];
    
}

//过程中不断渲染时会调用，只有blendedImage。
- (void)onStreamDrawDrawing:(StreamDrawStatus)status blendedImage:(UIImage *)blendedImage whitePaperGuid:(NSString *)whitePaperGuid imageScale:(float)scale;
{
    
    if(scale==1)
    {
        [self.toolBarView.btnPaint setEnabled:YES];
    }
    else
    {
        [self.toolBarView.btnPaint setEnabled:NO];
    }
    
    if(![self isBlankString:whitePaperGuid])
    {
        _isWhitePaper = YES;
        _whitePaperGUID = whitePaperGuid;
        noDrawImage = [self getImageFromCacheWithGUID:whitePaperGuid];
        if(!noDrawImage)
        {
            noDrawImage = ImageWithFile(@"blank.png");
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(whitePaperDidLoadHandler:) name:kImageRequestDidLoad object:nil];
            [Page createPageWithGUID:whitePaperGuid];//请求白板图片，显示白板层
            [[PageRequestManager defaultManager]goCommand:whitePaperGuid];
        }
        else
        {
            _oldImage = noDrawImage;
            [_lineManager blendWithImage:_oldImage];
        }
        [imgWhitePaper setHidden:NO];
        [imgWhitePaper setFrame:CGRectMake(0, 0, kImageWidth, kImageHight)];
        [imgWhitePaper prepareForReuse];
        [imgWhitePaper displayImage:noDrawImage];
        [self.pageContainer setHidden:YES];
    }
    _drawingImage = blendedImage;
    
    if(self.pageStateType==PageStateTypePaint)
    {
        return;
    }
    
    if(_isWhitePaper)
    {
        [imgWhitePaper.imageView setImage:blendedImage];
    }
    else
    {
        if(!streamPage)
        {
            streamPage =  [self.pageContainer getPageWithGUID:_sourceFileID];
        }
        [streamPage.imageView.imageView setImage:blendedImage];
    }
}


//渲染结束时调用，通常处理UI的一些事件。
- (void)onStreamDrawEnd:(StreamDrawStatus)status
{
    if(_isWhitePaper)
    {
        [imgWhitePaper.imageView setImage:nil];
        [imgWhitePaper setFrame:CGRectMake(0, 0, 0, 0)];
        [imgWhitePaper setHidden:YES];
        [self.pageContainer setHidden:NO];
        _oldImage = nil;
    }
    else
    {
        if(self.pageStateType!=PageStateTypePaint)
        {
            [streamPage.imageView.imageView setImage:_oldImage];
             _oldImage = nil;
        }
    }
    
   
    _isStreamMode = NO;
    _isWhitePaper = NO;
    _isInkPaper = NO;
    [self.toolBarView.btnPaint setEnabled:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:kCloseStreamMode object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kImageDidLoad object:nil];
    
    [self.pageContainer endPage];
}

//从板书拖出来，没有加载原图片渲染的透明Image
- (void)onWillBlendExistHandWritingImage:(UIImage *)image
{
    [self.pageContainer.currentPage.imageView.imageView setImage:image];
    [self.pageContainer.currentPage setOriginalImage:image];
    _inkLoaded = YES;
    [_lineManager blendExistHandWritingImageWithImage:noDrawImage];//获得原图后调用
}

//返回合成后的图片
- (void)onBlendedExistHandWritingImage:(UIImage *)image
{
    [[NSNotificationCenter defaultCenter]postNotificationName:kSaveInkImage object:UIImageJPEGRepresentation(image, 1)];
    [self.pageContainer.currentPage.imageView.imageView setImage:image];
    [self.pageContainer.currentPage setOriginalImage:image];
    _oldImage = image;
}

- (void)onCurrentFrameImageSavedWithImage:(UIImage *)image
{
    [pageContainer addPageWithGUID:_drawingGUID animated:NO isStream:YES];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    NSString *path = [self saveImage:imageData ToCacheWithGUID:_drawingGUID];
    PageVO *page = [Page getPageWithGUID:_drawingGUID];
    [page setGuid:_drawingGUID];
    [page setFileType:FileType_IMAGE];
    [page setIsCache:YES];
    [page setImagePath:path];
    [Page updatePage:page];
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:image,@"viewImage",_drawingGUID,GUID, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kImageRequestDidLoad object:dic];
}

- (void)inkImageWillLoadHandler:(NSNotification *)n
{
    _inkLoaded = NO;
    noDrawImage = nil;
    NSString *ink = n.object;
    [_lineManager getExistHandWritingImageWithGuid:ink];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(originalImageDidLoad:) name:kOriginalImageDidLoad object:nil];
}

//接到保存板书处理
- (void)saveStreamImageHandler:(NSNotification *)n
{
    if(_oldImage)
    {
        [_lineManager saveCurrentFrameImage];
    }
    NSString *dicStr = n.object;
    NSDictionary *dic = [dicStr JSONValue];
    
    if(dic)
    {
        _drawingGUID = [dic objectForKey:@"savedGuid"];
    }
}

//获得笔记图的原图
- (void)originalImageDidLoad:(NSNotification *)n
{
    NSDictionary *dic = n.object;
    if(dic)
    {
        UIImage *image = [dic objectForKey:@"viewImage"];
        
        
        noDrawImage = image;
        if(_inkLoaded)
        {
            [_lineManager blendExistHandWritingImageWithImage:image];//获得原图后调用
        }

        _isInkPaper = YES;
    }
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kOriginalImageDidLoad object:nil];
}

- (void)noDrawImageDidLoadHandler
{
    if(_isWhitePaper)return;
    streamPage = [pageContainer getPageWithGUID:_sourceFileID];
    _oldImage = streamPage.originalImage;
    [_lineManager blendWithImage:_oldImage];
    [self closeAllState];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kImageDidLoad object:nil];
}

- (void)whitePaperDidLoadHandler:(NSNotification *)n
{
    NSDictionary *dic = n.object;
    NSString *guid = [dic objectForKey:GUID];
    if([_whitePaperGUID isEqualToString:guid])
    {
        UIImage *image = [dic objectForKey:@"viewImage"];
        _oldImage = image;
        [_lineManager blendWithImage:_oldImage];
        [[NSNotificationCenter defaultCenter]removeObserver:self name:kImageRequestDidLoad object:nil];
    }
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    if(self.picker.sourceType==UIImagePickerControllerSourceTypeSavedPhotosAlbum)
    {
        if(image)
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:kOpenPaintMode object:image];
        }
        [popover dismissPopoverAnimated:YES];
    }
    else
    {
        [self.picker dismissViewControllerAnimated:YES completion:^{
            if(image)
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kOpenPaintMode object:image];
            }
        }];
    }
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.picker dismissViewControllerAnimated:YES completion:^{
        self.pageStateType = PageStateTypeNone;
    }];
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    self.pageStateType = PageStateTypeNone;
    return YES;
}

#pragma mark bubble

- (void)showBubbleViewWithType:(BubbleViewType)type selected:(int)index
{
    
    if(bubble)
    {
        [self hideBubbleViewWithType:bubble.bubbleType completion:^{
            
            CGFloat x;
            switch (type) {
                case BubbleViewTypeCamera:
                {
                    bubble = [BubbleView bubbleViewWithType:BubbleViewTypeCamera];
                    x = 859;
                }
                    break;
                case BubbleViewTypeColor:
                {
                    bubble = [BubbleView bubbleViewWithType:BubbleViewTypeColor];
                    [bubble setLineColor:index];
                    x = 348;
                }
                    break;
                case BubbleViewTypeLineWidth:
                {
                    bubble = [BubbleView bubbleViewWithType:BubbleViewTypeLineWidth];
                    [bubble setLineWidth:index];
                    x = 453;
                }
                    break;
                case BubbleViewTypeDelete:
                {
                    bubble = [BubbleView bubbleViewWithType:BubbleViewTypeDelete];
                    [bubble setLineColor:index];
                    x = 570;
                }
                    break;
                    
                default:
                {
                    x = 0;
                }
                    break;
            }
            
            [bubble setFrame:CGRectMake(x, SCREEN_HEIGHT-kBarHeight, bubble.frame.size.width, bubble.frame.size.height)];
            [self.view addSubview:bubble];
            [bubble setDelegate:self];
            [bubble setHidden:NO];
            [bubble setAlpha:0];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewDidShow object:[NSNumber numberWithInteger:type]];
            
            [UIView animateWithDuration:0.3 animations:^{
                [bubble setFrame:CGRectMake(x, SCREEN_HEIGHT-kBarHeight-(bubble.frame.size.height+10), bubble.frame.size.width, bubble.frame.size.height)];
                [bubble setAlpha:1];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.1 animations:^{
                    [bubble setFrame:CGRectMake(x, SCREEN_HEIGHT-kBarHeight-bubble.frame.size.height, bubble.frame.size.width, bubble.frame.size.height)];
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }];
    }
    else
    {
        CGFloat x;
        switch (type) {
            case BubbleViewTypeCamera:
            {
                bubble = [BubbleView bubbleViewWithType:BubbleViewTypeCamera];
                x = 859;
            }
                break;
            case BubbleViewTypeColor:
            {
                bubble = [BubbleView bubbleViewWithType:BubbleViewTypeColor];
                [bubble setLineColor:index];
                x = 348;
            }
                break;
            case BubbleViewTypeLineWidth:
            {
                bubble = [BubbleView bubbleViewWithType:BubbleViewTypeLineWidth];
                [bubble setLineWidth:index];
                x = 453;
            }
                break;
            case BubbleViewTypeDelete:
            {
                bubble = [BubbleView bubbleViewWithType:BubbleViewTypeDelete];
                [bubble setLineColor:index];
                x = 570;
            }
                break;
                
            default:
            {
                x = 0;
            }
                break;
        }
        
        [bubble setFrame:CGRectMake(x, SCREEN_HEIGHT-kBarHeight, bubble.frame.size.width, bubble.frame.size.height)];
        [self.view addSubview:bubble];
        [bubble setDelegate:self];
        [bubble setHidden:NO];
        [bubble setAlpha:0];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewDidShow object:[NSNumber numberWithInteger:type]];
        
        [UIView animateWithDuration:0.3 animations:^{
            [bubble setFrame:CGRectMake(x, SCREEN_HEIGHT-kBarHeight-(bubble.frame.size.height+10), bubble.frame.size.width, bubble.frame.size.height)];
            [bubble setAlpha:1];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 animations:^{
                [bubble setFrame:CGRectMake(x, SCREEN_HEIGHT-kBarHeight-bubble.frame.size.height, bubble.frame.size.width, bubble.frame.size.height)];
            } completion:^(BOOL finished) {
                
            }];
        }];
    }
    [bubbleMask setHidden:NO];
    
}

- (void)hideBubbleViewWithType:(BubbleViewType)type completion:(void (^)(void)) block
{
    if(bubble)
    {
        CGFloat x;
        switch (type) {
            case BubbleViewTypeCamera:
            {
                x = 859;
            }
                break;
            case BubbleViewTypeColor:
            {
                x = 348;
            }
                break;
            case BubbleViewTypeLineWidth:
            {
                x = 453;
            }
                break;
            case BubbleViewTypeDelete:
            {
                x = 570;
            }
                break;
                
            default:
            {
                x = 0;
            }
                break;
        }
        
            [UIView animateWithDuration:0.3 animations:^{
                [bubble setFrame:CGRectMake(x, SCREEN_HEIGHT-kBarHeight-(bubble.frame.size.height+10), bubble.frame.size.width, bubble.frame.size.height)];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.1 animations:^{
                    [bubble setFrame:CGRectMake(x, SCREEN_HEIGHT-kBarHeight, bubble.frame.size.width, bubble.frame.size.height)];
                    [bubble setAlpha:0];
                } completion:^(BOOL finished) {
                    [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewDidHide object:[NSNumber numberWithInteger:type]];
                    [bubble removeFromSuperview];
                    bubble = nil;
                    if(block)
                    {
                        block();
                    }
                }];
            }];
    }
    [bubbleMask setHidden:YES];
}

- (void)bubbleViewButtonDidClick:(NSUInteger) buttonIndex bubbleType:(BubbleViewType) type
{
    if(type==BubbleViewTypeCamera)
    {
        if(buttonIndex==0)
        {
            editImage = ImageWithFile(@"blank.png");
            
            if(editImage)
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kOpenPaintMode object:editImage];
            }
            
        }
        if(buttonIndex==1)
        {
            [self openCamera];
        }
        if(buttonIndex==2)
        {
            [self openAlbum];
        }
    }
    if(type==BubbleViewTypeColor)
    {
        UIColor *lineColor = [self getColorWithIndex:buttonIndex];
        [self changeColor:lineColor];
        SetObjectUserDefault([NSNumber numberWithUnsignedLong:buttonIndex], kPainterLineColor);
    }
    if(type==BubbleViewTypeLineWidth)
    {
        CGFloat width = [self getWidthWithIndex:buttonIndex];
        [self changeLineWidth:width];
        SetObjectUserDefault([NSNumber numberWithUnsignedLong:buttonIndex], kPainterLineWidth);
    }
    if(type==BubbleViewTypeDelete)
    {
        if(buttonIndex==0)
        {
            [self deleteAll];
        }
    }
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:type],@"type",[NSNumber numberWithInteger:buttonIndex],@"index", nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewSelect object:dic];
    
    [self hideBubbleViewWithType:type completion:nil];
}

- (void)bubbleViewShowHandler:(NSNotification *)n
{
    NSDictionary *dic = n.object;
    BubbleViewType type = [[dic objectForKey:@"type"] integerValue];
    int index = [[dic objectForKey:@"index"] intValue];
    [self showBubbleViewWithType:type selected:index];
}

- (void)bubbleViewHideHandler:(NSNotification *)n
{
    NSDictionary *dic = n.object;
    BubbleViewType type = [[dic objectForKey:@"type"] integerValue];
    [self hideBubbleViewWithType:type completion:nil];
}

- (void)bubbleMaskHandler
{
    if(bubble)
    {
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:bubble.bubbleType],@"type",[NSNumber numberWithInteger:0],@"index", nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:kBubbleViewHide object:dic];
    }
    else
    {
        [bubbleMask setHidden:YES];
    }
}

#pragma mark 上传文件


- (void)uploadFileWithName:(NSString*)fileName mediaType:(NSString*)mediaType
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:[[CachedUserInfo shared] tvmid] forKey:@"owner_tvmid"];
    [dic setObject:@"交流" forKey:@"onto_res_pack"];
    [dic setObject:mediaType forKey:@"file_type[]"];
    [dic setObject:[[CachedUserInfo shared] realName] forKey:@"title[]"];
    NSURL *filePath = [NSURL fileURLWithPath:fileName];
    
    AFJSONResponseSerializer *serializer =[AFJSONResponseSerializer serializer];
    [serializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil]];
    [AppDelegate sharedHttpRequestManager].responseSerializer =serializer;
    [[AppDelegate sharedHttpRequestManager] POST:[NSString stringWithFormat:@"%@/%@", [[KKCache sharedCache] HTTPIP], [ICEFileEntity URL_UPLOAD_FILE]] parameters:dic constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:filePath name:@"file[]" error:nil];
    }success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSArray *ary;
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dic =(NSDictionary *)responseObject;
            ary =(NSArray *)[dic objectForKey:@"msg"];
        }
        else
        {
            return;
        }
        
        if(!ary)return;
        
        if([ary count] > 0)
        {
            if([[ary objectAtIndex:0] objectForKey:@"guid"] != nil && ![[[ary objectAtIndex:0] objectForKey:@"guid"] isKindOfClass:[[NSNull null] class]])
            {
                NSDictionary* tempDict = [NSDictionary dictionaryWithObject:[[ary objectAtIndex:0] objectForKey:@"guid"] forKey:@"id"];
                NSArray* array = [NSArray arrayWithObject:tempDict];
                NSDictionary* pushDict  = [NSDictionary dictionaryWithObjectsAndKeys:@"resource", @"type", array, @"detail", nil];
                
                CommandEntity *commandEntity = [[CommandEntity alloc]init];
                commandEntity.CmdTYPE  = [CommandTypeEntity PUSH];
                commandEntity.body     = [pushDict JSONString];
                [[commandEntity OBJ] addObject:[GroupTypeEntity ALL_CAN_SEE]];
                [[[ICEHelper shared] iceManager] sendCmdEntity:commandEntity];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"上传交流包错误%@",error);
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
