//
//  PageRequestManager.m
//  EQClient
//
//  Created by xumeng on 1/24/14.
//  Copyright (c) 2014 xumeng. All rights reserved.
//

#import "PageRequestManager.h"
#import "Info.h"
#import "KKCache.h"
#import "ICEFileEntity.h"
#import "NSObject+Utils.h"
#import "NSString+JSONSerialization.h"
#import "AFHTTPRequestOperationManager.h"
#import "Page.h"
#import "NSString+.h"
#import "CachedUserInfo.h"
@interface PageRequestManager()
{
@private
    
    NSString *_oldPackageName;
    NSString *_currentPackageName;
    NSArray *_pageAry;
    NSMutableArray *_cacheAry;
    NSString *_lastRequestGUID;
    NSString *_currentRequestGUID;
    BOOL _isGoRequest;
    int errorCount;
    
    BOOL _isinkImage;
    NSString *_inkImageguid;
    NSString *_inkSourceImageguid;
    
    BOOL _allPackage;
    BOOL _canCacheAllPackage;
    NSMutableArray *_allPackageInfoAry;
    
}
@end
@implementation PageRequestManager
@synthesize infoRequestManager;
@synthesize imageRequestManager;

+ (instancetype)defaultManager
{
    static PageRequestManager *_instance = nil;
    static dispatch_once_t p;
    
    dispatch_once(&p, ^{
        _instance = [[self alloc] init];
        [_instance addCommands];
        _instance.infoRequestManager = [AFHTTPRequestOperationManager manager];
        [_instance.infoRequestManager.operationQueue setMaxConcurrentOperationCount:1];
        _instance.imageRequestManager = [AFHTTPRequestOperationManager manager];
        [_instance.imageRequestManager.operationQueue setMaxConcurrentOperationCount:1];
    });
    return _instance;
}

- (void)addCommands
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(cacheCommandHandler:) name:kReciveCacheNotificaiton object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pageImageReloadHandler:) name:kPageImageReload object:nil];
}

- (void)goCommand:(NSString *)guid
{
    
    [self.imageRequestManager.operationQueue cancelAllOperations];//重置请求
    
    errorCount = 0;
    _isGoRequest = NO;
    _isinkImage = NO;
    
    _lastRequestGUID = guid;
    _currentRequestGUID = guid;
    DLog(@"添加GO指令:%@",_lastRequestGUID);
    
    PageVO *page = [Page getPageWithGUID:guid];
    if(page)
    {
        if(page.isCache)//缓存了信息和图片
        {
            UIImage *image = [self getImageFromCacheWithGUID:page.guid];
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:image,@"viewImage",guid,GUID, nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:kImageRequestDidLoad object:dic];
            _lastRequestGUID = nil;
        }
        
    }
    
    if(![self isBlankString:_currentPackageName]&&![self isBlankString:_currentRequestGUID])//查询包信息
    {
        [self queryPackageInfoWithName:_currentPackageName];
    }
    
}
- (void)cacheCommandHandler:(NSNotification *)n
{
    if(_isGoRequest)return;
    
    if(_lastRequestGUID) //存在go 开始请求go图片
    {
        if(_canCacheAllPackage)
        {
            _cacheAry = nil;
            _canCacheAllPackage = NO;
        }
        _isGoRequest = YES;
        PageVO *page = [Page getPageWithGUID:_lastRequestGUID];
        
        if(page)
        {
            [self requestImageWithGUID:_lastRequestGUID]; //开始一个新page请求
        }
    }
    else if(![self isBlankString:_currentPackageName]&&![self isBlankString:_currentRequestGUID]) //不存在则开始一个缓存请求
    {
        if([self isBlankString:_oldPackageName])
        {
            //第一次，查询包信息
            _oldPackageName = _currentPackageName;
            [self queryPackageInfoWithName:_currentPackageName];
        }
        else
        {
            if([_currentPackageName isEqualToString:_oldPackageName])
            {
                //继续
                [self cacheHandler];
                
            }
            else
            {
                //更换包，查询包信息
                _oldPackageName = _currentPackageName;
                [self queryPackageInfoWithName:_currentPackageName];
            }
        }
    }
    else //缓存包前几张
    {
        if(!_allPackage)
        {
            _canCacheAllPackage = NO;
            [self queryAllPackageInfo];
            _allPackage = YES;
        }
        
        if(_canCacheAllPackage)
        {
            [self cacheHandler];
        }
    }
}

- (void)cacheHandler
{
    if(_cacheAry.count>0)
    {
        NSDictionary *dic = [_cacheAry objectAtIndex:0];
        NSString *guid = [dic objectForKey:GUID];
        [_cacheAry removeObjectAtIndex:0];
        PageVO *page = [Page getPageWithGUID:guid];
        
        if(page)
        {
            if(page.isCache)
            {
                [self cacheHandler];
                return;
            }
            else
            {
                DLog(@"开始缓存图片TAG:%@ GUID:%@",[dic objectForKey:Tag],guid);
                [self requestImageWithGUID:guid];
            }
        }
        else
        {
            DLog(@"开始缓存图片TAG:%@ GUID:%@",[dic objectForKey:Tag],guid);
            page = [Page createPageWithGUID:guid];
            [self requestImageWithGUID:guid];
        }
    }
}

- (void)pageImageReloadHandler:(NSNotification *)n
{
    NSDictionary *dic = (NSDictionary *)n.object;
    NSString *type = [dic objectForKey:@"type"];
    if([type isEqualToString:@"info"])
    {
        NSString *guid = [dic objectForKey:GUID];
        [self requestImageWithGUID:guid];
    }
    else if([type isEqualToString:@"image"])
    {
        PageVO *page = [dic objectForKey:@"page"];
        [self imageRequestHandler:page isInk:NO];
    }
}

- (void)requestImageWithGUID:(NSString *)guid
{
    NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:guid,GUID, nil];
    NSString *urlString = [NSString stringWithFormat:@"%@/%@",[[KKCache sharedCache] HTTPIP],[ICEFileEntity URL_SANDBOX_SEARCH_FILE]];
    
    AFJSONResponseSerializer *serializer =[AFJSONResponseSerializer serializer];
    [serializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil]];
    self.imageRequestManager.responseSerializer =serializer;
    [self.imageRequestManager POST:urlString parameters:d success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *ary;
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dic =(NSDictionary *)responseObject;
            ary =(NSArray *)[dic objectForKey:@"msg"];
        }
        else
        {
            return;
        }
        
        if(!ary)return;
        
        if([ary count] ==0)
        {
            
        }
        else
        {
            NSDictionary *resultDict = [ary objectAtIndex:0];
            NSDictionary *dict;
            
            if([[resultDict objectForKey:FileType]isEqualToString:@"PPT"]||[[resultDict objectForKey:FileType]isEqualToString:@"PDF"])
            {
                return;
            }
            
            if(_isinkImage)
            {
                NSString *fileName = [NSString stringWithFormat:@"%@.jpg",guid];
                dict = [self getImageInfoWithPackageName:[resultDict objectForKey:PackageName] guid:_inkSourceImageguid fileNameWithExtend:fileName width:[resultDict objectForKey:Width] height:[resultDict objectForKey:Height] fileType:[resultDict objectForKey:FileType]];//获取原图地址
                
                PageVO *page = [Page getPageWithGUID:_inkImageguid];//存在板书GUID
                [page setFileType:[resultDict objectForKey:FileType]];
                [page setGuid:_inkImageguid];
                [page setImageUrl:[dict objectForKey:@"realString"]];
                [Page updatePage:page];
                
                _isinkImage = NO;
                [self imageRequestHandler:page isInk:YES];
            }
            else
            {
                
                NSString * jsonStr= [NSString stringWithFormat:@"%@",[resultDict objectForKey:Tag]];
                NSRange foundObj=[jsonStr rangeOfString:@"ink" options:NSCaseInsensitiveSearch];
                if(foundObj.length>0) {
                    NSDictionary *jsonDic = [jsonStr JSONValue];
                    if(jsonDic)
                    {
                        NSString *ink = [jsonDic objectForKey:@"ink"];
                        NSString *source = [jsonDic objectForKey:@"source"];
                        if(![self isBlankString:ink])
                        {
                            _isinkImage = YES;
                            _inkImageguid = guid;
                            _inkSourceImageguid = source;
                            [self requestImageWithGUID:source];
                            [[NSNotificationCenter defaultCenter]postNotificationName:kInkImageWillLoad object:ink];
                        }
                        DLog(@"板书图GUID:%@,原图GUID:%@",guid,source);
                    }
                    
                }
                else
                {
                    dict = [self getImageInfoWithPackageName:[resultDict objectForKey:PackageName] guid:guid fileNameWithExtend:[resultDict objectForKey:FileNameWithExtend] width:[resultDict objectForKey:Width] height:[resultDict objectForKey:Height] fileType:[resultDict objectForKey:FileType]];
                    
                    PageVO *page = [Page getPageWithGUID:guid];
                    [page setFileType:[resultDict objectForKey:FileType]];
                    [page setGuid:guid];
                    if([[resultDict objectForKey:FileType] isEqualToString:FileType_VIDEO])
                    {
                        [page setImageUrl:[dict objectForKey:@"thumbString"]];
                        [page setResPath:[dict objectForKey:@"realString"]];
                    }
                    else
                    {
                        [page setImageUrl:[dict objectForKey:@"realString"]];
                    }
                    [Page updatePage:page];
                    
                    [self imageRequestHandler:page isInk:NO];
                    
                    _currentPackageName = [resultDict objectForKey:@"packname"];
                }
            }
            
            
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"获得图片信息错误%@",error);
        _isGoRequest = NO;
        if(_isinkImage)_isinkImage = NO;
    }];
}

- (void)imageRequestHandler:(PageVO *)page isInk:(BOOL)isInk
{
    
    self.imageRequestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [self.imageRequestManager GET:page.imageUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSUInteger headerLength = [[operation.response.allHeaderFields objectForKey:@"Content-Length"] integerValue];
        NSData *data = responseObject;
        if(headerLength!=data.length)
        {
            
            if(errorCount>2){
                _isGoRequest = NO;
                return;
            }
            [self imageRequestHandler:page isInk:isInk];
            errorCount++;
            return;//坏图
        }
        
        
        NSData *imageData = responseObject;
        
        NSString *path;
        DLog(@"-------SaveImage----guid:%@---",page.guid);
        
        PageVO *sp;
        
        
        NSDictionary *dict;
        if(isInk)
        {
            NSString* documentPath = LibraryPath;
            path = [documentPath stringByAppendingPathComponent:CachePath];
            path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", [NSString md5StringWithString:_inkImageguid]]];
            sp = [Page getPageWithGUID:_inkImageguid];
            [sp setImagePath:path];
            [sp setIsCache:YES];
            [Page updatePage:sp];

            [self saveImage:imageData ToCacheWithGUID:_inkSourceImageguid];//保存原图
            
            dict = [NSDictionary dictionaryWithObjectsAndKeys:page.fileType,FileType,[UIImage imageWithData:imageData],@"viewImage",_inkImageguid,GUID,@"yes",@"isInk", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:kOriginalImageDidLoad object:dict];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveInkImageHandler:) name:kSaveInkImage object:nil];
        }
        else
        {
            path = [self saveImage:imageData ToCacheWithGUID:page.guid];//保存入缓存
            sp = [Page getPageWithGUID:page.guid];
            [sp setImagePath:path];
            [sp setIsCache:YES];
            [Page updatePage:sp];
            
            dict = [NSDictionary dictionaryWithObjectsAndKeys:page.fileType,FileType,[UIImage imageWithData:imageData],@"viewImage",page.guid,GUID,@"no",@"isInk", nil];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kImageRequestDidLoad object:dict];
        
        
        _lastRequestGUID = nil;
        _isGoRequest = NO;
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"图片请求错误%@",error);
        if(errorCount>2)
        {
            _isGoRequest = NO;
            return;
        }
        [self imageRequestHandler:page isInk:isInk];
        errorCount++;
        return;//请求失败
    }];
}

- (void)saveInkImageHandler:(NSNotification *)n
{
    NSData *data = n.object;
    [self saveImage:data ToCacheWithGUID:_inkImageguid];//保存合成后的板书图
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kSaveInkImage object:nil];
}

- (void)queryPackageInfoWithName:(NSString *)name
{
    if([_currentPackageName isEqualToString:@"板书"])return;
    
    NSString*   urlString = [NSString stringWithFormat:@"%@/%@", [[KKCache sharedCache] HTTPIP], [ICEFileEntity URL_SEARCH_FILE]];
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:_currentPackageName,@"in_res_pack",@"id",@"order_column", @"1",@"is_asc",nil];
    AFJSONResponseSerializer *serializer =[AFJSONResponseSerializer serializer];
    [serializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil]];
    self.infoRequestManager.responseSerializer = serializer;
    [self.infoRequestManager GET:urlString parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        _pageAry = [responseObject objectForKey:@"msg"];
        
        DLog(@"获得包信息图GUID%@*********包信息%@*******图数量%d",_currentRequestGUID,_currentPackageName,_pageAry.count);
        _cacheAry = nil;
        _cacheAry = [[NSMutableArray alloc]init];
        int guidIndex = 0;
        for (guidIndex = 0; guidIndex < [_pageAry count]; guidIndex++)
        {
            NSDictionary *dic = [_pageAry objectAtIndex:guidIndex];
            NSString *guid = [dic objectForKey:@"guid"];
            if([guid isEqualToString:_currentRequestGUID])
            {
                break;
            }
        }
        
        for (int i = 1; i<=_pageAry.count; i++) {
            NSDictionary* dic;
            NSDictionary* dict;
            if(guidIndex+i<(_pageAry.count-1))
            {
                dic = [_pageAry objectAtIndex:guidIndex+i];
                dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",guidIndex+i],Tag,[dic objectForKey:@"guid"],GUID, nil];
                [_cacheAry addObject:dict];
            }
            
            if(guidIndex-i>0)
            {
                dic = [_pageAry objectAtIndex:guidIndex-i];
                dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",guidIndex-i],Tag,[dic objectForKey:@"guid"],GUID, nil];
                [_cacheAry addObject:dict];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"请求包信息错误%@",error);
    }];
}

- (void)queryAllPackageInfo
{
    _allPackageInfoAry = [[NSMutableArray alloc]init];
    NSString*   urlString = [NSString stringWithFormat:@"%@/ice3/search_folder.php?user_tvmid=%@", [[KKCache sharedCache] HTTPIP], [[CachedUserInfo shared] tvmid]];
    AFJSONResponseSerializer *serializer =[AFJSONResponseSerializer serializer];
    [serializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil]];
    self.infoRequestManager.responseSerializer = serializer;
    [self.infoRequestManager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        _pageAry = [responseObject objectForKey:@"msg"];
        NSDictionary *dic;
        NSMutableArray *ary = [[NSMutableArray alloc]init];
        for(int i = 0;i<_pageAry.count;i++)
        {
            dic = [_pageAry objectAtIndex:i];
            NSString *packageName = [dic objectForKey:PackageName];
            if(![packageName isEqualToString:@"交流"]&&![packageName isEqualToString:@"板书"])
            {
                [ary addObject:dic];
            }
        }
        __block NSInteger count = ary.count;
        for(int i = 0;i<ary.count;i++)
        {
            dic = [ary objectAtIndex:i];
            NSString *packageName = [dic objectForKey:PackageName];
            NSString*   urlString = [NSString stringWithFormat:@"%@/%@", [[KKCache sharedCache] HTTPIP], [ICEFileEntity URL_SEARCH_FILE]];
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:packageName,@"in_res_pack",@"id",@"order_column", @"1",@"is_asc",nil];
            AFJSONResponseSerializer *serializer =[AFJSONResponseSerializer serializer];
            [serializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil]];
            self.infoRequestManager.responseSerializer = serializer;
            [self.infoRequestManager GET:urlString parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSArray *ary = [responseObject objectForKey:@"msg"];
                [_allPackageInfoAry addObject:ary];
                count--;
                if(count==0)
                {
                    [self getAllPackageInfo];
                    
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                DLog(@"queryPackageInfo==%@",error);
                count--;
                if(count==0)
                {
                    DLog(@"_allPackageInfoAry%d",_allPackageInfoAry.count);
                    [self getAllPackageInfo];
                    
                }
            }];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"queryPackageInfo==%@",error);
    }];
}

- (void)getAllPackageInfo
{
    NSArray *ary;
    NSDictionary *dic;
    NSDictionary *dict;
    _cacheAry = nil;
    _cacheAry = [[NSMutableArray alloc]init];
//    int maxCount = 0;
//    for(int i = 0;i<_allPackageInfoAry.count;i++)
//    {
//        
//    }
    
    for(int i = 0;i<20;i++)
    {
        for (int j = 0; j<_allPackageInfoAry.count; j++) {
            ary = [_allPackageInfoAry objectAtIndex:j];
            if(ary.count>i)
            {
                dic = [ary objectAtIndex:i];
                dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",i],Tag,[dic objectForKey:@"guid"],GUID, nil];
                [_cacheAry addObject:dict];
            }
        }
    }
    _canCacheAllPackage = YES;
}

@end

