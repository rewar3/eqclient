//
//  PageVO.m
//  EQClient
//
//  Created by xumeng on 2/7/14.
//  Copyright (c) 2014 xumeng. All rights reserved.
//

#import "PageVO.h"

@implementation PageVO
@synthesize pid;
@synthesize guid;
@synthesize imagePath;
@synthesize imageUrl;
@synthesize resPath;
@synthesize fileType;
@synthesize isCache;
@end
