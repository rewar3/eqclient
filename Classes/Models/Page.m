//
//  Page.m
//  EQClient
//
//  Created by xumeng on 2/7/14.
//  Copyright (c) 2014 xumeng. All rights reserved.
//

#import "Page.h"
#import "FMDatabase.h"
@implementation Page

+ (PageVO *)createPageWithGUID:(NSString *)guid
{
    PageVO *page = [[PageVO alloc]init];
    page.guid = guid;
    FMDatabase *database  = [FMDatabase databaseWithPath:DatabasePath];
    if (![database open]) {
        ALERT_TITLE(@"数据库错误",@"Open database failed");
    }
    [database executeUpdate:@"insert into page (p_guid) values (?)",guid];
    [database close];
    return page;
    
}

+ (void)updatePage:(PageVO *)page
{
    FMDatabase *database  = [FMDatabase databaseWithPath:DatabasePath];
    if (![database open]) {
        ALERT_TITLE(@"数据库错误",@"Open database failed");
    }
    
    [database executeUpdate:@"update page set p_guid=?,p_imagepath=?,p_imageurl=?,p_respath=?,p_filetype=?,p_iscache=? where p_id=?",page.guid,page.imagePath,page.imageUrl,page.resPath,page.fileType,[NSNumber numberWithInt:page.isCache],[NSNumber numberWithInteger:page.pid]];
    [database close];
}

+ (PageVO *)getPageWithGUID:(NSString *)guid
{
    
    PageVO *page = [[PageVO alloc]init];
    
    FMDatabase *database  = [FMDatabase databaseWithPath:DatabasePath];
    if (![database open]) {
        ALERT_TITLE(@"数据库错误",@"Open database failed");
    }
    BOOL isNull = YES;
    FMResultSet *rs = [database executeQuery:@"SELECT * FROM page where p_guid=?",guid];
    while([rs next]) {
        [page setPid:[rs intForColumn:@"p_id"]];
        [page setGuid:[rs stringForColumn:@"p_guid"]];
        [page setImagePath:[rs stringForColumn:@"p_imagepath"]];
        [page setImageUrl:[rs stringForColumn:@"p_imageurl"]];
        [page setResPath:[rs stringForColumn:@"p_respath"]];
        [page setFileType:[rs stringForColumn:@"p_filetype"]];
        [page setIsCache:[rs boolForColumn:@"p_iscache"]];
        isNull = NO;
    }
    page.guid = guid;
    [database close];
    
    if(isNull)
    {
        return nil;
    }
    else
    {
        return page;
    }
    
}

@end
