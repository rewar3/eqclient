//
//  PageVO.h
//  EQClient
//
//  Created by xumeng on 2/7/14.
//  Copyright (c) 2014 xumeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PageVO : NSObject

@property (nonatomic)NSUInteger pid;
@property (nonatomic,strong)NSString *guid;
@property (nonatomic,strong)NSString *imagePath;//本地缓存地址
@property (nonatomic,strong)NSString *imageUrl;//图片下载地址
@property (nonatomic,strong)NSString *resPath;//视频资源地址
@property (nonatomic,strong)NSString *fileType;
@property (nonatomic)BOOL isCache;//是否已经缓存图片

@end
