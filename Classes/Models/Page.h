//
//  Page.h
//  EQClient
//
//  Created by xumeng on 2/7/14.
//  Copyright (c) 2014 xumeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PageVO.h"
@interface Page : NSObject

+ (PageVO *)createPageWithGUID:(NSString *)guid;
+ (void)updatePage:(PageVO *)page;
+ (PageVO *)getPageWithGUID:(NSString *)guid;
@end
