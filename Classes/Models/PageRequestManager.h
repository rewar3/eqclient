//
//  PageRequestManager.h
//  EQClient
//
//  Created by xumeng on 1/24/14.
//  Copyright (c) 2014 xumeng. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AFHTTPRequestOperationManager;
@interface PageRequestManager : NSObject

@property (nonatomic,strong)AFHTTPRequestOperationManager *infoRequestManager;
@property (nonatomic,strong)AFHTTPRequestOperationManager *imageRequestManager;


+ (instancetype)defaultManager;

- (void)goCommand:(NSString *)guid;


@end
