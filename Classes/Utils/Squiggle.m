//
//  Squiggle.m
//  Painter
//
//  Created by  ibokan on 10-9-7.
//  Copyright 2010 tencent.com. All rights reserved.
//

#import "Squiggle.h"


@implementation Squiggle
@synthesize pointsArray;
@synthesize strokeColor;
@synthesize lineWidth;
@synthesize isErase = isErase_ ;
- (id)init
{
    self = [super init];
	if (self)
	{
		pointsArray = [[NSMutableArray alloc] init];
        strokeColor = [[UIColor alloc] init];
	}
	
	return self;
}

- (void)addPoint:(CGPoint)point withLineWidth:(CGFloat)f
{
	//这里得用NSValue将结构体类型的CGPoint转换为对象
	NSValue *value = [NSValue valueWithCGPoint:point];//[NSValue valueWithBytes:&point objCType:@encode(CGPoint)];
    NSValue *width = [NSNumber numberWithFloat:f];
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:value,@"point",width,@"width", nil];
	[pointsArray addObject:dic];
	
}

- (void)dealloc
{
	strokeColor = nil;
	pointsArray = nil;
    
}

@end
