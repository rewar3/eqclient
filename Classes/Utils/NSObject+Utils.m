//
//  NSObject+Utils.m
//  EQClient
//
//  Created by xumeng on 12/24/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import "NSObject+Utils.h"
#import "KKCache.h"
#import "NSString+.h"

@implementation NSObject (Utils)

- (NSString *)saveImage:(NSData *)imageData ToCacheWithGUID:(NSString *)guid;
{
    NSString* documentPath = LibraryPath;
    NSString* directionPath = [documentPath stringByAppendingPathComponent:CachePath];
    NSFileManager* fileManager = [NSFileManager defaultManager];
    [fileManager createDirectoryAtPath:directionPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSString* filePath = [directionPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", [NSString md5StringWithString:guid]]];
    
    [imageData writeToFile:filePath atomically:YES];
    
    return filePath;
}

- (UIImage *)getImageFromCacheWithGUID:(NSString *)guid
{
    NSString* documentPath = LibraryPath;
    NSString* directionPath = [documentPath stringByAppendingPathComponent:CachePath];
    NSString* filePath = [directionPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", [NSString md5StringWithString:guid]]];
    
    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
    
    return image;
}

- (NSString*)getPathWithLibraryPath:(NSString*)path
{
    NSString* documentPath = LibraryPath;
    NSString* string = [documentPath stringByAppendingPathComponent:path];
    return string;
}

- (BOOL) isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}

- (CGSize)getImageNewSizeWithFrame:(CGSize) frame originalSize:(CGSize) imageSize
{
    CGFloat width;
    CGFloat height;
    CGFloat scale;
    
    if(imageSize.width<=frame.width && imageSize.height<=frame.height)
    {
        width =imageSize.width;
        height =imageSize.height;
    }
    else
    {
        if(imageSize.width>frame.width && imageSize.height>frame.height)
        {
            if(imageSize.width>imageSize.height)
            {
                scale =frame.width/imageSize.width;
            }
            else
            {
                scale =frame.height/imageSize.height;
            }
        }
        else
        {
            if(imageSize.width>frame.width)
            {
                scale =frame.width/imageSize.width;
            }
            else
            {
                scale =frame.height/imageSize.height;
            }
            
        }
        
        width =imageSize.width * scale;
        height =imageSize.height * scale;
    }
    return CGSizeMake(width, height);
}

- (NSDictionary *)getImageInfoWithPackageName:(NSString *)packageName guid:(NSString *)guid fileNameWithExtend:(NSString *)extend width:(NSNumber *)w height:(NSNumber *)h fileType:(NSString *)fileType
{
    NSDictionary *dic;
    
    if ([fileType isEqualToString:FileType_IMAGE] || [fileType isEqualToString:FileType_TEXT])
    {
//        NSString *thumbString = [[NSString stringWithFormat:@"%@/resource/%@/%@.jpg_w_512.jpg",[[KKCache sharedCache] HTTPIP],packageName,guid] encodeURL];
        
        NSString *realString ;
        
        int width = [w intValue];
        int height = [h intValue];
        int bigger = MAX(width, height);
        if (bigger == width && bigger > kMaxImageWidth) //2048
        {
            
            realString = [[NSString stringWithFormat:@"%@/resource/%@/%@.jpg_w_%d.jpg",[[KKCache sharedCache] HTTPIP],packageName,guid,kMaxImageWidth] encodeURL];
        }
        else if (bigger == height && bigger > kMaxImageHeight) //2048
        {
            
            realString = [[NSString stringWithFormat:@"%@/resource/%@/%@.jpg_h_%d.jpg",[[KKCache sharedCache] HTTPIP],packageName,guid,kMaxImageHeight] encodeURL];
        }
        else
        {
            realString = [[NSString stringWithFormat:@"%@/resource/%@/%@",[[KKCache sharedCache] HTTPIP],packageName,extend] encodeURL];
        }
        
        
        dic = [NSDictionary dictionaryWithObjectsAndKeys:fileType,FileType,realString,@"realString",guid,GUID, nil];
    }
    if([fileType isEqualToString:FileType_VIDEO])
    {
        NSString *resString =   [[NSString stringWithFormat:@"%@/resource/%@/%@",[[KKCache sharedCache] HTTPIP],packageName,extend] encodeURL];
        
        NSString *thumbString = [[NSString stringWithFormat:@"%@/resource/%@/%@.jpg_w_512.jpg",[[KKCache sharedCache] HTTPIP],packageName,guid] encodeURL];
        
        dic = [NSDictionary dictionaryWithObjectsAndKeys:FileType_VIDEO,FileType,thumbString,@"thumbString",resString,@"realString",guid,GUID, nil];
    }
    
    return dic;
}

- (UIColor *)getColorWithIndex:(NSInteger) index
{
    UIColor *lineColor;
    switch (index) {
        case LineColorWhite:
            lineColor = [UIColor whiteColor];
            break;
        case LineColorBlack:
            lineColor = [UIColor blackColor];
            break;
        case LineColorRed:
            lineColor = [UIColor redColor];
            break;
        case LineColorYellow:
            lineColor = [UIColor yellowColor];
            break;
        case LineColorBlue:
            lineColor = [UIColor blueColor];
            break;
        case LineColorGreen:
            lineColor = [UIColor greenColor];
            break;
        default:
            lineColor = [UIColor redColor];
            break;
    }
    return lineColor;
}

- (CGFloat)getWidthWithIndex:(NSInteger) index
{
    CGFloat width = 0.0;
    switch (index) {
        case LineWidthS:
            width = 5;
            break;
        case LineWidthM:
            width = 10;
            break;
        case LineWidthB:
            width = 15;
            break;
        default:
            width = 5;
            break;
    }
    return width;
}

@end
