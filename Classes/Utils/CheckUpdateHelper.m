//
//  CheckUpdateHelper.m
//  WiFiPlusLiveTwo
//
//  Created by 王金宇 on 13-10-29.
//  Copyright (c) 2013年 王金宇. All rights reserved.
//

#import "CheckUpdateHelper.h"
#import "AppDelegate.h"
@implementation CheckUpdateHelper

+ (id)shared
{
    static id pRet = nil;
    if(pRet == nil)
    {
        pRet = [[self alloc] init];
    }
    return pRet;
}

- (void)checkUpdatefForAuto:(BOOL)autoCheck
{
    _autoCheck = autoCheck;
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *url = [NSString stringWithFormat:@"http://update.tvmining.com/SoftUpdateServer/update/update?product=EQ37&device=iPad&version=%@_Release&format=json",version];
    
    AFJSONResponseSerializer *serializer =[AFJSONResponseSerializer serializer];
    [serializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/html;charset=utf-8", nil]];
    [AppDelegate sharedHttpRequestManager].responseSerializer =serializer;
    [[AppDelegate sharedHttpRequestManager] GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(responseObject == Nil || responseObject == nil)
        {
            return;
        }
        NSDictionary *dict = responseObject;
        NSArray* array = [dict objectForKey:@"versionlist"];
        if(array != nil && [array count] > 0)
        {
            _versionList = [array objectAtIndex:0];
            NSString* ignoreVersion = [[NSUserDefaults standardUserDefaults] objectForKey:@"ignoreversion"];
            NSString* newVersion = [[[_versionList objectForKey:@"version"] componentsSeparatedByString:@"_"] objectAtIndex:0];
            if(![newVersion isEqualToString:ignoreVersion])
            {
                //没有忽略的更新;
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:[_versionList objectForKey:@"describe"] delegate:self cancelButtonTitle:@"下次提醒" otherButtonTitles:@"更新", nil];
                alertView.tag = 0;
                [alertView show];
            }
        }
        else
        {
            if(!_autoCheck)
            {
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"当前版本为最新版本，无需更新" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                alert.tag = 1;
                [alert show];
            }
        }

        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"获取自动更新信息错误%@",error);
    }];
    
    
//    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//    NSString* string = [NSString stringWithFormat:@"http://update.tvmining.com/SoftUpdateServer/update/update?product=%@&device=iPhone&version=%@_Release&format=json", kAppName, [[SharedManager shared ] kAPPVersion]];
//    ASIHTTPRequest* request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:string]];
//    [request setTimeOutSeconds:20.0f];
//    [request setDelegate:self];
//    [request setDidFailSelector:@selector(requestReleaseNoteFailed:)];
//    [request setDidFinishSelector:@selector(requestReleaseNoteFinished:)];
//    [request startAsynchronous];
}

//- (void)requestReleaseNoteFinished:(ASIHTTPRequest*)request
//{
//    if([request responseString] == Nil || [request responseString] == nil || [[request responseString] isEqualToString:@""])
//    {
//        return;
//    }
//    NSDictionary* dict = [[request responseString] JSONValue];
//    NSArray* array = [dict objectForKey:@"versionlist"];
//    
//    if(array != nil && [array count] > 0)
//    {
//        _versionList = [array objectAtIndex:0];
//        NSString* ignoreVersion = [[NSUserDefaults standardUserDefaults] objectForKey:@"ignoreversion"];
//        NSString* newVersion = [[[_versionList objectForKey:@"version"] componentsSeparatedByString:@"_"] objectAtIndex:0];
//        if(![newVersion isEqualToString:ignoreVersion])
//        {
//            //没有忽略的更新;
//            CustomAlertView* alertView = [[CustomAlertView alloc] initWithTitle:@"提示" message:[_versionList objectForKey:@"describe"] delegate:self cancelButtonTitle:@"不再提醒" otherButtonTitles:@"更新", nil];
//            alertView.tag = 0;
//            [alertView show];
//        }
//    }
//    else
//    {
//        if(!_autoCheck)
//        {
//            CustomAlertView* alert = [[CustomAlertView alloc] initWithTitle:@"提示" message:@"当前版本为最新版本，无需更新" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
//            alert.tag = 1;
//            [alert show];
//        }
//    }
//}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 0)
    {
        if(buttonIndex == 0)
        {
//            [[NSUserDefaults standardUserDefaults] setObject:[[[_versionList objectForKey:@"version"] componentsSeparatedByString:@"_"] objectAtIndex:0] forKey:@"ignoreversion"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_versionList objectForKey:@"addr"]]];
        }
    }
}

@end
