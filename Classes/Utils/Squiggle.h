//
//  Squiggle.h
//  Painter
//
//  Created by  ibokan on 10-9-7.
//  Copyright 2010 tencent.com. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Squiggle : NSObject 
{
    NSMutableArray *pointsArray;
	UIColor *strokeColor;
	float lineWidth;
    BOOL           isErase_  ;
}

@property (nonatomic, strong) NSMutableArray *pointsArray;
@property (nonatomic, strong) UIColor *strokeColor;
@property (nonatomic, assign) BOOL      isErase ;
@property float lineWidth;

- (void)addPoint:(CGPoint)point withLineWidth:(CGFloat)f ;

@end
