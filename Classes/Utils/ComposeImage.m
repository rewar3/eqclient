//
//  ComposeImage.m
//  WiFiPlusLiveTwo
//
//  Created by 王金宇 on 13-12-11.
//  Copyright (c) 2013年 王金宇. All rights reserved.
//

#import "ComposeImage.h"

@implementation ComposeImage

- (id)initWithImage:(UIImage*)image painterView:(PainterView*)painterView
{
    if(self = [super init])
    {
        _drawImage = image;
        _painterView = painterView;
        [self calculateSizeOnImage:image];
    }
    return self;
}

- (NSString*)saveIcon:(NSData*)imageData direction:(NSString*)direction fileName:(NSString*)fileName
{
    NSString* documentPath = LibraryPath;
    NSString* directionPath = [documentPath stringByAppendingPathComponent:direction];
    NSFileManager* fileManager = [NSFileManager defaultManager];
    [fileManager createDirectoryAtPath:directionPath withIntermediateDirectories:YES attributes:nil error:nil];
    NSString* filePath = [directionPath stringByAppendingPathComponent:fileName];
    
    [imageData writeToFile:filePath atomically:YES];
//    NSString* newPathWithoutRoot = [NSString stringWithFormat:@"%@/%@", direction, fileName];
    
    return filePath;
}

- (CGLayerRef)getDrawLayer
{
    CGFloat w = _realWidth ;
    CGFloat h = _realHeight ;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGLayerRef layerRef = CGLayerCreateWithContext(context, CGSizeMake(w, h), NULL);
    CGContextRef drawContext = CGLayerGetContext(layerRef);
    
    for (Squiggle *squiggle in [_painterView finishSquiggles])
    {
        NSMutableArray *pointArray1 = [squiggle pointsArray];
        
        UIColor *squiggleColor = squiggle.strokeColor;
        CGColorRef colorRef = [squiggleColor CGColor];
        CGContextSetStrokeColorWithColor(drawContext, colorRef);
        
        CGContextSetLineJoin(drawContext, kCGLineJoinRound);
        CGContextSetLineCap(drawContext, kCGLineCapRound);
        CGContextSetBlendMode(drawContext, kCGBlendModeCopy);
        //        CGContextSetAlpha(drawContext, 0.5);
        CGContextSetAllowsAntialiasing(drawContext, YES);
        
        //            CGPoint firstPoint;
        //            //复制第1个元素值到firstPoint点中
        //            firstPoint = [(NSValue *)[[pointArray1 objectAtIndex:0] objectForKey:@"point"] CGPointValue];
        //            CGContextSetLineWidth(drawContext, [(NSNumber *)[[pointArray1 objectAtIndex:0] objectForKey:@"width"] floatValue]);
        //
        //            CGContextMoveToPoint(drawContext, firstPoint.x*_drawScale, firstPoint.y*_drawScale);
        //            for (int i = 1; i < [pointArray1 count]; i++)
        //            {
        //                //将下一个点复制值,然后再在这两点之间画线
        //                CGPoint point = [(NSValue *)[[pointArray1 objectAtIndex:i] objectForKey:@"point"] CGPointValue];;
        //                CGContextSetLineWidth(drawContext, [(NSNumber *)[[pointArray1 objectAtIndex:0] objectForKey:@"width"] floatValue]*_drawScale);
        //                CGContextAddLineToPoint(drawContext, point.x*_drawScale, point.y*_drawScale);
        //            }
        //
        //            CGContextStrokePath(drawContext);
        if ([pointArray1 count] >= 3) {
            
            for (int i = 0; i < [pointArray1 count]; i++)
            {
                
                //将下一个点复制值,然后再在这两点之间画线
                if (i == 0)
                {
                    CGPoint point0 = [(NSValue *)[[pointArray1 objectAtIndex:i] objectForKey:@"point"] CGPointValue];
                    
                    CGContextSetLineWidth(drawContext, [(NSNumber *)[[pointArray1 objectAtIndex:0] objectForKey:@"width"] floatValue]);
                    CGContextMoveToPoint(drawContext, point0.x * _drawScale, point0.y*_drawScale);
                    
                }
                else
                {
                    if (i == 1)
                    {
                        CGContextAddLineToPoint(drawContext, midPoint([(NSValue *)[[pointArray1 objectAtIndex:0] objectForKey:@"point"] CGPointValue], [(NSValue *)[[pointArray1 objectAtIndex:1] objectForKey:@"point"] CGPointValue]).x*_drawScale, midPoint([(NSValue *)[[pointArray1 objectAtIndex:0] objectForKey:@"point"] CGPointValue], [(NSValue *)[[pointArray1 objectAtIndex:1] objectForKey:@"point"] CGPointValue]).y*_drawScale);
                        CGContextSetLineWidth(drawContext, [(NSNumber *)[[pointArray1 objectAtIndex:i] objectForKey:@"width"] floatValue]*_drawScale);
                    }
                    else
                    {
                        CGContextMoveToPoint(drawContext, midPoint([(NSValue *)[[pointArray1 objectAtIndex:i-2] objectForKey:@"point"] CGPointValue], [(NSValue *)[[pointArray1 objectAtIndex:i-1] objectForKey:@"point"] CGPointValue]).x*_drawScale, midPoint([(NSValue *)[[pointArray1 objectAtIndex:i-2] objectForKey:@"point"] CGPointValue], [(NSValue *)[[pointArray1 objectAtIndex:i-1] objectForKey:@"point"] CGPointValue]).y*_drawScale);
                        
                        CGContextAddQuadCurveToPoint(drawContext, [(NSValue *)[[pointArray1 objectAtIndex:i-1] objectForKey:@"point"] CGPointValue].x*_drawScale, [(NSValue *)[[pointArray1 objectAtIndex:i-1] objectForKey:@"point"] CGPointValue].y*_drawScale, midPoint([(NSValue *)[[pointArray1 objectAtIndex:i-1] objectForKey:@"point"] CGPointValue], [(NSValue *)[[pointArray1 objectAtIndex:i] objectForKey:@"point"] CGPointValue]).x*_drawScale, midPoint([(NSValue *)[[pointArray1 objectAtIndex:i-1] objectForKey:@"point"] CGPointValue], [(NSValue *)[[pointArray1 objectAtIndex:i] objectForKey:@"point"] CGPointValue]).y*_drawScale );
                        CGContextSetLineWidth(drawContext, [(NSNumber *)[[pointArray1 objectAtIndex:i] objectForKey:@"width"] floatValue]*_drawScale);
                    }
                    
                }
            }
        }
        CGContextStrokePath(drawContext);
    }
    return layerRef ;
}

- (NSString*)createDoneImage
{
    CGFloat w = _realWidth ;
    CGFloat h = _realHeight ;
    
    UIGraphicsBeginImageContext(CGSizeMake(w, h));
    CGContextRef context = UIGraphicsGetCurrentContext();
    [_drawImage drawInRect:CGRectMake(0, 0, w, h)];
    
    CGLayerRef layerRef = [self getDrawLayer];
    CGContextDrawLayerAtPoint(context, CGPointZero, layerRef);
    CGLayerRelease(layerRef);
    UIImage *doneimage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *tempData = UIImageJPEGRepresentation(doneimage, 1);
    _newImagePath = [self  saveIcon:tempData direction:@"drawimage" fileName:@"drawimage.jpg"];
    return _newImagePath;
}

- (void)calculateSizeOnImage:(UIImage *)img
{
    _drawImage = img ;
    
    _realWidth = img.size.width;
    _realHeight = img.size.height;
    
    CGFloat realScale = _realWidth/_realHeight ;
    
    if (realScale >= _painterView.frame.size.width/_painterView.frame.size.height)
    {
        if(_realWidth >= _painterView.frame.size.width)
        {
            _olderSize = CGSizeMake(_painterView.frame.size.width, _realHeight/_realWidth * _painterView.frame.size.width);
            _drawScale = _realWidth/_painterView.frame.size.width ;
        }
        else
        {
            //            olderSize = CGSizeMake(_realWidth, _realHeight);
            //            _drawScale = 1.f ;
            _drawScale = _realWidth/_painterView.frame.size.width ;
            _olderSize = CGSizeMake(_painterView.frame.size.width, _realHeight * _painterView.frame.size.width/_realWidth);
        }
        
    }
    else
    {
        if (_realHeight >= _painterView.frame.size.height)
        {
            _olderSize = CGSizeMake(_realWidth/_realHeight*_painterView.frame.size.height, _painterView.frame.size.height);
            _drawScale = _realHeight/_painterView.frame.size.height ;
        }
        else
        {
            //            olderSize = CGSizeMake(_realWidth, _realHeight);
            //            _drawScale = 1.f ;
            _drawScale = _realHeight/_painterView.frame.size.height ;
            _olderSize = CGSizeMake(_realWidth*_painterView.frame.size.height/_realHeight, _painterView.frame.size.height);
        }
    }
}

@end
