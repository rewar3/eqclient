//
//  CacheImage.m
//  EQClient
//
//  Created by xumeng on 1/23/14.
//  Copyright (c) 2014 xumeng. All rights reserved.
//

#import "CacheImage.h"

@implementation CacheImage


+ (instancetype)sharedInstance
{
    static CacheImage *_instance = nil;
    static dispatch_once_t p;
    
    dispatch_once(&p, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

- (void)cachedImageWithGUID:(NSString *)guid
{
    
}

@end
