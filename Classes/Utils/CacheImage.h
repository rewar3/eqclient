//
//  CacheImage.h
//  EQClient
//
//  Created by xumeng on 1/23/14.
//  Copyright (c) 2014 xumeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheImage : NSObject

+ (instancetype)sharedInstance;


- (void)cachedImageWithGUID:(NSString *)guid;

//- (UIImage *)existCachedImageWithGUID:(NSString *)guid;
@end
