//
//  ComposeImage.h
//  WiFiPlusLiveTwo
//
//  Created by 王金宇 on 13-12-11.
//  Copyright (c) 2013年 王金宇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PainterView.h"

@interface ComposeImage : NSObject
{
    PainterView*    _painterView;
    
    CGFloat         _realWidth;
    CGFloat         _realHeight;
    CGFloat         _drawScale;
    CGSize          _olderSize;
    NSString*       _newImagePath;
    UIImage*        _drawImage;
}

- (id)initWithImage:(UIImage*)image painterView:(PainterView*)painterView;

- (NSString*)createDoneImage;

@end
