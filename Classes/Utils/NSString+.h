//
//  NSString+MD5.h
//  TvmLive
//
//  Created by wsw on 10-11-14.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface NSString(md5)

+ (NSString *)md5StringWithString:(NSString *)string ;
+ (NSString *)changeToFileNameWithURLString:(NSString *)str ;
+ (NSString *)timeStyleHHMMSSWithFloat:(float)f ;
- (NSString *)stringFromMD5;
- (NSString *)filtered ;
- (NSString*)encodeURL;
@end
