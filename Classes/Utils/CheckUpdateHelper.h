//
//  CheckUpdateHelper.h
//  WiFiPlusLiveTwo
//
//  Created by 王金宇 on 13-10-29.
//  Copyright (c) 2013年 王金宇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckUpdateHelper : NSObject<UIAlertViewDelegate>
{
    NSDictionary*   _versionList;
    BOOL            _autoCheck;
}

+ (id)shared;

- (void)checkUpdatefForAuto:(BOOL)autoCheck;

@end
