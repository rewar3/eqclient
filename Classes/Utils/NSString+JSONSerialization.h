//
//  NSString+JSONSerialization.h
//  WiFiPlusLiveTwo
//
//  Created by 王金宇 on 13-10-29.
//  Copyright (c) 2013年 王金宇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>


@interface NSString (JSONSerialization)

- (id)JSONValue;
- (id)JSONValueTrans;

@end

@interface NSData (JSONSerialization)

- (id)JSONValue;

@end

@interface NSObject (JSONSerialization)

- (NSString*)JSONString;
- (NSData*)JSONData;

@end

@interface NSString (EncodeString)

+(NSString*)md5StringWithString:(NSString*)string;

@end