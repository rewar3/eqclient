//
//  NSString+MD5.m
//  TvmLive
//
//  Created by wsw on 10-11-14.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "NSString+.h"


@implementation NSString(md5)

+(NSString*)md5StringWithString:(NSString*)string
{
	const char *cStr = [string UTF8String];
    unsigned char result[16];
    CC_MD5(cStr,strlen(cStr),result);
    return [[NSString stringWithFormat:
			 @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",	//32位
			 result[0], result[1], result[2], result[3],
			 result[4], result[5], result[6], result[7],
			 result[8], result[9], result[10], result[11],
			 result[12], result[13], result[14], result[15]]
            lowercaseString]; 
}

+(NSString*)changeToFileNameWithURLString:(NSString*)str 
{
    NSString *a = [str stringByReplacingOccurrencesOfString:@":" withString:@"_"];
    NSString *b = [a stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    NSString *c = [b stringByReplacingOccurrencesOfString:@"." withString:@"_"];
    return c ;
}
+(NSString*)timeStyleHHMMSSWithFloat:(float)f 
{
    
    int inputSeconds = (int)f;
    int hours =  inputSeconds / 3600;
    int minutes = ( inputSeconds - hours * 3600 ) / 60; 
    int seconds = inputSeconds - hours * 3600 - minutes * 60; 
    
    NSString *theTime = [NSString stringWithFormat:@"%.2d:%.2d:%.2d", hours, minutes,seconds];
    return theTime ;
}

- (NSString *) stringFromMD5{
    
    if(self == nil || [self length] == 0)
        return nil;
    
    const char *value = [self UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    
    return outputString;
}

- (NSString *)filtered
{
    return [self stringByReplacingOccurrencesOfString:@"//" withString:@"/"];
}

- (NSString*)encodeURL
{
    NSString *newString = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                                kCFAllocatorDefault,
                                                                                                (__bridge  CFStringRef)self, NULL, (CFStringRef)@"+",                                                                                       kCFStringEncodingUTF8);
    if (newString) {
        return newString;
    }
    return @"";
}

@end
