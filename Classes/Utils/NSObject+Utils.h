//
//  NSObject+Utils.h
//  EQClient
//
//  Created by xumeng on 12/24/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, LineColor) {
    LineColorWhite = 0,
    LineColorBlack = 1,
    LineColorBlue = 2,
    LineColorRed = 3,
    LineColorYellow = 4,
    LineColorGreen = 5
};

typedef NS_ENUM(NSInteger, LineWidth) {
    LineWidthS = 0,
    LineWidthM = 1,
    LineWidthB = 2
};

@interface NSObject (Utils)

- (NSString *)saveImage:(NSData *)imageData ToCacheWithGUID:(NSString *)guid;

- (UIImage *)getImageFromCacheWithGUID:(NSString *)guid;

- (NSString*)getPathWithLibraryPath:(NSString*)path;

- (BOOL) isBlankString:(NSString *)string;

- (CGSize)getImageNewSizeWithFrame:(CGSize) frame originalSize:(CGSize) imageSize;

- (NSDictionary *)getImageInfoWithPackageName:(NSString *)packageName guid:(NSString *)guid fileNameWithExtend:(NSString *)extend width:(NSNumber *)w height:(NSNumber *)h fileType:(NSString *)fileType;

- (UIColor *)getColorWithIndex:(NSInteger) index;

- (CGFloat)getWidthWithIndex:(NSInteger) index;

@end
