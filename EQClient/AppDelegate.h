//
//  AppDelegate.h
//  EQClient
//
//  Created by xumeng on 12/10/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperationManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (AFHTTPRequestOperationManager *)sharedHttpRequestManager;

- (void)loginViewChange;
@end
