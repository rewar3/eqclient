//
//  Definef.h
//  NeweReader
//
//  Created by xumeng on 7/17/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//



#define ISPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define ISIOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)

//#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
//#else
//#   define DLog(...)
//#endif

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

#define SCREEN_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)

#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:a]

#define DocumentPath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define LibraryPath [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0]

#define DatabasePath [DocumentPath stringByAppendingPathComponent:@"eq.db"]

#define ImageNamed(A) [UIImage imageNamed:A]
#define ImageWithFile(A) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:A ofType:nil]]

#define ALERT_TITLE(title,msg)	{UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];[alert show];}

#define NSStandardUserDefaults [NSUserDefaults standardUserDefaults]
#define SetObjectUserDefault(value,key) {[NSStandardUserDefaults setObject:value forKey:key]; [NSStandardUserDefaults synchronize];}
#define GetObjectUserDefault(key) [NSStandardUserDefaults objectForKey:key]

#define kReciveCacheNotificaiton @"kReciveCacheNotificaiton"
#define CachePath  @"com.tvmining.image.cache"