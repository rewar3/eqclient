//
//  AppDelegate.m
//  EQClient
//
//  Created by xumeng on 12/10/13.
//  Copyright (c) 2013 xumeng. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "IndexViewController.h"
#import "NSObject+Utils.h"
#import "PageRequestManager.h"
#import "FMDatabase.h"
#import "CheckUpdateHelper.h"
@interface AppDelegate()
{
    @private
    LoginViewController *_loginViewController;
    IndexViewController *_indexViewController;
}

@end

@implementation AppDelegate

static AFHTTPRequestOperationManager *httpRequstManager;
+ (AFHTTPRequestOperationManager *)sharedHttpRequestManager
{
    if(!httpRequstManager)
    {
        httpRequstManager = [AFHTTPRequestOperationManager manager];
        [httpRequstManager.operationQueue setMaxConcurrentOperationCount:5];
    }
    return httpRequstManager;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:[self getPathWithLibraryPath:CachePath] error:nil];
    [fileManager removeItemAtPath:DatabasePath error:nil];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    _loginViewController = [[LoginViewController alloc]init];
    
    [self.window setRootViewController:_loginViewController];
    
    [self.window makeKeyAndVisible];
    
//    [self redirectNSLogToDocumentFolder];//保存日志

    [self initLoaclDataBase];
    [PageRequestManager defaultManager];
    
    [[CheckUpdateHelper shared] checkUpdatefForAuto:YES];
    return YES;
}

-(void)initLoaclDataBase
{
    FMDatabase *database  = [FMDatabase databaseWithPath:DatabasePath];
    if (![database open]) {
        ALERT_TITLE(@"数据库错误",@"Open database failed");
        return;
    }
    
    FMResultSet *s = [database executeQuery:@"SELECT COUNT(*) as count FROM sqlite_master where name='page'"];
    int totalCount=0;
    while([s next]) {
        totalCount = [s intForColumnIndex:0];
    }
    if(totalCount==0)
    {
        [database executeUpdate:@"CREATE TABLE 'page' (p_id INTEGER PRIMARY KEY AUTOINCREMENT, p_guid VARCHAR, p_filetype VARCHAR, p_imageurl VARCHAR,p_imagepath VARCHAR, p_respath VARCHAR,p_iscache BOOL)"];
    }
    [database close];
}

- (void)loginViewChange
{
    _indexViewController = [[IndexViewController alloc]init];
    
    [self.window setRootViewController:_indexViewController];
    _loginViewController = nil;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if ([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)])
    {
        if ([[UIDevice currentDevice] isMultitaskingSupported])
        {
            UIApplication *application = [UIApplication sharedApplication]; //Get the shared application instance
            __block UIBackgroundTaskIdentifier background_task; //Create a task object
            background_task = [application beginBackgroundTaskWithExpirationHandler: ^
                               {
                                   [application endBackgroundTask: background_task]; //Tell the system that we are done with the tasks
                                   background_task = UIBackgroundTaskInvalid; //Set the task to be invalid
                               }];
        }
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)redirectNSLogToDocumentFolder
{
    //如果已经连接Xcode调试则不输出到文件
    if(isatty(STDOUT_FILENO)) {
        return;
    }
    
    UIDevice *device = [UIDevice currentDevice];
    if([[device model] hasSuffix:@"Simulator"]){ //在模拟器不保存到文件中
        return;
    }
    
    //将NSlog打印信息保存到Document目录下的Log文件夹下
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *logDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Log"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL fileExists = [fileManager fileExistsAtPath:logDirectory];
    if (!fileExists) {
        [fileManager createDirectoryAtPath:logDirectory  withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"]; //每次启动后都保存一个新的日志文件中
    NSString *dateStr = [formatter stringFromDate:[NSDate date]];
    NSString *logFilePath = [logDirectory stringByAppendingFormat:@"/%@.log",dateStr];
    
    // 将log输入到文件
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stdout);
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stderr);
    
    //未捕获的Objective-C异常日志
    NSSetUncaughtExceptionHandler (&UncaughtExceptionHandler);
}

void UncaughtExceptionHandler(NSException* exception)
{
    NSString* name = [ exception name ];
    NSString* reason = [ exception reason ];
    NSArray* symbols = [ exception callStackSymbols ]; // 异常发生时的调用栈
    NSMutableString* strSymbols = [ [ NSMutableString alloc ] init ]; //将调用栈拼成输出日志的字符串
    for ( NSString* item in symbols )
    {
        [ strSymbols appendString: item ];
        [ strSymbols appendString: @"\r\n" ];
    }
    
    //将crash日志保存到Document目录下的Log文件夹下
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *logDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Log"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL fileExists = [fileManager fileExistsAtPath:logDirectory];
    if (!fileExists) {
        [fileManager createDirectoryAtPath:logDirectory  withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *logFilePath = [logDirectory stringByAppendingPathComponent:@"UncaughtException.log"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateStr = [formatter stringFromDate:[NSDate date]];
    
    NSString *crashString = [NSString stringWithFormat:@"<- %@ ->[ Uncaught Exception ]\r\nName: %@, Reason: %@\r\n[ Fe Symbols Start ]\r\n%@[ Fe Symbols End ]", dateStr, name, reason, strSymbols];
    //把错误日志写到文件中
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding], "a+", (__bridge FILE *)(crashString));
    
}


@end
